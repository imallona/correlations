#!/bin/bash
#$ -S /bin/bash
#$ -e /imppc/labs/maplab/imallona/jobs/colonomics/meth_tumors-e.log
#$ -o /imppc/labs/maplab/imallona/jobs/colonomics/meth_tumors-o.log
#$ -N corr_tumor
#$ -t 1-253
#$ -q imppc@curri1,imppc@curri2,imppc@curri4,imppc@curri5,imppc@curri6,imppc@curri7,imppc12@curri10 
#$ -V
#$ -M imallona@imppc.org
#$ -l exclusive

# set +x
SRC="$IHOME"/src/correlations

FOLDER=/home/labs/maplab/imallona/TCGA_Correlations
PARSER="$SRC"/stable_fchen_parser/colonomics_fchen_parser.R

USER="imallona"
PASSWORD=""
DBNAME="colonomics_correlations"
HOST="overlook"
PORT=5432

# that is, we will overestimate the cpu number 1.25 times
NPROC_OVERLOAD=1
NTHREADS=$(echo "$(nproc)*$NPROC_OVERLOAD" | bc)

# array job naming (each line at SEEDFILE is an input filename)
SEEDFILE="$SRC"/combinations_reversed_autosomes
# mind that the file at $SEEDFILE contains something like '2 20 X', that is,
# is the second job and will involve chr20 and chrX
SEED=$(awk -v  sge="${SGE_TASK_ID}"  'NR==sge' $SEEDFILE)

FIRST_CHROM=$(echo $SEED | cut -d" " -f2)
SECOND_CHROM=$(echo $SEED | cut -d" " -f3)

# database-compliant view and table names start
SCHEMA=colonomics_tumor
METH_BIG_TABLE=humanmethylation450

# data destination
FILTERED_CORR_TABLE=meth_filtered_correlations_chr${FIRST_CHROM}_chr${SECOND_CHROM}

# database-compliant view and table names end

# cut-offs for standard deviation
METH_SD_THRESHOLD=0.05

# cut-off for correlation coefficient and pvalue
METH_CORR_THRESHOLD=0.5
METH_PVAL_THRESHOLD=0.01

echo Starting at $(date) for $SEED and $FIRST_CHROM and $SECOND_CHROM as chromosomes


if [[ $FIRST_CHROM -eq $SECOND_CHROM ]]
then
    echo Got an intrachromosome job for $FIRST_CHROM

    Rscript $PARSER --args "readPath <- \"$FOLDER\"" "writePath <- readPath" "corFromTableToTable(drv=dbDriver(\"PostgreSQL\"), \
user=\"$USER\", \
password=\"$PASSWORD\", \
host=\"$HOST\", \
dbname=\"$DBNAME\", \
port=\"$PORT\", \
from.schema = \"$SCHEMA\", \
from.table = \"$METH_BIG_TABLE\" , \
from.join = \"annotation.humanmethylation450_probeinfo\",
from.condition = \"humanmethylation450.probe = humanmethylation450_probeinfo.probe AND chromosome = \'$FIRST_CHROM\'\", 
to.schema = \"$SCHEMA\", \
to.table = \"$FILTERED_CORR_TABLE\", \
stdev.threshold.from = $METH_SD_THRESHOLD, \
stdev.threshold.and = $METH_SD_THRESHOLD, \
pval.threshold = $METH_PVAL_THRESHOLD, \
corr.threshold = $METH_CORR_THRESHOLD, \
nthreads = $NTHREADS )" 

else

    echo Got an interchromosome job for $FIRST_CHROM and $SECOND_CHROM

    Rscript $PARSER --args "readPath <- \"$FOLDER\"" "writePath <- readPath" "corFromTableToTable(drv=dbDriver(\"PostgreSQL\"), \
user=\"$USER\", \
password=\"$PASSWORD\", \
host=\"$HOST\", \
dbname=\"$DBNAME\", \
port=\"$PORT\", \
from.schema = \"$SCHEMA\", \
from.table = \"$METH_BIG_TABLE\" , \
from.join = \"annotation.humanmethylation450_probeinfo\",
from.condition = \"humanmethylation450.probe = humanmethylation450_probeinfo.probe AND chromosome = \'$FIRST_CHROM\'\", 
and.schema = \"$SCHEMA\", \
and.table = \"$METH_BIG_TABLE\", \
and.join = \"annotation.humanmethylation450_probeinfo\",
and.condition = \"humanmethylation450.probe = humanmethylation450_probeinfo.probe AND chromosome = \'$SECOND_CHROM\'\", 
to.schema = \"$SCHEMA\", \
to.table = \"$FILTERED_CORR_TABLE\", \
stdev.threshold.from = $METH_SD_THRESHOLD, \
stdev.threshold.and = $METH_SD_THRESHOLD, \
pval.threshold = $METH_PVAL_THRESHOLD, \
corr.threshold = $METH_CORR_THRESHOLD, \
nthreads = $NTHREADS )" 

fi

echo Ending at $(date) for $SEED and $FIRST_CHROM and $SECOND_CHROM as chromosomes
