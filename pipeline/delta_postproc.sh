#!/bin/bash
#$ -S /bin/bash
#$ -e /imppc/labs/maplab/imallona/jobs/correlations_array/delta_mod-e.log
#$ -o /imppc/labs/maplab/imallona/jobs/correlations_array/delta_mod-o.log
#$ -N delta_modules
#$ -V
#$ -q imppcv3
#$ -m be
#$ -M imallona@igtp.cat
#$ -l h_vmem=100G

Rscript $IHOME/src/correlations/analysis/igraph_communities_filtering_by_degree_by_sign.R

Rscript $IHOME/src/correlations/analysis/igraph_communities_no_sign.R

