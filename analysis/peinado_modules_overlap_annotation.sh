#!/bin/bash
##
## Requires peinado_modules_overlap_annotation.Rmd to be run first
##
## Izaskun Mallona, GPLv3
## 21st june 2018

## the path the bedfiles are located at
DAT="$IHOME"/correlations_simpler/peinado_modules_overlap_annotation
WD="$DAT"
NTHREADS=8

mkdir -p $WD
cd $WD

for fn in $(find $WD -name "*bed")
do
    tag=$(basename $fn _probes.bed)

    perl /soft/bio/homer-4.8/bin/annotatePeaks.pl \
         "$fn" \
         hg19  \
         -annStats "$WD"/"$tag"_annot.stats \
         -go "$WD"/"$tag"/go \
         > $tag_annotated.tab

    perl /soft/bio/homer-4.8/bin/findMotifsGenome.pl \
         "$fn" \
         hg19  \
         "$WD"/"$tag"/motifs \
         -size 200 \
         -p "$NTHREADS" \
         -preparsedDir "$WD"/preparsedDir
    
done

