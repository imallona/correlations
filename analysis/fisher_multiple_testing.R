#!/usr/bin/env R
##
## Multiple testing exploring
##
## Leverages correlations based upon Fisher transforms and finds optima
##
## Izaskun Mallona, 14th Apr 2016


COLONOMICS_TUMOR_THRES <- 0.05/(251733^2)
COLONOMICS_NORMAL_THRES <- 0.05/(99346^2)
COLONOMICS_SIZE <- 90
COAD_TUMOR_THRES <- 0.05/(245981^2)
COAD_TUMOR_SIZE <- 256
COAD_NORMAL_THRES <- 0.05/(122398^2)
COAD_NORMAL_SIZE <- 38

## Fisher Z transformation
## got from https://github.com/kn3in/ffbw/blob/master/R/fishe.r
fisher_z <- function(rho) {
  0.5 * log((1 + rho) / (1 - rho))
}

## Asymptotic p-value
## got from https://github.com/kn3in/ffbw/blob/master/R/fishe.r
fisher_to_pval <- function(rho, samp_size) {
	2 * pnorm(fisher_z(-abs(rho)) * sqrt(samp_size - 3))
}


bonferroni_significant_reversed <- function(rho, samp_size, threshold) {
    pval <- fisher_to_pval(rho = rho, samp_size = samp_size)
    return(threshold-pval)
}


optimize_experiment <- function(samp_size, threshold){
    optimize(f = bonferroni_significant_reversed,
             interval = c(0, 1),
             tol = 0.000001,
             maximum = TRUE,
             samp_size = samp_size,
             threshold = threshold)  
}

optimize_experiment(samp_size = COLONOMICS_SIZE, threshold = COLONOMICS_TUMOR_THRES)

optimize_experiment(samp_size = COLONOMICS_SIZE, threshold = COLONOMICS_NORMAL_THRES)

optimize_experiment(samp_size = COAD_TUMOR_SIZE, threshold = COAD_TUMOR_THRES)

optimize_experiment(samp_size = COAD_NORMAL_SIZE, threshold = COAD_NORMAL_THRES)



