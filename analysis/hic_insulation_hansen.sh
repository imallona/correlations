#!/bin/bash
##
## hic-like insulation plots against hansen's blocks and dmrs (bedgraphs)
## http://www.nature.com/ng/journal/v43/n8/full/ng.865.html#supplementary-information
##
## requires hic_runner.sh to be run first
##
## 28th june 2017
## Izaskun Mallona, GPLv2

WD="$IHOME"/correlations_simpler/hic_insulation_hansen_v2

oldpythonpath=$PYTHONPATH

export PYTHONPATH=""
# hicplotter-0.7.3
source /soft/general/virtenvs/hicplotter-0.7.3/bin/activate

WHICH="chr22 chr21 chr20 chr19 chr18 chr17 chr16 chr15 chr14"

BEDTOMATRIX="$IHOME"/src/correlations/analysis/bedToMatrix.py
VIRTENVPATH=/soft/general/virtenvs/hicplotter-0.7.3
# WD_HIC=$IHOME/correlations_simpler/hic
WD_HIC=$IHOME/correlations_simpler/hic_v2
CPG_DENSITY=$IHOME/correlations_simpler/fortin_ab_compartments_bedgraphs/cpg_number_1000000.tsv
## fortin's called compartments
AB=$IHOME/correlations_simpler/fortin_ab_compartments_bedgraphs

mkdir -p $WD
cd $WD

# hansen's vmrs and blocks
wget http://www.nature.com/ng/journal/v43/n8/extref/ng.865-S2.xls
wget http://www.nature.com/ng/journal/v43/n8/extref/ng.865-S3.xls

# fortin's compartments
wget https://static-content.springer.com/esm/art%3A10.1186%2Fs13059-015-0741-y/MediaObjects/13059_2015_741_MOESM2_ESM.txt

echo 'export to tsvs'

## building bedgraphs
# 4th column should be a floating number for histograms.
# 5th column should be an rgb color for tile or arc plots.
# 6th column should be a string for tile plots. Columns after 6th will be ignored.



awk '{
  OFS=FS="\t"; 
  if($10=="hyper"){
    print $1,$2,$3,"1.0","85,0,0",$10
  } else if ($10=="hypo") {
    print $1,$2,$3,"1.0","86,86,149",$10
  }
}' ng.865-S2.tsv | sortBed >  ng.865-S2.bedgraph

cut -f1,2,3,4 ng.865-S2.bedgraph > ng.865-S2.bed

## same for vmrs

awk '{
  OFS=FS="\t"; 
  if($11=="boundaryShift"){
    print $1,$2,$3,"1.0","255,170,170",$11
  } else if ($11=="lossOfRegulation") {
    print $1,$2,$3,"1.0","212,106,106",$11
  } else if ($11=="novelMethylation") {
    print $1,$2,$3,"1.0","85,0,0",$11
  } else if ($11=="other") {
    print $1,$2,$3,"1.0","128,21,21",$11
  }
}' ng.865-S3.tsv | sortBed >  ng.865-S3.bedgraph

cut -f1,2,3,4 ng.865-S3.bedgraph > ng.865-S3.bed

## hic matrices construct at different resolutions

# for res in 100000 1000000
for res in 1000000
do
    for dataset in colonomics_tumor colonomics_normal coad coad_normal_38
    do
        mkdir -p "$WD"/"$dataset"/"$res"
        cd  "$WD"/"$dataset"/"$res"
        
        for chr in $(seq 22 -1 1)
        do
            echo $chr
            echo $dataset

            item=chr"$chr"_"$dataset"
            chr=chr"$chr"

            sed  '/^$/d' "$WD_HIC"/"$dataset"/"$item"_hic_min_rho_0.8_resolution_"$res".bed > foo
            mv foo  "$WD_HIC"/"$dataset"/"$item"_hic_"$res".bed
            
            python "$BEDTOMATRIX" "$WD_HIC"/"$dataset"/"$item"_hic_"$res".bed
            
            ## custom made ab compartments
            awk '
{
 OFS=FS="\t";
 gsub(/"/, "");
 print $2,$3,$4,$(NF-1)
}' "$AB"/"$dataset"_"$chr"_"$res"_compartments.bed > ab_"$chr"_"$dataset"_"$res".bedgraph

        done
        cd $WD
    done
done

## python insulation calls

cd $WD

# for res in 100000 1000000
for res in 1000000
do
    CPG_DENSITY=$IHOME/correlations_simpler/fortin_ab_compartments_bedgraphs/cpg_number_"$res".tsv
    
    for chr in $(seq 22 -1 1)
    do
        echo $chr
        chr=chr"$chr"

        ## ab compartments and other stuff
        fgrep coad "$WD"/13059_2015_741_MOESM2_ESM.txt | fgrep $chr | cut -f2-5 > ab.bedgraph
        fgrep hypo "$WD"/ng.865-S2.bedgraph | fgrep $chr > hypo.bedgraph
        fgrep hyper "$WD"/ng.865-S2.bedgraph | fgrep $chr > hyper.bedgraph

        ##test with side by side plots
        python "$VIRTENVPATH"/bin/HiCPlotter_fix.py \
               -f "$WD_HIC"/colonomics_tumor/"$chr"_colonomics_tumor_hic_"$res".bed_matrix.txt \
               "$WD_HIC"/coad/"$chr"_coad_hic_"$res".bed_matrix.txt \
               "$WD_HIC"/colonomics_normal/"$chr"_colonomics_normal_hic_"$res".bed_matrix.txt \
               "$WD_HIC"/coad_normal_38/"$chr"_coad_normal_38_hic_"$res".bed_matrix.txt \
               -n colonomics_tumor_"$chr" \
               coad_"$chr" \
               colonomics_normal_"$chr" \
               coad_normal_38_"$chr" \
               -chr "$chr" \
               -o montage \
               -fh 0  \
               -pi 1 \
               -t hypo.bedgraph,hyper.bedgraph,"$WD"/ng.865-S3.bedgraph \
               hypo.bedgraph,hyper.bedgraph,"$WD"/ng.865-S3.bedgraph \
               hypo.bedgraph,hyper.bedgraph,"$WD"/ng.865-S3.bedgraph \
               hypo.bedgraph,hyper.bedgraph,"$WD"/ng.865-S3.bedgraph \
               -tl hansen_hypo,hansen_hyper,hansen_small \
               hansen_hypo,hansen_hyper,hansen_small \
               hansen_hypo,hansen_hyper,hansen_small \
               hansen_hypo,hansen_hyper,hansen_small \
               -tt 0 \
               -ptr 1 \
               -hist ab.bedgraph,"$CPG_DENSITY","$WD"/colonomics_tumor/"$res"/ab_"$chr"_colonomics_tumor_"$res".bedgraph \
               ab.bedgraph,"$CPG_DENSITY","$WD"/coad/"$res"/ab_"$chr"_coad_"$res".bedgraph \
               ab.bedgraph,"$CPG_DENSITY","$WD"/colonomics_normal/"$res"/ab_"$chr"_colonomics_normal_"$res".bedgraph \
               ab.bedgraph,"$CPG_DENSITY","$WD"/coad_normal_38/"$res"/ab_"$chr"_coad_normal_38_"$res".bedgraph \
               -hl fortin_ab,probes,custom_ab \
               fortin_ab,probes,custom_ab \
               fortin_ab,probes,custom_ab \
               fortin_ab,probes,custom_ab \
               -fhist 1,1,1 1,1,1 1,1,1 1,1,1 \
               -r "$res"

        python "$VIRTENVPATH"/bin/HiCPlotter_fix.py \
               -f "$WD_HIC"/colonomics_tumor/"$chr"_colonomics_tumor_hic_"$res".bed_matrix.txt \
               "$WD_HIC"/coad/"$chr"_coad_hic_"$res".bed_matrix.txt \
               "$WD_HIC"/colonomics_normal/"$chr"_colonomics_normal_hic_"$res".bed_matrix.txt \
               "$WD_HIC"/coad_normal_38/"$chr"_coad_normal_38_hic_"$res".bed_matrix.txt \
               -n colonomics_tumor_"$chr" \
               coad_"$chr" \
               colonomics_normal_"$chr" \
               coad_normal_38_"$chr" \
               -chr "$chr" \
               -o simple \
               -fh 0  \
               -pi 1 \
               -tt 0 \
               -ptr 1 \
               -hist "$CPG_DENSITY" "$CPG_DENSITY" "$CPG_DENSITY" "$CPG_DENSITY" \
               -hl probes probes probes probes \
               -fhist 1 1 1 1 \
               -r "$res"

        rm ab.bedgraph hypo.bedgraph hyper.bedgraph

    done
done

deactivate
export PYTHONPATH=$oldpythonpath


## test start





# cd $WD

# for dataset in colonomics_tumor colonomics_normal coad coad_normal_38
# do
#     mkdir -p "$WD"/"$dataset"
#     cd "$WD"/"$dataset"
#     for chr in $(seq 22 -1 1)
#     do
#         echo $chr
#         echo $dataset

#         item=chr"$chr"_"$dataset"
#         chr=chr"$chr"


#         ## preparing the hic contact matrix start
#         sed  '/^$/d' "$WD_HIC"/"$dataset"/"$item"_hic_min_rho_0.8_resolution_1000000.bed >  foo
#         mv foo  "$WD_HIC"/"$dataset"/"$item"_hic.bed
        
#         python "$BEDTOMATRIX" "$WD_HIC"/"$dataset"/"$item"_hic.bed
        
#         ## ab compartments and other stuff
#         fgrep coad "$WD"/13059_2015_741_MOESM2_ESM.txt | fgrep $chr | cut -f2-5 > ab.bedgraph
#         fgrep hypo "$WD"/ng.865-S2.bedgraph | fgrep $chr > hypo.bedgraph
#         fgrep hyper "$WD"/ng.865-S2.bedgraph | fgrep $chr > hyper.bedgraph
        
#         # ## histogram
#         # python "$VIRTENVPATH"/bin/HiCPlotter_fix.py \
#         #        -f "$WD_HIC"/"$dataset"/"$item"_hic.bed_matrix.txt \
#         #        -n "$dataset $chr" \
#         #        -chr "$chr" \
#         #        -o fortin_hansen_insulation \
#         #        -fh 0  \
#         #        -pi 1 \
#         #        -t "$WD"/ng.865-S2.bedgraph,"$WD"/ng.865-S3.bedgraph \
#         #        -tl hansen_delta,hansen_small -tt 0 \
#         #        -ptr 1 \
#         #        -hist ab.bedgraph \
#         #        -hl fortin_ab \
#         #        -fhist 1 \
#         #        -r 1000000 \
#         #        -ptd 1 #\
#         # # -pptd 1

#         # ## histogram
#         # # python "$VIRTENVPATH"/bin/HiCPlotter_fix.py \
#         # python $IHOME/src/correlations/analysis/HiCPlotter_fix_insulation.py \
#         #        -f "$WD_HIC"/"$dataset"/"$item"_hic.bed_matrix.txt \
#         #        -n "$dataset $chr" \
#         #        -chr "$chr" \
#         #        -o fortin_hansen_insulation \
#         #        -fh 0  \
#         #        -pi 1 \
#         #        -t hypo.bedgraph,hyper.bedgraph,"$WD"/ng.865-S3.bedgraph \
#         #        -tl hansen_hypo,hansen_hyper,hansen_small -tt 0 \
#         #        -ptr 1 \
#         #        -hist ab.bedgraph \
#         #        -hl fortin_ab \
#         #        -fhist 1 \
#         #        -r 1000000 \
#         #        -ptd 1 #\
#         # # -pptd 1

#         ## test with cpg density
#         python $IHOME/src/correlations/analysis/HiCPlotter_fix_insulation.py \
#                -f "$WD_HIC"/"$dataset"/"$item"_hic.bed_matrix.txt \
#                -n "$dataset $chr" \
#                -chr "$chr" \
#                -o fortin_hansen_insulation \
#                -fh 0  \
#                -pi 1 \
#                -t hypo.bedgraph,hyper.bedgraph,"$WD"/ng.865-S3.bedgraph \
#                -tl hansen_hypo,hansen_hyper,hansen_small -tt 0 \
#                -ptr 1 \
#                -hist ab.bedgraph,"$CPG_DENSITY" \
#                -hl fortin_ab,probes \
#                -fhist 1,1 \
#                -r 1000000 \
#                -ptd 1 #\

        
        
        

#         rm ab.bedgraph hypo.bedgraph hyper.bedgraph

        
#     done
# done


# chr=chr17
# ##test with side by side plots
# python "$VIRTENVPATH"/bin/HiCPlotter_fix.py \
#        -f "$WD_HIC"/colonomics_tumor/"$chr"_colonomics_tumor_hic.bed_matrix.txt \
#        "$WD_HIC"/coad/"$chr"_coad_hic.bed_matrix.txt \
#        "$WD_HIC"/colonomics_normal/"$chr"_colonomics_normal_hic.bed_matrix.txt \
#        "$WD_HIC"/coad_normal_38/"$chr"_coad_normal_38_hic.bed_matrix.txt \
#        -n colonomics_tumor_"$chr" \
#        coad_"$chr" \
#        colonomics_normal_"$chr" \
#        coad_normal_38_"$chr" \
#        -chr "$chr" \
#        -o montage \
#        -fh 0  \
#        -pi 1 \
#        -t hypo.bedgraph,hyper.bedgraph,"$WD"/ng.865-S3.bedgraph \
#        hypo.bedgraph,hyper.bedgraph,"$WD"/ng.865-S3.bedgraph \
#        hypo.bedgraph,hyper.bedgraph,"$WD"/ng.865-S3.bedgraph \
#        hypo.bedgraph,hyper.bedgraph,"$WD"/ng.865-S3.bedgraph \
#        -tl hansen_hypo,hansen_hyper,hansen_small \
#        hansen_hypo,hansen_hyper,hansen_small \
#        hansen_hypo,hansen_hyper,hansen_small \
#        hansen_hypo,hansen_hyper,hansen_small \
#        -tt 0 \
#        -ptr 1 \
#        -hist ab.bedgraph,"$CPG_DENSITY" \
#        ab.bedgraph,"$CPG_DENSITY" \
#        ab.bedgraph,"$CPG_DENSITY" \
#        ab.bedgraph,"$CPG_DENSITY" \
#        -hl fortin_ab,probes \
#        fortin_ab,probes \
#        fortin_ab,probes \
#        fortin_ab,probes \
#        -fhist 1,1 1,1 1,1 1,1 \
#        -r 1000000



# ## tests on homemade A/B compartments (1M resolution)
# cd $WD

# for dataset in colonomics_tumor colonomics_normal coad coad_normal_38
# do
#     mkdir -p "$WD"/"$dataset"
#     cd "$WD"/"$dataset"
#     for chr in $(seq 22 -1 1)
#     do
#         echo $chr
#         echo $dataset

#         item=chr"$chr"_"$dataset"
#         chr=chr"$chr"

#         awk '{
#  OFS=FS="\t";
# gsub(/"/, "");
#  print $2,$3,$4,$(NF-1)
# }' "$AB"/"$dataset"_"$chr"_1000000_compartments.bed > $WD/"$dataset"/ab_"$chr"_"$dataset".bedgraph


#     done
# done

# cd $WD

# for chr in $(seq 22 -1 15)
# do
#     echo $chr
#     chr=chr"$chr"

#     ## ab compartments and other stuff
#     fgrep coad "$WD"/13059_2015_741_MOESM2_ESM.txt | fgrep $chr | cut -f2-5 > ab.bedgraph
#     fgrep hypo "$WD"/ng.865-S2.bedgraph | fgrep $chr > hypo.bedgraph
#     fgrep hyper "$WD"/ng.865-S2.bedgraph | fgrep $chr > hyper.bedgraph
    

#     ##test with side by side plots
#     python "$VIRTENVPATH"/bin/HiCPlotter_fix.py \
#            -f "$WD_HIC"/colonomics_tumor/"$chr"_colonomics_tumor_hic.bed_matrix.txt \
#            "$WD_HIC"/coad/"$chr"_coad_hic.bed_matrix.txt \
#            "$WD_HIC"/colonomics_normal/"$chr"_colonomics_normal_hic.bed_matrix.txt \
#            "$WD_HIC"/coad_normal_38/"$chr"_coad_normal_38_hic.bed_matrix.txt \
#            -n colonomics_tumor_"$chr" \
#            coad_"$chr" \
#            colonomics_normal_"$chr" \
#            coad_normal_38_"$chr" \
#            -chr "$chr" \
#            -o montage \
#            -fh 0  \
#            -pi 1 \
#            -t hypo.bedgraph,hyper.bedgraph,"$WD"/ng.865-S3.bedgraph \
#            hypo.bedgraph,hyper.bedgraph,"$WD"/ng.865-S3.bedgraph \
#            hypo.bedgraph,hyper.bedgraph,"$WD"/ng.865-S3.bedgraph \
#            hypo.bedgraph,hyper.bedgraph,"$WD"/ng.865-S3.bedgraph \
#            -tl hansen_hypo,hansen_hyper,hansen_small \
#            hansen_hypo,hansen_hyper,hansen_small \
#            hansen_hypo,hansen_hyper,hansen_small \
#            hansen_hypo,hansen_hyper,hansen_small \
#            -tt 0 \
#            -ptr 1 \
#            -hist ab.bedgraph,"$CPG_DENSITY","$WD"/colonomics_tumor/ab_"$chr"_colonomics_tumor.bedgraph \
#            ab.bedgraph,"$CPG_DENSITY","$WD"/coad/ab_"$chr"_coad.bedgraph \
#            ab.bedgraph,"$CPG_DENSITY","$WD"/colonomics_normal/ab_"$chr"_colonomics_normal.bedgraph \
#            ab.bedgraph,"$CPG_DENSITY","$WD"/coad_normal_38/ab_"$chr"_coad_normal_38.bedgraph \
#            -hl fortin_ab,probes,custom_ab \
#            fortin_ab,probes,custom_ab \
#            fortin_ab,probes,custom_ab \
#            fortin_ab,probes,custom_ab \
#            -fhist 1,1,1 1,1,1 1,1,1 1,1,1 \
#            -r 1000000

#     rm ab.bedgraph hypo.bedgraph hyper.bedgraph
# done



# ## control with fortin published and our fortin predict
# chr=chr17
# dataset='colonomics_tumor'
# cd "$WD"/"$dataset"
# item="$chr"_"$dataset"

# awk '{
#  OFS=FS="\t";
# gsub(/"/, "");
#  print $2,$3,$4,$(NF-1)
# }' "$AB"/"$dataset"_"$chr"_100000_compartments.bed > $WD/"$dataset"/ab_"$chr"_"$dataset".100k.bedgraph


# ## ab compartments and other stuff
# fgrep coad "$WD"/13059_2015_741_MOESM2_ESM.txt | fgrep $chr | cut -f2-5 > ab.bedgraph
# fgrep hypo "$WD"/ng.865-S2.bedgraph | fgrep $chr > hypo.bedgraph
# fgrep hyper "$WD"/ng.865-S2.bedgraph | fgrep $chr > hyper.bedgraph

# python $IHOME/src/correlations/analysis/HiCPlotter_fix_insulation.py \
#        -f "$WD_HIC"/"$dataset"/"$item"_hic.bed_matrix.txt \
#        -n "$dataset $chr" \
#        -chr "$chr" \
#        -o control_colonomics_tumor \
#        -fh 0  \
#        -pi 1 \
#        -t hypo.bedgraph,hyper.bedgraph,"$WD"/ng.865-S3.bedgraph \
#        -tl hansen_hypo,hansen_hyper,hansen_small -tt 0 \
#        -ptr 1 \
#        -hist ab.bedgraph,$WD/"$dataset"/ab_"$chr"_"$dataset".100k.bedgraph,"$CPG_DENSITY" \
#        -hl fortin_ab,custom_ab,probes \
#        -fhist 1,1,1 \
#        -r 1000000 \
#        -ptd 1 #\




# ### higher resolution (100K) #################################################################


# RESOLUTION=100000
# CPG_DENSITY=$IHOME/correlations_simpler/fortin_ab_compartments_bedgraphs/cpg_number_"$RESOLUTION".tsv


# ## tests on homemade A/B compartments (1M resolution)
# cd $WD

# for dataset in colonomics_tumor colonomics_normal coad coad_normal_38
# do
#     mkdir -p "$WD"/"$dataset"
#     cd "$WD"/"$dataset"
#     for chr in $(seq 22 -1 21)
#     do
#         echo $chr
#         echo $dataset

#         item=chr"$chr"_"$dataset"
#         chr=chr"$chr"

#         ## ab predict formatting
#         awk '{
#  OFS=FS="\t";
# gsub(/"/, "");
#  print $2,$3,$4,$(NF-1)
# }' "$AB"/"$dataset"_"$chr"_"$RESOLUTION"_compartments.bed > \
#             $WD/"$dataset"/ab_"$chr"_"$dataset"_"$RESOLUTION".bedgraph

#         ## homemade hic-like stuff formatting
#         sed  '/^$/d' "$WD_HIC"/"$RESOLUTION"/"$dataset"/"$item"_hic_min_rho_0.8_resolution_"$RESOLUTION".bed\
#              >  foo

#         mv foo  "$WD_HIC"/"$RESOLUTION"/"$dataset"/"$item"_hic.bed
        
#         python "$BEDTOMATRIX" "$WD_HIC"/"$RESOLUTION"/"$dataset"/"$item"_hic.bed

#     done
# done


# ## plotting
# cd $WD
# for chr in $(seq 22 -1 21)
# do
#     echo $chr

#     chr=chr"$chr"

#     ## published ab and hansen's hypo and hyper        
#     fgrep coad "$WD"/13059_2015_741_MOESM2_ESM.txt | fgrep $chr | cut -f2-5 > ab.bedgraph
#     fgrep hypo "$WD"/ng.865-S2.bedgraph | fgrep $chr > hypo.bedgraph
#     fgrep hyper "$WD"/ng.865-S2.bedgraph | fgrep $chr > hyper.bedgraph

#      python "$VIRTENVPATH"/bin/HiCPlotter_fix.py \
#            -f "$WD_HIC"/"$RESOLUTION"/colonomics_tumor/"$chr"_colonomics_tumor_hic.bed_matrix.txt \
#            "$WD_HIC"/"$RESOLUTION"/coad/"$chr"_coad_hic.bed_matrix.txt \
#            "$WD_HIC"/"$RESOLUTION"/colonomics_normal/"$chr"_colonomics_normal_hic.bed_matrix.txt \
#            "$WD_HIC"/"$RESOLUTION"/coad_normal_38/"$chr"_coad_normal_38_hic.bed_matrix.txt \
#            -n colonomics_tumor_"$chr" \
#            coad_"$chr" \
#            colonomics_normal_"$chr" \
#            coad_normal_38_"$chr" \
#            -chr "$chr" \
#            -o montage \
#            -fh 0  \
#            -pi 1 \
#            -t hypo.bedgraph,hyper.bedgraph,"$WD"/ng.865-S3.bedgraph \
#            hypo.bedgraph,hyper.bedgraph,"$WD"/ng.865-S3.bedgraph \
#            hypo.bedgraph,hyper.bedgraph,"$WD"/ng.865-S3.bedgraph \
#            hypo.bedgraph,hyper.bedgraph,"$WD"/ng.865-S3.bedgraph \
#            -tl hansen_hypo,hansen_hyper,hansen_small \
#            hansen_hypo,hansen_hyper,hansen_small \
#            hansen_hypo,hansen_hyper,hansen_small \
#            hansen_hypo,hansen_hyper,hansen_small \
#            -tt 0 \
#            -ptr 1 \
#            -hist ab.bedgraph,"$CPG_DENSITY","$WD"/colonomics_tumor/ab_"$chr"_colonomics_tumor_"$RESOLUTION".bedgraph \
#            ab.bedgraph,"$CPG_DENSITY","$WD"/coad/ab_"$chr"_coad_"$RESOLUTION".bedgraph \
#            ab.bedgraph,"$CPG_DENSITY","$WD"/colonomics_normal/ab_"$chr"_colonomics_normal_"$RESOLUTION".bedgraph \
#            ab.bedgraph,"$CPG_DENSITY","$WD"/coad_normal_38/ab_"$chr"_coad_normal_38_"$RESOLUTION".bedgraph \
#            -hl fortin_ab,probes,custom_ab \
#            fortin_ab,probes,custom_ab \
#            fortin_ab,probes,custom_ab \
#            fortin_ab,probes,custom_ab \
#            -fhist 1,1,1 1,1,1 1,1,1 1,1,1 \
#            -r 100000

#     rm ab.bedgraph hypo.bedgraph hyper.bedgraph
# done





# deactivate
# export PYTHONPATH=$oldpythonpath
