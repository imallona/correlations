#!/bin/env/ R
##
##
## TCGA coad tumor deseq analysis as splitted by clues/knn groups
## Requires the output of tcga_expression_retrieval_api.sh
##
## Izaskun Mallona, GPL
## 18th nov 2016

library(DESeq2)
library("BiocParallel")
library("pheatmap")
library("RColorBrewer")
library(Cairo)
library(ggplot2)
## library(TCGAbiolinks) ## GEA GO analysis
library(biomaRt)
library(testthat)

NTHREADS <- 6
IHOME <- '/imppc/labs/maplab/imallona'
WD <- file.path(IHOME, 'correlations', 'tcga_expression_gdc')

DATA <- file.path(IHOME, 'correlations_simpler', 'clues_knn', 'k10')
## trans communities import
TRANS <- file.path(IHOME, 'correlations_simpler',
                   'trans_communities_subset', 'results')

OUTDIR <- file.path(WD, 'out')


## functions start


##' .. content for \description{} (no empty lines) ..
##'
##' .. content for \details{} ..
##' @title 
##' @param dat a deseq differential expression output
##' @return 
##' @author Izaskun Mallona
retrieve_significant <- function(dat) {
    dat <- dat[!is.na(dat$padj) &
                           dat$padj < 0.05,]
    return(dat)
}

##' @title 
##' @param contrasts_results a list of contrasts run by deseq
##' @return 
##' @author Izaskun Mallona
write_significant <- function(contrasts_results) {

    mart <- useMart(biomart = "ensembl", dataset = "hsapiens_gene_ensembl")
   
    
    for (name in names(contrasts_results)) {
        print(name)
        dat <- retrieve_significant(contrasts_results[[name]])
        genes <- rownames(dat)
        genes <- sapply(strsplit( genes, '.', fixed = TRUE), function(x) return(x[[1]]))

        annotated <- getBM(attributes = c("ensembl_gene_id",
                               "description",
                               "external_gene_name",
                               "gene_biotype"),
                           filters = "ensembl_gene_id", values = genes,
                           mart = mart)

        dat$ensembl_gene_id <- genes
        dat <- merge(as.data.frame(dat), annotated,
                     by = 'ensembl_gene_id',
                     all.x = TRUE)
        dat <- dat[order(dat$pvalue),]
        
        ## ansEA <- TCGAanalyze_EAcomplete(TFname = name, genes)

        dir.create(file.path(OUTDIR, name), showWarnings = FALSE)

        write.csv(
            dat,
            file = file.path(OUTDIR, name, paste0(name, 'differentially_expressed_genes.csv')),
            row.names = FALSE)

        setwd(WD)
    }
}


## returns all the possible pairwise comparisons of a vector of elements
pair_pairwise <- function(members) {
    members <- sort(members)
    to_return <- list()
    
    i <- 1
    while (i < length(members)) {
        j <- i+1
        while (j <= length(members)) {
            to_return[[paste(members[i], members[j], sep = '-')]] <- c(members[i], members[j])
            j <- j+1
        }
        i <- i+1
    }
    return(to_return)
}

test_pair_pairwise <- function() {
    test_that("pairwise pairing", {
        foo <- structure(list(`2-3` = c("2", "3"), `2-4` = c("2", "4"),
                              `3-4` = c("3", "4")), .Names = c("2-3", "2-4", "3-4"))
        expect_equal(pair_pairwise(c('2', '4', '3')), foo)       
    })
}

## functions end

test_pair_pairwise()

###############


setwd(WD)

print('Reading counts')

counts <- read.table(pipe("grep ENSG0 coad_rnaseq_counts.tsv"),
                     sep = "\t",
                     stringsAsFactors = FALSE, header = FALSE)
header <- read.table("coad_rnaseq_counts.tsv",
                     sep = "\t",
                     stringsAsFactors = FALSE, header = FALSE, nrows = 1)

colnames(counts) <- header


print('Reading the mapping')
mapping <- read.table("coad_rnaseq_mapping.tsv",
                      sep = "\t",
                      stringsAsFactors = FALSE, header = TRUE)

print('Merging them')

rownames(mapping) <- mapping$file_name
colnames(counts) <- gsub('-', '_', mapping[as.character(header[1,]),2])

counts <- counts[,-2]
colnames(counts)[1] <- 'gene'

print('Retrieving those samples belonging to the DNA-meth-scrutinized cohort')

curr <-  'colonomics_tumor_inner_min_degree_1_min_degree_1_min_size_10_min_rho_0.8_positive_only'

## trans communities set up
cn_ids <-read.table(
    file.path(TRANS,
              sprintf('%s_comm_membership.tsvtrans_communities', curr)))$V1

## communities <- communities[communities > 4]
## communities <- cn_ids[cn_ids > 4]

## not iterating now

## comm <- 5
for (comm in communities) {
    tryCatch({
        dataset <- 'coad'
        curr <- read.table(
            file.path(DATA, comm,
                      sprintf('community_%s_%s_clusters.tsv', comm, dataset)),
            header = TRUE)

        curr_counts <- counts
        
        curr$id <-  gsub('-', '_', curr$id)
        curr <- curr[!is.na(curr$clues_membership),]

        table(curr$id %in% colnames(counts))

        ## FALSE 
        ##   226

        table(strtrim(curr$id,16) %in% strtrim(colnames(counts),16))
        ## FALSE  TRUE 
        ##     1   225

        colnames(curr_counts) <- strtrim(colnames(curr_counts),16)

        selected <- intersect (strtrim(curr$id,16), colnames(curr_counts))

        curr_counts <- curr_counts[, c('gene', selected)]

        dim(curr_counts)
        ## [1] 60433   226

        rownames(curr_counts) <- curr_counts[,1]
        curr_counts <- curr_counts[,-1]

        print("Running DESeq contrasts as the colonomics' with limma")

        curr$id <- strtrim(curr$id, 16)

        curr$clues_membership <- as.factor(sprintf('comm_%s_cluster_%s',
                                                   comm,
                                                   curr$clues_membership))

        curr <- curr[curr$id %in% selected,]

        dds <- DESeqDataSetFromMatrix(countData = curr_counts,
                                      colData = curr,
                                      design = ~ clues_membership)

        dds.deseq <- DESeq(dds, parallel = TRUE)

        vsd <- varianceStabilizingTransformation(dds.deseq, blind = TRUE)

        contrasts <- NULL

        ## k <- 1
        ## for (i in  1:nlevels(colData(vsd)$clues_membership)) {
        ##     a <- levels(colData(vsd)$clues_membership)[i]
        
        ##     for (j in (i+1):nlevels(colData(vsd)$clues_membership)) {
        ##         b <- levels(colData(vsd)$clues_membership)[j]
        ##         if (a != b & !is.na(b)) {
        
        ##             contrasts[[k]] <- list(paste0('clues_membership', a),
        ##                                    paste0('clues_membership', b))
        ##             k = k + 1
        ##         }
        ##     }
        ## }

        
        ##
        members <- levels(colData(vsd)$clues_membership)
        paired_members <- pair_pairwise(members)
        for (item in names(paired_members)) {
            contrasts[[item]] <- list(paste0('clues_membership', paired_members[[item]][1]),
                                      paste0('clues_membership', paired_members[[item]][2]))
        }
        
        ##

        contrasts_results <- list()

        for (i in 1:length(contrasts)){

            contrasts_results[[paste(contrasts[[i]],
                                     collapse = "__vs__")]] <- results(dds.deseq,
                                         contrast = contrasts[[i]])
        }

        dir.create (OUTDIR)

        write_significant(contrasts_results = contrasts_results)
    }, error = function(x) print(paste('Skipped', comm)))
}
