#!/usr/bin/env R
##
## meth-PC1 N vs T comparison
##
## both for tcga and colonomics data
##
## Izaskun Mallona, GPL v2
##
## 4 feb 2016 

IHOME <- '/imppc/labs/maplab/imallona'
WD <- file.path(IHOME, "correlations", 'colonomics_components_directly_from_communities_normal_vs_tumor') 

## PCS <- file.path(IHOME, 'correlations', 'colonomics_components_directy_from_communities')
PCS <- file.path(IHOME, 'correlations', 'components_from_colonomics_tumor_communities_applied_to_colonomics',
                 'applied_to_colonomics_data')

COHORT <- 'tcga'
COHORT <- 'colonomics'

NORMALS_TUMOR <- file.path(PCS, 'normal_data_components_for_communities_built_from_tumors.tab')
TUMORS_TUMOR <- file.path(PCS, 'tumor_data_components_for_communities_built_from_tumors.tab')

cat('Processing tumor-derived PCs\n')

dir.create(WD)


## the components
tumor_pca <- list()
tumor_pca[['normal']] <- read.table(NORMALS_TUMOR, header = TRUE, sep = "\t", stringsAsFactors = FALSE)
tumor_pca[['tumor']] <-  read.table(TUMORS_TUMOR, header = TRUE, sep = "\t", stringsAsFactors = FALSE)

## homogeneous naming conventions

if (COHORT == 'tcga') {
    for (ttype in c('normal', 'tumor')) {
        colnames(tumor_pca[[ttype]])[1] <- 'sample'
        tumor_pca[[ttype]]$sample <- gsub('-', '_', tumor_pca[[ttype]]$sample)
        tumor_pca[[ttype]]$patient <- sapply(strsplit(tumor_pca[[ttype]]$sample, '_'), function(x) return(x[1]))
    }

    communities <- colnames(tumor_pca[['normal']])[2:(ncol(tumor_pca[['normal']]) - 1)]
} else if (COHORT == 'colonomics') {
    for (ttype in c('normal', 'tumor')) {
        tumor_pca[[ttype]]$patient <- strtrim(rownames(tumor_pca[[ttype]]), 5)
    }
}

communities <- colnames(tumor_pca[[ttype]])[1:(ncol(tumor_pca[[ttype]]) -1)]

pdf(file.path(WD, 'significant_correlations_normal_tumor.pdf'), useDingbats = FALSE)


res <- list()
## par(mfrow=c(3,3))
for (com in communities){
    ctest <- cor.test(tumor_pca[['normal']][,com], tumor_pca[['tumor']][,com],
                      method = 'spearman', exact = TRUE)
    if (ctest$p.value < 0.01) {
        ymin <- min(tumor_pca[['normal']][,com], tumor_pca[['tumor']][,com])
        ymax <- max(tumor_pca[['normal']][,com], tumor_pca[['tumor']][,com])

   
        plot(tumor_pca[['normal']][,com], tumor_pca[['tumor']][,com],
             main = paste(com, '\n', sprintf('corr = %0.2g', ctest$estimate, 2),
                 sprintf('pval = %0.2g', ctest$p.value, 2)),
             xlab = 'colonomics_normal', ylab = 'colonomics_tumor',
             ylim = c(ymin, ymax), xlim = c(ymin, ymax))
             
    }

    res[[com]] <- c('colonomics_tumor_derived_community', 'colonomics_data', com, as.character(ctest))
}

dev.off()

cnames <- c('original_pca', 'methylation_data_used_to_build_the_PCAs', 'community', 's', 'null', 'pval', 'rho', 'hypothesis', 'alternative', 'method', 'data')

write.table(x = t(as.data.frame(res)), file = file.path(WD, 'correlations_normal_tumor.tsv'),
            quote = FALSE, col.names = cnames, row.names = FALSE, sep = "\t")

rm(res)

## the same for TCGA' data 

## source(file.path(IHOME, 'src', 'correlations', 'analysis', 'database_connection.R'))

## this will include PC1 calculation as it wasn't done before
print('Exec pca_directly_from_components with the adequate db_config file')
