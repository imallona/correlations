#!/usr/bin/env R
##
## gets a look at the highly connected cpgs, outliers at the log log CCDF plots 
## also evaluates monk's imprinting sites
##
## http://genome.cshlp.org/content/suppl/2014/01/15/gr.164913.113.DC1/Supplemental_Table_S1.xlsx
##
## 26th june 2017
## Izaskun Mallona, GPLv2

library(RPostgreSQL)
library(plyr)
library(ggbio)
library(GenomicRanges)
library(Cairo)

require(beanplot)
library(gplots)


IHOME <- '/imppc/labs/maplab/imallona' 
SRC2 <- file.path(IHOME, 'src')
DB_CONF <- file.path(SRC2, 'correlations', 'db_imallona_local_beech.txt')
WD <- file.path(IHOME, 'correlations_simpler', 'imprinting')
NTHREADS <- 11

source(file.path(IHOME, 'src', 'correlations', 'analysis', 'database_connection.R'))

## being cs a vector of degrees
degree.distrib.from.degrees <- function(cs, cumulative) {
    hi <- hist(cs, -1:max(cs), plot = FALSE)$density
    if (!cumulative) {
        res <- hi
    }
    else {
        res <- rev(cumsum(rev(hi)))
    }
    res
}

dir.create(WD)
setwd(WD)

drv <- dbDriver("PostgreSQL")
db_conf <- get_db_parameters(DB_CONF)
con <- dbConnect(drv,
                 user = db_conf[['user']],
                 password = db_conf[['password']],
                 dbname = 'colonomics_correlations',
                 host = db_conf[['host']],
                 port = db_conf[['port']])

trans <- get_query_at_once(stmt = '
select meth_probe_a, meth_probe_b
from colonomics_tumor.meth_filtered_correlations c,
annotation.humanmethylation450_probeinfo a1,
annotation.humanmethylation450_probeinfo a2
where c.meth_probe_a = a1.probe and
 c.meth_probe_b = a2.probe and
 a1.chromosome != a2.chromosome
', con = con)

nrow(trans)

## getting the degrees

ft <-  count(c(trans$meth_probe_a, trans$meth_probe_b))
d <- ft$freq

dd <- degree.distrib.from.degrees(cs = d, cumulative = TRUE)

ft <- ft[order(ft$freq, decreasing = TRUE),]
rownames(ft) <- 1:nrow(ft)

## top 1 percentile
top <- ft[1:(nrow(ft)/100),]

## simple bedtools overlaps
selected <-  paste0("('", paste0(top$x, collapse = "','"), "')")

stmt <- sprintf('select chromosome, meth_start, meth_end, probe
from annotation.humanmethylation450_probeinfo
where probe in %s', selected)

top_bed <- get_query_at_once(stmt = stmt, con = con)
top_bed$chromosome <- paste0('chr', top_bed$chromosome)

write.table(x = top_bed,
            file = file.path(WD, 'top_percentile_trans.bed'),
            col.names = FALSE,
            row.names = FALSE,
            sep = "\t", quote = FALSE)

## bedtools sort -i top_percentile_trans.bed  > sorted; mv sorted top_percentile_trans.bed
## bedtools sort -i data/monks_s1_450k_sliding_windows_analysis.bed  > sorted
## mv sorted data/monks_s1_450k_sliding_windows_analysis.bed
## cut -f1-3 data/monks_s1_63_common_windows.bed |bedtools sort  > sorted
## mv sorted data/monks_s1_63_common_windows.bed



## for item in data/monks_s1_450k_sliding_windows_analysis.bed data/monks_s1_63_common_windows.bed 
## do
##   bedtools intersect -a top_percentile_trans.bed \
##    -b "$item" \
##   -c  > "$(basename $item .bed)".count


##   bedtools intersect -a top_percentile_trans.bed \
##    -b "$item" \
##    -wb  > "$(basename $item .bed)".intersect

##   cut -f5 "$(basename $item .bed)".count | sort |  uniq -c
##   echo -------
## done

ft[ft$x == 'cg09003373',] ## 37
ft[ft$x == 'cg14088957',] ## 298
ft[ft$x == 'cg06100421',] ## 
ft[ft$x == 'cg01784351',] ## 


## ggbio stuff

data(hg19IdeogramCyto, package = "biovizBase")
## hg19 <- keepSeqlevels(hg19IdeogramCyto, paste0("chr", c(1:22, "X", "Y")))
hg19 <- keepSeqlevels(hg19IdeogramCyto, paste0("chr", c(1:22)))

probes.granges <- GRanges(seqnames = top_bed[,1],
                          ranges = IRanges(start=top_bed[,2],
                              end = top_bed[,3]),
                          strand = "*")

elementMetadata(probes.granges) <- 'top_percentile'

## probes.granges <- keepSeqlevels(probes.granges, paste0("chr",c(1:22)))
seqlengths(probes.granges) <- seqlengths(
    hg19IdeogramCyto)[names(seqlengths(probes.granges))]

data(hg19Ideogram, package = "biovizBase")


p <- ggplot(hg19) + layout_karyogram(cytoband = TRUE)
p <- p + layout_karyogram(probes.granges, geom = "rect", ylim = c(11, 21), color = "red")
p

CairoPDF(file.path(WD,'top_percentile_rich_probes.pdf'))
print(p)
dev.off()

rm(p)

## now with imprinted overlayed

imprinted.file <- file.path(WD, 'data', 'monks_s1_63_common_windows.bed')
imprinted.data <- read.table(imprinted.file, header=FALSE, sep="\t",stringsAsFactors=FALSE)

imprinted.granges <- GRanges(seqnames = imprinted.data[,1],
                             ranges = IRanges(start=imprinted.data[,2],
                                 end = imprinted.data[,3]),
                             strand = "*")


seqlengths(imprinted.granges) <- seqlengths(
    hg19IdeogramCyto)[names(seqlengths(imprinted.granges))]

rm(p)
p <- ggplot(hg19) + layout_karyogram(cytoband = TRUE)

p <- p + layout_karyogram(probes.granges, geom = "rect", ylim = c(11, 21), color = "darkred") +
    layout_karyogram(imprinted.granges, geom = "rect", ylim = c(11, 21), color = "blue")

p

CairoPDF(file.path(WD,'top_percentile_rich_probes_red_monk_in_blue.pdf'))
print(p)
dev.off()

## now plotting the frequencies ranges
## head(ft)


## ft$freq2 <- ft$freq
## rownames(ft) <- ft$x
## ft <- ft[,2:3]
## heatmap(as.matrix(ft))
## ## selected <-  paste0("('", paste0(ft$x, collapse = "','"), "')")

## ## stmt <- sprintf('select chromosome, meth_start, meth_end, probe
## ## from annotation.humanmethylation450_probeinfo
## ## where probe in %s', selected)

## ## locs <- get_query_at_once(stmt = stmt, con = con)
## ## str(locs)
## ## locs$chromosome <- as.numeric(locs$chromosome)

## let's represent these

## require(stats)  # both 'density' and its default method
## with(faithful, {
##     plot(density(eruptions, bw = 0.15))
##     rug(eruptions)
##     rug(jitter(eruptions, amount = 0.01), side = 3, col = "light blue")
## })

## data(iris)
## plot(density(iris[,1]))
## rug(iris[,1])

## get imprinted genes

genes <- read.table(file.path(WD, 'imprinted_gene_symbols.txt'))$V1
head(genes)
selected <-  paste0("('", paste0(genes, collapse = "','"), "')")

stmt <- sprintf('select probe
from annotation.humanmethylation450_probeinfo
where closest_tss_gene_name in %s', selected)

impr_annot <- get_query_at_once(stmt = stmt, con = con)

table(impr_annot$probe %in% ft$x)

ft$impr_list <- ft$x %in% impr_annot$probe
ft$rank <- 1:nrow(ft)

plot(ft$rank, ft$freq, col = ifelse(ft$impr_list, 'red', 'black'),
     pch = ifelse(ft$impr_list, '19', '.') )

summary(lm(ft$freq~ ft$impr_list))

rug(rownames(ft[ft$impr_list,]))

boxplot(ft$freq~ ft$impr_list)

sink(file.path(WD, 'degree_vs_imprinted_any_cpg.txt'))
dim(ft)
head(ft)
table(ft$impr_list)
by(data = ft, ft$impr_list, function(x) mean(x$freq))
by(data = ft, ft$impr_list, function(x) sd(x$freq))
by(data = ft, ft$impr_list, function(x) summary(x$freq))
sink()

by(data = ft, ft$impr_list, function(x) mean(x$freq))
sink(file.path(WD, 'wilcox_degree_vs_imprinted.txt'))

wilcox.test(ft$freq~ as.factor(ft$impr_list))
sink()


## not included these because due to the gene-symbol based data retrieval,
## as anotated at the postgres, some imprinted cpgs are lost
## and does not totally fit the imprinted.count file

## sink(file.path(WD, 'degree_vs_imprinted_top_1_percentile.txt'))
## mini_ft <- ft[1:(nrow(ft)/100),]
## dim(mini_ft)
## head(mini_ft)
## table(mini_ft$impr_list)
## by(data = mini_ft, mini_ft$impr_list, function(x) mean(x$freq))
## by(data = mini_ft, mini_ft$impr_list, function(x) sd(x$freq))
## by(data = mini_ft, mini_ft$impr_list, function(x) summary(x$freq))
## sink()

## by(data = ft, ft$impr_list, function(x) mean(x$freq))
## sink(file.path(WD, 'wilcox_degree_vs_imprinted.txt'))

## wilcox.test(ft$freq~ as.factor(ft$impr_list))
## sink()


## so let's build a series of plots with the beta values of:
## - top ranked
## - imprinted
## - random background

## imprinted cpgs
head(impr_annot)
imprinted <- list(cpgs = paste0("('", paste0(impr_annot$probe, collapse = "','"), "')"))
imprinted[['normal']] <- get_query_at_once(
    stmt = sprintf('select * from colonomics_normal.humanmethylation450 where probe in %s',
        imprinted$cpgs),
    con = con)
imprinted[['tumor']] <- get_query_at_once(
    stmt = sprintf('select * from colonomics_tumor.humanmethylation450 where probe in %s',
        imprinted$cpgs),
    con = con)

str(imprinted)

## top_ranked

topl <- list(cpgs =  paste0("('", paste0(top$x, collapse = "','"), "')"))
topl[['tumor']] = get_query_at_once(
        stmt = sprintf('select * from colonomics_tumor.humanmethylation450 where probe in %s',
            topl$cpgs),
        con = con)
topl[['normal']] <- get_query_at_once(
    stmt = sprintf('select * from colonomics_normal.humanmethylation450 where probe in %s',
        topl$cpgs), con = con)

str(topl)

## random background

random <- list('tumor' =
                   get_query_at_once(
                       stmt = 'select * from colonomics_tumor.humanmethylation450 limit 500',
                       con = con),
               'normal' = get_query_at_once(
                   stmt = 'select * from colonomics_normal.humanmethylation450 limit 500',
                   con = con))
             
str(random)


boxplot(list(tumor = unlist(random$tumor[,2:ncol(random$tumor)]),
             normal = unlist(random$normal[,2:ncol(random$normal)])))



stripchart(list(tumor = unlist(random$tumor[,2:ncol(random$tumor)])[1:500],
                normal = unlist(random$normal[,2:ncol(random$normal)])[1:500]),
           vertical = T, method = "jitter",  
           add = TRUE, pch = 19)

## test

beanplot(list(
    random_normal = unlist(random$normal[,2:ncol(random$normal)]),
    random_tumor = unlist(random$tumor[,2:ncol(random$tumor)]),
    imprinted_normal = unlist(imprinted$normal[,2:ncol(imprinted$normal)])[1:5000],
    imprinted_tumor = unlist(imprinted$tumor[,2:ncol(imprinted$tumor)])[1:5000],
    rich_normal = unlist(topl$normal[,2:ncol(topl$normal)]),
    rich_tumor = unlist(topl$tumor[,2:ncol(topl$tumor)])  
),
         what=c(0,1,1,0),
         col = c(rep('blue', 2), rep('gray', 2)),
         ylab = 'methylation (beta)',
         las = 2)


## bottom, left, top, and right. The default is c(5.1, 4.1, 4.1, 2.1).

pdf(file.path(WD, 'dna_meth_beanplots_random_and_rich.pdf'))
par(mar = c(7.1, 4.1, 4.1, 2.1))
beanplot(list(
    random_normal = unlist(random$normal[,2:ncol(random$normal)]),
    random_tumor = unlist(random$tumor[,2:ncol(random$tumor)]),
    rich_normal = unlist(topl$normal[,2:ncol(topl$normal)]),
    rich_tumor = unlist(topl$tumor[,2:ncol(topl$tumor)])  
),
         what=c(0,1,0,0),
         col = 'gray80',
         ylab = 'methylation (beta)',
         las = 2)
dev.off()

## x11()

for (item in c('normal', 'tumor')) {

    rownames(random[[item]]) <- random[[item]]$probe
    random[[item]] <- random[[item]][,2:ncol(random[[item]])]

    random[[item]] <- random[[item]][,sort(colnames(random[[item]]))]

}





random_hc_probe <- hclust(dist(random[['tumor']]), method = 'ward')
top_hc_sample <- hclust(dist(t(topl[['tumor']])), method = 'ward')

plot(random_hc_probe)

mycol <- gray(seq(1,0, length.out=100))

png(file.path(WD, 'random_normal_heatmap.png'), width = 600, height = 600)
heatmap.2(as.matrix(random[['normal']]), 
          col=mycol,
          labRow = '',
          sepcolor='white',
          trace="none",
          Rowv = as.dendrogram(random_hc_probe),
          Colv = as.dendrogram(top_hc_sample),          
          ## scale="row",
          sepwidth=0.05,  
          main = 'random normal')

dev.off()

png(file.path(WD, 'random_tumor_heatmap.png'), width = 600, height = 600)

heatmap.2(as.matrix(random[['tumor']]),
          ## labCol = colnames(random[['tumor']][,2:ncol(random$tumor)]),  
          col=mycol,
          labRow = '',
          sepcolor='white',
          trace="none",
          Rowv = as.dendrogram(random_hc_probe),
          Colv = as.dendrogram(top_hc_sample), 
          ## Colv = NULL,
          ## scale="row",
          sepwidth=0.05,  
          main = 'random tumor')
dev.off()

## same for rich probes



for (item in c('normal', 'tumor')) {

    rownames(topl[[item]]) <- topl[[item]]$probe
    topl[[item]] <- topl[[item]][,2:ncol(topl[[item]])]

    topl[[item]] <- topl[[item]][,sort(colnames(topl[[item]]))]

}


topl_hc_probe <- hclust(dist(topl[['tumor']]), method = 'ward')
topl_hc_sample <- hclust(dist(t(topl[['tumor']])), method = 'ward')

plot(topl_hc_probe)

mycol <- gray(seq(1,0, length.out=100))

png(file.path(WD, 'rich_probes_top_percentile_normal_heatmap.png'), width = 600, height = 600)
heatmap.2(as.matrix(topl[['normal']]), 
          col=mycol,
          labRow = '',
          sepcolor='white',
          trace="none",
          Rowv = as.dendrogram(topl_hc_probe),
          Colv = as.dendrogram(topl_hc_sample),
          ## scale="row",
          sepwidth=0.05,  
          main = 'topl normal')

dev.off()

png(file.path(WD, 'rich_probes_top_percentile_tumor_heatmap.png'), width = 600, height = 600)

heatmap.2(as.matrix(topl[['tumor']]),
          ## labCol = colnames(topl[['tumor']][,2:ncol(topl$tumor)]),  
          col=mycol,
          labRow = '',
          sepcolor='white',
          trace="none",
          Rowv = as.dendrogram(topl_hc_probe),
          Colv = as.dendrogram(topl_hc_sample),
          ## scale="row",
          sepwidth=0.05,  
          main = 'topl tumor')
dev.off()




## wtf, imprinted list does not look so imprinted
## what about monk's cpgs?

## test

top$impr_list <- top$x %in% impr_annot$probe
top$rank <- 1: nrow(top)

top$freq 
plot(top$rank, top$freq, col = ifelse(top$impr_list, 'red', 'black'),
     pch = ifelse(top$impr_list, '19', '.'))
