#!/usr/bin/env R
##
## Sankey diagram generator based in google vis
##
## Izaskun Mallona, 12nd june 2017

library(googleVis)

IHOME <- '/imppc/labs/maplab/imallona'
SRC2 <- file.path(IHOME, 'src/correlations/analysis/')

DATA <- file.path(IHOME, 'correlations', 'colonomics_rho_restrictive_communities')
WD <- file.path(IHOME, 'correlations_simpler', 'sankey_communities_google_vis')

dir.create(WD)
setwd(WD)


COAD_NORMAL_FN <- file.path(
    IHOME, 
    'correlations',
    'tcga_rho_restrictive_communities',
    'coad_normal_38_inner_min_degree_1_min_degree_1_min_size_10_min_rho_0.8_positive_only',
    'coad_normal_38_inner_min_degree_1_min_degree_1_min_size_10_min_rho_0.8_positive_only_comm_membership.tsv')

COAD_TUMOR_FN <- file.path(
    IHOME,
    'correlations',
    'tcga_rho_restrictive_communities',
    'coad_inner_min_degree_1_min_degree_1_min_size_10_min_rho_0.8_positive_only',
    'coad_inner_min_degree_1_min_degree_1_min_size_10_min_rho_0.8_positive_only_comm_membership.tsv')

COLONOMICS_NORMAL_FN <- file.path(
    IHOME,
    'correlations',
    'colonomics_rho_restrictive_communities',
    'colonomics_normal_inner_min_degree_1_min_degree_1_min_size_10_min_rho_0.8_positive_only',
    'colonomics_normal_inner_min_degree_1_min_degree_1_min_size_10_min_rho_0.8_positive_only_comm_membership.tsv')

COLONOMICS_TUMOR_FN <- file.path(
    IHOME,
    'correlations',
    'colonomics_rho_restrictive_communities',
    'colonomics_tumor_inner_min_degree_1_min_degree_1_min_size_10_min_rho_0.8_positive_only',
    'colonomics_tumor_inner_min_degree_1_min_degree_1_min_size_10_min_rho_0.8_positive_only_comm_membership.tsv')



## repeating the whole datasets analysis


MEMBERSHIPS <- c(COLONOMICS_TUMOR_FN, COLONOMICS_NORMAL_FN,
                 COAD_NORMAL_FN, COAD_TUMOR_FN)
dat <- list()
for (fn in MEMBERSHIPS) {
    dat[[basename(fn)]] <- read.table(fn)
    dat[[basename(fn)]]$probe <- rownames(dat[[basename(fn)]])
}

str(dat)



## names(dat)

## goo <- data.frame(From = 'colonomics_normal',
##                   To = 'colonomics_tumor',
##                   Weight = length(intersect(dat[[basename(COLONOMICS_NORMAL_FN)]]$probe,
##                       dat[[basename(COLONOMICS_TUMOR_FN)]]$probe)),
##                   stringsAsFactors = FALSE)


## goo <- rbind(goo, c('colonomics_tumor', 'coad_tumor',
##                     length(intersect(dat[[basename(COLONOMICS_TUMOR_FN)]]$probe,
##                                      dat[[basename(COAD_TUMOR_FN)]]$probe))))





## goo <- rbind(goo, c('coad_tumor', 'coad_normal',
##                     length(intersect(dat[[basename(COAD_TUMOR_FN)]]$probe,
##                                      dat[[basename(COAD_NORMAL_FN)]]$probe))))



## goo <- rbind(goo, c('colonomics_normal', 'coad_normal',
##                     length(intersect(dat[[basename(COLONOMICS_NORMAL_FN)]]$probe,
##                                      dat[[basename(COAD_NORMAL_FN)]]$probe))))

## ## goo <- rbind(goo, c( 'else', 'else_colonomics',
## ##                     length(setdiff(dat[[basename(COLONOMICS_NORMAL_FN)]]$probe,
## ##                                    dat[[basename(COLONOMICS_TUMOR_FN)]]$probe))))

## goo$From <- as.factor(goo$From)
## goo$To <- as.factor(goo$To)
## goo$Weight <- as.numeric(goo$Weight)
## Sankey <- gvisSankey(goo, from="From", to="To", weight="Weight",
##                      options=list(
##                        sankey="{link: {color: { fill: '#d799ae' } },
##                             node: { color: { fill: '#a61d4c' },
##                             label: { color: '#871b47' } }}"))

## plot(Sankey)




goo <- data.frame(From = 'colonomics_normal',
                  To = 'colonomics_tumor',
                  Weight = length(intersect(dat[[basename(COLONOMICS_NORMAL_FN)]]$probe,
                      dat[[basename(COLONOMICS_TUMOR_FN)]]$probe)),
                  stringsAsFactors = FALSE)

goo <- rbind(goo, c('colonomics_normal', 'colonomics_setdiff',
                    length(setdiff(dat[[basename(COLONOMICS_NORMAL_FN)]]$probe,
                                     dat[[basename(COLONOMICS_TUMOR_FN)]]$probe))))

goo <- rbind(goo, c('colonomics_tumor', 'coad_tumor',
                    length(intersect(dat[[basename(COLONOMICS_TUMOR_FN)]]$probe,
                                     dat[[basename(COAD_TUMOR_FN)]]$probe))))


goo <- rbind(goo, c('colonomics_tumor', 'tumor_setdiff',
                    length(setdiff(dat[[basename(COLONOMICS_TUMOR_FN)]]$probe,
                                   dat[[basename(COAD_TUMOR_FN)]]$probe))))


goo <- rbind(goo, c('coad_tumor', 'coad_normal',
                    length(intersect(dat[[basename(COAD_TUMOR_FN)]]$probe,
                                     dat[[basename(COAD_NORMAL_FN)]]$probe))))

goo <- rbind(goo, c('coad_tumor', 'coad_setdiff',
                    length(setdiff(dat[[basename(COAD_TUMOR_FN)]]$probe,
                                     dat[[basename(COAD_NORMAL_FN)]]$probe))))

goo <- rbind(goo, c('colonomics_normal', 'coad_normal',
                    length(intersect(dat[[basename(COLONOMICS_NORMAL_FN)]]$probe,
                                     dat[[basename(COAD_NORMAL_FN)]]$probe))))




goo$From <- as.factor(goo$From)
goo$To <- as.factor(goo$To)
goo$Weight <- as.numeric(goo$Weight)
## Sankey <- gvisSankey(goo, from="From", to="To", weight="Weight",
##                      options=list(
##                        sankey="{link: {color: { fill: '#d799ae' } },
##                             node: { color: { fill: '#a61d4c' },
##                             label: { color: '#871b47' } }}"))

## plot(Sankey)

Sankey <- gvisSankey(goo, from="From", to="To", weight="Weight",
                     options=list(
                         width = 800,
                         height = 600,
                       sankey="{link: {color: { fill: '#6fa1ba' } },
                            node: { color: { fill: '#a61d4c' },
                            label: { fontSize:14, color: '#000000' } }}"))


plot(Sankey)

Sankey <- gvisSankey(goo, from="From", to="To", weight="Weight",
                     options=list(
                         width = 1400,
                         height = 600,
                       sankey="{link: {color: { fill: '#6fa1ba' } },
                            node: { color: { fill: '#a61d4c' },
                            label: { fontSize:14, color: '#000000' } }}"))


plot(Sankey)

## ## pool of elses

## universe <- unlist(sapply(dat, function(x) return(x$probe)))
## ft <- as.data.frame(table(universe))
## table(ft$Freq)
## universe <- as.character(ft[ft$Freq>1, 'universe'])
## ## universe <- unique(universe)


## goo <- data.frame(From = 'colonomics_normal',
##                   To = 'colonomics_tumor',
##                   Weight = length(intersect(dat[[basename(COLONOMICS_NORMAL_FN)]]$probe,
##                       dat[[basename(COLONOMICS_TUMOR_FN)]]$probe)),
##                   stringsAsFactors = FALSE)

## goo <- rbind(goo, c('colonomics_normal', 'else',                    
##                     length(setdiff(universe, dat[[basename(COLONOMICS_NORMAL_FN)]]$probe))))

## goo <- rbind(goo, c('colonomics_tumor', 'coad_tumor',
##                     length(intersect(dat[[basename(COLONOMICS_TUMOR_FN)]]$probe,
##                                      dat[[basename(COAD_TUMOR_FN)]]$probe))))


## goo <- rbind(goo, c('colonomics_tumor', 'else',
##                     length(setdiff(universe,
##                                    dat[[basename(COLONOMICS_TUMOR_FN)]]$probe))))


## goo <- rbind(goo, c('coad_tumor', 'coad_normal',
##                     length(intersect(dat[[basename(COAD_TUMOR_FN)]]$probe,
##                                      dat[[basename(COAD_NORMAL_FN)]]$probe))))

## goo <- rbind(goo, c('coad_tumor', 'else',
##                     length(setdiff(universe, dat[[basename(COAD_TUMOR_FN)]]$probe))))

## goo <- rbind(goo, c('colonomics_normal', 'coad_normal',
##                     length(intersect(dat[[basename(COLONOMICS_NORMAL_FN)]]$probe,
##                                      dat[[basename(COAD_NORMAL_FN)]]$probe))))

## goo <- rbind(goo, c('coad_normal', 'else',
##                     length(setdiff(universe, dat[[basename(COAD_NORMAL_FN)]]$probe))))

## goo <- rbind(goo, c('colonomics_normal', 'coad_tumor',
##                     length(intersect(dat[[basename(COLONOMICS_NORMAL_FN)]]$probe,
##                                      dat[[basename(COAD_TUMOR_FN)]]$probe))))

                    

## goo$From <- as.factor(goo$From)
## goo$To <- as.factor(goo$To)
## goo$Weight <- as.numeric(goo$Weight)
## Sankey <- gvisSankey(goo, from="From", to="To", weight="Weight",
##                      options=list(
##                        sankey="{link: {color: { fill: '#d799ae' } },
##                             node: { color: { fill: '#a61d4c' },
##                             label: { color: '#871b47' } }}"))

## plot(Sankey)

