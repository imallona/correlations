#!/bin/bash

# echo 'Retrieve clinical data'

# url="https://tcga-data.nci.nih.gov/tcga/damws/jobprocess/xml?&disease=COAD&platformType=-999&preservationMethodCode=Frozen&protectedStatus=N&sampleList=%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%2C"

# echo "$url"

# echo 'Saved at'

# wd="$IHOME"/correlations/clinical_data_tcga/Clinical/Biotab/

# echo $wd

# CIMP data can be conveniently fetched from cbioportal by selecting the
#  Nature 2012 paper on coadread

# http://www.cbioportal.org/study.do?cancer_study_id=coadread_tcga_pub#summary

wd="$IHOME"/correlations/clinical_data_cbioportal/coadread_tcga_pub_clinical_data.txt

# Beware, the overlap with the tcga coadread correlations samples is small. Rather
#  use the provisional release (though no cimp is available)

# http://www.cbioportal.org/study.do?cancer_study_id=coadread_tcga

wd="$IHOME"/correlations/clinical_data_cbioportal/coadread_tcga_clinical_data.txt
