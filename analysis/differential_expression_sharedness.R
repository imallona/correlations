#!/usr/bin/env/R
##
## are there community patterns on diff expression?
##
## Izaskun Mallona
## 12nd dec 2017

TASK ='differential_expression_sharedness'
HOME = '/home/labs/maplab/imallona'
IHOME = '/imppc/labs/maplab/imallona'
WD = file.path(IHOME, 'correlations_simpler', TASK)
DATA_TCGA <- file.path(IHOME, 'correlations_purity', 'tcga_expression_gdc', 'out')
DATA_CLX <-  file.path(IHOME, 'correlations_purity', 'clues_knn_expression', 'out')

TRANS <- c(1, 2, 3, 4, 5, 8, 12, 13, 26, 32, 41, 46, 47, 64, 71, 83, 120, 152, 174,
           184, 190, 192, 198, 218, 270, 276, 322, 327, 598, 2352, 2992, 3237)


dir.create(WD)
setwd(WD)

fns_tcga <- file.path(DATA_TCGA, list.files(DATA_TCGA, pattern = 'clues*',
                                            recursive = TRUE))
fns_clx <-  file.path(DATA_CLX, list.files(DATA_CLX, pattern = '*significant*'))

tcga <- list()
for (fn in fns_tcga) {
    tcga[[basename(fn)]] <- read.csv(fn, stringsAsFactors = FALSE)
    tcga[[basename(fn)]] <- data.frame(gene_symbol = tcga[[basename(fn)]]$external_gene_name,
                                       log2fc = tcga[[basename(fn)]]$log2FoldChange)

}

clx <- list()
for (fn in fns_clx) {
    clx[[basename(fn)]] <- read.csv(fn, sep = " ", stringsAsFactors = FALSE)
    clx[[basename(fn)]] <- data.frame(gene_symbol = clx[[basename(fn)]]$gene.symbol,
                                      log2fc = clx[[basename(fn)]]$logFC)
}

head(tcga[[1]])
head(clx[[1]])

corpus <- unique(c(as.character(do.call(rbind.data.frame, tcga)[,1]),
                   as.character(do.call(rbind.data.frame, clx)[,1])))

length(corpus)


stop('extend this to non 1 vs 2 comparisons, there is a case!')

d <- data.frame(gene_symbol = sort(corpus), stringsAsFactors = FALSE)
rownames(d) <- d$gene_symbol
head(d)

for (module in TRANS) {
    for (dataset in c('tcga', 'clx')) {
        if (dataset == 'tcga')
            tag <- sprintf('clues_membershipcomm_%s_cluster_1__vs__clues_membershipcomm_%s_cluster_2differentially_expressed_genes.csv', module, module)
        else if (dataset == 'clx')
            tag <- sprintf('limma_%s_significant_comm_1_cluster_1-comm_%s_cluster_2.tsv',
                           module, module)

        short_tag <- sprintf('%s_%s', dataset,  module)
        d[,short_tag] <- 0
        curr <- get(dataset)[[tag]]

        if (!is.null(curr) && nrow(curr) > 0) {
            for (i in 1:nrow(curr)) {
                symbol <- as.character(curr[i ,'gene_symbol'])
                
                if (!is.na(symbol) && !is.na(curr[i, 'log2fc']))
                    d[symbol, short_tag] <- curr[i, 'log2fc']
            }
        }
    }
}

head(d)
colnames(d)

rownames(d)
d[1,]
library(pheatmap)

## pheatmap(d,
##          cluster_rows = FALSE,
##          show_rownames = TRUE,
##          cluster_cols = FALSE)
## ## , annotation_col=df)


library(gplots)

d <- d[,-1]

head(d)
## heatmap.2(as.matrix(d),
##           Colv = FALSE, 
##           dendrogram = "none",
##           Rowv = FALSE,
##           trace = 'none')
##           ## col = gray.colors)
##           ## cellnote = sapply(as.data.frame(infinium_norm), function(x) sprintf('%.2e', x)),
##           ## cellnote = sprintf('%.2e', baz),
##           ## cellnote = baz,
##           ## cellnote = round(infinium_norm, 2),
##           ## notecol = "green")


table(apply(d, 1, function(x) all(is.na(x))))
MAX <- max(abs(min(d)), max(d))

breaks <- c(seq(
    from = -MAX,
    to = MAX,
    length.out = 50))

palette <- colorRampPalette(c("red", "white", "green"))(length(breaks)-1)

heatmap.2(head(as.matrix(d), 300),
          Colv = TRUE, 
          dendrogram = "none",
          Rowv = TRUE,
          col = palette,
          breaks = breaks,
          trace = 'none')


breaks <- c(-MAX, -0.001, 0.001, MAX)
    
palette <- c("darkred", "white", "darkgreen")

heatmap.2(head(as.matrix(d[,grep('tcga', colnames(d))]), 700),
          Colv = TRUE, 
          dendrogram = "both",
          hclustfun = function(x) hclust(x,method = 'ward.D2'),
          Rowv = TRUE,
          col = palette,
          breaks = breaks,
          trace = 'none')

heatmap.2(head(as.matrix(d), 700),
          Colv = TRUE, 
          dendrogram = "both",
          hclustfun = function(x) hclust(x,method = 'ward.D2'),
          Rowv = TRUE,
          col = palette,
          breaks = breaks,
          trace = 'none')

png('test_all_tcga.png', width = 1500, height = 1500)
heatmap.2(as.matrix(d[,grep('tcga', colnames(d))]),
          Colv = TRUE, 
          dendrogram = "both",
          hclustfun = function(x) hclust(x, method = 'ward.D2'),
          Rowv = TRUE,
          col = palette,
          breaks = breaks,
          trace = 'none')
dev.off()

png('test_all.png', width = 1500, height = 1500)
heatmap.2(as.matrix(d),
          Colv = TRUE, 
          dendrogram = "both",
          hclustfun = function(x) hclust(x, method = 'ward.D2'),
          Rowv = TRUE,
          col = palette,
          breaks = breaks,
          trace = 'none')
dev.off()
## col = gray.colors)
## cellnote = sapply(as.data.frame(infinium_norm), function(x) sprintf('%.2e', x)),
## cellnote = sprintf('%.2e', baz),
## cellnote = baz,
## cellnote = round(infinium_norm, 2),
## notecol = "green")
