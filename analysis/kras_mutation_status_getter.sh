#!/bin/bash
##
## Retrieves the coadread clinical annotations from TCGA and looks
## for kras mutation
##
## Writes to STDOUT a matrix like
# TCGA-F4-6854	NO
# TCGA-AA-3525	NA
# TCGA-AZ-4681	NA
# TCGA-AD-A5EK	NA
# TCGA-D5-6529	NA
# TCGA-AA-A02R	NA
# TCGA-DM-A288	YES
# TCGA-AA-3692	NA
# TCGA-AA-3552	NA
##
## Being NA: Not available, YES: KRAS mutated; NO: KRAS no mutated
##
## Izaskun Mallona, GPLv2
## 24th march 2016

OD=~/kras_tcga

mkdir -p $OD

cd $OD

echo 'Query for clinical data'
# https://tcga-data.nci.nih.gov/tcga/damws/jobprocess/xml?&disease=COAD&platformType=-999&preservationMethodCode=Frozen&protectedStatus=N&sampleList=%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%2C

# tar xvf 46b7dffc-63d7-48cb-b5cc-b4d0c17abc43.tar

ln -s "$IHOME"/correlations/msi_categorization/Clinical/

files=$(find -L Clinical -name "*clinical*xml")

regex_id='.*<shared:bcr_patient_barcode preferred_name.*>(TCGA-.*{12})</shared:bcr_patient_barcode>'
# regex_msi='.*<coad_read_shared:microsatellite_instability preferred_name="microsatellite_instability".*procurement_status="(.*{10,13}).*" restricted="false" source_system_identifier="[0-9]+"[ />]{1,3}([YES|NO]{0,3}).*'
# regex_mutation='.*<coad_read_shared:kras_mutation_found preferred_name="kras_mutation_found" display_order="53" cde="2932340" cde_ver="1.000" xsd_ver="2.3" tier="2" owner="TSS" procurement_status="Completed" restricted="false" source_system_identifier="1110797">(.*)</coad_read_shared:kras_mutation_found>.*'

:<<EOF
<coad_read_shared:kras_mutation_found preferred_name="kras_mutation_found" display_order="53" cde="2932340" cde_ver="1.000" xsd_ver="2.3" tier="2" owner="TSS" procurement_status="Completed" restricted="false" source_system_identifier="1082013">YES</coad_read_shared:kras_mutation_found>
<coad_read_shared:kras_mutation_found preferred_name="kras_mutation_found" display_order="53" cde="2932340" cde_ver="1.000" xsd_ver="2.3" tier="2" owner="TSS" procurement_status="Completed" restricted="false" source_system_identifier="922726">YES</coad_read_shared:kras_mutation_found>
EOF


regex_mutation='.*<coad_read_shared:kras_mutation_found preferred_name="kras_mutation_found" display_order="53" cde="2932340" cde_ver="1.000" xsd_ver="2.3" tier="2" owner="TSS" procurement_status="Completed" restricted="false" source_system_identifier=(.*)>(.*)</coad_read_shared:kras_mutation_found>.*'

printf "%s\t%s\n" "sample_id" "kras_mutation"

for f in $files
do
    mutation_status=NA
    sample_id=""
    while read line
    do

        if [[ $line =~ $regex_id ]]
        then
            sample_id="${BASH_REMATCH[1]}"
        else
            
            if [[ $line =~ $regex_mutation ]]
            then
                # echo "${BASH_REMATCH[0]}"
                # completed="${BASH_REMATCH[1]}"
                mutation_status="${BASH_REMATCH[2]}"

                ## that is, was non tested
                if [ -z "$mutation_status" ]
                then
                    mutation_status=NA
                fi                                
            fi
        fi
        
    done < "$f"
    printf "%s\t%s\n" "$sample_id" "$mutation_status"
done

