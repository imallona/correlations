#!/usr/bin/env/R
##
## are CNV driving antimeth modules?
##
##
## Izaskun Mallona
## 05 sept 2018

## library(pheatmap)
library(ggplot2)
library(Cairo)
library(viridis)
library(corrplot)

TASK ='cnv_technical_opposite_ranks'
HOME = '/home/labs/maplab/imallona'
IHOME = '/imppc/labs/maplab/imallona'
WD = file.path(IHOME, 'correlations_simpler', TASK)
NFS_WD = file.path(IHOME, 'correlations_simpler', TASK)

SCHEMA <- 'colonomics_tumor'  ## schema the module detection was carried out with
INNER_MIN_DEGREE <- 1         ## first prunning (in-chromosome prunning)
MIN_DEGREE <- 1               ## vertex min degree
MIN_SIZE <- 10                ## community min size
MIN_RHO <- 0.8                ## minimum rho coefficient value to fetch the correlation
LABEL <- sprintf('%s_inner_min_degree_%s_min_degree_%s_min_size_%s_min_rho_%s_positive_only',
                 SCHEMA, INNER_MIN_DEGREE, MIN_DEGREE, MIN_SIZE, MIN_RHO)

## trans-communities as produced by trans_communities_subsetter.R
MEMBERSHIP <- file.path(IHOME,
                        'correlations',
                        'colonomics_rho_restrictive_communities',
                        LABEL,
                        sprintf('%s_comm_membership.tsv', LABEL))

                                        # trans communities
TRANS <- c(1, 2, 3, 4, 5, 8, 12, 13, 26, 32, 41, 46, 47, 64, 71, 83, 120, 152, 174,
           184, 190, 192, 198, 218, 270, 276, 322, 327, 598, 2352, 2992, 3237)

beta2m <- function(beta) {
    m <- log2(beta/(1 - beta))
    return(m)
}

m2beta <- function(m) {
    beta <- 2^m/(2^m + 1)
    return(beta)
}

##

dir.create(WD)
dir.create(NFS_WD)

setwd(WD)

partition <- read.table(MEMBERSHIP, header = TRUE)
partition$probe <- row.names(partition)

load(file.path(IHOME, 'data', 'colonomics', 'methylation', 'betas-filtQC-140425.RData'))

load(file.path(IHOME, 'data', 'colonomics', 'cnv', 'CLX.RData'))

cnv <- minimal
rownames(cnv) <- sprintf('chr%s:%s-%s', minimal$Chromosome, minimal$Start, minimal$End)
colnames(cnv)[4:ncol(cnv)] <- paste0(colnames(cnv)[4:ncol(cnv)], '_T')

meth <- betas[,grep('.*_T$', colnames(betas), perl = TRUE)]

common <- intersect(colnames(meth), colnames(cnv))

## getting overlaps etc using bedtools

write.table(file = file.path(WD, 'mrr.bed'),
            x = data.frame(chr = sprintf('chr%s', cnv$Chromosome),
                           start = as.character(cnv$Start),
                           end = as.character(cnv$End),
                           id = rownames(cnv)),
            sep = '\t', col.names = FALSE, row.names = FALSE,
            quote = FALSE)


trans <- partition[partition$x %in% TRANS, 'probe',]
trans_loci <- a450cpg[trans, c('CHR', 'MAPINFO', 'MAPINFO', 'IlmnID')]
trans_loci$MAPINFO.1 <- trans_loci$MAPINFO.1 + 1

write.table(file = file.path(WD, 'probes.bed'),
            x = data.frame(chr = sprintf('chr%s', trans_loci$CHR),
                           start = as.character(trans_loci$MAPINFO),
                           end = as.character(trans_loci$MAPINFO.1),
                           id = rownames(trans_loci)),
            sep = '\t', col.names = FALSE, row.names = FALSE,
            quote = FALSE)

system('sort -k1,1 -k2,2n probes.bed > probes.sorted.bed')
system('sort -k1,1 -k2,2n mrr.bed > mrr.sorted.bed')

system('bedtools closest -d -t first -a probes.sorted.bed -b mrr.sorted.bed > probes_closest_mrr.bed')
system('wc -l *bed')
  ## 13279 mrr.bed
  ## 13279 mrr.sorted.bed
  ## 46906 probes.bed
  ## 46906 probes.sorted.bed
  ## 46906 probes_closest_mrr.bed


closest <- read.table(file.path(WD, 'probes_closest_mrr.bed'), header = FALSE)
dim(closest)

colnames(closest) <- c('cchr', 'cstart', 'cend', 'probe',
                       'mchr', 'mstart', 'mend', 'mrr',
                       'dist')

closest <- merge(closest, partition, by = 'probe', all.x = TRUE)
dim(closest)


## number of CpGs on top of an MRR
mrr_counts <- data.frame(module = as.character(TRANS),
                         overlap_mrr = NA,
                         dont_overlap_mrr = NA,
                         row.names = as.character(TRANS))

for (module in TRANS) {
    module <- as.character(module)
    curr <- closest[closest$x == module,]
    mrr_counts[module, 'overlap_mrr'] <- sum(curr$dist == 0)
    mrr_counts[module, 'dont_overlap_mrr'] <- sum(curr$dist != 0)
    stopifnot( (mrr_counts[module, 'overlap_mrr'] +  mrr_counts[module, 'dont_overlap_mrr']) ==
               nrow(partition[partition$x == module,]))
    
}

mrr_counts$proportion <- mrr_counts$overlap_mrr / (mrr_counts$overlap_mrr +
                                                   mrr_counts$dont_overlap_mrr)

write.csv(x = mrr_counts,
          file = file.path(WD, 'trans_cpgs_overlapping_mrrs_summary.csv'),
          row.names = FALSE)

## plotting the rainplots for ranks sorting




## chunk from sample_ranks_across_modules.R start



meth <- list(n = betas[,grep('.*_N$', colnames(betas), perl = TRUE)]/1000,
             t = betas[,grep('.*_T$', colnames(betas), perl = TRUE)]/1000)

common <- sort(intersect(strtrim(colnames(meth$n), width = 5),
                         strtrim(colnames(meth$t), width = 5)))

meth$t <- meth$t[,paste0(common, '_T')]
meth$n <- meth$n[,paste0(common, '_N')]



set.seed(1)

stratifiers <- NULL
for (community in TRANS) {
    curr <- sample(partition[partition$x == community, 'probe'], 1)
    stratifiers <- c(stratifiers, curr)
}

names(stratifiers) <- TRANS



d <- list()
draw <- list()
for (item in c('n', 't')) {

    m <- matrix(nrow = length(stratifiers), ncol = ncol(meth[[item]]), data = NA)
    rownames(m) <- names(stratifiers)
    colnames(m) <- colnames(meth[[item]])

    for (a in names(stratifiers)) {
        m[a,] <- meth[[item]][stratifiers[[a]],] 
    }

    M <- cor(t(m), method = 'spearman')

    ## res1 <- cor.mtest(m, conf.level = 0.95, method = 'spearman')

    ## corrplot(M, p.mat = res1, type = 'lower', method = 'ellipse',
    ##          addCoef.col = 'black', sig.level = 0.05)

    col <- colorRampPalette(c("darkred", "white", "blue"))
    pal <- col(20)

    pal[6:15] <- 'white'

    png(file.path(WD, sprintf('corrplot_stratifiers_%s.png', item)),
        width = 1100, height = 1100)
    corrplot(M,  type = 'lower', method = 'ellipse',
             addCoef.col = 'black', col = pal)
    dev.off()

    pdf(file.path(WD, sprintf('corrplot_stratifiers_%s.pdf', item)) , width = 16, height = 16)
    corrplot(M,  type = 'lower', method = 'ellipse',
             addCoef.col = 'black', col = pal)
    dev.off()
    
    d[[item]] <- M
    draw[[item]] <- m
}







get_sorted_patients <-function(community, stratifier_probe, a450cpg, num_patients, chr) {
    probes <- partition[partition$x == community, 'probe']

    annot <- a450cpg[probes,]
    annot <- annot[annot$CHR == chr,]
    annot <- annot[order(annot$CHR, annot$MAPINFO),]

    curr_meth <- list(t = t(meth$t[rownames(annot),]),
                      n = t(meth$n[rownames(annot),]))

    stratifier <-  list(t = t(meth$t[stratifier_probe,]),
                        n = t(meth$n[stratifier_probe,]))


    ranks <- rank(t(meth$t[stratifier_probe,]))
    ranks2 <- colnames(t(meth$t[stratifier_probe,]))
    names(ranks) <- ranks2
    
    ## selected_samples_t <-  names(sort(ranks)[seq(from = 1, to = length(ranks),
    ##                               length.out = num_patients)])

    ## selected_samples_n <- sprintf('%s_N', strtrim(selected_samples_t, 5))

    selected_samples <-  strtrim(names(sort(ranks)[seq(from = 1, to = length(ranks),
                                                       length.out = num_patients)]), 5)

    selected_samples_t <- sprintf('%s_T', selected_samples)
    selected_samples_n <- sprintf('%s_N', selected_samples)

    meth_t <- t(curr_meth$t[selected_samples_t,])
    colnames(meth_t) <- strtrim(colnames(meth_t), 5)

    meth_n <- t(curr_meth$n[selected_samples_n,])
    colnames(meth_n) <- strtrim(colnames(meth_n), 5)
    
    return(list(selected_samples = selected_samples,
                meth_t = meth_t,
                meth_n = meth_n))
    
}


add.alpha <- function(col, alpha=1){
    apply(sapply(col, col2rgb)/255, 2, 
          function(x) 
              rgb(x[1], x[2], x[3], alpha=alpha))  
}

for (num_patients in c(10,20,92,96)) {

    for (stratifier in as.character(stratifiers[1])) {
        chr <- '17'
        png(file.path(WD, sprintf('chr%s_mounted_%s_%s_patients.png', chr, stratifier, num_patients)),
            width = 800, height = 500)
        par(mfrow = c(2,2),
            oma = c(5,4,0,0) + 0.1,
            mar = c(1.4,1.4,1.4,1.4) + 0.1,
            cex.axis = 1.5, cex.lab = 1.5)
        
        d <- list()

        for (mod in c('1', '2')) {
            tmp <- get_sorted_patients(community = mod, stratifier_probe = stratifier,
                                       a450cpg = a450cpg,
                                       num_patients = num_patients,
                                       chr = chr)

            d[[mod]] <- tmp
        }

        for (mod in c('1', '2')) {
            for (type in c('meth_t', 'meth_n')) {
                palette <- viridis::viridis(num_patients)
                
                plot(y = d[[mod]][[type]][,1],
                     x = 1:nrow(d[[mod]][[type]]),
                     ylim = c(0,1.1),
                     xlim = c(min(a450cpg[a450cpg$CHR == chr, 'MAPINFO']),
                     (max(a450cpg[a450cpg$CHR == chr, 'MAPINFO']))),
                     type = 'n')
                
                ## legend('topleft', sprintf('module %s', mod),
                ##        bty = 'n', cex = 1.5)
                legend('topleft',
                       sprintf('module %s (%s)', mod,
                               ifelse(type == 'meth_t', yes = 'tumor', no = 'normal')),
                       bty = 'n', cex = 1.5)

                i <- 1
                for (ss in d[[mod]][['selected_samples']]) {
                    dat <- d[[mod]][[type]][,ss]
                    
                    lines(x = a450cpg[names(dat), 'MAPINFO'],
                          y = dat,
                          lty="solid",
                          type = 'p',
                          pch = 20,
                          cex = 0.5,
                          col= palette[i],
                          ## col= 'black',
                          lwd = 1)
                    i <- i + 1
                }
                
                ## legend('topright', title = stratifier,
                ##        ## col = palette,
                ##        col = colorRampPalette(c("blue","black", "red"))(num_patients),
                ##        d[[mod]]$selected_samples,
                ##        cex = 1.2,
                ##        pch = 19,
                ##        ncol = 2)

            }

            title( ylab = 'DNA methylation (beta)',
                  xlab = sprintf('position along chr%s', chr),
                  outer = TRUE, line = 1)

            
        }
        dev.off()

        ## legend now

        pdf(file.path(WD, sprintf('chr%s_mounted_%s_%s_legend.pdf', chr, stratifier, num_patients)),
            width = 7, height = 7)

        plot(y = d[[mod]][[type]][,1],
             x = 1:nrow(d[[mod]][[type]]),
             ylim = c(0,1.1),
             xlim = c(min(a450cpg[a450cpg$CHR == chr, 'MAPINFO']),
             (max(a450cpg[a450cpg$CHR == chr, 'MAPINFO']))),
             type = 'n')

        legend('topright', title = stratifier,
               ## col = palette,
               ## col = colorRampPalette(c("blue","darkgreen", "red"))(num_patients),
               col = palette,
               d[[mod]]$selected_samples,
               cex = 1.2,
               pch = 19,
               ncol = 2)
        dev.off()
        
    }
}



## now getting only those outside CNVs



for (num_patients in c(10,20,92,96)) {

    for (stratifier in as.character(stratifiers[1])) {
        chr <- '17'
        png(file.path(WD, sprintf('chr%s_mounted_%s_%s_patients_outside_mrrs.png', chr, stratifier, num_patients)),
            width = 800, height = 500)
        par(mfrow = c(2,2),
            oma = c(5,4,0,0) + 0.1,
            mar = c(1.4,1.4,1.4,1.4) + 0.1,
            cex.axis = 1.5, cex.lab = 1.5)
        
        d <- list()

        for (mod in c('1', '2')) {
            tmp <- get_sorted_patients(community = mod, stratifier_probe = stratifier,
                                       a450cpg = a450cpg,
                                       num_patients = num_patients,
                                       chr = chr)

            d[[mod]] <- tmp
        

            ## filtering for CpGs outside CNVs
            for (item in c('meth_t', 'meth_n')) {
                probes_outside_mrr <- as.character(closest[closest$x == mod &
                                                           closest$dist != 0 &
                                                           closest$cchr == 'chr17',
                                                           'probe'])

                d[[mod]][[item]] <- d[[mod]][[item]][probes_outside_mrr,]
            }

        }
        
        for (mod in c('1', '2')) {
            for (type in c('meth_t', 'meth_n')) {
                palette <- viridis::viridis(num_patients)
                
                plot(y = d[[mod]][[type]][,1],
                     x = 1:nrow(d[[mod]][[type]]),
                     ylim = c(0,1.1),
                     xlim = c(min(a450cpg[a450cpg$CHR == chr, 'MAPINFO']),
                     (max(a450cpg[a450cpg$CHR == chr, 'MAPINFO']))),
                     type = 'n')
                
                ## legend('topleft', sprintf('module %s', mod),
                ##        bty = 'n', cex = 1.5)
                legend('topleft',
                       sprintf('module %s (%s)', mod,
                               ifelse(type == 'meth_t', yes = 'tumor', no = 'normal')),
                       bty = 'n', cex = 1.5)

                i <- 1
                for (ss in d[[mod]][['selected_samples']]) {
                    dat <- d[[mod]][[type]][,ss]
                    
                    lines(x = a450cpg[names(dat), 'MAPINFO'],
                          y = dat,
                          lty="solid",
                          type = 'p',
                          pch = 20,
                          cex = 0.5,
                          col= palette[i],
                          ## col= 'black',
                          lwd = 1)
                    i <- i + 1
                }
                
                ## legend('topright', title = stratifier,
                ##        ## col = palette,
                ##        col = colorRampPalette(c("blue","black", "red"))(num_patients),
                ##        d[[mod]]$selected_samples,
                ##        cex = 1.2,
                ##        pch = 19,
                ##        ncol = 2)

            }

            title( ylab = 'DNA methylation (beta)',
                  xlab = sprintf('position along chr%s', chr),
                  outer = TRUE, line = 1)

            
        }
        dev.off()

        ## legend now

        pdf(file.path(WD, sprintf('chr%s_mounted_%s_%s_legend_outside_mrrs.pdf', chr, stratifier, num_patients)),
            width = 7, height = 7)

        plot(y = d[[mod]][[type]][,1],
             x = 1:nrow(d[[mod]][[type]]),
             ylim = c(0,1.1),
             xlim = c(min(a450cpg[a450cpg$CHR == chr, 'MAPINFO']),
             (max(a450cpg[a450cpg$CHR == chr, 'MAPINFO']))),
             type = 'n')

        legend('topright', title = stratifier,
               ## col = palette,
               ## col = colorRampPalette(c("blue","darkgreen", "red"))(num_patients),
               col = palette,
               d[[mod]]$selected_samples,
               cex = 1.2,
               pch = 19,
               ncol = 2)
        dev.off()
        
    }
}




## chunk from sample_ranks_across_modules.R end


## adding horizontal lines depicting gains and losses



for (num_patients in c(10,20,92,96)) {

    for (stratifier in as.character(stratifiers[1])) {
        for (chr in 1:22){            
            png(file.path(WD, sprintf('chr%s_mounted_%s_%s_patients_highlighted_mrrs.png', chr, stratifier, num_patients)),
            width = 800, height = 500)
            par(mfrow = c(2,1),
                oma = c(5,4,0,0) + 0.1,
                mar = c(1.4,1.4,1.4,1.4) + 0.1,
                cex.axis = 1.5, cex.lab = 1.5)
            
            d <- list()

            for (mod in c('1', '2')) {
                tmp <- get_sorted_patients(community = mod, stratifier_probe = stratifier,
                                           a450cpg = a450cpg,
                                           num_patients = num_patients,
                                           chr = chr)

                d[[mod]] <- tmp
                

                ## ## filtering for CpGs outside CNVs
                ## for (item in c('meth_t', 'meth_n')) {
                ##     probes_outside_mrr <- as.character(closest[closest$x == mod &
                ##                                                closest$dist != 0 &
                ##                                                closest$cchr == 'chr17',
                ##                                                'probe'])

                ##     d[[mod]][[item]] <- d[[mod]][[item]][probes_outside_mrr,]
                ## }

            }
            
            for (mod in c('1', '2')) {
                for (type in c('meth_t')) {
                    palette <- viridis::viridis(num_patients)
                    
                    plot(y = d[[mod]][[type]][,1],
                         x = 1:nrow(d[[mod]][[type]]),
                         ylim = c(0,1.1),
                         xlim = c(min(a450cpg[a450cpg$CHR == chr, 'MAPINFO']),
                             (max(a450cpg[a450cpg$CHR == chr, 'MAPINFO']))),
                         type = 'n')
                    
                    ## legend('topleft', sprintf('module %s', mod),
                    ##        bty = 'n', cex = 1.5)
                    legend('topleft',
                           sprintf('module %s (%s)', mod,
                                   ifelse(type == 'meth_t', yes = 'tumor', no = 'normal')),
                           bty = 'n', cex = 1.5)

                    i <- 1
                    for (ss in d[[mod]][['selected_samples']]) {
                        dat <- d[[mod]][[type]][,ss]
                        
                        lines(x = a450cpg[names(dat), 'MAPINFO'],
                              y = dat,
                              lty="solid",
                              type = 'p',
                              pch = 20,
                              cex = 0.5,
                              col= palette[i],
                              ## col= 'black',
                              lwd = 1)
                        i <- i + 1
                    }


                    ## mrr segments start
                    for (mrr in unique(closest$mrr)) {
                        curr <- closest[closest$mrr == mrr, ][1,]
                        if (curr$mchr == sprintf('chr%s', chr)) {
                            segments(x0 = curr$mstart,
                                     x1 = curr$mend,
                                     y0 = 0,
                                     y1 = 0,
                                     lwd = 10,
                                     col = 'darkred')
                        }
                    }
                    ## mrr segments end
                    
                    ## legend('topright', title = stratifier,
                    ##        ## col = palette,
                    ##        col = colorRampPalette(c("blue","black", "red"))(num_patients),
                    ##        d[[mod]]$selected_samples,
                    ##        cex = 1.2,
                    ##        pch = 19,
                    ##        ncol = 2)

                }

                title( ylab = 'DNA methylation (beta)',
                      xlab = sprintf('position along chr%s', chr),
                      outer = TRUE, line = 1)

                
            }
            dev.off()

            
        }
    }
}



## now getting 10 patients at random and highlighting whether they have a gain/loss

## num_patients <- 10

## stratifier <-  as.character(stratifiers[1])
## for (chr in 1:22){            
##     ## png(file.path(WD, sprintf('chr%s_mounted_%s_%s_patients_highlighted_mrrs.png', chr, stratifier, num_patients)),
##     ##     width = 800, height = 500)
##     ## par(mfrow = c(2,1),
##     ##     oma = c(5,4,0,0) + 0.1,
##     ##     mar = c(1.4,1.4,1.4,1.4) + 0.1,
##     ##     cex.axis = 1.5, cex.lab = 1.5)
    
##     d <- list()

##     for (mod in c('1', '2')) {
##         tmp <- get_sorted_patients(community = mod, stratifier_probe = stratifier,
##                                    a450cpg = a450cpg,
##                                    num_patients = num_patients,
##                                    chr = chr)

        

##         ##  ## getting status gain/loss etc
##         ##     probes <- rownames(tmp$meth_t)
##         ##     rownames(closest) <- closest$probe
##         ##     closest_mrr <- as.character(closest[probes, 'mrr'])
##         ##     overlaps_bool <- closest[probes, 'dist'] == 0
##         ##     closest_mrr[!overlaps_bool] <- NA


##         ## }

##         ## preparing data for peinado start

##         tmp$meth_t <- as.data.frame(tmp$meth_t)
##         colnames(tmp$meth_t) <- sprintf('meth_tumor_%s', colnames(tmp$meth_t))
##         tmp$meth_t$probe <- rownames(tmp$meth_t)
##         closest$probe <- as.character(closest$probe)
##         tmp <- merge(tmp$meth_t, closest, by.x = 'probe', by.y = 'probe', all.x = TRUE)
##         colnames(tmp)[19:20] <- c('distance_cpg_t_mrr', 'cometh_module')

##         cnv_bak <- cnv
##         colnames(cnv_bak) <- paste0(colnames(cnv_bak), '_cnv')
##         cnv_bak$mrr <- rownames(cnv_bak)
##         patients <- intersect(paste0(gsub('meth_tumor_', '', colnames(tmp)), '_T_cnv'),
##                               colnames(cnv_bak))
##         cnv_bak <- cnv_bak[,c( patients, 'mrr')]

##         tmp <- merge(tmp, cnv_bak, by.x = 'mrr', by.y = 'mrr', all.x = TRUE)
        
##         d[[mod]] <- tmp
##     }
    
##     write.csv(file.path(WD, sprintf('peinado_plot_cnv_meth_chr%s.csv', chr)),
##               x = rbind(d[[1]], d[[2]]),
##               row.names = FALSE,
##               quote = FALSE)

##     type <- 'meth_t'

##     par(mfrow = c(10,2),
##         oma = c(5,4,0,0) + 0.1,
##         mar = c(1.4,1.4,1.4,1.4) + 0.1,
##         cex.axis = 1.5, cex.lab = 1.5)

##     for (mod in c(1,2)) {
##         ## plot start
##         ## plot(y = d[[mod]][,1],
##         ##      x = 1:nrow(d[[mod]]),
##         ##      ylim = c(0,1.1),
##         ##      xlim = c(min(a450cpg[a450cpg$CHR == chr, 'MAPINFO']),
##         ##      (max(a450cpg[a450cpg$CHR == chr, 'MAPINFO']))),
##         ##      type = 'n')

##         ## legend('topleft',
##         ##        sprintf('module %s (%s)', mod,
##         ##                ifelse(type == 'meth_t', yes = 'tumor', no = 'normal')),
##         ##        bty = 'n', cex = 1.5)

##         i <- 1
##         for (ss in grep('meth_tumor_', colnames(d[[mod]]), value =  TRUE)) {

##             plot(y = d[[mod]][,1],
##              x = 1:nrow(d[[mod]]),
##              ylim = c(0,1.1),
##              xlim = c(min(a450cpg[a450cpg$CHR == chr, 'MAPINFO']),
##              (max(a450cpg[a450cpg$CHR == chr, 'MAPINFO']))),
##              type = 'n')

            
##             cnv_sample <- sprintf('%s_T_cnv', gsub('meth_tumor_', '', ss))
            
##             row.names(d[[mod]]) <- d[[mod]]$probe
##             dat <- d[[mod]][,ss]
##             names(dat) <- d[[mod]]$probe

##             col_cnv <- rep('black', length(dat))
##             col_cnv[d[[mod]][,cnv_sample] == -1 & d[[mod]]$distance_cpg_t_mrr == 0] <- 'darkred'
##             col_cnv[d[[mod]][,cnv_sample] == -1 & d[[mod]]$distance_cpg_t_mrr == 0] <- 'blue'
            
##             lines(x = a450cpg[names(dat), 'MAPINFO'],
##                   y = dat,
##                   lty="solid",
##                   type = 'p',
##                   pch = 20,
##                   cex = 0.5,
##                   col= col_cnv,
##                   ## col= 'black',
##                   lwd = 1)
##             i <- i + 1
##         }


##         ## mrr segments start
##         for (mrr in unique(closest$mrr)) {
##             curr <- closest[closest$mrr == mrr, ][1,]
##             if (curr$mchr == sprintf('chr%s', chr)) {
##                 segments(x0 = curr$mstart,
##                          x1 = curr$mend,
##                          y0 = 0,
##                          y1 = 0,
##                          lwd = 10,
##                          col = 'darkred')
##             }
##         }
##         ## mrr segments end
##     }



##     title( ylab = 'DNA methylation (beta)',
##           xlab = sprintf('position along chr%s', chr),
##           outer = TRUE, line = 1)
##     ## plot end

    
## }



num_patients <- 10

stratifier <-  as.character(stratifiers[1])
for (chr in 1:22){            
    ## png(file.path(WD, sprintf('chr%s_mounted_%s_%s_patients_highlighted_mrrs.png', chr, stratifier, num_patients)),
    ##     width = 800, height = 500)
    ## par(mfrow = c(2,1),
    ##     oma = c(5,4,0,0) + 0.1,
    ##     mar = c(1.4,1.4,1.4,1.4) + 0.1,
    ##     cex.axis = 1.5, cex.lab = 1.5)
    
    d <- list()

    for (mod in c('1', '2')) {
        tmp <- get_sorted_patients(community = mod, stratifier_probe = stratifier,
                                   a450cpg = a450cpg,
                                   num_patients = num_patients,
                                   chr = chr)

        

        ##  ## getting status gain/loss etc
        ##     probes <- rownames(tmp$meth_t)
        ##     rownames(closest) <- closest$probe
        ##     closest_mrr <- as.character(closest[probes, 'mrr'])
        ##     overlaps_bool <- closest[probes, 'dist'] == 0
        ##     closest_mrr[!overlaps_bool] <- NA


        ## }

        ## preparing data for peinado start

        tmp$meth_t <- as.data.frame(tmp$meth_t)
        colnames(tmp$meth_t) <- sprintf('meth_tumor_%s', colnames(tmp$meth_t))
        tmp$meth_t$probe <- rownames(tmp$meth_t)
        closest$probe <- as.character(closest$probe)
        tmp <- merge(tmp$meth_t, closest, by.x = 'probe', by.y = 'probe', all.x = TRUE)
        colnames(tmp)[19:20] <- c('distance_cpg_t_mrr', 'cometh_module')

        cnv_bak <- cnv
        colnames(cnv_bak) <- paste0(colnames(cnv_bak), '_cnv')
        cnv_bak$mrr <- rownames(cnv_bak)
        patients <- intersect(paste0(gsub('meth_tumor_', '', colnames(tmp)), '_T_cnv'),
                              colnames(cnv_bak))
        cnv_bak <- cnv_bak[,c( patients, 'mrr')]

        tmp <- merge(tmp, cnv_bak, by.x = 'mrr', by.y = 'mrr', all.x = TRUE)
        
        d[[mod]] <- tmp
    }
    
    write.csv(file.path(WD, sprintf('peinado_plot_cnv_meth_chr%s.csv', chr)),
              x = rbind(d[[1]], d[[2]]),
              row.names = FALSE,
              quote = FALSE)

    type <- 'meth_t'

      png(file.path(WD, sprintf('chr%s_mounted_%s_%s_patients_mrr_colored.png', chr, stratifier, num_patients)),
          width = 800, height = 1500)
    
    par(mfrow = c(10,2),
        oma = c(5,4,0,0) + 0.1,
        mar = c(1.4,1.4,1.4,1.4) + 0.1,
        cex.axis = 1.5, cex.lab = 1.5)

    
    for (ss in grep('meth_tumor_', colnames(d[[mod]]), value =  TRUE)) {
        ## plot start
        ## plot(y = d[[mod]][,1],
        ##      x = 1:nrow(d[[mod]]),
        ##      ylim = c(0,1.1),
        ##      xlim = c(min(a450cpg[a450cpg$CHR == chr, 'MAPINFO']),
        ##      (max(a450cpg[a450cpg$CHR == chr, 'MAPINFO']))),
        ##      type = 'n')

        ## legend('topleft',
        ##        sprintf('module %s (%s)', mod,
        ##                ifelse(type == 'meth_t', yes = 'tumor', no = 'normal')),
        ##        bty = 'n', cex = 1.5)

        i <- 1
       
        for (mod in c(1,2)) {
            plot(y = d[[mod]][,1],
             x = 1:nrow(d[[mod]]),
             ylim = c(0,1.1),
             xlim = c(min(a450cpg[a450cpg$CHR == chr, 'MAPINFO']),
             (max(a450cpg[a450cpg$CHR == chr, 'MAPINFO']))),
             type = 'n',
             ann=FALSE, xaxt="n", yaxt="n")

            
            cnv_sample <- sprintf('%s_T_cnv', gsub('meth_tumor_', '', ss))
            
            row.names(d[[mod]]) <- d[[mod]]$probe
            dat <- d[[mod]][,ss]
            names(dat) <- d[[mod]]$probe

            col_cnv <- rep('black', length(dat))
            col_cnv[d[[mod]][,cnv_sample] == -1 & d[[mod]]$distance_cpg_t_mrr == 0] <- 'darkred'
            col_cnv[d[[mod]][,cnv_sample] == 1 & d[[mod]]$distance_cpg_t_mrr == 0] <- 'blue'

            cex <- rep(0.5, length(dat))
            cex[col_cnv != 'black'] <- 1
            
            lines(x = a450cpg[names(dat), 'MAPINFO'],
                  y = dat,
                  lty="solid",
                  type = 'p',
                  pch = 20,
                  cex = cex,
                  col= col_cnv,
                  ## col= 'black',
                  lwd = 1)

            title( main = sprintf('%s module %s', ss, mod),
                  outer = FALSE, line = 1)

            ## if (i == 19 | i == 20) {
            ##     axis(1)
            ##     axis(2)
            ## }
            i <- i + 1
        }

        title( ylab = 'DNA methylation (beta)',
              xlab = sprintf('position along chr%s', chr),
              outer = TRUE, line = 1)

       
        
    }



    ## title( ylab = 'DNA methylation (beta)',
    ##       xlab = sprintf('position along chr%s', chr),
    ##       outer = TRUE, line = 1)
    ## ## plot end
    dev.off()

    
}





 for (item in colnames(tmp)) print(item)
## [1] "mrr" the minimal recurrent region (mrr) identifier (coordinate)
## [1] "probe" the cpg
## [1] "meth_tumor_W2095" randomly picked 10 patients methylation from tumor (beta value)
## [1] "meth_tumor_W2049"
## [1] "meth_tumor_Z2038"
## [1] "meth_tumor_F2077"
## [1] "meth_tumor_R2094"
## [1] "meth_tumor_E2023"
## [1] "meth_tumor_X2034"
## [1] "meth_tumor_T2047"
## [1] "meth_tumor_Q2040"
## [1] "meth_tumor_L2020"
## [1] "cchr" probe chr
## [1] "cstart" probe start
## [1] "cend" probe end
## [1] "mchr" mrr chr
## [1] "mstart" mrr start
## [1] "mend" mrr end
## [1] "distance_cpg_t_mrr" distance between probe and mmr; 0 stands for overlap
## [1] "cometh_module" cometh module (only for modules 1 and 2)
## [1] "W2095_T_cnv" randomly picked 10 patients cnv status; -1 loss, 0 nd, 1 gain
## [1] "W2049_T_cnv"
## [1] "Z2038_T_cnv"
## [1] "F2077_T_cnv"
## [1] "R2094_T_cnv"
## [1] "E2023_T_cnv"
## [1] "X2034_T_cnv"
## [1] "T2047_T_cnv"
## [1] "Q2040_T_cnv"
## [1] "L2020_T_cnv"








stop('till here')



## list of minimal recurrent regions (mrrs)
lmin <- list()
for (module in TRANS) {
    module <- as.character(module)
    lmin[[module]] <- list()
    probes <- partition[partition$x == module, 'probe']
    probes_loci <- a450cpg[probes, c('IlmnID', 'CHR', 'MAPINFO')]
    related <- NULL
    for (probe in probes) {
        related <-  rownames(
            cnv[(cnv$Chromosome == probes_loci[probe, 'CHR']) &
                (probes_loci[probe, 'MAPINFO'] >= cnv$Start) &
                (probes_loci[probe, 'MAPINFO'] < cnv$End),]) 

        if (length(related) > 0) {
            lmin[[module]][[related]] <- c(lmin[[module]][[related]], probe)
        }
        related <- NULL
    }

    ## getting the dnameth stats according to the mrr and its gain/loss/nc status,
    ## this is done module wise due to the fact mod1 and mod2 are antimeth etc
    ## since highly correlated we pick only the first CpG
    for (mrr in names(lmin[[module]])) {
        curr_probes <- lmin[[module]][[mrr]][1]
        gained <- common[as.numeric(cnv[mrr, common]) == 1]
        kept <- common[as.numeric(cnv[mrr, common]) == 0]
        lost <- common[as.numeric(cnv[mrr, common]) == -1]
        
        curr_meth = list(lost = meth[curr_probes, lost]/1000,
                         kept = meth[curr_probes,kept]/1000,
                         gained = meth[curr_probes,gained]/1000)
        lmin[[module]][[mrr]] <- curr_meth
        
    }
    
    
}

save(lmin, file = file.path(WD, 'lmin.RData'))


plots <- list()
for (module in names(lmin)) {

    ## curr <- rbindlist(lmin[[module]], fill=TRUE)
    
    ## curr <- unlist(lmin[[module]])
    set.seed(1)
    if (length(lmin[[module]]) > 10) {
        mrrs <- names(lmin[[module]])[sample( length(lmin[[module]]), 10)]
    } else {
        mrrs <- names(lmin[[module]])
    }

    dat <- data.frame(mrr = NULL, meth = NULL, cnv = NULL)
    for (mrr in mrrs){
        
        curr <- unlist(lmin[[module]][[mrr]])
        
        fd <- data.frame(mrr = mrr,
                         meth = curr,
                         cnv = sapply(strsplit(names(curr), split = '.', fixed = TRUE),
                                      function(x) return(x[1])))

        dat <- rbind(dat, fd)
        
    }
    dat$cnv <- factor( dat$cnv , c('lost', 'kept', 'gained'))
    plots[[module]] <- ggplot(dat, aes(x = mrr, y = meth, fill = cnv)) + 
        geom_boxplot()  + geom_point(shape = 16, position=position_jitterdodge()) +
        ggtitle(sprintf("Module %s overlapping MRRs", module)) +
        xlab("MRR") + ylab("DNA meth (beta value)") + ylim(0, 1) +
        theme(axis.text.x = element_text(angle = 90, hjust = 1))

}

pdf("cnv.pdf")
invisible(lapply(plots, print))
dev.off()







## using the whole dataset

plots <- list()
models <- list()
for (module in names(lmin)) {
    tryCatch({

        ## curr <- rbindlist(lmin[[module]], fill=TRUE)
        
        ## curr <- unlist(lmin[[module]])
        dat <- data.frame(mrr = NULL, meth = NULL, cnv = NULL)

        set.seed(1)
        if (length(lmin[[module]]) > 50) {
            mrrs <- names(lmin[[module]])[sample( length(lmin[[module]]), 50)]
        } else {
            mrrs <- names(lmin[[module]])
        }
        
        for (mrr in mrrs){
            
            curr <- unlist(lmin[[module]][[mrr]])
            
            fd <- data.frame(mrr = mrr,
                             meth = curr,
                             cnv = sapply(strsplit(names(curr), split = '.', fixed = TRUE),
                                          function(x) return(x[1])))

            dat <- rbind(dat, fd)
            
        }
        dat$cnv <- factor( dat$cnv , c('lost', 'kept', 'gained'))
        plots[[module]] <- ggplot(dat, aes(x = mrr, y = meth, fill = cnv)) + 
            geom_boxplot()  + geom_point(shape = 16, position=position_jitterdodge()) +
            ggtitle(sprintf("Module %s overlapping MRRs", module)) +
            xlab("MRR") + ylab("DNA meth (beta value)") + ylim(0, 1) +
            theme(axis.text.x = element_text(angle = 90, hjust = 1))

        ggsave(file.path(WD, sprintf('cnv_50_mrrs_module_%s.png', module)), height = 7, width = 21)

        dat$cnv <- relevel(dat$cnv, 'kept')
        models[[module]] <- lm(beta2m(meth) ~ cnv+mrr, data = dat)
    }, error = function(x) print('skipped'))
}

## png("cnv_all_.png")
## invisible(lapply(plots, print))
## dev.off()

for (fit in models) {
    coefficients(fit) # model coefficients    
    confint(fit, level=0.95) 
    fitted(fit) 
    residuals(fit)
    anova(fit) 
    ## vcov(fit) # covariance matrix for model parameters 
    plot(fit)
}


## rather focus on mod1 vs mod2 and the gain/loss impact

for (module in c('1', '2')) {
    dat <- data.frame(mrr = NULL, meth = NULL, cnv = NULL)
    for (mrr in names(lmin[[module]])){
        
        curr <- unlist(lmin[[module]][[mrr]])
        
        fd <- data.frame(mrr = mrr,
                         meth = curr,
                         cnv = sapply(strsplit(names(curr), split = '.', fixed = TRUE),
                                      function(x) return(x[1])))

        dat <- rbind(dat, fd) ## @todo fix, extremely unefficient
        
    }
    dat$cnv <- factor( dat$cnv , c('lost', 'kept', 'gained'))
    
    dat$cnv <- relevel(dat$cnv, 'kept')
    m1 <- lm(beta2m(meth) ~ cnv, data = dat)
    m2 <- update(m1, . ~ . + mrr, data = dat)
    print(anova(m1, m2))
    print(head(confint(m2)))
    
}

