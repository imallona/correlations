#!/usr/bin/env/R
##
## looking for negative correlations linking module 1 and 2
##
## Izaskun Mallona
## 23 jan 2018

## stop('swaps_results etc is not ok, only takes into account intra chr stuff!!')
## stop('the error was detected Mon Mar 12 14:34:30 CET 2018')


library(RPostgreSQL)
library(plyr)
library(parallel)
library(igraph)

TASK ='negative_correlations_links_zurich'
HOME = '/home/labs/maplab/imallona'
IHOME = '/imppc/labs/maplab/imallona'
## WD = file.path(HOME, 'correlations_simpler', TASK)
NFS_WD = file.path(IHOME, 'correlations_simpler', TASK)
WD <- NFS_WD

MAXITER <- 20000 ## mofa train max iterations

SCHEMA <- 'colonomics_tumor'  ## schema the module detection was carried out with
INNER_MIN_DEGREE <- 1         ## first prunning (in-chromosome prunning)
MIN_DEGREE <- 1               ## vertex min degree
MIN_SIZE <- 10                ## community min size
MIN_RHO <- 0.8                ## minimum rho coefficient value to fetch the correlation
LABEL <- sprintf('%s_inner_min_degree_%s_min_degree_%s_min_size_%s_min_rho_%s_positive_only',
                 SCHEMA, INNER_MIN_DEGREE, MIN_DEGREE, MIN_SIZE, MIN_RHO)

DB_CONF <- file.path(IHOME, 'src', 'correlations', 'db_imallona.txt')

## trans-communities as produced by trans_communities_subsetter.R
MEMBERSHIP <- file.path(IHOME,
                        'correlations',
                        'colonomics_rho_restrictive_communities',
                        LABEL,
                        sprintf('%s_comm_membership.tsv', LABEL))

no_cores <- 11

source(file.path(IHOME, 'src', 'correlations', 'analysis', 'database_connection.R'))


dir.create(WD, showWarnings = FALSE)
setwd(WD)

print('Connecting to the database')

## database-related functions start
drv <- dbDriver("PostgreSQL")

db_conf <- get_db_parameters(DB_CONF)

partition <- read.table(MEMBERSHIP, header = TRUE)
partition$probe <- row.names(partition)

a <- paste0("('", paste0(partition[partition$x == 1, 'probe'], collapse = "','"), "')")
b <- paste0("('", paste0(partition[partition$x == 2, 'probe'], collapse = "','"), "')")




## ## probes from in module 1 and 2 which were co-methylating normally in normals
## ## and are anti-methylating in tumors
## swaps_broken <- function(chr1) {
    
##     tryCatch({
##         dt <- list()
##         dn <- list()
##         dcommon <- NULL
##         dt[[chr1]] <- list()
##         dn[[chr1]] <- list()

##         for (chr2 in 1:22) {
##             if (chr2 >= chr1) {
##                 print(sprintf('chr%s chr%s', chr1, chr2))

##                 normal_stmt <- sprintf('
## select *
## from colonomics_normal.meth_filtered_correlations_chr%s_chr%s
## where
##  (
##   (meth_probe_a IN %s
##     and meth_probe_b IN %s)
##   or
##   (meth_probe_b IN %s
##     and meth_probe_a IN %s)
##  )
##  and correlation >= 0.8

##  ', chr1, chr2, a, b, a, b)
##                 dn[[chr1]][[chr2]] <- get_query_at_once(con, normal_stmt)
                
##                 dn[[chr1]][[chr2]][,'id'] <- NA
                
##                 for (i in 1:nrow( dn[[chr1]][[chr2]])) {
##                     dn[[chr1]][[chr2]][i, 'id'] <- paste(
##                         sort(c(dn[[chr1]][[chr2]][i, 'meth_probe_a'],
##                                dn[[chr1]][[chr2]][i, 'meth_probe_b'])), collapse = '_')
##                 }

##                                         # these are the candidates
##                 anti_a <-  paste0("('", paste0(intersect(partition[partition$x == 1, 'probe'],
##                                                          c(dn[[chr1]][[chr2]][, 'meth_probe_a'],
##                                                            dn[[chr1]][[chr2]][, 'meth_probe_b'])),
##                                                collapse = "','"), "')")

##                 anti_b <-  paste0("('", paste0(intersect(partition[partition$x == 2, 'probe'],
##                                                          c(dn[[chr1]][[chr2]][, 'meth_probe_a'],
##                                                            dn[[chr1]][[chr2]][, 'meth_probe_b'])),
##                                                collapse = "','"), "')")

                
##                 anti_stmt <- sprintf('
## select *
## from colonomics_tumor.meth_filtered_correlations_chr%s_chr%s
## where
##  ((meth_probe_a IN %s and meth_probe_b IN %s)
##  or
##  (meth_probe_b IN %s and meth_probe_a IN %s))
##  and correlation < -0.8',
##                                      chr1, chr2, anti_a, anti_b, anti_a, anti_b)
##                 dt[[chr1]][[chr2]] <- get_query_at_once(con, anti_stmt)
##                 dt[[chr1]][[chr2]][,'id'] <- NA
##                 for (i in 1:nrow( dt[[chr1]][[chr2]])) {
##                     dt[[chr1]][[chr2]][i, 'id'] <- paste(sort(
##                         c(dt[[chr1]][[chr2]][i, 'meth_probe_a'],
##                           dt[[chr1]][[chr2]][i, 'meth_probe_b'])), collapse = '_')
##                 }

##                 dcommon <- c(dcommon, intersect( dt[[chr1]][[chr2]][,'id'],
##                                                 dn[[chr1]][[chr2]][, 'id']))
##                 ## print(dcommon)
                
##             }
##         }
##         ## dbDisconnect(con)
        
##         ## return(dcommon)
##         return(list(dcommon = dcommon,
##                     dn = dn,
##                     dt = dt))
##     }, error = function(x) return(list(dcommon = dcommon,
##            dn = dn,
##            dt = dt)))
## }


## probes from in module 1 and 2 which were co-methylating normally in normals
## and are anti-methylating in tumors
swaps <- function(chr1) {
    dt <- list()
    dn <- list()
    dcommon <- NULL
    dt[[as.character(chr1)]] <- list()
    dn[[as.character(chr1)]] <- list()

    ## also some file cons to write results line by line
    fn <- 'swaps_test'
    fn_n <- sprintf('%s_normal.csv', fn)
    fn_t <- sprintf('%s_tumor.csv', fn)
    
    for (chr2 in 1:22) {
        if (chr2 >= chr1) {
            print(sprintf('chr%s chr%s', chr1, chr2))

            normal_stmt <- sprintf('
select *
from colonomics_normal.meth_filtered_correlations_chr%s_chr%s
where
 (
  (meth_probe_a IN %s
    and meth_probe_b IN %s)
  or
  (meth_probe_b IN %s
    and meth_probe_a IN %s)
 )
 and correlation >= 0.8

 ', chr1, chr2, a, b, a, b)

            curr <- get_query_at_once(con, normal_stmt)
            
            dn[[as.character(chr1)]][[as.character(chr2)]] <- curr

            if (nrow(curr) > 0) {
                curr[,'id'] <- NA

                for (i in 1:nrow( curr)) {
                    curr[i, 'id'] <- paste(
                        sort(c(curr[i, 'meth_probe_a'],
                               curr[i, 'meth_probe_b'])), collapse = '_')
                }

                
                ## these are the candidates
                anti_a <-  paste0("('", paste0(intersect(partition[partition$x == 1, 'probe'],
                                                         c(curr[, 'meth_probe_a'],
                                                           curr[, 'meth_probe_b'])),
                                               collapse = "','"), "')")

                anti_b <-  paste0("('", paste0(intersect(partition[partition$x == 2, 'probe'],
                                                         c(curr[, 'meth_probe_a'],
                                                           curr[, 'meth_probe_b'])),
                                               collapse = "','"), "')")

                
                anti_stmt <- sprintf('
select *
from colonomics_tumor.meth_filtered_correlations_chr%s_chr%s
where
 ((meth_probe_a IN %s and meth_probe_b IN %s)
 or
 (meth_probe_b IN %s and meth_probe_a IN %s))
 and correlation < -0.8',
                                     chr1, chr2, anti_a, anti_b, anti_a, anti_b)
                
                ## dt[[chr1]][[chr2]] <- get_query_at_once(con, anti_stmt)

                curr_t <- get_query_at_once(con, anti_stmt)

                if (nrow(curr_t) > 0) {
                    curr_t[,'id'] <- NA
                    for (i in 1:nrow( curr_t)) {
                        curr_t[i, 'id'] <- paste(sort(
                            c(curr_t[i, 'meth_probe_a'],
                              curr_t[i, 'meth_probe_b'])), collapse = '_')
                    }
                }

                dn[[as.character(chr1)]][[as.character(chr2)]] <- curr
                dt[[as.character(chr1)]][[as.character(chr2)]] <- curr_t

                write.table(x = curr, file = fn_n, append = TRUE, sep = "\t",
                            col.names = FALSE, row.names = FALSE)
                write.table(x = curr_t, file = fn_t, append = TRUE, sep = "\t", 
                            col.names = FALSE, row.names = FALSE) 
                
                if (nrow(curr_t) > 0 & nrow(curr) > 0) {
                    dcommon <- c(dcommon,
                                 intersect(dt[[as.character(chr1)]][[as.character(chr2)]][,'id'],
                                           dn[[as.character(chr1)]][[as.character(chr2)]][,'id']))
                }

            }
            ## print(dcommon)
 
            
        }
    }

    return(list(dcommon = dcommon,
                dn = dn,
                dt = dt))
}




## probes from in module 1 and 2 which were co-methylating normally in normals
## and are anti-methylating in tumors
swaps_test_incomplete <- function(chr1) {
    dt <- list()
    dn <- list()
    dcommon <- NULL
    dt[[as.character(chr1)]] <- list()
    dn[[as.character(chr1)]] <- list()

    for (chr2 in 1:3) {
        if (chr2 >= chr1) {
            print(sprintf('chr%s chr%s', chr1, chr2))

            normal_stmt <- sprintf('
select *
from colonomics_normal.meth_filtered_correlations_chr%s_chr%s
where
 (
  (meth_probe_a IN %s
    and meth_probe_b IN %s)
  or
  (meth_probe_b IN %s
    and meth_probe_a IN %s)
 )
 and correlation >= 0.8

 ', chr1, chr2, a, b, a, b)

            curr <- get_query_at_once(con, normal_stmt)
            
            dn[[as.character(chr1)]][[as.character(chr2)]] <- curr

            if (nrow(curr) > 0) {
                curr[,'id'] <- NA

                for (i in 1:nrow( curr)) {
                    curr[i, 'id'] <- paste(
                        sort(c(curr[i, 'meth_probe_a'],
                               curr[i, 'meth_probe_b'])), collapse = '_')
                }

                
                ## these are the candidates
                anti_a <-  paste0("('", paste0(intersect(partition[partition$x == 1, 'probe'],
                                                         c(curr[, 'meth_probe_a'],
                                                           curr[, 'meth_probe_b'])),
                                               collapse = "','"), "')")

                anti_b <-  paste0("('", paste0(intersect(partition[partition$x == 2, 'probe'],
                                                         c(curr[, 'meth_probe_a'],
                                                           curr[, 'meth_probe_b'])),
                                               collapse = "','"), "')")

                
                anti_stmt <- sprintf('
select *
from colonomics_tumor.meth_filtered_correlations_chr%s_chr%s
where
 ((meth_probe_a IN %s and meth_probe_b IN %s)
 or
 (meth_probe_b IN %s and meth_probe_a IN %s))
 and correlation < -0.8',
                                     chr1, chr2, anti_a, anti_b, anti_a, anti_b)
                
                ## dt[[chr1]][[chr2]] <- get_query_at_once(con, anti_stmt)

                curr_t <- get_query_at_once(con, anti_stmt)

                if (nrow(curr_t) > 0) {
                    curr_t[,'id'] <- NA
                    for (i in 1:nrow( curr_t)) {
                        curr_t[i, 'id'] <- paste(sort(
                            c(curr_t[i, 'meth_probe_a'],
                              curr_t[i, 'meth_probe_b'])), collapse = '_')
                    }
                }

                dn[[as.character(chr1)]][[as.character(chr2)]] <- curr
                dt[[as.character(chr1)]][[as.character(chr2)]] <- curr_t

                
                if (nrow(curr_t) > 0 & nrow(curr) > 0) {
                    dcommon <- c(dcommon,
                                 intersect(dt[[as.character(chr1)]][[as.character(chr2)]][,'id'],
                                           dn[[as.character(chr1)]][[as.character(chr2)]][,'id']))
                }

            }
            ## print(dcommon)
 
            
        }
    }

    return(list(dcommon = dcommon,
                dn = dn,
                dt = dt))
}



## probes in modules 1 and 2 (tumor) which were connected (co-methylating) in normals
present_in_normals <- function(chr1) {
    dn <- list()
    
    for (chr2 in 1:22) {
        if (chr2 >= chr1) {
            print(sprintf('chr%s chr%s', chr1, chr2))

            normal_stmt <- sprintf('
select *
from colonomics_normal.meth_filtered_correlations_chr%s_chr%s
where
 (
  (meth_probe_a IN %s
    and meth_probe_b IN %s)
  or
  (meth_probe_b IN %s
    and meth_probe_a IN %s)
 )
 and correlation >= 0.8

 ', chr1, chr2, a, b, a, b)
            dn[[chr2]] <- get_query_at_once(con, normal_stmt)                
        }
    }
    
    return(dn)
}


## probes in either 1 or 2 that anti-methylate to something from non-1 and non-2 modules
## which whom they are co-methylating in normals

centinel <- function(chr1) {
    tryCatch({
        dt <- list()
        dn <- list()
        dcommon <- NULL
        for (chr1 in 1:22) {
            dt[[chr1]] <- list()
            dn[[chr1]] <- list()
            for (chr2 in 1:22) {
                if (chr2 >= chr1) {
                    print(sprintf('chr%s chr%s', chr1, chr2))
                    anti_stmt <- sprintf('
select *
from colonomics_tumor.meth_filtered_correlations_chr%s_chr%s
where
 (
  (meth_probe_a IN %s
    and meth_probe_b NOT IN %s)
  or
  (meth_probe_b IN %s
    and meth_probe_a NOT IN %s)
 )
 and correlation < -0.8

 ', chr1, chr2, a, b, a, b)
                    dt[[chr1]][[chr2]] <- get_query_at_once(con, anti_stmt)

                    ## sort(c(foo$meth_probe_a[1], foo$meth_probe_b[2]))
                    dt[[chr1]][[chr2]][,'id'] <- NA
                    for (i in 1:nrow( dt[[chr1]][[chr2]])) {
                        dt[[chr1]][[chr2]][i, 'id'] <- paste(sort(
                            c(dt[[chr1]][[chr2]][i, 'meth_probe_a'],
                              dt[[chr1]][[chr2]][i, 'meth_probe_b'])), collapse = '_')
                    }

                    anti_a <-  paste0("('", paste0(intersect(partition[partition$x == 1, 'probe'],
                                                             c(dt[[chr1]][[chr2]][, 'meth_probe_a'],
                                                               dt[[chr1]][[chr2]][, 'meth_probe_b'])),
                                                   collapse = "','"), "')")

                    anti_b <-  paste0("('", paste0(intersect(partition[partition$x == 2, 'probe'],
                                                             c(dt[[chr1]][[chr2]][, 'meth_probe_a'],
                                                               dt[[chr1]][[chr2]][, 'meth_probe_b'])),
                                                   collapse = "','"), "')")
                    ## dt[[chr1]][[chr2]][,'id'] <- 

                    normal_stmt <- sprintf('
select *
from colonomics_normal.meth_filtered_correlations_chr%s_chr%s
where
 (
  (meth_probe_a IN %s
    and meth_probe_b IN %s)
  or
  (meth_probe_b IN %s
    and meth_probe_a IN %s)
 )
 and correlation >= 0.8

 ', chr1, chr2, anti_a, anti_b, anti_a, anti_b)
                    dn[[chr1]][[chr2]] <- get_query_at_once(con, normal_stmt)
                    
                    dn[[chr1]][[chr2]][,'id'] <- NA
                    
                    for (i in 1:nrow( dn[[chr1]][[chr2]])) {
                        dn[[chr1]][[chr2]][i, 'id'] <- paste(
                            sort(c(dn[[chr1]][[chr2]][i, 'meth_probe_a'],
                                   dn[[chr1]][[chr2]][i, 'meth_probe_b'])), collapse = '_')
                    }

                    dcommon <- c(dcommon, intersect( dt[[chr1]][[chr2]][,'id'],
                                                    dn[[chr1]][[chr2]][, 'id']))
                    print(dcommon)
                    
                }
            }
        }

        return(list(dcommon = dcommon,
                    dn = dn,
                    dt = dt))
    }, error =  function(x) return(list(dcommon = dcommon,
           dn = dn,
           dt = dt)))
}



drv <- dbDriver("PostgreSQL")

db_conf <- get_db_parameters(DB_CONF)

con <- dbConnect(drv,
                 user = db_conf[['user']],
                 password = db_conf[['password']],
                 dbname = 'colonomics_correlations',
                 host = db_conf[['host']],
                 port = db_conf[['port']])




## stop('debugging Tue Mar 20 12:42:24 CET 2018')
## stop('swaps_results etc is not ok, only takes into account intra chr stuff!!')
## stop('the error was detected Mon Mar 12 14:34:30 CET 2018')








cl <- makeCluster(no_cores)

clusterExport(cl = cl, list("swaps", "swaps_test_incomplete", "present_in_normals", "centinel",
                  "a", "b", "get_query_at_once", "DB_CONF",
                  "get_db_parameters", "drv", "partition"),
              envir=environment())

clusterEvalQ(cl, library(RPostgreSQL))
clusterEvalQ(cl, {
    library(DBI)
    library(RPostgreSQL)
    drv <- dbDriver("PostgreSQL")
    
    db_conf <- get_db_parameters(DB_CONF)

    con <- dbConnect(drv,
                     user = db_conf[['user']],
                     password = db_conf[['password']],
                     dbname = 'colonomics_correlations',
                     host = db_conf[['host']],
                     port = db_conf[['port']])
    NULL
})

## lapply(21:22, foo)

test_results <- parLapply(cl, 1:3, swaps_test_incomplete)
swaps_results <- parLapply(cl, 1:22, swaps)

clusterEvalQ(cl, {
    dbDisconnect(con)
})

stopCluster(cl)

print('call negative_correlations_links_zurich_postproc.R here')

stop('till here')
