#!/bin/bash
##
## Assigns an HMM to each location according to a priorization algorithm
## Uses HMM hESC1 data
##
## Based upon /imppc/labs/maplab/imallona/src/nsuma_expression/repeats_hmm_associator.sh
##
## Postprocessed by analysis/pairwise_genomic_categories_enrichment.R
##
## Izaskun Mallona, GPLv3
## 10th april 2017

## the path the bedfiles are located at
DAT="$IHOME"/correlations_simpler/pairwise_genomic_categories_enrichment
WD="$DAT"

HMM=wgEncodeAwgSegmentationChromhmmH1hesc.bed

hmms=(Active_Promoter Candidate_Strong_enhancer Candidate_Weak_enhancer_DNase CTCF_Candidate_Insulator Heterochromatin_Repetitive_Copy_Number_Variation Inactive_Promoter Low_activity_proximal_to_active_states Polycomb_repressed Promoter_Flanking Transcription_associated)

SEED=1

mkdir -p $WD
cd $WD

echo 'Retrieving hg19 stats'
mysql --user=genome --host=genome-mysql.cse.ucsc.edu -A -e \
      "select chrom, size from hg19.chromInfo" > ${WD}/hg19.genome


echo 'Retrieving chromatin colors'
mysql --user=genome --host=genome-mysql.cse.ucsc.edu -A -e \
      'SELECT chrom, chromStart, chromEnd, name 
     FROM hg19.wgEncodeAwgSegmentationChromhmmH1hesc;' | awk  '{if (NR!=1) {print}}' > "$WD"/"$HMM"

echo 'Grouping colors'

sed -i 's/TssF/Active_Promoter/g' "$HMM"
sed -i 's/Tss/Active_Promoter/g' "$HMM"
sed -i 's/PromF/Promoter_Flanking/g' "$HMM"
sed -i 's/PromP/Inactive_Promoter/g' "$HMM"
sed -i 's/EnhWF/Candidate_Weak_enhancer_DNase/g' "$HMM"
sed -i 's/EnhW/Candidate_Weak_enhancer_DNase/g' "$HMM"
sed -i 's/EnhF/Candidate_Strong_enhancer/g' "$HMM"
sed -i 's/Enh/Candidate_Strong_enhancer/g' "$HMM"
sed -i 's/EnF/Candidate_Strong_enhancer/g' "$HMM"
sed -i 's/DnaseU/Candidate_Weak_enhancer_DNase/g' "$HMM"
sed -i 's/DnaseD/Candidate_Weak_enhancer_DNase/g' "$HMM"
sed -i 's/FaireW/Candidate_Weak_enhancer_DNase/g' "$HMM"
sed -i 's/CtcfO/CTCF_Candidate_Insulator/g' "$HMM"
sed -i 's/Ctcf/CTCF_Candidate_Insulator/g' "$HMM"
sed -i "s/Gen5'/Transcription_associated/g" "$HMM"
sed -i "s/ElonW/Transcription_associated/g" "$HMM"
sed -i "s/Elon/Transcription_associated/g" "$HMM"
sed -i "s/Gen3'/Transcription_associated/g" "$HMM"
sed -i "s/Pol2/Transcription_associated/g" "$HMM"
sed -i "s/H4K20/Transcription_associated/g" "$HMM"
sed -i "s/Low/Low_activity_proximal_to_active_states/g" "$HMM"
sed -i "s/ReprD/Polycomb_repressed/g" "$HMM"
sed -i "s/ReprW/Polycomb_repressed/g" "$HMM"
sed -i "s/Repr/Polycomb_repressed/g" "$HMM"
sed -i "s/Quies/Heterochromatin_Repetitive_Copy_Number_Variation/g" "$HMM"
sed -i "s/Art/Heterochromatin_Repetitive_Copy_Number_Variation/g" "$HMM"

echo 'Assigning a weight to each color'

cp $HMM ${HMM}_priorized

sed -i "s/CTCF_Candidate_Insulator/1/g" ${HMM}_priorized
sed -i "s/Active_Promoter/2/g" ${HMM}_priorized
sed -i "s/Inactive_Promoter/3/g" ${HMM}_priorized
sed -i "s/Candidate_Strong_enhancer/4/g" ${HMM}_priorized
sed -i "s/Candidate_Weak_enhancer_DNase/5/g" ${HMM}_priorized
sed -i "s/Promoter_Flanking/6/g" ${HMM}_priorized
sed -i "s/Transcription_associated/7/g" ${HMM}_priorized
sed -i "s/Low_activity_proximal_to_active_states/8/g" ${HMM}_priorized
sed -i "s/Polycomb_repressed/9/g" ${HMM}_priorized
sed -i "s/Heterochromatin_Repetitive_Copy_Number_Variation/10/g" ${HMM}_priorized

cut -f4 wgEncodeAwgSegmentationChromhmmH1hesc.bed  | sort | uniq -c

echo 'Sorting'

for fn in $(find . -name "*bed") 
do

    sort -k1,1 -k2,2n $fn > foo; mv foo $fn

done


cd ${WD}

echo 'Adding an identifier to each cpg, as they can repeat'
beds=( $(find . -maxdepth 1 -name "sampled*bed" -type f | sort -V) )

for fn in  ${beds[@]}
do
    awk '{FS=OFS="\t"; print $1,$2,$3,NR,$4}' $fn > foo
    mv foo $fn
done


echo 'Fixing the coordinates to correspond to a C'

for fn in  ${beds[@]}
do
    awk '{FS=OFS="\t"; print $1,$2-1,$3-1,$4,$5}' $fn > foo
    mv foo $fn
done

echo 'Chromatin coloring'
## only elements with the highest priority are taken into account
## hierarchy by hmm

for bed in ${beds[@]}
do
    
    if [  -f $bed ]
    then
        
        bedtools intersect -wa -wb -a $bed \
                 -b "$HMM"_priorized >  ${bed}_colored.bed

        bedtools groupby -i ${bed}_colored.bed \
                 -g 1-4 -c 9 -o min > ${bed}_colored_reduced.bed

        awk '{print $5"\t"}' ${bed}_colored.bed | paste - ${bed}_colored_reduced.bed > foo.bed
        mv foo.bed  ${bed}_colored_reduced.bed

        rm  ${bed}_colored.bed
        ## getting the states back
        sed -i "s/1$/CTCF_Candidate_Insulator/g" ${bed}_colored_reduced.bed
        sed -i "s/2$/Active_Promoter/g" ${bed}_colored_reduced.bed
        sed -i "s/3$/Inactive_Promoter/g" ${bed}_colored_reduced.bed
        sed -i "s/4$/Candidate_Strong_enhancer/g" ${bed}_colored_reduced.bed
        sed -i "s/5$/Candidate_Weak_enhancer_DNase/g" ${bed}_colored_reduced.bed
        sed -i "s/6$/Promoter_Flanking/g" ${bed}_colored_reduced.bed
        sed -i "s/7$/Transcription_associated/g" ${bed}_colored_reduced.bed
        sed -i "s/8$/Low_activity_proximal_to_active_states/g" ${bed}_colored_reduced.bed
        sed -i "s/9$/Polycomb_repressed/g" ${bed}_colored_reduced.bed
        sed -i "s/10$/Heterochromatin_Repetitive_Copy_Number_Variation/g" ${bed}_colored_reduced.bed
    fi
done

wc -l *bed

printf "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n" \
       "category" \
       "n" \
       "Active_Promoter" \
       "Candidate_Strong_enhancer" \
       "Candidate_Weak_enhancer_DNase" \
       "CTCF_Candidate_Insulator" \
       "Heterochromatin_Repetitive_Copy_Number_Variation" \
       "Inactive_Promoter" \
       "Low_activity_proximal_to_active_states" \
       "Polycomb_repressed"\
       "Promoter_Flanking" \
       "Transcription_associated" > annotated_by_hmm.tsv

for bed in ${beds[@]}
do
    if [ -f $bed ]
    then
        curr=${bed}_colored_reduced.bed
        n=$(wc -l $curr | cut -d" " -f1)

        for hmm in ${hmms[@]}
                   
        do
            printf -v $hmm $(grep -c $hmm $curr)
        done

        printf "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n" \
               "$bed" \
               "$n" \
               "$Active_Promoter" \
               "$Candidate_Strong_enhancer" \
               "$Candidate_Weak_enhancer_DNase" \
               "$CTCF_Candidate_Insulator" \
               "$Heterochromatin_Repetitive_Copy_Number_Variation" \
               "$Inactive_Promoter" \
               "$Low_activity_proximal_to_active_states" \
               "$Polycomb_repressed"\
               "$Promoter_Flanking" \
               "$Transcription_associated"
    fi            
done >>  annotated_by_hmm.tsv

echo 'Background stuff, whole genome and scrutinized by infinium (to compare against afterwards)'


echo 'Generating a whole genome background to compare against'

cp wgEncodeAwgSegmentationChromhmmH1hesc.bed_priorized genome_wide.bed 

sed -i "s/1$/CTCF_Candidate_Insulator/g" genome_wide.bed
sed -i "s/2$/Active_Promoter/g" genome_wide.bed
sed -i "s/3$/Inactive_Promoter/g" genome_wide.bed
sed -i "s/4$/Candidate_Strong_enhancer/g" genome_wide.bed
sed -i "s/5$/Candidate_Weak_enhancer_DNase/g" genome_wide.bed
sed -i "s/6$/Promoter_Flanking/g" genome_wide.bed
sed -i "s/7$/Transcription_associated/g" genome_wide.bed
sed -i "s/8$/Low_activity_proximal_to_active_states/g" genome_wide.bed
sed -i "s/9$/Polycomb_repressed/g" genome_wide.bed
sed -i "s/10$/Heterochromatin_Repetitive_Copy_Number_Variation/g" genome_wide.bed

ln -s genome_wide.bed genome_wide.bed_colored_reduced.bed

echo 'Generating a 450 chip-locations (cg ones) to compare against'

psql -d regional_profile \
     -U imallona \
     -h overlook \
     -t -A -F $'\t' -c \
     "
select chr, cg_start-1, cg_end -1 
from annotations.humanmethylation450kannot
where probetype = 'cg';" > infinium.bed


# echo 'Fixing the coordinates to correspond to a C'

# awk '{FS=OFS="\t"; print $1,$2-1,$3-1,$4}' infinium.bed > foo
# mv foo infinium.bed



bed=infinium.bed
bedtools intersect -wa -wb -a $bed \
         -b "$HMM"_priorized >  ${bed}_colored.bed

bedtools groupby -i ${bed}_colored.bed \
         -g 1-3 -c 7 -o min > ${bed}_colored_reduced.bed

sed -i "s/1$/CTCF_Candidate_Insulator/g" ${bed}_colored_reduced.bed
sed -i "s/2$/Active_Promoter/g" ${bed}_colored_reduced.bed
sed -i "s/3$/Inactive_Promoter/g" ${bed}_colored_reduced.bed
sed -i "s/4$/Candidate_Strong_enhancer/g" ${bed}_colored_reduced.bed
sed -i "s/5$/Candidate_Weak_enhancer_DNase/g" ${bed}_colored_reduced.bed
sed -i "s/6$/Promoter_Flanking/g" ${bed}_colored_reduced.bed
sed -i "s/7$/Transcription_associated/g" ${bed}_colored_reduced.bed
sed -i "s/8$/Low_activity_proximal_to_active_states/g" ${bed}_colored_reduced.bed
sed -i "s/9$/Polycomb_repressed/g" ${bed}_colored_reduced.bed
sed -i "s/10$/Heterochromatin_Repetitive_Copy_Number_Variation/g" ${bed}_colored_reduced.bed


printf "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n" \
       "category" \
       "n" \
       "Active_Promoter" \
       "Candidate_Strong_enhancer" \
       "Candidate_Weak_enhancer_DNase" \
       "CTCF_Candidate_Insulator" \
       "Heterochromatin_Repetitive_Copy_Number_Variation" \
       "Inactive_Promoter" \
       "Low_activity_proximal_to_active_states" \
       "Polycomb_repressed"\
       "Promoter_Flanking" \
       "Transcription_associated" > backgrounds_by_hmm.tsv



for bed in infinium.bed genome_wide.bed
do
    if [ -f $bed ]
    then
        curr=${bed}_colored_reduced.bed
        n=$(wc -l $curr | cut -d" " -f1)

        for hmm in ${hmms[@]}
                   
        do
            printf -v $hmm $(grep -c $hmm $curr)
        done

        printf "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n" \
               "$bed" \
               "$n" \
               "$Active_Promoter" \
               "$Candidate_Strong_enhancer" \
               "$Candidate_Weak_enhancer_DNase" \
               "$CTCF_Candidate_Insulator" \
               "$Heterochromatin_Repetitive_Copy_Number_Variation" \
               "$Inactive_Promoter" \
               "$Low_activity_proximal_to_active_states" \
               "$Polycomb_repressed"\
               "$Promoter_Flanking" \
               "$Transcription_associated"
    fi            
done >>  backgrounds_by_hmm.tsv


echo 'Homer annotation'

## same with homer annotation

cd $WD

beds=( $(find . -maxdepth 1 -name "sampled*bed" -type f | sort -V | grep -v colored))

for bed in ${beds[@]}
do
    ## homer format
    ofn=$(basename $bed .bed)
    awk '{FS=OFS="\t"; print $5";"$4,$1,$2,$3,"+"}' "$bed" > "$ofn".homer
    perl /soft/bio/homer-4.8/bin/annotatePeaks.pl \
         "$ofn".homer \
         hg19  \
         > $(basename $bed .bed)_annotated.tab

    ## for painless reading by R or whatever
    sed -i "s/'/prima/g" $(basename $bed .bed)_annotated.tab
done

## this lacks pairwise correlation ids
bed=infinium.bed
perl /soft/bio/homer-4.8/bin/annotatePeaks.pl \
         "$bed" \
         hg19  \
         > $(basename $bed .bed)_annotated.tab

sed -i "s/'/prima/g" $(basename $bed .bed)_annotated.tab
