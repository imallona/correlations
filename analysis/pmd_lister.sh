#!/bin/bash
##
## Lister's partially methylated domains
## https://www.nature.com/articles/nature08514

TASK="pmd_lister"
WD="$IHOME"/correlations_simpler/"$TASK"

BEDTOOLS=/soft/bio/bedtools2-2.26.master/bin/bedtools

mkdir -p $WD
cd $WD

## lister data (supp tables, tables 2, PMD in IMR90)
wc -l nature08514-s2_table_9.bed

## top imprinted data
ln -s "$IHOME"/correlations_simpler/imprinting/top_percentile_trans.bed


$BEDTOOLS sort -i top_percentile_trans.bed > top_percentile_trans.bed.sorted
$BEDTOOLS sort -i nature08514-s2_table_9.bed > foo
mv foo nature08514-s2_table_9.bed

$BEDTOOLS closest \
          -a top_percentile_trans.bed.sorted \
          -b nature08514-s2_table_9.bed \
          -D b > top_percentile_trans.bed.dist.pmd

## what about the correlating and the 450k items?

psql -d colonomics_correlations -t -A -F$'\t' \
     -c "select concat('chr', chromosome), meth_start, meth_end, probe 
         from annotation.humanmethylation450_probeinfo;" | fgrep cg | $BEDTOOLS sort > chip.bed

$BEDTOOLS closest \
          -a chip.bed \
          -b nature08514-s2_table_9.bed \
          -D b > chip.dist.pmd


# mmm, the correlating probes I can fetch from any partitioning into communities task
cut -f1 -d" "  "$IHOME"/correlations/colonomics_rho_restrictive_communities/colonomics_tumor_inner_min_degree_1_min_degree_1_min_size_10_min_rho_0.8_positive_only/*membership* \
    | tr -d '"' > correlating_cgs


fgrep -f correlating_cgs chip.bed > correlating_cgs.bed

wc -l *

$BEDTOOLS closest \
          -a correlating_cgs.bed \
          -b nature08514-s2_table_9.bed \
          -D b > correlating_cgs.dist.pmd

for item in *dist.pmd
do
    intersect=$(grep $'\t'"0$" $item | wc -l)
    total=$(wc -l $item)
    echo $item $intersect $total
done

# chip.dist.pmd 147257 482421 chip.dist.pmd
# correlating_cgs.dist.pmd 21065 63130 correlating_cgs.dist.pmd
# top_percentile_trans.bed.dist.pmd 132 484 top_percentile_trans.bed.dist.pmd
