#!/bin/bash

WD="$IHOME"/correlations/colonomics_communities_direct_annotation
DATASETS="normal tumor"
OUT=~/correlations_simpler/colonomics_communities_motifs

## bedfile generation
## commented because was already run
echo 'Check if the Rscript communities_subclusters_coordinates_getter.R'
echo ' is needed or not (and toggle comment)'
# Rscript $SRC/correlations/analysis/communities_subclusters_coordinates_getter.R

## homer-based annotation
for fn in $(find $WD -iname probes.bed)
do
    d=$(dirname $fn)
    perl /soft/bio/homer-4.7/bin/annotatePeaks.pl \
        "$d"/probes.bed \
        hg19  \
        -annStats "$d"/probes_annotation_stats.bed \
        -go "$d"/go \
        > "$d"/probes_annotated.tab
done

echo 'Check whether the Rscript plot_annotation_stats_homer.R'
echo ' is needed or not (and toggle comment)'
# Rscript plot_annotation_stats_homer.R


####################################################################
## 20th Oct 2016 TFBS-based search
####################################################################

echo 'Motif search'

mkdir -p "$OUT"/preparsedDir

NTHREADS=6

cd "$OUT"

mysql --user=genome --host=genome-mysql.cse.ucsc.edu -A -e \
      "select chrom, size from hg19.chromInfo"  > "$OUT"/hg19.genome


# Note that CpGs closer than 200 bp are merged and then the merged, bigger
# interval is chopped into 200 bp (non overlapping) regions
# which are finally slopped at the flanks, if they do not get 200 bp length


# for fn in $(find  "$WD"/tumor/ -maxdepth 1 -type d)
# do
#     curr_comm=$(basename $fn)

while IFS= read -r var
do
    fn="$WD"/tumor/tumor_comm_"$var"
    curr_comm=$(basename $fn)
    

    # standardizing sizes (collapsing probes)
    mkdir "$OUT"/"$curr_comm"
    ln -s "$fn"/probes.bed "$OUT"/"$curr_comm"/probes.bed

    bedtools sort -i "$OUT"/"$curr_comm"/probes.bed > foo
    mv foo "$OUT"/"$curr_comm"/probes_sorted.bed

    bedtools slop -i "$OUT"/"$curr_comm"/probes_sorted.bed \
             -g "$OUT"/hg19.genome \
             -b 100 > "$OUT"/"$curr_comm"/probes.tmp

    bedtools merge -i "$OUT"/"$curr_comm"/probes.tmp > \
             "$OUT"/"$curr_comm"/probes.tmp2

    bedtools makewindows -b "$OUT"/"$curr_comm"/probes.tmp2 \
             -w 201 > "$OUT"/"$curr_comm"/probes.tmp3

    awk '{OFS=FS="\t"; printf "%s\t%u\t%u\n", $1, $2-(201-($3-$2))/2,  $3+ (201-($3-$2))/2}' "$OUT"/"$curr_comm"/probes.tmp3 > "$OUT"/"$curr_comm"/probes.tmp4

    mv "$OUT"/"$curr_comm"/probes.tmp4 "$OUT"/"$curr_comm"/probes_standardized.bed
    rm "$OUT"/"$curr_comm"/probes.tmp*

    perl /soft/bio/homer-4.8/bin/findMotifsGenome.pl \
         "$OUT"/"$curr_comm"/probes_standardized.bed \
         hg19  \
         "$OUT"/"$curr_comm"/out \
         -size 200 \
         -p "$NTHREADS" \
         -preparsedDir "$OUT"/preparsedDir \
         -mask
# done
done < $IHOME/correlations_simpler/trans_communities_subset/results/colonomics_tumor_inner_min_degree_1_min_degree_1_min_size_10_min_rho_0.8_positive_only_comm_membership.tsvtrans_communities



## example run
# perl /soft/bio/homer-4.8/bin/findMotifsGenome.pl \
#      probes.bed \
#      hg19  \
#      out \
#      -size 200 \
#      -p "$NTHREADS" \
#      -preparsedDir "$TMP"




# for fn in $(find $WD -iname probes.bed)
# do
#     d=$(dirname $fn)
#     perl /soft/bio/homer-4.7/bin/findMotifsGenome.pl \
#         "$d"/probes.bed \
#         hg19  \
#         -len 8,10,12 \
#         -size 200 \
#         -p "$NTHREADS" \
#         > "$d"/tfbs/probes_annotated.tab
# done
