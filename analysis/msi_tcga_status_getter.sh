#!/bin/bash
##
## Retrieves the coadread clinical annotations from TCGA and looks
## for microsatellite unstability
##
## Writes to STDOUT a matrix like
# TCGA-D5-6533	Not Available	NA
# TCGA-CM-4748	Not Available	NA
# TCGA-AZ-5403	Not Available	NA
# TCGA-F4-6808	Completed	NO
# TCGA-A6-6782	Not Available	NA
##
## Izaskun Mallona, GPLv2
## 24th march 2016

OD=~/msi_tcga

cd $OD

echo 'Query for clinical data'
# https://tcga-data.nci.nih.gov/tcga/damws/jobprocess/xml?&disease=COAD&platformType=-999&preservationMethodCode=Frozen&protectedStatus=N&sampleList=%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%2C

# tar xvf 46b7dffc-63d7-48cb-b5cc-b4d0c17abc43.tar

files=$(find Clinical -name "*clinical*xml")

regex_id='.*<shared:bcr_patient_barcode preferred_name.*>(TCGA-.*{12})</shared:bcr_patient_barcode>'
regex_msi='.*<coad_read_shared:microsatellite_instability preferred_name="microsatellite_instability".*procurement_status="(.*{10,13}).*" restricted="false" source_system_identifier="[0-9]+"[ />]{1,3}([YES|NO]{0,3}).*'

printf "%s\t%s\t%s\n" "sample_id" "analysis_completed" "msi_status"
for f in $files
do
    while read line
    do

        if [[ $line =~ $regex_id ]]
        then
            sample_id="${BASH_REMATCH[1]}"
        else
            
            if [[ $line =~ $regex_msi ]]
            then
                # echo "${BASH_REMATCH[0]}"
                completed="${BASH_REMATCH[1]}"
                msi_status="${BASH_REMATCH[2]}"

                ## that is, was non tested
                if [ -z "$msi_status" ]
                then
                    msi_status=NA
                fi
                
                printf "%s\t%s\t%s\n" "$sample_id" "$completed" "$msi_status"
            fi
        fi
    done < "$f"
done


## now for mononucleotide panels
files=$(find Clinical -name "*auxiliary*xml")

regex_id='.*<shared:bcr_patient_barcode.*>(TCGA-.*{12})</shared:bcr_patient_barcode>'
regex_mss='<auxiliary:mononucleotide_and_dinucleotide_marker_panel_analysis_status cde="3226963" xsd_ver="2.4" procurement_status="Completed" owner="">(.*)</auxiliary:mononucleotide_and_dinucleotide_marker_panel_analysis_status>'

printf "%s\t%s\t%s\n" "sample_id" "mss_status"
for f in $files
do
    while read line
    do
        if [[ $line =~ $regex_id ]]
        then
            sample_id="${BASH_REMATCH[1]}"
        else            
            if [[ $line =~ $regex_mss ]]
            then
                completed="${BASH_REMATCH[1]}"
                
                printf "%s\t%s\t%s\n" "$sample_id" "$completed" 
            fi
            
        fi
    done < "$f"
done
