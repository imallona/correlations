#!/bin/bash
#
# dnmts etc and modules association

TASK="modules_association_to_chip"
IHOME=/imppc/labs/maplab/imallona
HOME=/home/labs/maplab/imallona
WD=$HOME/correlations_simpler/$TASK
IWD=$IHOME/correlations_simpler/$TASK
COMMUNITIES_WD="$IHOME"/correlations/colonomics_communities_direct_annotation

BEDTOOLS=/software/debian-8/bio/bedtools2-2.26.master/bin/bedtools

mkdir -p $WD $IWD
cd $IWD

# https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE38938
wget https://www.ncbi.nlm.nih.gov/geo/download/?acc=GSE38938&format=file

tar xvf GSE38938_RAW.tar
gunzip *bed.gz

cd ..

mysql --user=genome --host=genome-mysql.cse.ucsc.edu -A -e  \
      "select chrom, size from hg19.chromInfo" | \
    sort -k1,1  | \
    grep -P "^chr[0-9]{1,2}\t"  > hg19.genome
   

psql -d colonomics_correlations -t -A -F$'\t' \
     -c "select concat('chr', chromosome), meth_start, meth_end, probe 
         from annotation.humanmethylation450_probeinfo a,
              colonomics_tumor.meth_probe_standard_deviation s
         where a.probe = s.meth_probe and
              s.sd >= 0.05;" | fgrep cg | $BEDTOOLS sort > chip.bed

sort -k1,1 -k2,2n -i hg19.genome > foo
mv foo hg19.genome

$BEDTOOLS genomecov -i chip.bed -g hg19.genome > chip.genomecov.bed

 awk '
{ 
  OFS=FS="\t";
  if ($2=="1") print $1,$3;

}
' chip.genomecov.bed > with_sd.genome

for comm  in 1 2 3 4 5 8 12 13 26 32 41 46 47 64 71 83 120 152 174 \
              184 190 192 198 218 270 276 322 327 598 2352 2992 3237
do

    ln -s "$COMMUNITIES_WD"/tumor/tumor_comm_"$comm"/probes.bed probes_"$comm".bed
    sort -k 1,1 -k2,2n probes_"$comm".bed > probes_"$comm"_sorted.bed

    for annot in $(find . -name "GSM*bed" ! -name "probe*")
    do
        echo $annot
       
        
        grep -P "^chr[0-9]{1,2}\t" "$annot" | $BEDTOOLS sort > "$(basename $annot .bed)".nosex

        # "$BEDTOOLS" reldist \
        #             -a probes_"$comm"_sorted.bed \
        #             -b "$(basename $annot .bed)".nosex > \
        #             "$(basename $comm .bed)"_"$(basename $annot .bed)".reldist

        # "$BEDTOOLS" fisher \
        #             -a probes_"$comm"_sorted.bed \
        #             -b "$(basename $annot .bed)".nosex \
        #             -g with_sd.genome > \
        #             "$(basename $comm .bed)"_"$(basename $annot .bed)".fisher

        ## please note chip.bed already includes a sd threshold
        "$BEDTOOLS" intersect \
                    -b "$(basename $annot .bed)".nosex \
                    -a chip.bed \
                    -wa -sorted | \
            "$BEDTOOLS" fisher \
                        -a probes_"$comm"_sorted.bed \
                        -b - \
                        -g with_sd.genome > \
                        "$(basename $comm .bed)"_"$(basename $annot .bed)".fisher
    done

done
