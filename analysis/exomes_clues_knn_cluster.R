#!/usr/bin/env R
##
## Looks for somatic mutations enrichment according to the clues/knn methylation
##   profile membership
##
## 11th Nov 2016
## Izaskun Mallona, GPL v2
## Update 3rd Jan 2017

## Rather use exomes_clues_knn_cluster.Rmd instead
