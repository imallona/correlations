#!/bin/bash
# 
# nci 60 datasets to check meth behaviour as compared to cometh modules
#
# 15th jan 2018

TASK="nci60"
IHOME=/imppc/labs/maplab/imallona
WD=~/correlations_simpler/"$TASK"
NFS_WD="$IHOME"/correlations_simpler/"$TASK"

mkdir -p $WD $NFS_WD

cd $WD

wget https://discover.nci.nih.gov/cellminerdata/normalizedArchives/nci60_DNA__Illumina_450K_methylation_Beta_values.zip

unzip nci60_DNA__Illumina_450K_methylation_Beta_values.zip
