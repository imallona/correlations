#!/bin/bash
# are some community members enriched for colon dmrs?
#
# Izaskun Mallona
# 4th oct 2017

# /imppc/labs/maplab/imallona/src/correlations/hansen_blocks.sh


TASK="dmrs_overlap_by_community"
IHOME=/imppc/labs/maplab/imallona
WD=~/correlations_simpler/"$TASK"
NFS_WD=$IHOME/correlations_simpler/"$TASK"
BEDTOOLS=/software/debian-8/bio/bedtools2-2.26.master/bin/bedtools
COMMUNITIES_WD="$IHOME"/correlations/colonomics_communities_direct_annotation
## the upper bedtools messes up the number of db intervals, they do not match
# to the real thing (noticed 13th dec 2017) 
OLDBEDTOOLS=/software/debian-8/bio/bedtools2-2.23.0/bin/bedtools 

mkdir -p $NFS_WD

cd $NFS_WD

## community members bedfiles
## already calculated when dealing with homer annotation


## trans communities only, as in
# /imppc/labs/maplab/imallona/correlations_simpler/trans_communities_subset/

for comm in 1 2 3 4 5 8 12 13 26 32 41 46 47 64 71 83 120 152 174 184 190 192 198 218 \
              270 276 322 327 598 2352 2992 3237 
do
    ln -s "$COMMUNITIES_WD"/tumor/tumor_comm_"$comm"/probes.bed probes_"$comm".bed
    sort -k 1,1 -k2,2n probes_"$comm".bed > probes_"$comm"_sorted.bed

done

    

## hansen bedfiles

# hansen's vmrs and blocks
wget http://www.nature.com/ng/journal/v43/n8/extref/ng.865-S2.xls
wget http://www.nature.com/ng/journal/v43/n8/extref/ng.865-S3.xls

# fortin's compartments
wget https://static-content.springer.com/esm/art%3A10.1186%2Fs13059-015-0741-y/MediaObjects/13059_2015_741_MOESM2_ESM.txt

echo 'export to tsvs'


awk '{
  OFS=FS="\t"; 
  if($10=="hyper"){
    print $1,$2,$3,"1.0","85,0,0",$10
  } else if ($10=="hypo") {
    print $1,$2,$3,"1.0","86,86,149",$10
  }
}' ng.865-S2.tsv | sortBed >  ng.865-S2.bedgraph

cut -f1,2,3,4 ng.865-S2.bedgraph > ng.865-S2.bed

## same for vmrs

awk '{
  OFS=FS="\t"; 
  if($11=="boundaryShift"){
    print $1,$2,$3,"1.0","255,170,170",$11
  } else if ($11=="lossOfRegulation") {
    print $1,$2,$3,"1.0","212,106,106",$11
  } else if ($11=="novelMethylation") {
    print $1,$2,$3,"1.0","85,0,0",$11
  } else if ($11=="other") {
    print $1,$2,$3,"1.0","128,21,21",$11
  }
}' ng.865-S3.tsv | sortBed >  ng.865-S3.bedgraph

cut -f1,2,3,4 ng.865-S3.bedgraph > ng.865-S3.bed

# fortin's compartments

fgrep coad 13059_2015_741_MOESM2_ESM.txt | cut -f2-6 > fortin.bedgraph
fgrep open fortin.bedgraph > open_fortin.bedgraph
fgrep closed fortin.bedgraph > closed_fortin.bedgraph

## let's tokenize the dmrs, too
for item in hypo hyper
do
    fgrep "$item" ng.865-S2.bedgraph > hansen_"$item".bedgraph
done

for item in boundaryShift lossOfRegulation novelMethylation other
do
    fgrep "$item" ng.865-S3.bedgraph > hansen_"$item".bedgraph
done

## building masks (genome to covered by the 450k) to be used with regioner start

psql -d colonomics_correlations -t -A -F$'\t' \
     -c "select concat('chr', chromosome), meth_start, meth_end, probe 
         from annotation.humanmethylation450_probeinfo a,
              colonomics_tumor.meth_probe_standard_deviation s
         where a.probe = s.meth_probe and
              s.sd >= 0.05;" | fgrep cg | $BEDTOOLS sort > chip.bed

grep -P "^chr[0-9]{1,2}\t" chip.bed > chip

mv chip chip.bed

wc -l chip.bed

$BEDTOOLS complement -i chip.bed -g hg19.genome > chip_complement.bed

## building masks end


# bedtools reldist


for comm in 1 2 3 4 5 8 12 13 26 32 41 46 47 64 71 83 120 152 174 184 190 192 198 218 \
              270 276 322 327 598 2352 2992 3237 
do
    mkdir -p "$comm"
    for annot in open_fortin.bedgraph closed_fortin.bedgraph \
                                      hansen_*graph
    do
        "$BEDTOOLS" reldist \
                    -a probes_"$comm"_sorted.bed \
                    -b "$annot" > \
                    "$comm"/"$(basename $comm .bed)"_"$(basename $annot .bedgraph)".reldist
    done     

done


# bedtools fisher

# mysql --user=genome --host=genome-mysql.cse.ucsc.edu -A -e  \
#       "select chrom, size from hg19.chromInfo" | \
#     awk '{FS=OFS="\t"; print $1,"0",$2}' | \
#     sort -k1,1  | \
#          grep -P "^chr[0-9]{1,2}\t"  > hg19.genome

mysql --user=genome --host=genome-mysql.cse.ucsc.edu -A -e  \
      "select chrom, size from hg19.chromInfo" | \
    sort -k1,1  | \
    grep -P "^chr[0-9]{1,2}\t"  > hg19.genome
   
    
for comm in 1 2 3 4 5 8 12 13 26 32 41 46 47 64 71 83 120 152 174 184 190 192 198 218 \
              270 276 322 327 598 2352 2992 3237 
do
   
    for annot in open_fortin.bedgraph closed_fortin.bedgraph \
                                      hansen_*graph
    do

        grep -P "^chr[0-9]{1,2}\t" "$annot" | $BEDTOOLS sort > "$(basename $annot .bedgraph)".nosex
        
        "$OLDBEDTOOLS" fisher \
                    -a probes_"$comm"_sorted.bed \
                    -b "$(basename $annot .bedgraph)".nosex \
                    -g hg19.genome > \
                    "$comm"/"$(basename $comm .bed)"_"$(basename $annot .bedgraph)".fisher

        # to be parsed with R
        echo one$'\t'two$'\t'three$'\t'four > header
        sed 's/|/\t/g' "$comm"/"$(basename $comm .bed)"_"$(basename $annot .bedgraph)".fisher | \
            tr -d '#' | \
            sed 's/ //g' | \
            sed 's/:/\t/g' | \
            grep -v '_' > bar
                                                    
        cat header bar > "$comm"/"$(basename $comm .bed)"_"$(basename $annot .bedgraph)".fisher.str
        rm bar

        "$BEDTOOLS" reldist \
                    -a probes_"$comm"_sorted.bed \
                    -b "$(basename $annot .bedgraph)".nosex > \
                    "$comm"/"$(basename $comm .bed)"_"$(basename $annot .bedgraph)".nosex.reldist
    done     

done


# read.table('baz', col.names = c(1,2,3,4), header = F, fill = T)
