#!/bin/bash
##
## Assigns an HMM to each location according to a priorization algorithm
## Uses HMM hESC1 data
##
## Based upon /imppc/labs/maplab/imallona/src/nsuma_expression/repeats_hmm_associator.sh
##
## Postprocessed by analysis/hmm_associator_as_in_nsuma_plotter.R
##
## Izaskun Mallona, GPLv3
## 20th jan 2017

## the path the bedfiles are located at
DAT="$IHOME"/correlations_simpler/colonomics_communities_motifs
WD="$IHOME"/correlations_simpler/hmm_as_in_nsuma

HMM=wgEncodeAwgSegmentationChromhmmH1hesc.bed

hmms=(Active_Promoter Candidate_Strong_enhancer Candidate_Weak_enhancer_DNase CTCF_Candidate_Insulator Heterochromatin_Repetitive_Copy_Number_Variation Inactive_Promoter Low_activity_proximal_to_active_states Polycomb_repressed Promoter_Flanking Transcription_associated)

SEED=1


mkdir -p $WD
cd $WD

echo Renaming input bedfiles
# BEDS=(foo.bed bar.bed)
cd $DAT
for fn in $(find . -iname "probes.bed")
do
    bn=$(dirname $fn)
    cp "$fn" "$WD"/"$bn"".bed"
    # echo "$bn"
done

cd $WD

echo 'Retrieving hg19 stats'
mysql --user=genome --host=genome-mysql.cse.ucsc.edu -A -e \
      "select chrom, size from hg19.chromInfo" > ${WD}/hg19.genome


echo 'Retrieving chromatin colors'
mysql --user=genome --host=genome-mysql.cse.ucsc.edu -A -e \
      'SELECT chrom, chromStart, chromEnd, name 
     FROM hg19.wgEncodeAwgSegmentationChromhmmH1hesc;' | awk  '{if (NR!=1) {print}}' > "$WD"/"$HMM"

echo 'Grouping colors'

sed -i 's/TssF/Active_Promoter/g' "$HMM"
sed -i 's/Tss/Active_Promoter/g' "$HMM"
sed -i 's/PromF/Promoter_Flanking/g' "$HMM"
sed -i 's/PromP/Inactive_Promoter/g' "$HMM"
sed -i 's/EnhWF/Candidate_Weak_enhancer_DNase/g' "$HMM"
sed -i 's/EnhW/Candidate_Weak_enhancer_DNase/g' "$HMM"
sed -i 's/EnhF/Candidate_Strong_enhancer/g' "$HMM"
sed -i 's/Enh/Candidate_Strong_enhancer/g' "$HMM"
sed -i 's/EnF/Candidate_Strong_enhancer/g' "$HMM"
sed -i 's/DnaseU/Candidate_Weak_enhancer_DNase/g' "$HMM"
sed -i 's/DnaseD/Candidate_Weak_enhancer_DNase/g' "$HMM"
sed -i 's/FaireW/Candidate_Weak_enhancer_DNase/g' "$HMM"
sed -i 's/CtcfO/CTCF_Candidate_Insulator/g' "$HMM"
sed -i 's/Ctcf/CTCF_Candidate_Insulator/g' "$HMM"
sed -i "s/Gen5'/Transcription_associated/g" "$HMM"
sed -i "s/ElonW/Transcription_associated/g" "$HMM"
sed -i "s/Elon/Transcription_associated/g" "$HMM"
sed -i "s/Gen3'/Transcription_associated/g" "$HMM"
sed -i "s/Pol2/Transcription_associated/g" "$HMM"
sed -i "s/H4K20/Transcription_associated/g" "$HMM"
sed -i "s/Low/Low_activity_proximal_to_active_states/g" "$HMM"
sed -i "s/ReprD/Polycomb_repressed/g" "$HMM"
sed -i "s/ReprW/Polycomb_repressed/g" "$HMM"
sed -i "s/Repr/Polycomb_repressed/g" "$HMM"
sed -i "s/Quies/Heterochromatin_Repetitive_Copy_Number_Variation/g" "$HMM"
sed -i "s/Art/Heterochromatin_Repetitive_Copy_Number_Variation/g" "$HMM"

echo 'Assigning a weight to each color'

cp $HMM ${HMM}_priorized

sed -i "s/CTCF_Candidate_Insulator/1/g" ${HMM}_priorized
sed -i "s/Active_Promoter/2/g" ${HMM}_priorized
sed -i "s/Inactive_Promoter/3/g" ${HMM}_priorized
sed -i "s/Candidate_Strong_enhancer/4/g" ${HMM}_priorized
sed -i "s/Candidate_Weak_enhancer_DNase/5/g" ${HMM}_priorized
sed -i "s/Promoter_Flanking/6/g" ${HMM}_priorized
sed -i "s/Transcription_associated/7/g" ${HMM}_priorized
sed -i "s/Low_activity_proximal_to_active_states/8/g" ${HMM}_priorized
sed -i "s/Polycomb_repressed/9/g" ${HMM}_priorized
sed -i "s/Heterochromatin_Repetitive_Copy_Number_Variation/10/g" ${HMM}_priorized

cut -f4 wgEncodeAwgSegmentationChromhmmH1hesc.bed  | sort | uniq -c

echo 'Sorting'

for fn in $(find . -name "*bed") 
do

    sort -k1,1 -k2,2n $fn > foo; mv foo $fn

done


cd ${WD}
echo 'Chromatin coloring'
## only elements with the highest priority are taken into account
## hierarchy by hmm
beds=( $(find . -name "tumor_comm*bed" -type f | sort -V) )

# for bed in $(find . -name "tumor_comm*bed" -type f | sort -V)
for bed in ${beds[@]}
do
    
    if [  -f $bed ]
    then
        
        bedtools intersect -wa -wb -a $bed \
                 -b "$HMM"_priorized >  ${bed}_colored.bed

        bedtools groupby -i ${bed}_colored.bed \
                 -g 1-3 -c 7 -o min > ${bed}_colored_reduced.bed

        rm  ${bed}_colored.bed
        ## getting the states back
        sed -i "s/1$/CTCF_Candidate_Insulator/g" ${bed}_colored_reduced.bed
        sed -i "s/2$/Active_Promoter/g" ${bed}_colored_reduced.bed
        sed -i "s/3$/Inactive_Promoter/g" ${bed}_colored_reduced.bed
        sed -i "s/4$/Candidate_Strong_enhancer/g" ${bed}_colored_reduced.bed
        sed -i "s/5$/Candidate_Weak_enhancer_DNase/g" ${bed}_colored_reduced.bed
        sed -i "s/6$/Promoter_Flanking/g" ${bed}_colored_reduced.bed
        sed -i "s/7$/Transcription_associated/g" ${bed}_colored_reduced.bed
        sed -i "s/8$/Low_activity_proximal_to_active_states/g" ${bed}_colored_reduced.bed
        sed -i "s/9$/Polycomb_repressed/g" ${bed}_colored_reduced.bed
        sed -i "s/10$/Heterochromatin_Repetitive_Copy_Number_Variation/g" ${bed}_colored_reduced.bed
    fi
done

wc -l *bed

printf "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n" \
       "category" \
       "n" \
       "Active_Promoter" \
       "Candidate_Strong_enhancer" \
       "Candidate_Weak_enhancer_DNase" \
       "CTCF_Candidate_Insulator" \
       "Heterochromatin_Repetitive_Copy_Number_Variation" \
       "Inactive_Promoter" \
       "Low_activity_proximal_to_active_states" \
       "Polycomb_repressed"\
       "Promoter_Flanking" \
       "Transcription_associated" > communities_by_hmm.tsv

for bed in ${beds[@]}
do
    if [ -f $bed ]
    then
        curr=${bed}_colored_reduced.bed
        n=$(wc -l $curr | cut -d" " -f1)

        for hmm in ${hmms[@]}
                   
        do
            printf -v $hmm $(grep -c $hmm $curr)
        done

        printf "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n" \
               "$bed" \
               "$n" \
               "$Active_Promoter" \
               "$Candidate_Strong_enhancer" \
               "$Candidate_Weak_enhancer_DNase" \
               "$CTCF_Candidate_Insulator" \
               "$Heterochromatin_Repetitive_Copy_Number_Variation" \
               "$Inactive_Promoter" \
               "$Low_activity_proximal_to_active_states" \
               "$Polycomb_repressed"\
               "$Promoter_Flanking" \
               "$Transcription_associated"
    fi            
done >>  communities_by_hmm.tsv

echo 'Background stuff, whole genome and scrutinized by infinium (to compare against afterwards)'


echo 'Generating a whole genome background to compare against'

cp wgEncodeAwgSegmentationChromhmmH1hesc.bed_priorized genome_wide.bed 

sed -i "s/1$/CTCF_Candidate_Insulator/g" genome_wide.bed
sed -i "s/2$/Active_Promoter/g" genome_wide.bed
sed -i "s/3$/Inactive_Promoter/g" genome_wide.bed
sed -i "s/4$/Candidate_Strong_enhancer/g" genome_wide.bed
sed -i "s/5$/Candidate_Weak_enhancer_DNase/g" genome_wide.bed
sed -i "s/6$/Promoter_Flanking/g" genome_wide.bed
sed -i "s/7$/Transcription_associated/g" genome_wide.bed
sed -i "s/8$/Low_activity_proximal_to_active_states/g" genome_wide.bed
sed -i "s/9$/Polycomb_repressed/g" genome_wide.bed
sed -i "s/10$/Heterochromatin_Repetitive_Copy_Number_Variation/g" genome_wide.bed

ln -s genome_wide.bed genome_wide.bed_colored_reduced.bed

echo 'Generating a 450 chip-locations to compare against'

psql -d regional_profile \
     -U imallona \
     -h overlook \
     -t -A -F $'\t' -c \
     "select chr,cg_start,cg_end from annotations.humanmethylation450kannot" > infinium.bed


bed=infinium.bed
bedtools intersect -wa -wb -a $bed \
         -b "$HMM"_priorized >  ${bed}_colored.bed

bedtools groupby -i ${bed}_colored.bed \
         -g 1-3 -c 7 -o min > ${bed}_colored_reduced.bed

sed -i "s/1$/CTCF_Candidate_Insulator/g" ${bed}_colored_reduced.bed
sed -i "s/2$/Active_Promoter/g" ${bed}_colored_reduced.bed
sed -i "s/3$/Inactive_Promoter/g" ${bed}_colored_reduced.bed
sed -i "s/4$/Candidate_Strong_enhancer/g" ${bed}_colored_reduced.bed
sed -i "s/5$/Candidate_Weak_enhancer_DNase/g" ${bed}_colored_reduced.bed
sed -i "s/6$/Promoter_Flanking/g" ${bed}_colored_reduced.bed
sed -i "s/7$/Transcription_associated/g" ${bed}_colored_reduced.bed
sed -i "s/8$/Low_activity_proximal_to_active_states/g" ${bed}_colored_reduced.bed
sed -i "s/9$/Polycomb_repressed/g" ${bed}_colored_reduced.bed
sed -i "s/10$/Heterochromatin_Repetitive_Copy_Number_Variation/g" ${bed}_colored_reduced.bed


printf "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n" \
       "category" \
       "n" \
       "Active_Promoter" \
       "Candidate_Strong_enhancer" \
       "Candidate_Weak_enhancer_DNase" \
       "CTCF_Candidate_Insulator" \
       "Heterochromatin_Repetitive_Copy_Number_Variation" \
       "Inactive_Promoter" \
       "Low_activity_proximal_to_active_states" \
       "Polycomb_repressed"\
       "Promoter_Flanking" \
       "Transcription_associated" > backgrounds_by_hmm.tsv



for bed in infinium.bed genome_wide.bed
do
    if [ -f $bed ]
    then
        curr=${bed}_colored_reduced.bed
        n=$(wc -l $curr | cut -d" " -f1)

        for hmm in ${hmms[@]}
                   
        do
            printf -v $hmm $(grep -c $hmm $curr)
        done

        printf "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n" \
               "$bed" \
               "$n" \
               "$Active_Promoter" \
               "$Candidate_Strong_enhancer" \
               "$Candidate_Weak_enhancer_DNase" \
               "$CTCF_Candidate_Insulator" \
               "$Heterochromatin_Repetitive_Copy_Number_Variation" \
               "$Inactive_Promoter" \
               "$Low_activity_proximal_to_active_states" \
               "$Polycomb_repressed"\
               "$Promoter_Flanking" \
               "$Transcription_associated"
    fi            
done >>  backgrounds_by_hmm.tsv


## update 5th dec 2017: adding top connected probes

cd $WD

ln -s "$IHOME"/correlations_simpler/imprinting/top_percentile_trans.bed



bed=top_percentile_trans.bed
cut -f1-3 $bed | sortBed > foo
mv foo $bed

bedtools intersect -wa -wb -a $bed \
         -b "$HMM"_priorized >  ${bed}_colored.bed

bedtools groupby -i ${bed}_colored.bed \
         -g 1-3 -c 7 -o min > ${bed}_colored_reduced.bed

sed -i "s/1$/CTCF_Candidate_Insulator/g" ${bed}_colored_reduced.bed
sed -i "s/2$/Active_Promoter/g" ${bed}_colored_reduced.bed
sed -i "s/3$/Inactive_Promoter/g" ${bed}_colored_reduced.bed
sed -i "s/4$/Candidate_Strong_enhancer/g" ${bed}_colored_reduced.bed
sed -i "s/5$/Candidate_Weak_enhancer_DNase/g" ${bed}_colored_reduced.bed
sed -i "s/6$/Promoter_Flanking/g" ${bed}_colored_reduced.bed
sed -i "s/7$/Transcription_associated/g" ${bed}_colored_reduced.bed
sed -i "s/8$/Low_activity_proximal_to_active_states/g" ${bed}_colored_reduced.bed
sed -i "s/9$/Polycomb_repressed/g" ${bed}_colored_reduced.bed
sed -i "s/10$/Heterochromatin_Repetitive_Copy_Number_Variation/g" ${bed}_colored_reduced.bed


printf "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n" \
       "category" \
       "n" \
       "Active_Promoter" \
       "Candidate_Strong_enhancer" \
       "Candidate_Weak_enhancer_DNase" \
       "CTCF_Candidate_Insulator" \
       "Heterochromatin_Repetitive_Copy_Number_Variation" \
       "Inactive_Promoter" \
       "Low_activity_proximal_to_active_states" \
       "Polycomb_repressed"\
       "Promoter_Flanking" \
       "Transcription_associated" > top_connected_by_hmm.tsv


curr=${bed}_colored_reduced.bed
n=$(wc -l $curr | cut -d" " -f1)

for hmm in ${hmms[@]}
           
do
    printf -v $hmm $(grep -c $hmm $curr)
done

printf "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n" \
       "$bed" \
       "$n" \
       "$Active_Promoter" \
       "$Candidate_Strong_enhancer" \
       "$Candidate_Weak_enhancer_DNase" \
       "$CTCF_Candidate_Insulator" \
       "$Heterochromatin_Repetitive_Copy_Number_Variation" \
       "$Inactive_Promoter" \
       "$Low_activity_proximal_to_active_states" \
       "$Polycomb_repressed"\
       "$Promoter_Flanking" \
       "$Transcription_associated" >> top_connected_by_hmm.tsv
