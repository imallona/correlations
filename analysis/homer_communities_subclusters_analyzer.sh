#!/bin/bash

WD="$IHOME"/correlations/colonomics_communities_clusters
DATASETS="normal tumor"
NUM=10 # of clusters and communities

## bedfile generation
## commented because was already run
echo 'Check if the Rscript communities_subclusters_coordinates_getter.R is needed or not (and toggle comment)'
# Rscript $SRC/correlations/analysis/communities_subclusters_coordinates_getter.R
## homer-based annotation

## this is only for correlative clusters and communities

# for dataset in ${DATASETS[@]}
# do
#     for comm in $(seq 1 $NUM)
#     do 
#         for clust in $(seq 1 $NUM)
#         do
#             perl /soft/bio/homer-4.7/bin/annotatePeaks.pl \
#                 "$WD"/"$dataset"/"$dataset"_comm_"$comm"_clust_"$clust"/probes.bed \
#                 hg19  \
#                 -annStats "$WD"/"$dataset"/"$dataset"_comm_"$comm"_clust_"$clust"/probes_annotation_stats.bed \
#                 -go "$WD"/"$dataset"/"$dataset"_comm_"$comm"_clust_"$clust"/go \
#                 > "$WD"/"$dataset"/"$dataset"_comm_"$comm"_clust_"$clust"/probes_annotated.tab
#         done
#     done  
# done


for fn in $(find $WD -iname probes.bed)
do
    d=$(dirname $fn)
    perl /soft/bio/homer-4.7/bin/annotatePeaks.pl \
        "$d"/probes.bed \
        hg19  \
        -annStats "$d"/probes_annotation_stats.bed \
        -go "$d"/go \
        > "$d"/probes_annotated.tab
done

echo 'Check whether the Rscript plot_annotation_stats_homer.R is needed or not (and toggle comment)'
# Rscript plot_annotation_stats_homer.R
