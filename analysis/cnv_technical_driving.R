#!/usr/bin/env/R
##
## are CNV driving cometh modules?
##
## To measure the influence of copy number variation in DNA co-methylation structure,
## we evaluated the DNA methylation status of co-methylating CpGs according to their
## local CNV status (gain, no change, loss). As some modules show opposite co-methylation trends,
## we ran the analysis for each co-methylation module independently. For the sake of simplicity
## we picked a maximum number of five overlapping minimal recurrent regions per module.
##
##
## Izaskun Mallona
## 05 sept 2018

## library(pheatmap)
library(ggplot2)

TASK ='cnv_technical_driving'
HOME = '/home/labs/maplab/imallona'
IHOME = '/imppc/labs/maplab/imallona'
WD = file.path(IHOME, 'correlations_simpler', TASK)
NFS_WD = file.path(IHOME, 'correlations_simpler', TASK)

SCHEMA <- 'colonomics_tumor'  ## schema the module detection was carried out with
INNER_MIN_DEGREE <- 1         ## first prunning (in-chromosome prunning)
MIN_DEGREE <- 1               ## vertex min degree
MIN_SIZE <- 10                ## community min size
MIN_RHO <- 0.8                ## minimum rho coefficient value to fetch the correlation
LABEL <- sprintf('%s_inner_min_degree_%s_min_degree_%s_min_size_%s_min_rho_%s_positive_only',
                 SCHEMA, INNER_MIN_DEGREE, MIN_DEGREE, MIN_SIZE, MIN_RHO)

## trans-communities as produced by trans_communities_subsetter.R
MEMBERSHIP <- file.path(IHOME,
                        'correlations',
                        'colonomics_rho_restrictive_communities',
                        LABEL,
                        sprintf('%s_comm_membership.tsv', LABEL))

                                        # trans communities
TRANS <- c(1, 2, 3, 4, 5, 8, 12, 13, 26, 32, 41, 46, 47, 64, 71, 83, 120, 152, 174,
           184, 190, 192, 198, 218, 270, 276, 322, 327, 598, 2352, 2992, 3237)

beta2m <- function(beta) {
    m <- log2(beta/(1 - beta))
    return(m)
}

m2beta <- function(m) {
    beta <- 2^m/(2^m + 1)
    return(beta)
}

##

dir.create(WD)
dir.create(NFS_WD)

setwd(WD)

partition <- read.table(MEMBERSHIP, header = TRUE)
partition$probe <- row.names(partition)

load(file.path(IHOME, 'data', 'colonomics', 'methylation', 'betas-filtQC-140425.RData'))

load(file.path(IHOME, 'data', 'colonomics', 'cnv', 'CLX.RData'))

cnv <- minimal
rownames(cnv) <- sprintf('chr%s:%s-%s', minimal$Chromosome, minimal$Start, minimal$End)
colnames(cnv)[4:ncol(cnv)] <- paste0(colnames(cnv)[4:ncol(cnv)], '_T')

meth <- betas[,grep('.*_T$', colnames(betas), perl = TRUE)]

common <- intersect(colnames(meth), colnames(cnv))

## list of minimal recurrent regions (mrrs)
lmin <- list()
for (module in TRANS) {
    module <- as.character(module)
    lmin[[module]] <- list()
    probes <- partition[partition$x == module, 'probe']
    probes_loci <- a450cpg[probes, c('IlmnID', 'CHR', 'MAPINFO')]
    related <- NULL
    for (probe in probes) {
        related <-  rownames(
            cnv[(cnv$Chromosome == probes_loci[probe, 'CHR']) &
                (probes_loci[probe, 'MAPINFO'] >= cnv$Start) &
                (probes_loci[probe, 'MAPINFO'] < cnv$End),]) 

        if (length(related) > 0) {
            lmin[[module]][[related]] <- c(lmin[[module]][[related]], probe)
        }
        related <- NULL
    }

    ## getting the dnameth stats according to the mrr and its gain/loss/nc status,
    ## this is done module wise due to the fact mod1 and mod2 are antimeth etc
    ## since highly correlated we pick only the first CpG
    for (mrr in names(lmin[[module]])) {
        curr_probes <- lmin[[module]][[mrr]][1]
        gained <- common[as.numeric(cnv[mrr, common]) == 1]
        kept <- common[as.numeric(cnv[mrr, common]) == 0]
        lost <- common[as.numeric(cnv[mrr, common]) == -1]
        
        curr_meth = list(lost = meth[curr_probes, lost]/1000,
                         kept = meth[curr_probes,kept]/1000,
                         gained = meth[curr_probes,gained]/1000)
        lmin[[module]][[mrr]] <- curr_meth
        
    }
    
    
}

save(lmin, file = file.path(WD, 'lmin.RData'))


plots <- list()
for (module in names(lmin)) {

    ## curr <- rbindlist(lmin[[module]], fill=TRUE)
    
    ## curr <- unlist(lmin[[module]])
    set.seed(1)
    if (length(lmin[[module]]) > 10) {
        mrrs <- names(lmin[[module]])[sample( length(lmin[[module]]), 10)]
    } else {
        mrrs <- names(lmin[[module]])
    }

    dat <- data.frame(mrr = NULL, meth = NULL, cnv = NULL)
    for (mrr in mrrs){
        
        curr <- unlist(lmin[[module]][[mrr]])
        
        fd <- data.frame(mrr = mrr,
                         meth = curr,
                         cnv = sapply(strsplit(names(curr), split = '.', fixed = TRUE),
                                      function(x) return(x[1])))

        dat <- rbind(dat, fd)
        
    }
    dat$cnv <- factor( dat$cnv , c('lost', 'kept', 'gained'))
    plots[[module]] <- ggplot(dat, aes(x = mrr, y = meth, fill = cnv)) + 
        geom_boxplot()  + geom_point(shape = 16, position=position_jitterdodge()) +
        ggtitle(sprintf("Module %s overlapping MRRs", module)) +
        xlab("MRR") + ylab("DNA meth (beta value)") + ylim(0, 1) +
        theme(axis.text.x = element_text(angle = 90, hjust = 1))

}

pdf("cnv.pdf")
invisible(lapply(plots, print))
dev.off()


## using the whole dataset

plots <- list()
models <- list()
for (module in names(lmin)) {
    tryCatch({

        ## curr <- rbindlist(lmin[[module]], fill=TRUE)
        
        ## curr <- unlist(lmin[[module]])
        dat <- data.frame(mrr = NULL, meth = NULL, cnv = NULL)

        set.seed(1)
        if (length(lmin[[module]]) > 50) {
            mrrs <- names(lmin[[module]])[sample( length(lmin[[module]]), 50)]
        } else {
            mrrs <- names(lmin[[module]])
        }
        
        for (mrr in mrrs){
            
            curr <- unlist(lmin[[module]][[mrr]])
            
            fd <- data.frame(mrr = mrr,
                             meth = curr,
                             cnv = sapply(strsplit(names(curr), split = '.', fixed = TRUE),
                                          function(x) return(x[1])))

            dat <- rbind(dat, fd)
            
        }
        dat$cnv <- factor( dat$cnv , c('lost', 'kept', 'gained'))
        plots[[module]] <- ggplot(dat, aes(x = mrr, y = meth, fill = cnv)) + 
            geom_boxplot()  + geom_point(shape = 16, position=position_jitterdodge()) +
            ggtitle(sprintf("Module %s overlapping MRRs", module)) +
            xlab("MRR") + ylab("DNA meth (beta value)") + ylim(0, 1) +
            theme(axis.text.x = element_text(angle = 90, hjust = 1))

        ggsave(file.path(WD, sprintf('cnv_50_mrrs_module_%s.png', module)), height = 7, width = 21)

        dat$cnv <- relevel(dat$cnv, 'kept')
        models[[module]] <- lm(beta2m(meth) ~ cnv+mrr, data = dat)
    }, error = function(x) print('skipped'))
}

## png("cnv_all_.png")
## invisible(lapply(plots, print))
## dev.off()

for (fit in models) {
    coefficients(fit) # model coefficients    
    confint(fit, level=0.95) 
    fitted(fit) 
    residuals(fit)
    anova(fit) 
    ## vcov(fit) # covariance matrix for model parameters 
    plot(fit)
}


## rather focus on mod1 vs mod2 and the gain/loss impact

for (module in c('1', '2')) {
    dat <- data.frame(mrr = NULL, meth = NULL, cnv = NULL)
    for (mrr in names(lmin[[module]])){
        
        curr <- unlist(lmin[[module]][[mrr]])
        
        fd <- data.frame(mrr = mrr,
                         meth = curr,
                         cnv = sapply(strsplit(names(curr), split = '.', fixed = TRUE),
                                      function(x) return(x[1])))

        dat <- rbind(dat, fd) ## @todo fix, extremely unefficient
        
    }
    dat$cnv <- factor( dat$cnv , c('lost', 'kept', 'gained'))
    
    dat$cnv <- relevel(dat$cnv, 'kept')
    m1 <- lm(beta2m(meth) ~ cnv, data = dat)
    m2 <- update(m1, . ~ . + mrr, data = dat)
    print(anova(m1, m2))
    print(head(confint(m2)))
    
}
