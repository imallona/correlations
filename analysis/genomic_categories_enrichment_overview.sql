-- COAD dataset exploration in terms of ensGene categories (exon, intron..) overepresentation
-- 
-- Izaskun Mallona, GPL v2
--
-- Fri Dec 12 12:51:08 CET 2014


-- CREATE OR REPLACE VIEW analysis.genomic_categories_pairs AS (
--   SELECT COUNT(*), 
--       CASE WHEN c.correlation >= 0.5 THEN 'pos'
--             WHEN c.correlation <=0.5 THEN 'neg'
--       END AS correlation,

--       CASE WHEN g_a.coding_exons >= 1 THEN 'coding_exon'
--            WHEN g_a.cpgislandext >=1 THEN 'cpgislandext' 
--            WHEN g_a.five_utr_exons >= 1 THEN 'five_utr_exon'
--            WHEN g_a.introns >= 1 THEN 'intron'
--            WHEN g_a.rmsk >= 1 THEN 'repeat'  
--            WHEN g_a.three_utr_exons >= 1 THEN 'three_utr_exon' 
--            ELSE 'none'  
--       END AS type_a,

--       CASE WHEN g_b.coding_exons >= 1 THEN 'coding_exon'
--            WHEN g_b.cpgislandext >=1 THEN 'cpgislandext'  
--            WHEN g_b.five_utr_exons >= 1 THEN 'five_utr_exon'
--            WHEN g_b.introns >= 1 THEN 'intron' 
--            WHEN g_b.rmsk >= 1 THEN 'repeat'      
--            WHEN g_b.three_utr_exons >= 1 THEN 'three_utr_exon'
--            ELSE 'none'
--       END AS type_b


--    FROM coad.meth_filtered_correlations c, 
--         annotations.infinium_probes_by_ensgene_hg19_genomic_categories g_a,
--         annotations.infinium_probes_by_ensgene_hg19_genomic_categories g_b

--    WHERE  c.meth_probe_a = g_a.probe
--         AND c.meth_probe_b = g_b.probe
--    GROUP BY 
--       CASE WHEN c.correlation >= 0.5 THEN 'pos'
--             WHEN c.correlation <=0.5 THEN 'neg'
--       END,
      
--       CASE WHEN g_a.coding_exons >= 1 THEN 'coding_exon'
--            WHEN g_a.cpgislandext >=1 THEN 'cpgislandext'
--            WHEN g_a.five_utr_exons >= 1 THEN 'five_utr_exon'
--            WHEN g_a.introns >= 1 THEN 'intron'
--            WHEN g_a.rmsk >= 1 THEN 'repeat'  
--            WHEN g_a.three_utr_exons >= 1 THEN 'three_utr_exon'   
--            ELSE 'none'      

--       END,

--       CASE WHEN g_b.coding_exons >= 1 THEN 'coding_exon'
--            WHEN g_b.cpgislandext >=1 THEN 'cpgislandext'   
--            WHEN g_b.five_utr_exons >= 1 THEN 'five_utr_exon'
--            WHEN g_b.introns >= 1 THEN 'intron' 
--            WHEN g_b.rmsk >= 1 THEN 'repeat'      
--            WHEN g_b.three_utr_exons >= 1 THEN 'three_utr_exon'
--            ELSE 'none'    
--       END

-- );

-- -- requires a lot of space in disk, beware!
-- -- \copy (SELECT * FROM analysis.genomic_categories_pairs) TO /imppc/labs/maplab/imallona/correlations/genomic_categories_pairs.tsv WITH delimiter E'\t'


-- -- test end

-- coad start             -----------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION coad_create_views_for_genomic_categories_analysis() RETURNS integer as $$
DECLARE
    view_stmt text;
    -- copy_stmt text;
    
BEGIN
    FOR i in 1..22 LOOP
        view_stmt := format('
CREATE OR REPLACE VIEW analysis.coad_genomic_categories_pairs_%s AS (
  SELECT COUNT(*), 
      CASE WHEN c.correlation >= 0.5 THEN ''pos''
            WHEN c.correlation <=0.5 THEN ''neg''
      END AS correlation,

      CASE WHEN g_a.coding_exons >= 1 THEN ''coding_exon''
           WHEN g_a.cpgislandext >=1 THEN ''cpgislandext'' 
           WHEN g_a.five_utr_exons >= 1 THEN ''five_utr_exon''
           WHEN g_a.introns >= 1 THEN ''intron''
           WHEN g_a.rmsk >= 1 THEN ''repeat''  
           WHEN g_a.three_utr_exons >= 1 THEN ''three_utr_exon'' 
           ELSE ''none''  
      END AS type_a,

      CASE WHEN g_b.coding_exons >= 1 THEN ''coding_exon''
           WHEN g_b.cpgislandext >=1 THEN ''cpgislandext''  
           WHEN g_b.five_utr_exons >= 1 THEN ''five_utr_exon''
           WHEN g_b.introns >= 1 THEN ''intron'' 
           WHEN g_b.rmsk >= 1 THEN ''repeat''      
           WHEN g_b.three_utr_exons >= 1 THEN ''three_utr_exon''
           ELSE ''none''
      END AS type_b


   FROM coad.meth_filtered_correlations_chr%s c, 
        annotations.infinium_probes_by_ensgene_hg19_genomic_categories g_a,
        annotations.infinium_probes_by_ensgene_hg19_genomic_categories g_b

   WHERE  c.meth_probe_a = g_a.probe
        AND c.meth_probe_b = g_b.probe
   GROUP BY 
      CASE WHEN c.correlation >= 0.5 THEN ''pos''
            WHEN c.correlation <=0.5 THEN ''neg''
      END,
      
      CASE WHEN g_a.coding_exons >= 1 THEN ''coding_exon''
           WHEN g_a.cpgislandext >=1 THEN ''cpgislandext''
           WHEN g_a.five_utr_exons >= 1 THEN ''five_utr_exon''
           WHEN g_a.introns >= 1 THEN ''intron''
           WHEN g_a.rmsk >= 1 THEN ''repeat''  
           WHEN g_a.three_utr_exons >= 1 THEN ''three_utr_exon''   
           ELSE ''none''      

      END,

      CASE WHEN g_b.coding_exons >= 1 THEN ''coding_exon''
           WHEN g_b.cpgislandext >=1 THEN ''cpgislandext''   
           WHEN g_b.five_utr_exons >= 1 THEN ''five_utr_exon''
           WHEN g_b.introns >= 1 THEN ''intron'' 
           WHEN g_b.rmsk >= 1 THEN ''repeat''      
           WHEN g_b.three_utr_exons >= 1 THEN ''three_utr_exon''
           ELSE ''none''    
      END

);
        ', i, i);

        EXECUTE view_stmt;

    END LOOP;
    RETURN 0;
END
$$ language 'plpgsql';

SELECT * FROM coad_create_views_for_genomic_categories_analysis();

\copy (SELECT * FROM analysis.coad_genomic_categories_pairs_22) TO '/imppc/labs/maplab/imallona/correlations/genomic_categories_pairs_22.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_genomic_categories_pairs_21) TO '/imppc/labs/maplab/imallona/correlations/genomic_categories_pairs_21.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_genomic_categories_pairs_20) TO '/imppc/labs/maplab/imallona/correlations/genomic_categories_pairs_20.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_genomic_categories_pairs_19) TO '/imppc/labs/maplab/imallona/correlations/genomic_categories_pairs_19.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_genomic_categories_pairs_18) TO '/imppc/labs/maplab/imallona/correlations/genomic_categories_pairs_18.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_genomic_categories_pairs_17) TO '/imppc/labs/maplab/imallona/correlations/genomic_categories_pairs_17.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_genomic_categories_pairs_16) TO '/imppc/labs/maplab/imallona/correlations/genomic_categories_pairs_16.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_genomic_categories_pairs_15) TO '/imppc/labs/maplab/imallona/correlations/genomic_categories_pairs_15.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_genomic_categories_pairs_14) TO '/imppc/labs/maplab/imallona/correlations/genomic_categories_pairs_14.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_genomic_categories_pairs_13) TO '/imppc/labs/maplab/imallona/correlations/genomic_categories_pairs_13.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_genomic_categories_pairs_12) TO '/imppc/labs/maplab/imallona/correlations/genomic_categories_pairs_12.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_genomic_categories_pairs_11) TO '/imppc/labs/maplab/imallona/correlations/genomic_categories_pairs_11.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_genomic_categories_pairs_10) TO '/imppc/labs/maplab/imallona/correlations/genomic_categories_pairs_10.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_genomic_categories_pairs_9) TO '/imppc/labs/maplab/imallona/correlations/genomic_categories_pairs_9.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_genomic_categories_pairs_8) TO '/imppc/labs/maplab/imallona/correlations/genomic_categories_pairs_8.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_genomic_categories_pairs_7) TO '/imppc/labs/maplab/imallona/correlations/genomic_categories_pairs_7.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_genomic_categories_pairs_6) TO '/imppc/labs/maplab/imallona/correlations/genomic_categories_pairs_6.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_genomic_categories_pairs_5) TO '/imppc/labs/maplab/imallona/correlations/genomic_categories_pairs_5.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_genomic_categories_pairs_4) TO '/imppc/labs/maplab/imallona/correlations/genomic_categories_pairs_4.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_genomic_categories_pairs_3) TO '/imppc/labs/maplab/imallona/correlations/genomic_categories_pairs_3.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_genomic_categories_pairs_2) TO '/imppc/labs/maplab/imallona/correlations/genomic_categories_pairs_2.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_genomic_categories_pairs_1) TO '/imppc/labs/maplab/imallona/correlations/genomic_categories_pairs_1.tsv' WITH delimiter E'\t';



CREATE OR REPLACE VIEW analysis.coad_genomic_categories_pairs_of_scrutinized_background AS (
  SELECT COUNT(*), 
      
      CASE WHEN g_a.coding_exons >= 1 THEN 'coding_exon'
           WHEN g_a.cpgislandext >=1 THEN 'cpgislandext' 
           WHEN g_a.five_utr_exons >= 1 THEN 'five_utr_exon'
           WHEN g_a.introns >= 1 THEN 'intron'
           WHEN g_a.rmsk >= 1 THEN 'repeat'  
           WHEN g_a.three_utr_exons >= 1 THEN 'three_utr_exon' 
           ELSE 'none'  
      END AS type

   FROM coad.meth_probe_standard_deviation sd, 
        annotations.infinium_probes_by_ensgene_hg19_genomic_categories g_a


   WHERE  sd.meth_probe = g_a.probe
        AND sd.sd >= 0.05

   GROUP BY 
      CASE WHEN g_a.coding_exons >= 1 THEN 'coding_exon'
           WHEN g_a.cpgislandext >=1 THEN 'cpgislandext'
           WHEN g_a.five_utr_exons >= 1 THEN 'five_utr_exon'
           WHEN g_a.introns >= 1 THEN 'intron'
           WHEN g_a.rmsk >= 1 THEN 'repeat'  
           WHEN g_a.three_utr_exons >= 1 THEN 'three_utr_exon'   
           ELSE 'none'      

      END
);

\copy (SELECT * FROM coad_genomic_categories_pairs_of_scrutinized_background) TO '/imppc/labs/maplab/imallona/correlations/coad_genomic_categories_pairs_of_scrutinized_background.tsv' WITH delimiter E'\t';


-- coad end              -----------------------------------------------------------------------------------




-- coad normal 38 start   -----------------------------------------------------------------------------------


CREATE OR REPLACE FUNCTION coad_normal_38_create_views_for_genomic_categories_analysis() RETURNS integer as $$
DECLARE
    view_stmt text;
    -- copy_stmt text;
    
BEGIN
    FOR i in 1..22 LOOP
        view_stmt := format('
CREATE OR REPLACE VIEW analysis.coad_normal_38_genomic_categories_pairs_%s AS (
  SELECT COUNT(*), 
      CASE WHEN c.correlation >= 0.5 THEN ''pos''
            WHEN c.correlation <=0.5 THEN ''neg''
      END AS correlation,

      CASE WHEN g_a.coding_exons >= 1 THEN ''coding_exon''
           WHEN g_a.cpgislandext >=1 THEN ''cpgislandext'' 
           WHEN g_a.five_utr_exons >= 1 THEN ''five_utr_exon''
           WHEN g_a.introns >= 1 THEN ''intron''
           WHEN g_a.rmsk >= 1 THEN ''repeat''  
           WHEN g_a.three_utr_exons >= 1 THEN ''three_utr_exon'' 
           ELSE ''none''  
      END AS type_a,

      CASE WHEN g_b.coding_exons >= 1 THEN ''coding_exon''
           WHEN g_b.cpgislandext >=1 THEN ''cpgislandext''  
           WHEN g_b.five_utr_exons >= 1 THEN ''five_utr_exon''
           WHEN g_b.introns >= 1 THEN ''intron'' 
           WHEN g_b.rmsk >= 1 THEN ''repeat''      
           WHEN g_b.three_utr_exons >= 1 THEN ''three_utr_exon''
           ELSE ''none''
      END AS type_b


   FROM coad_normal_38.meth_filtered_correlations_chr%s c, 
        annotations.infinium_probes_by_ensgene_hg19_genomic_categories g_a,
        annotations.infinium_probes_by_ensgene_hg19_genomic_categories g_b

   WHERE  c.meth_probe_a = g_a.probe
        AND c.meth_probe_b = g_b.probe
   GROUP BY 
      CASE WHEN c.correlation >= 0.5 THEN ''pos''
            WHEN c.correlation <=0.5 THEN ''neg''
      END,
      
      CASE WHEN g_a.coding_exons >= 1 THEN ''coding_exon''
           WHEN g_a.cpgislandext >=1 THEN ''cpgislandext''
           WHEN g_a.five_utr_exons >= 1 THEN ''five_utr_exon''
           WHEN g_a.introns >= 1 THEN ''intron''
           WHEN g_a.rmsk >= 1 THEN ''repeat''  
           WHEN g_a.three_utr_exons >= 1 THEN ''three_utr_exon''   
           ELSE ''none''      

      END,

      CASE WHEN g_b.coding_exons >= 1 THEN ''coding_exon''
           WHEN g_b.cpgislandext >=1 THEN ''cpgislandext''   
           WHEN g_b.five_utr_exons >= 1 THEN ''five_utr_exon''
           WHEN g_b.introns >= 1 THEN ''intron'' 
           WHEN g_b.rmsk >= 1 THEN ''repeat''      
           WHEN g_b.three_utr_exons >= 1 THEN ''three_utr_exon''
           ELSE ''none''    
      END

);
        ', i, i);

        EXECUTE view_stmt;

    END LOOP;
    RETURN 0;
END
$$ language 'plpgsql';

SELECT * FROM coad_normal_38_create_views_for_genomic_categories_analysis();

\copy (SELECT * FROM analysis.coad_normal_38_genomic_categories_pairs_22) TO '/imppc/labs/maplab/imallona/correlations/coad_normal_38_genomic_categories_pairs_22.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_normal_38_genomic_categories_pairs_21) TO '/imppc/labs/maplab/imallona/correlations/coad_normal_38_genomic_categories_pairs_21.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_normal_38_genomic_categories_pairs_20) TO '/imppc/labs/maplab/imallona/correlations/coad_normal_38_genomic_categories_pairs_20.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_normal_38_genomic_categories_pairs_19) TO '/imppc/labs/maplab/imallona/correlations/coad_normal_38_genomic_categories_pairs_19.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_normal_38_genomic_categories_pairs_18) TO '/imppc/labs/maplab/imallona/correlations/coad_normal_38_genomic_categories_pairs_18.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_normal_38_genomic_categories_pairs_17) TO '/imppc/labs/maplab/imallona/correlations/coad_normal_38_genomic_categories_pairs_17.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_normal_38_genomic_categories_pairs_16) TO '/imppc/labs/maplab/imallona/correlations/coad_normal_38_genomic_categories_pairs_16.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_normal_38_genomic_categories_pairs_15) TO '/imppc/labs/maplab/imallona/correlations/coad_normal_38_genomic_categories_pairs_15.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_normal_38_genomic_categories_pairs_14) TO '/imppc/labs/maplab/imallona/correlations/coad_normal_38_genomic_categories_pairs_14.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_normal_38_genomic_categories_pairs_13) TO '/imppc/labs/maplab/imallona/correlations/coad_normal_38_genomic_categories_pairs_13.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_normal_38_genomic_categories_pairs_12) TO '/imppc/labs/maplab/imallona/correlations/coad_normal_38_genomic_categories_pairs_12.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_normal_38_genomic_categories_pairs_11) TO '/imppc/labs/maplab/imallona/correlations/coad_normal_38_genomic_categories_pairs_11.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_normal_38_genomic_categories_pairs_10) TO '/imppc/labs/maplab/imallona/correlations/coad_normal_38_genomic_categories_pairs_10.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_normal_38_genomic_categories_pairs_9) TO '/imppc/labs/maplab/imallona/correlations/coad_normal_38_genomic_categories_pairs_9.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_normal_38_genomic_categories_pairs_8) TO '/imppc/labs/maplab/imallona/correlations/coad_normal_38_genomic_categories_pairs_8.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_normal_38_genomic_categories_pairs_7) TO '/imppc/labs/maplab/imallona/correlations/coad_normal_38_genomic_categories_pairs_7.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_normal_38_genomic_categories_pairs_6) TO '/imppc/labs/maplab/imallona/correlations/coad_normal_38_genomic_categories_pairs_6.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_normal_38_genomic_categories_pairs_5) TO '/imppc/labs/maplab/imallona/correlations/coad_normal_38_genomic_categories_pairs_5.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_normal_38_genomic_categories_pairs_4) TO '/imppc/labs/maplab/imallona/correlations/coad_normal_38_genomic_categories_pairs_4.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_normal_38_genomic_categories_pairs_3) TO '/imppc/labs/maplab/imallona/correlations/coad_normal_38_genomic_categories_pairs_3.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_normal_38_genomic_categories_pairs_2) TO '/imppc/labs/maplab/imallona/correlations/coad_normal_38_genomic_categories_pairs_2.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_normal_38_genomic_categories_pairs_1) TO '/imppc/labs/maplab/imallona/correlations/coad_normal_38_genomic_categories_pairs_1.tsv' WITH delimiter E'\t';

-- coad normal 38 end     -----------------------------------------------------------------------------------




-- coad mini 40 start     -----------------------------------------------------------------------------------



CREATE OR REPLACE FUNCTION coad_mini_40_create_views_for_genomic_categories_analysis() RETURNS integer as $$
DECLARE
    view_stmt text;
    -- copy_stmt text;
    
BEGIN
    FOR i in 1..22 LOOP
        view_stmt := format('
CREATE OR REPLACE VIEW analysis.coad_mini_40_genomic_categories_pairs_%s AS (
  SELECT COUNT(*), 
      CASE WHEN c.correlation >= 0.5 THEN ''pos''
            WHEN c.correlation <=0.5 THEN ''neg''
      END AS correlation,

      CASE WHEN g_a.coding_exons >= 1 THEN ''coding_exon''
           WHEN g_a.cpgislandext >=1 THEN ''cpgislandext'' 
           WHEN g_a.five_utr_exons >= 1 THEN ''five_utr_exon''
           WHEN g_a.introns >= 1 THEN ''intron''
           WHEN g_a.rmsk >= 1 THEN ''repeat''  
           WHEN g_a.three_utr_exons >= 1 THEN ''three_utr_exon'' 
           ELSE ''none''  
      END AS type_a,

      CASE WHEN g_b.coding_exons >= 1 THEN ''coding_exon''
           WHEN g_b.cpgislandext >=1 THEN ''cpgislandext''  
           WHEN g_b.five_utr_exons >= 1 THEN ''five_utr_exon''
           WHEN g_b.introns >= 1 THEN ''intron'' 
           WHEN g_b.rmsk >= 1 THEN ''repeat''      
           WHEN g_b.three_utr_exons >= 1 THEN ''three_utr_exon''
           ELSE ''none''
      END AS type_b


   FROM coad_mini_40.meth_filtered_correlations_chr%s c, 
        annotations.infinium_probes_by_ensgene_hg19_genomic_categories g_a,
        annotations.infinium_probes_by_ensgene_hg19_genomic_categories g_b

   WHERE  c.meth_probe_a = g_a.probe
        AND c.meth_probe_b = g_b.probe
   GROUP BY 
      CASE WHEN c.correlation >= 0.5 THEN ''pos''
            WHEN c.correlation <=0.5 THEN ''neg''
      END,
      
      CASE WHEN g_a.coding_exons >= 1 THEN ''coding_exon''
           WHEN g_a.cpgislandext >=1 THEN ''cpgislandext''
           WHEN g_a.five_utr_exons >= 1 THEN ''five_utr_exon''
           WHEN g_a.introns >= 1 THEN ''intron''
           WHEN g_a.rmsk >= 1 THEN ''repeat''  
           WHEN g_a.three_utr_exons >= 1 THEN ''three_utr_exon''   
           ELSE ''none''      

      END,

      CASE WHEN g_b.coding_exons >= 1 THEN ''coding_exon''
           WHEN g_b.cpgislandext >=1 THEN ''cpgislandext''   
           WHEN g_b.five_utr_exons >= 1 THEN ''five_utr_exon''
           WHEN g_b.introns >= 1 THEN ''intron'' 
           WHEN g_b.rmsk >= 1 THEN ''repeat''      
           WHEN g_b.three_utr_exons >= 1 THEN ''three_utr_exon''
           ELSE ''none''    
      END

);
        ', i, i);

        EXECUTE view_stmt;

    END LOOP;
    RETURN 0;
END
$$ language 'plpgsql';

SELECT * FROM coad_mini_40_create_views_for_genomic_categories_analysis();

\copy (SELECT * FROM analysis.coad_mini_40_genomic_categories_pairs_22) TO '/imppc/labs/maplab/imallona/correlations/coad_mini_40_genomic_categories_pairs_22.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_mini_40_genomic_categories_pairs_21) TO '/imppc/labs/maplab/imallona/correlations/coad_mini_40_genomic_categories_pairs_21.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_mini_40_genomic_categories_pairs_20) TO '/imppc/labs/maplab/imallona/correlations/coad_mini_40_genomic_categories_pairs_20.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_mini_40_genomic_categories_pairs_19) TO '/imppc/labs/maplab/imallona/correlations/coad_mini_40_genomic_categories_pairs_19.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_mini_40_genomic_categories_pairs_18) TO '/imppc/labs/maplab/imallona/correlations/coad_mini_40_genomic_categories_pairs_18.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_mini_40_genomic_categories_pairs_17) TO '/imppc/labs/maplab/imallona/correlations/coad_mini_40_genomic_categories_pairs_17.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_mini_40_genomic_categories_pairs_16) TO '/imppc/labs/maplab/imallona/correlations/coad_mini_40_genomic_categories_pairs_16.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_mini_40_genomic_categories_pairs_15) TO '/imppc/labs/maplab/imallona/correlations/coad_mini_40_genomic_categories_pairs_15.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_mini_40_genomic_categories_pairs_14) TO '/imppc/labs/maplab/imallona/correlations/coad_mini_40_genomic_categories_pairs_14.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_mini_40_genomic_categories_pairs_13) TO '/imppc/labs/maplab/imallona/correlations/coad_mini_40_genomic_categories_pairs_13.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_mini_40_genomic_categories_pairs_12) TO '/imppc/labs/maplab/imallona/correlations/coad_mini_40_genomic_categories_pairs_12.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_mini_40_genomic_categories_pairs_11) TO '/imppc/labs/maplab/imallona/correlations/coad_mini_40_genomic_categories_pairs_11.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_mini_40_genomic_categories_pairs_10) TO '/imppc/labs/maplab/imallona/correlations/coad_mini_40_genomic_categories_pairs_10.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_mini_40_genomic_categories_pairs_9) TO '/imppc/labs/maplab/imallona/correlations/coad_mini_40_genomic_categories_pairs_9.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_mini_40_genomic_categories_pairs_8) TO '/imppc/labs/maplab/imallona/correlations/coad_mini_40_genomic_categories_pairs_8.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_mini_40_genomic_categories_pairs_7) TO '/imppc/labs/maplab/imallona/correlations/coad_mini_40_genomic_categories_pairs_7.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_mini_40_genomic_categories_pairs_6) TO '/imppc/labs/maplab/imallona/correlations/coad_mini_40_genomic_categories_pairs_6.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_mini_40_genomic_categories_pairs_5) TO '/imppc/labs/maplab/imallona/correlations/coad_mini_40_genomic_categories_pairs_5.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_mini_40_genomic_categories_pairs_4) TO '/imppc/labs/maplab/imallona/correlations/coad_mini_40_genomic_categories_pairs_4.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_mini_40_genomic_categories_pairs_3) TO '/imppc/labs/maplab/imallona/correlations/coad_mini_40_genomic_categories_pairs_3.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_mini_40_genomic_categories_pairs_2) TO '/imppc/labs/maplab/imallona/correlations/coad_mini_40_genomic_categories_pairs_2.tsv' WITH delimiter E'\t';
\copy (SELECT * FROM analysis.coad_mini_40_genomic_categories_pairs_1) TO '/imppc/labs/maplab/imallona/correlations/coad_mini_40_genomic_categories_pairs_1.tsv' WITH delimiter E'\t';




-- coad mini 40 end       -----------------------------------------------------------------------------------
