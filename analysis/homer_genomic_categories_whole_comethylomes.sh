#!/bin/bash
##
## Retrieves genomic categories from whole co-methylomes
## Largely inspired by homer_communities_analyzer.sh
##
## Actually this was not run as instead we calculated enrichment in correlations
##  rather than probes
## So run pairwise_genomic_categories_enrichment.sh instead
##
## 7th apr 2017

WD="$IHOME"/correlations_simpler/homer_genomic_categories_whole_comethylomes

HOST=overlook
PORT=5432
USER=imallona
# DB=colonomics_correlations
PASS=""

mkdir -p $WD
cd $WD

export PGPASSWORD=$PASS

DB=colonomics_correlations
for schema in colonomics_tumor colonomics_normal
do            
        stmt_nested="SELECT chromosome, meth_start, meth_end, probe
                     FROM annotation.humanmethylation450_probeinfo
                     WHERE probe IN (
                      SELECT DISTINCT(probe) 
           FROM
          ( 
           (
            SELECT distinct(meth_probe_a) as probe
             FROM "$schema".meth_filtered_correlations c
             WHERE correlation >= 0.8
           )
              UNION ALL
           (
             SELECT distinct(meth_probe_b) as probe
             FROM "$schema".meth_filtered_correlations c
             WHERE correlation >= 0.8
           )
          ) as probe
         );"
        
        psql -F $'\t' --no-align -t -d $DB -U $USER -h $HOST -p $PORT \
             -c "$stmt_nested" > "$db"_"$schema".nested   
    
done

## same for TCGA

DB=meth_correlations
for schema in coad coad_normal_38
do            
        stmt_nested="SELECT chromosome, meth_start, meth_end, probe
                     FROM coad.humanmethylation450_probeinfo
                     WHERE probe IN (
                      SELECT DISTINCT(probe) 
           FROM
          ( 
           (
            SELECT distinct(meth_probe_a) as probe
             FROM "$schema".meth_filtered_correlations c
             WHERE correlation >= 0.8
           )
              UNION ALL
           (
             SELECT distinct(meth_probe_b) as probe
             FROM "$schema".meth_filtered_correlations c
             WHERE correlation >= 0.8
           )
          ) as probe
         );"
        
        psql -F $'\t' --no-align -t -d $DB -U $USER -h $HOST -p $PORT \
             -c "$stmt_nested" > "$db"_"$schema".nested   
    
done
