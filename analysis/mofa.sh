#!/bin/bash

# colonomics cnv parser by cytoband
##
## 10th oct 2017
## Izaskun Mallona

DATA=$IHOME/data/colonomics/cnv
TASK="mofa"
IHOME=/imppc/labs/maplab/imallona
HOME=/home/labs/maplab/imallona
WD=$HOME/correlations_simpler/$TASK
BEDTOOLS=/software/debian-8/bio/bedtools2-2.26.master/bin/bedtools

mkdir -p $WD

cd $WD

# mysql --user=genome --host=genome-mysql.cse.ucsc.edu -A -e \
# "select chrom, min(chromStart), max(chromEnd),  concat(chrom, substr(name,1,1))
# from hg19.cytoBand
# group by concat(chrom, substr(name,1,1));" | awk '{if (NR!=1) {print}}' > cytobands.bed

mysql --user=genome --host=genome-mysql.cse.ucsc.edu -A -e \
"select chrom, chromStart, chromEnd, name
from hg19.cytoBand;" | awk '{if (NR!=1) {print}}' > cytobands.bed


for patient in $(cut -f2 $DATA/CLX_CN_segments_2017Jan20.txt  | uniq)
do
    echo $patient
    
    fgrep $patient $DATA/CLX_CN_segments_2017Jan20.txt | \
        awk '{FS=OFS="\t"; print "chr"$3,$4,$5,$8}' > "$patient".bed

    $BEDTOOLS intersect -a cytobands.bed \
              -b "$patient".bed \
              -loj > "$patient"_cytoband.bed
done 

## to be continued (get the first item at the tie, or something like that)
