#!/bin/bash
##
## do communities have meth cnmf signatures?
##
## Izaskun Mallona
##29th sept 2017

TASK='meth_cnmf_communities'
HOME=/home/labs/maplab/imallona
IHOME=/imppc/labs/maplab/imallona
WD="$HOME"/correlations_simpler/"$TASK"
WD_IHOME="$IHOME"/correlations_simpler/"$TASK"

mkdir -p $WD $WD_IHOME

cd $WD

wget http://gdac.broadinstitute.org/runs/analyses__2016_01_28/data/COAD-TP/20160128/gdac.broadinstitute.org_COAD-TP.Methylation_Clustering_CNMF.Level_4.2016012800.0.0.tar.gz

tar xzvf  *gz

ln -s gdac.broadinstitute.org_COAD-TP.Methylation_Clustering_CNMF.Level_4.2016012800.0.0/COAD-TP.subclassmarkers.txt


ln -s gdac.broadinstitute.org_COAD-TP.Methylation_Clustering_CNMF.Level_4.2016012800.0.0/COAD-TP.bestclus.txt
