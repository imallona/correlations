#!/bin/bash
##
## Requires pairwise_genomic_categories_enrichment.sh to be run first
##
## Izaskun Mallona, GPLv3
## 18th apr 2017

## the path the bedfiles are located at
DAT="$IHOME"/correlations_simpler/pairwise_genomic_categories_enrichment
WD="$IHOME"/correlations_simpler/active_promoters_dissection

hmms=(Active_Promoter Candidate_Strong_enhancer Candidate_Weak_enhancer_DNase CTCF_Candidate_Insulator Heterochromatin_Repetitive_Copy_Number_Variation Inactive_Promoter Low_activity_proximal_to_active_states Polycomb_repressed Promoter_Flanking Transcription_associated)

SEED=1
NTHREADS=11

mkdir -p $WD
cd $WD

datasets=(coad_normal_38 colonomics_tumor coad colonomics_normal)

echo Please run active_promoters_dissection.R

for hmm in ${hmms[@]}
do
    for category in normal_only tumor_only shared_among_three not_correlating
    do
        fn="$WD"/"$hmm"/"$hmm"_"$category".tsv
        background="$WD"/"$hmm"/"$hmm"_not_correlating.tsv
        perl /soft/bio/homer-4.8/bin/annotatePeaks.pl \
             "$fn" \
             hg19  \
             -annStats "$WD"/"$hmm"/"$hmm"_"$category"_annot.stats \
             -go "$WD"/"$hmm"/"$hmm"_"$category"/go \
             -genomeOntology "$WD"/"$hmm"/"$hmm"_"$category"/genome_ontology \
             > $(basename $fn tsv)_annotated.tab

          perl /soft/bio/homer-4.8/bin/findMotifsGenome.pl \
             "$fn" \
             hg19  \
             "$WD"/"$hmm"/"$hmm"_"$category"/motifs_against_non_changing \
             -bg "$background" \
             -size given \
             -p "$NTHREADS" \
             -preparsedDir "$WD"/preparsedDir
          
    done
done
