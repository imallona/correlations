#!/bin/bash
##
## Retrieves the coad MAF files from TCGA
##  and subsets them to a list of candidate genes
##
## Izaskun Mallona, 8th apr 2016

WD="$IHOME"/correlations/tcga_somatic_mutations

MUTATIONS=(KRAS APC TP53 BRAF)

# in coad, somatic mutations are called
#  from exomes sequenced by two technologies
SOLID=hgsc.bcm.edu__ABI_SOLiD_DNA_Sequencing_level2.maf
GA=hgsc.bcm.edu__Illumina_Genome_Analyzer_DNA_Sequencing_level2.maf

echo 'Get the somatic mutations from the TCGA with the following query'

query="https://tcga-data.nci.nih.gov/tcga/damws/jobprocess/xml?&disease=COAD&platformType=7&center=8&platform=Mutation%20Calling&level=2&preservationMethodCode=Frozen&protectedStatus=N&sampleList=%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%2C"

echo "$query"

echo 'Uncompress and store the output files and move to that WD'
cd $WD

echo 'Retrieve the KRAS or whatever mutations (subsetting the MAF file)'
echo 'Mind that GA and SOLID platforms will be merged'

mkdir subset && cd $_

ln -s $WD/Somatic_Mutations/BCM__SOLiD_DNASeq/Level_2/"$SOLID"
ln -s $WD/Somatic_Mutations/BCM__IlluminaGA_DNASeq/Level_2/"$GA"

# file header, no matter which it is

head -1 "$GA" > header.maf

for mut in "${MUTATIONS[@]}"
do
    grep -i "$mut" "$SOLID" > solid_"$mut".maf
    grep -i "$mut" "$GA" > ga_"$mut".maf
    cat header.maf solid_"$mut".maf ga_"$mut".maf > coad_"$mut".maf
done

rm header.maf ga*maf solid*maf

echo 'Further R processing'
echo 'analysis/pca_communities_association_with_mutations_tcga.R'
