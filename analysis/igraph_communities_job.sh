#!/bin/bash
#$ -S /bin/bash
#$ -e /imppc/labs/maplab/imallona/jobs/delta_net-e.log
#$ -o /imppc/labs/maplab/imallona/jobs/delta_net-o.log
#$ -N delta_net
#$ -V
#$ -M imallona@igtp.cat
#$ -m e
#$ -q imppcv3
#$ -pe smp 1

set +x

echo Starting at $(date)

cd $IHOME/tmp

R --slave --vanilla < $IHOME/src/correlations/analysis/igraph_communities_filtering_by_degree_by_sign.R

echo Ending at $(date)
