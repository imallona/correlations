#!/bin/bash
#
# gistic by arm retrieval

TASK="copy_number_gistic"
IHOME=/imppc/labs/maplab/imallona
HOME=/home/labs/maplab/imallona
WD=$HOME/correlations_simpler/$TASK

mkdir -p $WD $NFS_WD
cd $WD

wget http://gdac.broadinstitute.org/runs/analyses__2016_01_28/data/COAD-TP/20160128/gdac.broadinstitute.org_COAD-TP.Correlate_Clinical_vs_CopyNumber_Arm.Level_4.2016012800.0.0.tar.gz

tar xzvf gdac.broadinstitute.org_COAD-TP.Correlate_Clinical_vs_CopyNumber_Arm.Level_4.2016012800.0.0.tar.gz

ln -s gdac.broadinstitute.org_COAD-TP.Correlate_Clinical_vs_CopyNumber_Arm.Level_4.2016012800.0.0/transformed.cor.cli.txt

mysql --user=genome --host=genome-mysql.cse.ucsc.edu -A -e \
"select chrom, min(chromStart), max(chromEnd),  concat(chrom, substr(name,1,1))
from hg19.cytoBand
group by concat(chrom, substr(name,1,1));" | awk '{if (NR!=1) {print}}' > cytobands.bed
