#!/bin/bash

echo 'Mind the product of the bedfile column six'

# for normalized  correlations, awk $4*10000 is fine
# for raw correlations, awk '{OFS="\t"; print $1,$2,$3,$4*1000} works

oldpythonpath=$PYTHONPATH

export PYTHONPATH=""
 # hicplotter-0.7.3
source /soft/general/virtenvs/hicplotter-0.7.3/bin/activate

WHICH="chr22 chr21 chr20 chr19 chr18 chr17 chr16 chr15 chr14"
# WHICH="chr15"

# # WD=/imppc/labs/maplab/imallona/test
# IHOME=/imppc/labs/maplab/imallona/
# WD="$IHOME"/correlations/colonomics_rho_restrictive_communities/hicplotter/resolution_1000000
# # WD="$IHOME"/correlations/colonomics_rho_restrictive_communities/hicplotter/negative
BEDTOMATRIX="$IHOME"/src/correlations/analysis/bedToMatrix.py
WD=$IHOME/correlations_simpler/hic
VIRTENVPATH=/soft/general/virtenvs/hicplotter-0.7.3

cd $WD

# ln -s /soft/bio/hicplotter/data/
# ln -s ~/Desktop/HiCPlotter-Beta0.3.25/data

for dataset in colonomics_tumor colonomics_normal coad coad_normal_38
do
    cd "$WD"/"$dataset"
    
    for chr in $(seq 22 -1 14)
    do
        # item="chr"$item
        
        item=chr"$chr"_"$dataset"
        chr=chr"$chr"
        
        echo $item
        sed  '/^$/d' "$item"_hic_min_rho_0.8_resolution_1000000.bed >  foo
        mv foo  "$item"_hic.bed

        sed  '/^$/d' "$item"_hic_norm_min_rho_0.8_resolution_1000000.bed >  foo
        ## if not multiplied by 1000, no plotting is done
        awk '{OFS="\t"; print $1,$2,$3,$4*1000}' foo > "$item"_hic_norm_product.bed
        ##here!!! ñññññññ
        
        # awk '{OFS="\t"; print $1,$2,$3,$4*10000}' foo > "$item"_hic_norm_product.bed
        python "$BEDTOMATRIX" "$item"_hic.bed
        python "$BEDTOMATRIX" "$item"_hic_norm_product.bed

        # python "$VIRTENVPATH"/bin/HiCPlotter_fix.py -f "$item"_hic.bed_matrix.txt \
        #        -n raw \
        #        -chr "$chr" \
        #        -r 50000 \
        #        -o test_another_raw -fh 0  -ptd 1 -pptd 1 -pi 1  \
        #        -t data/HiC/Human/GM12878_Enhancer.bed,data/HiC/Human/GM12878_Txn.bed,data/HiC/Human/GM12878_Het.bed \
        #        -tl Enhancer,Transcribed,Heterochromatin -a data/HiC/Human/GM12878.Rad21.bed -al RAD21 \
        #        -hist data/HiC/Human/wgEncodeUwRepliSeqGm12878WaveSignalRep1.bedGraph -hl repliseq -hm 100


        # python /soft/general/virtenvs/hicplotter-0.7.3/bin/HiCPlotter.py -f "$item"_hic_norm_product.bed_matrix.txt \
        #        -n normalized \
        #        -chr "$chr" \
        #        -r 50000 \
        #        -o test_another -fh 0  -ptd 1 -pptd 1 -pi 1  \
        #        -t data/HiC/Human/GM12878_Enhancer.bed,data/HiC/Human/GM12878_Txn.bed,data/HiC/Human/GM12878_Het.bed \
        #        -tl Enhancer,Transcribed,Heterochromatin -a data/HiC/Human/GM12878.Rad21.bed -al RAD21 \
        #        -hist data/HiC/Human/wgEncodeUwRepliSeqGm12878WaveSignalRep1.bedGraph -hl repliseq -hm 100
    
          python "$VIRTENVPATH"/bin/HiCPlotter_fix.py -f "$item"_hic.bed_matrix.txt \
               -n raw \
               -chr "$chr" \
               -r 50000 \
               -o test_another_raw -fh 0  -ptd 1 -pptd 1 -pi 1 


        python /soft/general/virtenvs/hicplotter-0.7.3/bin/HiCPlotter.py -f "$item"_hic_norm_product.bed_matrix.txt \
               -n normalized \
               -chr "$chr" \
               -r 50000 \
               -o test_another -fh 0  -ptd 1 -pptd 1 -pi 1          
        # rm "$item"_hic_norm_product.bed_matrix.txt "$item"_hic_norm_product.bed foo "$item"_hic.bed_matrix.txt
        rm foo
    done
done

## now comparing matrices
cd $WD
echo 'check the bins here'

# for chr in $(seq 22 -1 14)
# do
#     # chr=chr"$chr"

   
#     python /soft/general/virtenvs/hicplotter-0.7.3/bin/HiCPlotter.py -f \
#            $WD/colonomics_tumor/chr"$chr"_colonomics_tumor_hic.bed_matrix.txt \
#            $WD/colonomics_normal/chr"$chr"_colonomics_normal_hic.bed_matrix.txt \
#            -chr chr"$chr" \
#            -n Colonomics_tumor Colonomics_normal \
#            -o comparison_colonomics_raw \
#            -s 4850 -e 5400 -mm 8 \
#            -r 50000 -c 1

#     python /soft/general/virtenvs/hicplotter-0.7.3/bin/HiCPlotter.py -f \
#            $WD/coad/chr"$chr"_coad_hic.bed_matrix.txt \
#            $WD/coad_normal_38/chr"$chr"_coad_normal_38_hic.bed_matrix.txt \
#            -chr chr"$chr" \
#            -n TCGA_tumor TCGA_normal \
#            -o comparison_tcga_raw \
#            -s 4850 -e 5400 -mm 8 \
#            -r 50000 -c 1

#     ## now normalized

#     python /soft/general/virtenvs/hicplotter-0.7.3/bin/HiCPlotter.py -f \
#            $WD/colonomics_tumor/chr"$chr"_colonomics_tumor_hic_norm_product.bed_matrix.txt \
#            $WD/colonomics_normal/chr"$chr"_colonomics_normal_hic_norm_product.bed_matrix.txt \
#            -chr chr"$chr" \
#            -n Colonomics_tumor Colonomics_normal \
#            -o comparison_colonomics_normalized \
#            -s 4850 -e 5400 -mm 8 \
#            -r 50000 -c 1

#     python /soft/general/virtenvs/hicplotter-0.7.3/bin/HiCPlotter.py -f \
#            $WD/coad/chr"$chr"_coad_hic.bed_matrix.txt \
#            $WD/coad_normal_38/chr"$chr"_coad_normal_38_hic.bed_matrix.txt \
#            -chr chr"$chr" \
#            -n TCGA_tumor TCGA_normal \
#            -o comparison_tcga_normalized \
#            -s 4850 -e 5400 -mm 8 \
#            -r 50000 -c 1

    
# done

## are bining resolution needed at all?

for chr in $(seq 22 -1 14)
do
 

    ## now normalized

    python /soft/general/virtenvs/hicplotter-0.7.3/bin/HiCPlotter.py -f \
           $WD/colonomics_tumor/chr"$chr"_colonomics_tumor_hic_norm_product.bed_matrix.txt \
           $WD/colonomics_normal/chr"$chr"_colonomics_normal_hic_norm_product.bed_matrix.txt \
           -chr "$chr" \
           -n Colonomics_tumor Colonomics_normal \
           -o comparison_colonomics_normalized \
           -c 1

    python /soft/general/virtenvs/hicplotter-0.7.3/bin/HiCPlotter.py -f \
           $WD/coad/chr"$chr"_coad_hic.bed_matrix.txt \
           $WD/coad_normal_38/chr"$chr"_coad_normal_38_hic.bed_matrix.txt \
           -chr "$chr" \
           -n TCGA_tumor TCGA_normal \
           -o comparison_tcga_normalized \
           -c 1

    python /soft/general/virtenvs/hicplotter-0.7.3/bin/HiCPlotter.py -f \
           $WD/colonomics_tumor/chr"$chr"_colonomics_tumor_hic.bed_matrix.txt \
           $WD/colonomics_normal/chr"$chr"_colonomics_normal_hic.bed_matrix.txt \
           -chr chr"$chr" \
           -n Colonomics_tumor Colonomics_normal \
           -o comparison_colonomics_raw \
           -c 1

    python /soft/general/virtenvs/hicplotter-0.7.3/bin/HiCPlotter.py -f \
           $WD/coad/chr"$chr"_coad_hic.bed_matrix.txt \
           $WD/coad_normal_38/chr"$chr"_coad_normal_38_hic.bed_matrix.txt \
           -chr chr"$chr" \
           -n TCGA_tumor TCGA_normal \
           -o comparison_tcga_raw \
           -c 1
done



deactivate
export PYTHONPATH=$oldpythonpath
