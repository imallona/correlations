#!/usr/bin/env R
## script to represent rho histograms produced by database/correlation_counter_by_intervals.sql

## 21 aug 2015

IHOME <- '/imppc/labs/maplab/imallona'

BASE <- file.path(IHOME, 'correlations', 'colonomics')

TUMOR_DISTINCT_FN <- file.path(BASE, 'counts_of_distinct_probes_by_rho_intervals', 'colonomics_tumor_no_threshold.psql_output.tsv')
NORMAL_DISTINCT_FN <- file.path(BASE, 'counts_of_distinct_probes_by_rho_intervals', 'colonomics_normal_no_threshold.psql_output.tsv')

TUMOR_FN <- file.path(BASE, 'counts_by_rho_intervals', 'colonomics_tumor_no_threshold.psql_output.tsv')
NORMAL_FN <- file.path(BASE, 'counts_by_rho_intervals', 'colonomics_normal_no_threshold.psql_output.tsv')


collapse_by_interval <- function(dat) {
    res <- list()

    for (i in seq.int(-10,10)/10) 
        res[[as.character(i)]] <- sum(dat[dat$corr_greater_or_equal_than == i,]$count)

    return(res)
}

## this does not work
collapse_by_interval_broken <- function(dat) {
    res <- list()
    for (i in seq(-1, 1, 0.1)) 
        res[[as.character(i)]] <- sum(dat[dat$corr_greater_or_equal_than == i,]$count)

    return(res)
}


plot_density <- function(fn, main, col, ylim) {
    dat <- read.table(fn, header = FALSE, sep = "\t")
    colnames(dat) <- c('chr_a', 'chr_b', 'corr_greater_or_equal_than', 'corr_less_than', 'count')
    dat_proc <- collapse_by_interval(dat)

    plot(as.numeric(dat_proc), xaxt = 'n', type = 'b',
         ylab = 'density',
         xlab = 'rho',
         main = main,
         col = col,
         lwd = 2)
    axis(side = 1, at=1:length(dat_proc), labels = names(dat_proc), las = 2)
    abline(v=11)
    abline(v=6)
    abline(v=16)
    
}

dir.create(file.path(BASE, 'results'))

pdf(file.path(BASE, 'results', 'counts_by_rho_intervals.pdf'))

plot_density(fn = TUMOR_FN, main = 'colonomics tumor', col = 'blue')

plot_density(fn = NORMAL_FN, main = 'colonomics normal', col = 'blue')

plot_density(fn = TUMOR_DISTINCT_FN, main = 'colonomics tumor distinct', col = 'blue')

plot_density(fn = NORMAL_DISTINCT_FN, main = 'colonomics normal distinct', col = 'blue')

dev.off()


