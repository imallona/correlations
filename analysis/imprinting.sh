#!/bin/bash
##
## Interleaved imprinting.R
##
## gets a look at the highly connected cpgs, outliers at the log log CCDF plots 
## also evaluates monk's imprinting sites
##
## http://genome.cshlp.org/content/suppl/2014/01/15/gr.164913.113.DC1/Supplemental_Table_S1.xlsx
##
## and http://www.geneimprint.com/site/genes-by-species
##
## 26th june 2017
## Izaskun Mallona, GPLv2

WD="$IHOME"/correlations_simpler/imprinting

cd $WD

bedtools sort -i top_percentile_trans.bed  > sorted; mv sorted top_percentile_trans.bed
bedtools sort -i data/monks_s1_450k_sliding_windows_analysis.bed  > sorted
mv sorted data/monks_s1_450k_sliding_windows_analysis.bed
cut -f1-3 data/monks_s1_63_common_windows.bed |bedtools sort  > sorted
mv sorted data/monks_s1_63_common_windows.bed



for item in data/monks_s1_450k_sliding_windows_analysis.bed data/monks_s1_63_common_windows.bed 
do
  bedtools intersect -a top_percentile_trans.bed \
   -b "$item" \
  -c  > "$(basename $item .bed)".count


  bedtools intersect -a top_percentile_trans.bed \
   -b "$item" \
   -wb  > "$(basename $item .bed)".intersect

  cut -f5 "$(basename $item .bed)".count | sort |  uniq -c
  echo -------
done


## now getting the geneimprint db

## gene list comes from http://www.geneimprint.com/site/genes-by-species


fgrep -v "NotImprinted" data/geneimprint_human.csv | cut -f1 -d"," > imprinted_gene_symbols.txt

mysql  -h  genome-mysql.cse.ucsc.edu -A -u genome -D hg19 \
       -e '
select distinct X.geneSymbol,G.chrom,min(G.txStart),max(G.txEnd)
from knownGene as G, kgXref as X 
where X.geneSymbol in ("NDUFA4P1", "GFI1", "DIRAS3", "FUCA1", "RNU5D-1", "BMP8B", "DVL1", "TP73", "WDR8", "RPL22", "PRDM16", "PEX10", "TMEM52", "HSPA6", "PTPN14", "OBSCN", "HIST3H2BB", "OR11L1", "LRRTM1", "OTX1", "VAX2", "CCDC85A", "CYP1B1", "ABCG8", "ZFP36L2", "GPR1", "ZDBF2", "TIGD1", "MYEOV2", "ALDH1L1", "ZIC1", "HES1", "FGFRL1", "KIAA1530", "SPON2", "NAP1L5", "ADAMTS16", "CDH18", "CSF2", "VTRNA2-1", "TNDM", "PRIM2", "BTNL2", "FAM50B", "MRAP2", "C6orf117", "LIN28B", "AIM1", "PLAGL1", "HYMAI", "IGF2R", "SLC22A2", "SLC22A3", "BRP44L", "GRB10", "DDC", "GLI3", "HOXA2", "HOXA11", "EVX1", "HOXA3", "HOXA4", "HOXA5", "TMEM60", "PEG10", "MAGI2", "SGCE", "CALCR", "PPP1R9A", "PON1", "DLX5", "TFPI2", "COPG2", "COPG2IT1", "CPA4", "MEST", "MESTIT1", "KLF14", "FASTK", "SLC4A2", "PURG", "DLGAP2", "NKAIN3", "LY6D", "KCNK9", "GPT", "ZFAT-AS1", "ZFAT", "PEG13", "GLIS3", "APBA1", "C9orf85", "FLJ46321", "LMX1B", "PHPT1", "C9orf116", "EGFL7", "GATA3", "CTNNA3", "LDB1", "NKX6-2", "C10orf93", "PAOX", "VENTX", "C10orf91", "INPP5F V2", "WT1", "WT1-AS", "KCNQ1OT1", "PKP3", "OSBPL5", "ZNF215", "KCNQ1DN", "NAP1L4", "SLC22A18AS", "TH", "SLC22A18", "CDKN1C", "TRPM5", "PHLDA2", "IGF2", "IGF2AS", "INS", "KCNQ1", "IFITM1", "B4GALNT4", "H19", "RAB1B", "ANO1", "KBTBD3", "ZC3H12C", "SDHD", "NTM", "ABCC9", "RBP5", "SLC26A10", "SLC38A4", "HOXC4", "HOXC9", "CDK4", "E2F7", "DCN", "FBRSL1", "KIAA1545", "HTR2A", "RB1", "FLJ40296", "FAM70B", "FOXG1", "FERMT2", "DIO3", "MEG3", "DLK1", "RTL1", "MEG8", "MAGEL2", "MKRN3", "UBE3A", "ZNF127AS", "NPAP1", "SNORD107", "SNORD116@", "SNORD108", "SNORD115@", "SNORD109B", "SNORD109A", "SNORD115-48", "PWCR1", "SNRPN", "ATP10A", "GABRB3", "GABRA5", "NDN", "SNORD64", "GABRG3", "SNURF", "GATM", "RASGRF1", "MIR184", "IRAIN", "NAA60", "ZNF597", "SOX8", "SALL1", "C16orf57", "ACD", "FOXF1", "TMEM88", "TP53", "PYY2", "HOXB2", "HOXB3", "LOC100131170", "BRUNOL4", "FAM59A", "TCEB3C", "PPAP2C", "DNMT1", "CHMP2A", "TSHZ3", "CHST8", "ZNF225", "ZIM2", "ZNF264", "MIMT1", "MZF1", "LILRB4", "PEG3", "ZNF229", "MIR371A", "NLRP2", "ZIM3", "USP29", "C20orf82", "ISM1", "PSIMCT-1", "NNAT", "BLCAP", "HM13", "MCTS2", "GDAP1L1", "SGK2", "COL9A3", "GNAS", "L3MBTL", "MIR296", "SANG", "MIR298", "GNASAS", "C20orf20", "SIM2", "DGCR6L", "DGCR6", "FLJ20464") and 
 X.kgId=G.name
group by geneSymbol' > imprinted.tsv

# cut -f2,3,4 imprinted.tsv | sed  '1d' | sortBed > imprinted.bed
awk '{OFS=FS="\t"; print $2,$3,$4,$1}'  imprinted.tsv | sed  '1d' | sortBed > imprinted.bed

item=imprinted.bed

bedtools intersect -a top_percentile_trans.bed \
         -b "$item" \
         -c  > "$(basename $item .bed)".count


bedtools intersect -a top_percentile_trans.bed \
         -b "$item" \
         -wb  > "$(basename $item .bed)".intersect

cut -f5 "$(basename $item .bed)".count | sort |  uniq -c
echo -------

cut -f8 imprinted.intersect  | sort | uniq -c


