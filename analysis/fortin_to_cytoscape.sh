#!/bin/bash
##
##

DATA=$IHOME/"correlations_simpler/fortin_ab_compartments_bedgraphs"
TASK="fortin_to_cytoscape"
IHOME=/imppc/labs/maplab/imallona
HOME=/home/labs/maplab/imallona
WD=$HOME/correlations_simpler/$TASK
BEDTOOLS=/software/debian-8/bio/bedtools2-2.26.master/bin/bedtools

RESOLUTION=100000
COHORT='colonomics_tumor'
mkdir -p $WD

cd $WD

touch fortin.bed
for fn in $(find $DATA -name "$COHORT""*""$RESOLUTION"_compartments.bed)
do
    awk 'NR > 1{OFS="\t";print $2,$3,$4,$NF}' $fn | tr -d '"' >> fortin.bed
done

$BEDTOOLS sort -i fortin.bed > foo
mv foo fortin.bed

## intersecting to 450k

psql -d colonomics_correlations -t -A -F$'\t' \
     -c "select concat('chr', chromosome), meth_start, meth_end, probe 
         from annotation.humanmethylation450_probeinfo;" | $BEDTOOLS sort > chip.bed


$BEDTOOLS intersect \
          -a  chip.bed \
          -b fortin.bed \
          -wa -wb > chip_fortin.bed 
