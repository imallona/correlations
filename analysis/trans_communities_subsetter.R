#!/usr/bin/env R
##
## Extracts from membership files those partitions in which the cgs (probe
## identifiers) are located in either different chromosomes or at least
## at MIN_WIDTH distance (i.e. 1 Mbp)
##
## Izaskun Mallona, 28th July 2016, GPLv2

library(RPostgreSQL)


IHOME <- '/imppc/labs/maplab/imallona'
WD <- file.path(IHOME, 'correlations_simpler', 'trans_communities_subset')
OUT <- file.path(WD, 'results')

MIN_SIZE <- 10
MIN_WIDTH <- 1e6  ## basepairs

DB_CONF <- file.path(IHOME, 'src', 'correlations', 'db_imallona.txt')

##

INNER_MIN_DEGREE <- 1
MIN_DEGREE <- 5
MIN_SIZE <- 10
MIN_RHO <- 0.80

COAD_NORMAL_FN <- file.path(
    IHOME, 
    'correlations',
    'tcga_rho_restrictive_communities',
    'coad_normal_38_inner_min_degree_1_min_degree_1_min_size_10_min_rho_0.8_positive_only',
    'coad_normal_38_inner_min_degree_1_min_degree_1_min_size_10_min_rho_0.8_positive_only_comm_membership.tsv')

COAD_TUMOR_FN <- file.path(
    IHOME,
    'correlations',
    'tcga_rho_restrictive_communities',
    'coad_inner_min_degree_1_min_degree_1_min_size_10_min_rho_0.8_positive_only',
    'coad_inner_min_degree_1_min_degree_1_min_size_10_min_rho_0.8_positive_only_comm_membership.tsv')

COAD_MINI_TUMOR_FN <- file.path(
    IHOME,
    'correlations',
    'tcga_rho_restrictive_communities',
    'coad_mini_40_inner_min_degree_1_min_degree_1_min_size_10_min_rho_0.8_positive_only',
    'coad_mini_40_inner_min_degree_1_min_degree_1_min_size_10_min_rho_0.8_positive_only_comm_membership.tsv')

COLONOMICS_NORMAL_FN <- file.path(
    IHOME,
    'correlations',
    'colonomics_rho_restrictive_communities',
    'colonomics_normal_inner_min_degree_1_min_degree_1_min_size_10_min_rho_0.8_positive_only',
    'colonomics_normal_inner_min_degree_1_min_degree_1_min_size_10_min_rho_0.8_positive_only_comm_membership.tsv')

COLONOMICS_TUMOR_FN <- file.path(
    IHOME,
    'correlations',
    'colonomics_rho_restrictive_communities',
    'colonomics_tumor_inner_min_degree_1_min_degree_1_min_size_10_min_rho_0.8_positive_only',
    'colonomics_tumor_inner_min_degree_1_min_degree_1_min_size_10_min_rho_0.8_positive_only_comm_membership.tsv')

## MEMBERSHIPS <- c(COLONOMICS_NORMAL_FN, COLONOMICS_TUMOR_FN, COAD_MINI_TUMOR_FN,
                 ## COAD_NORMAL_FN, COAD_TUMOR_FN)

MEMBERSHIPS <- c(COLONOMICS_TUMOR_FN, COLONOMICS_NORMAL_FN,
                 COAD_NORMAL_FN, COAD_TUMOR_FN)

##

source(file.path(IHOME, 'src', 'correlations', 'analysis', 'database_connection.R'))

## functions start

read_membership_file <- function(fn) {
    dat <- read.table(fn)
    dat$probe <- rownames(dat)
    dat$community <- dat$x
    return(dat)
}

## functions end

dir.create(OUT, showWarnings = TRUE, recursive = TRUE)
setwd(OUT)

drv <- dbDriver("PostgreSQL")

db_conf <- get_db_parameters(DB_CONF)

con <- dbConnect(drv,
                 user = db_conf[['user']],
                 password = db_conf[['password']],
                 dbname = 'regional_profile',
                 host = db_conf[['host']],
                 port = db_conf[['port']])

trans_communities <- list()
for (membership in MEMBERSHIPS) {
    
    curr <- basename(membership)
    print(curr)
    
    dat <- read_membership_file(membership)
    selected <- paste0("'", paste0(dat$probe, collapse = "','"), "'")

    stmt <- sprintf('select probe, chr, cg_start
                     from annotations.humanmethylation450kannot
                     where probe IN (%s);', selected)

    annotated <- get_query_at_once(con, stmt)

    dat <- merge(dat, annotated, by = 'probe')

    ## get unique community identifiers
    ## for each unique identifier
    trans_communities[[curr]] <- vector()
    for (community in unique(dat$community)) {
        dcom <- dat[dat$community == community,]
        if (nrow(dcom) >= MIN_SIZE) {
            ## if spanning several chr
            if (nlevels(as.factor(dcom$chr)) > 1)
                trans_communities[[curr]] <- append(community,
                                                    trans_communities[[curr]])
            ## if located at the same chr but far away
            else if ((max(dcom$cg_start) - min(dcom$cg_start)) > MIN_WIDTH)
                trans_communities[[curr]] <- append(community,
                                                    trans_communities[[curr]])
        }
    }
    
    save(trans_communities, file = file.path(OUT, 'trans_communities.RData'))
    write.table(trans_communities[[curr]][order(trans_communities[[curr]])],
                file = file.path(OUT, paste0(curr, 'trans_communities')),
                row.names = FALSE, col.names = FALSE)
}
