-- A method to count how many correlations did get a pair of chromomes in a correlations interval;
-- i.e.
-- chr1 vs chr2 how many between 0.6 and 0.7 of corr threshold
--
-- typical output
--
-- select count_correlations_by_intervals(1,22)
-- NOTICE:  chr_a chr_b corr_greater_or_equal_than corr_less_than count
-- NOTICE:  1 1 -1 -0.9 75
-- NOTICE:  1 1 -0.9 -0.8 19289
-- NOTICE:  1 1 -0.8 -0.7 184709
-- NOTICE:  1 1 -0.7 -0.6 544654
-- NOTICE:  1 1 -0.6 -0.5 1105586
-- NOTICE:  1 1 -0.5 -0.4 0
-- NOTICE:  1 1 -0.4 -0.3 0
-- NOTICE:  1 1 -0.3 -0.2 0
-- NOTICE:  1 1 -0.2 -0.1 0
-- NOTICE:  1 1 -0.1 0 0
-- NOTICE:  1 1 0 0.1 0
-- NOTICE:  1 1 0.1 0.2 0
-- NOTICE:  1 1 0.2 0.3 0


CREATE OR REPLACE FUNCTION count_correlations_by_intervals(integer, integer, text) 
  RETURNS integer AS 
$$
  DECLARE
     chr1 ALIAS FOR $1;
     chr2 ALIAS FOR $2;
     p_schema ALIAS FOR $3;
     query TEXT;
     total INTEGER;
     corr RECORD;
     curr FLOAT;
 BEGIN
     total:=0;
     RAISE NOTICE 'chr_a chr_b corr_greater_or_equal_than corr_less_than count';
     FOR i in chr1..chr2 LOOP
         FOR j in chr1..chr2 LOOP
             IF i <= j THEN
                IF EXISTS (SELECT tablename 
                           FROM pg_tables 
                           WHERE tablename = 'meth_filtered_correlations_chr' || i::TEXT || '_chr'  || j::TEXT 
                           AND schemaname = p_schema)

                THEN            
                  FOR corr IN (SELECT step/10::float AS foo FROM generate_series(-10,10) AS step) LOOP
                    curr := corr.foo; 
                      -- total := total + i;
                      query := 'SELECT COUNT(*) 
                      FROM ' || p_schema || '.meth_filtered_correlations_chr' || i::TEXT || '_chr'  || j::TEXT || 
                      ' WHERE correlation >=  ' || curr || 
                      ' AND correlation < (' || curr || ' + 0.1)'; 
                      EXECUTE  query INTO total;
                      RAISE NOTICE '% % % % %', i, j, curr, (curr + 0.1), total;                       
                  END LOOP;
                END IF;
             END IF;
         END LOOP;
     END LOOP;
     RETURN total;
     -- EXCEPTION WHEN undefined_table 
     --   THEN RETURN 0; -- do nothing 
  END
$$ language 'plpgsql';


-- @todo untested, under development
CREATE OR REPLACE FUNCTION count_distinct_probes_by_correlations_by_intervals(integer, integer, text) 
  RETURNS integer AS 
$$

DECLARE
    chr1 ALIAS FOR $1;
    chr2 ALIAS FOR $2;
    p_schema ALIAS FOR $3;
    query text;
    total integer;
    corr record;
    curr float;
BEGIN
    total:=0;
    RAISE NOTICE 'chr_a chr_b corr_greater_or_equal_than corr_less_than count_distinct_probes';
    FOR i in chr1..chr2 LOOP
        FOR j in chr1..chr2 LOOP
           IF EXISTS (SELECT tablename 
                      FROM pg_tables 
                      WHERE tablename = 'meth_filtered_correlations_chr' || i::TEXT || '_chr'  || j::TEXT 
                      AND schemaname = p_schema)
           THEN
             IF i < j THEN
                FOR corr IN (SELECT step/10::FLOAT AS foo FROM generate_series(-10,10) AS step) LOOP
               -- FOR corr in (select corr from (values(-1), (-0.9)) s(a)) LOOP
                   curr := corr.foo; 
                     -- total := total + i;
                     query := 'SELECT count(distinct(meth_probe_a)) 
                     FROM ' || p_schema || '.meth_filtered_correlations_chr' || i::TEXT || '_chr'  || j::TEXT || 
                     ' WHERE correlation >=  ' || curr || 
                     ' AND correlation < (' || curr || ' + 0.1)'; 
                     EXECUTE  query INTO total;
                     RAISE NOTICE '% % % % %', i, j, curr, (curr + 0.1), total;
                END LOOP;
                
            ELSIF i = j THEN

              FOR corr IN (SELECT step/10::FLOAT AS foo FROM generate_series(-10,10) AS step) LOOP
                 -- FOR corr in (select corr from (values(-1), (-0.9)) s(a)) LOOP
                     curr := corr.foo; 
                       -- total := total + i;
                     query := '
                       SELECT COUNT(DISTINCT(probe)) FROM

                       (SELECT meth_probe_a as probe, count(distinct(meth_probe_a)) as count 
                       FROM ' || p_schema || '.meth_filtered_correlations_chr' || i::TEXT || '_chr'  || j::TEXT || 
                       ' WHERE correlation >=  ' || curr || 
                       ' AND correlation < (' || curr || ' + 0.1) 
                       GROUP BY meth_probe_a

                       UNION ALL

                       SELECT meth_probe_b as probe, count(distinct(meth_probe_B)) as count
                       FROM ' || p_schema || '.meth_filtered_correlations_chr' || i::TEXT || '_chr'  || j::TEXT || 
                       ' WHERE correlation >=  ' || curr || 
                       ' AND correlation < (' || curr || ' + 0.1) 
                       GROUP BY meth_probe_b) as nested
                       '; 
                     EXECUTE  query INTO total;
                     RAISE NOTICE '% % % % %', i, j, curr, (curr + 0.1), total;                     
                  END LOOP;     
              END IF;
            END IF;
        END LOOP;
    END LOOP;
    RETURN total;
END
$$ language 'plpgsql';

-- main


-- select * from count_distinct_probes_by_correlations_by_intervals(9,10, 'colonomics_normal_no_threshold');

