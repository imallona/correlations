-- tables and views for speeding up the correlations browser
--
-- June the 1st 2017
-- Izaskun Mallona

-- -- this would return the ids of correlating cpgs if located ath meth probe_a
-- SELECT i.closest_tss_gene_name,  array_agg(meth_probe_a) as probes_a        
-- FROM annotation.humanmethylation450_probeinfo i
-- LEFT JOIN coad.meth_filtered_correlations_chr22_chr22 m ON m.meth_probe_a = i.probe
-- GROUP BY i.closest_tss_gene_name;


-- -- test
-- SELECT i.closest_tss_gene_name, count(*)       
-- FROM annotation.humanmethylation450_probeinfo i
-- LEFT JOIN coad.meth_filtered_correlations_chr22_chr22 m ON m.meth_probe_a = i.probe
-- group by i.closest_tss_gene_name
-- limit 2;


-- -- SELECT i.closest_tss_gene_name, count(*)       
-- -- FROM annotation.humanmethylation450_probeinfo i
-- -- LEFT JOIN coad.meth_filtered_correlations_chr22_chr22 m_a ON m_a.meth_probe_a = i.probe
-- -- LEFT JOIN coad.meth_filtered_correlations_chr22_chr22 m_b ON m_b.meth_probe_b = i.probe
-- -- group by i.closest_tss_gene_name
-- -- limit 2;

-- -- inner join, keeping meth_probe_a and meth_probe_b separated
-- -- (@todo check whether R Postgres eats an array like this)
-- SELECT i.closest_tss_gene_name,  array_agg(distinct(meth_probe_a)) as probes_a,
--  array_agg(distinct(meth_probe_b)) as probes_b     
-- FROM annotation.humanmethylation450_probeinfo i
-- INNER JOIN coad.meth_filtered_correlations_chr22_chr22 m ON
-- (
--         m.meth_probe_a = i.probe OR
--         m.meth_probe_b = i.probe
        
-- )
-- GROUP BY i.closest_tss_gene_name;

-- -- rather extract all the pairs, once queried, and store them just in case
-- SELECT i.closest_tss_gene_name,
--   array_agg(m.meth_probe_a) as probes_a,
--   array_agg(m.meth_probe_b) as probes_b
-- FROM annotation.humanmethylation450_probeinfo i
-- INNER JOIN coad.meth_filtered_correlations_chr22_chr22 m ON
-- (
--         m.meth_probe_a = i.probe OR
--         m.meth_probe_b = i.probe
        
-- )
-- GROUP BY i.closest_tss_gene_name
-- limit 1;

-- explain SELECT i.closest_tss_gene_name,
--   array_agg(m.meth_probe_a) as probes_a,
--   array_agg(m.meth_probe_b) as probes_b
-- FROM annotation.humanmethylation450_probeinfo i
-- INNER JOIN coad.meth_filtered_correlations_chr22_chr22 m ON
-- (
--         m.meth_probe_a = i.probe OR
--         m.meth_probe_b = i.probe
        
-- )
-- GROUP BY i.closest_tss_gene_name
-- limit 1;



-- this works, so let's materialize the views for each schemata


create or replace function create_view_aggregated_by_gene(text) 
                returns integer as 
$$
  declare 
    p_schema text;
    n integer;
  begin
  p_schema := $1;        
  n := 0;

  execute 'CREATE MATERIALIZED VIEW ' || p_schema || '.aggregated_by_gene AS (

   SELECT i.closest_tss_gene_name,
    array_agg(m.meth_probe_a) as probes_a,
    array_agg(m.meth_probe_b) as probes_b
   FROM annotation.humanmethylation450_probeinfo i
   INNER JOIN ' || p_schema || '.meth_filtered_correlations m ON
   (
        m.meth_probe_a = i.probe OR
        m.meth_probe_b = i.probe
        
   )
  WHERE correlation >= 0.8
  GROUP BY i.closest_tss_gene_name
  );';

  execute 'CREATE UNIQUE INDEX aggregated_symbol
   ON ' || p_schema || '.aggregated_by_gene (closest_tss_gene_name);';

  n := 1;
  return n;
  
  end;
$$ language 'plpgsql';

select * from create_view_aggregated_by_gene('coad');
select * from create_view_aggregated_by_gene('coad_normal_38');
select * from create_view_aggregated_by_gene('colonomics_tumor');
select * from create_view_aggregated_by_gene('colonomics_normal');



-- now generating a materialized view with the number of correlations for each CpG

create or replace function create_view_count_correlations(text) 
                returns integer as 
$$
  declare 
    p_schema text;
    n integer;
  begin
  p_schema := $1;        
  n := 0;

  execute 'CREATE MATERIALIZED VIEW ' || p_schema || '.count_correlations AS (

   SELECT i.probe, count(*) as num_positive_correlations
   FROM annotation.humanmethylation450_probeinfo i
   INNER JOIN ' || p_schema || '.meth_filtered_correlations m ON
   (
        m.meth_probe_a = i.probe OR
        m.meth_probe_b = i.probe
        
   )
  WHERE correlation >= 0.8
  GROUP BY i.probe
  );';

  execute 'CREATE UNIQUE INDEX aggregated_probe
   ON ' || p_schema || '.count_correlations (probe);';

  n := 1;
  return n;
  
  end;
$$ language 'plpgsql';

select * from create_view_count_correlations('colonomics_tumor');
select * from create_view_count_correlations('colonomics_normal');
select * from create_view_count_correlations('coad');
select * from create_view_count_correlations('coad_normal_38');


-- -- test n t counting by gene

-- select * from (
--   (select p.closest_tss_gene_name normals_gene, count(*) as normals_count
--   from annotation.humanmethylation450_probeinfo p,
--   colonomics_normal.meth_filtered_correlations_chr1_chr1 n
--   where (p.probe = n.meth_probe_a or p.probe = n.meth_probe_b) and
--   p.closest_tss_gene_name = 'C1orf216'
--   group by p.closest_tss_gene_name
--   ) as normals
--   FULL OUTER JOIN

--   (
--   select p.closest_tss_gene_name as tumors_gene, count(*) as tumors_count
--   from annotation.humanmethylation450_probeinfo p,
--   colonomics_tumor.meth_filtered_correlations_chr1_chr1 n
--   where (p.probe = n.meth_probe_a or p.probe = n.meth_probe_b) and
--   p.closest_tss_gene_name = 'C1orf216'
--   group by p.closest_tss_gene_name
--   ) as tumors
--   on normals.normals_gene = tumors.tumors_gene
--   );
  
-- -----------------------


-- select closest_tss_gene_name, probe, normals_count, tumors_count
-- from (
--  (select * from annotation.humanmethylation450_probeinfo) as annotation
--  LEFT JOIN
--  (
 
 
-- select tumors_count, normals_count, tumors_gene, normals_gene from (
--   (select p.closest_tss_gene_name normals_gene, count(*) as normals_count
--   from annotation.humanmethylation450_probeinfo p,
--   colonomics_normal.meth_filtered_correlations_chr1_chr1 n
--   where (p.probe = n.meth_probe_a or p.probe = n.meth_probe_b)
--   group by p.probe
--   ) as normals
--   FULL OUTER JOIN

--   (
--   select p.closest_tss_gene_name as tumors_gene, count(*) as tumors_count
--   from annotation.humanmethylation450_probeinfo p,
--   colonomics_tumor.meth_filtered_correlations_chr1_chr1 n
--   where (p.probe = n.meth_probe_a or p.probe = n.meth_probe_b)
--   group by p.probe
--   ) as tumors
--   on normals.normals_gene = tumors.tumors_gene
--   )
-- ) as foo
-- on annotation.closest_tss_gene_name = foo.tumors_gene

-- )
-- where foo.tumors_count > 0
-- and annotation.chromosome = '1'
-- limit 100;


-- ----------


create materialized view integrated_counts_by_probe as
(

select closest_tss_gene_name, probe, normals_count, tumors_count
from (
 (select * from annotation.humanmethylation450_probeinfo) as annotation
 LEFT JOIN
 (
 
 
select tumors_count, normals_count, tumors_probe, normals_probe from (

  (select p.probe as normals_probe, count(*) as normals_count
  from annotation.humanmethylation450_probeinfo p,
  colonomics_normal.meth_filtered_correlations n
  where (p.probe = n.meth_probe_a or p.probe = n.meth_probe_b)
  group by p.probe
  ) as normals
  FULL OUTER JOIN

  (
  select p.probe as tumors_probe, count(*) as tumors_count
  from annotation.humanmethylation450_probeinfo p,
  colonomics_tumor.meth_filtered_correlations n
  where (p.probe = n.meth_probe_a or p.probe = n.meth_probe_b)
  group by p.probe
  ) as tumors
  on normals.normals_probe = tumors.tumors_probe
  )
) as merged
on (annotation.probe = merged.normals_probe or annotation.probe = merged.tumors_probe)

)
);

-- same for tcga

create materialized view integrated_counts_by_probe as
(

select closest_tss_gene_name, probe, normals_count, tumors_count
from (
 (select * from annotation.humanmethylation450_probeinfo) as annotation
 LEFT JOIN
 (
 
 
select tumors_count, normals_count, tumors_probe, normals_probe from (

  (select p.probe as normals_probe, count(*) as normals_count
  from annotation.humanmethylation450_probeinfo p,
  coad_normal_38.meth_filtered_correlations n
  where (p.probe = n.meth_probe_a or p.probe = n.meth_probe_b)
  group by p.probe
  ) as normals
  FULL OUTER JOIN

  (
  select p.probe as tumors_probe, count(*) as tumors_count
  from annotation.humanmethylation450_probeinfo p,
  coad.meth_filtered_correlations n
  where (p.probe = n.meth_probe_a or p.probe = n.meth_probe_b)
  group by p.probe
  ) as tumors
  on normals.normals_probe = tumors.tumors_probe
  )
) as merged
on (annotation.probe = merged.normals_probe or annotation.probe = merged.tumors_probe)

)
);




-- trying to generalize it for each probeinfo






-- C1orf216


-- select *
-- from (annotation.humanmethylation450_probeinfo p
--  left join (
--   colonomics_tumor.aggregated_by_gene t
--   full outer join
--   colonomics_normal.aggregated_by_gene n
--   on t.closest_tss_gene_name = n.closest_tss_gene_name
--  ) by closest_tss_gene_name)
-- where p.closest_tss_gene_name = 'HES4';

-- view distances (non functional)

-- create view colonomics_tumor.corr_distances as (
--  select c.meth_probe_a, c.meth_probe_b
--  from colonomics_tumor.meth_filtered_correlations c,
--  annotation.humanmethylation450_probeinfo a,
--  annotation.humanmethylation450_probeinfo b,
--  where c.meth_probe_a = a.probe and
--  c.meth_probe_b = b.probe and
--  a.chromosome = b.chromosome and
--  abs(a.meth_start - b.meth_start) < max_dist and
--  abs(a.meth_start - b.meth_start) >= min_dist
-- );



-- create or replace function create_views_distances(text) 
--                 returns integer as 
-- $$
--   declare 
--     p_schema text;
--     n integer;
--     min_dist integer;
--     max_dist integer;
--     stmt := text;
--   begin
--   p_schema := $1;        
--   n := 0;
--   min_dist := 0;
--   max_dist := 0;

--    WHILE min_dist <= 10000 LOOP
--          RAISE NOTICE 'Max_Dist: %', max_dist;
--           RAISE NOTICE 'Min_Dist: %', min_dist;
--       max_dist := min_dist + 500;
--                 RAISE NOTICE 'Max_Dist: %', max_dist;
--           RAISE NOTICE 'Min_Dist: %', min_dist;

--       stmt := 'CREATE MATERIALIZED VIEW ' || p_schema || '.correlations_distances' ||
--       cast (min_dist as char) ||
--               '_' || cast (max_dist as char) ' AS (

-- create view ' || p_schema || '.corr_distances as (
-- select c.meth_probe_a, c.meth_probe_b
-- from ' || p_schema || '.meth_filtered_correlations c,
-- annotation.humanmethylation450_probeinfo a,
-- annotation.humanmethylation450_probeinfo b,
-- where c.meth_probe_a = a.probe and
-- c.meth_probe_b = b.probe and
-- a.chromosome = b.chromosome and
-- abs(a.meth_start - b.meth_start) < ' || max_dist || ' and
-- abs(a.meth_start - b.meth_start) >= ' || min_dist || ');';

--       execute ñññ
--       min_dist := min_dist + 500 ; 
--       RAISE NOTICE 'Max_Dist: %', max_dist;
      
--    END LOOP ;      

--   n := 1;
--   return n;
  
--   end;
-- $$ language 'plpgsql';

-- select * from create_views_distances('colonomics_tumor');
