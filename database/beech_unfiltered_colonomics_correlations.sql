-- no thresholded data storage
create or replace function create_correlations_no_threshold_schema (p_schema in text)
  returns integer as
$$

  declare 
    n integer;
  begin
    --
    n := 0;
  
    -- creating the parent table 
    execute 'CREATE TABLE ' || p_schema || '.meth_filtered_correlations(
            meth_probe_a VARCHAR(16) NOT NULL,
            meth_probe_b VARCHAR(16) NOT NULL,
            correlation FLOAT(4) NOT NULL,
            pvalue FLOAT(4));'; 

    execute  'CREATE TABLE ' || p_schema || '.meth_filtered_correlations_chr9_chr10() '
             || 'inherits (' || p_schema || '.meth_filtered_correlations);'; 


    execute  'CREATE TABLE ' || p_schema || '.meth_filtered_correlations_chr10_chr10() '
             || 'inherits (' || p_schema || '.meth_filtered_correlations);'; 

    n := 1;
    return n;

    --
  end;
$$ language 'plpgsql';


-- Schemata creation
CREATE SCHEMA colonomics_normal_no_threshold_test;

-- grants

GRANT USAGE ON SCHEMA colonomics_normal_no_threshold_test TO imallona;

ALTER DEFAULT PRIVILEGES IN SCHEMA colonomics_normal_no_threshold_test
      GRANT INSERT, UPDATE, DELETE, REFERENCES, ALTER, SELECT ON TABLES TO imallona;

-- bulk table creation

select * from create_correlations_no_threshold_schema_test ('colonomics_normal_no_threshold');

create or replace function create_correlations_no_threshold_schema_test (p_schema in text)
  returns integer as
$$

  declare 
    n integer;
  begin
    --
    n := 0;
  
    -- creating the parent table 
    execute 'CREATE TABLE ' || p_schema || '.test_meth_filtered_correlations(
            meth_probe_a VARCHAR(16) NOT NULL,
            meth_probe_b VARCHAR(16) NOT NULL,
            correlation FLOAT(4) NOT NULL,
            pvalue FLOAT(4));'; 

    execute  'CREATE TABLE ' || p_schema || '.test_meth_filtered_correlations_chr9_chr10() '
             || 'inherits (' || p_schema || '.test_meth_filtered_correlations);'; 


    execute  'CREATE TABLE ' || p_schema || '.test_meth_filtered_correlations_chr10_chr10() '
             || 'inherits (' || p_schema || '.test_meth_filtered_correlations);'; 

    n := 1;
    return n;

    --
  end;
$$ language 'plpgsql';

select * from create_correlations_no_threshold_schema_test ('colonomics_normal_no_threshold');
