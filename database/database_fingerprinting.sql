SELECT pg_size_pretty( pg_database_size( current_database() ) ) as human_size,
     pg_database_size( current_database() ) as raw_size;
       
SELECT nspname, relname AS "relation",
pg_size_pretty(pg_relation_size(C.oid)) AS "size"
    FROM pg_class C LEFT JOIN pg_namespace N ON (N.oid = C.relnamespace)
    ORDER BY pg_relation_size(C.oid) DESC
    LIMIT 10;
    
SELECT count(*)
    FROM pg_class C LEFT JOIN pg_namespace N ON (N.oid = C.relnamespace)
     WHERE nspname NOT IN ('pg_catalog', 'information_schema');

SELECT  nspname AS schemaname,relname,reltuples
FROM pg_class C
LEFT JOIN pg_namespace N ON (N.oid = C.relnamespace)
WHERE
 nspname NOT IN ('pg_catalog', 'information_schema') AND
 relkind='r' 
ORDER BY reltuples DESC;


-- including indexes



SELECT pg_size_pretty (pg_indexes_size('coad.meth_filtered_correlations_chr1_chr1'));
