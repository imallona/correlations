-- This builds up the schemas necessary to work with the colonomics_correlations db
-- 17th Aug 2015

-- creates the schema and the tables that will contain the correlations inserts
create or replace function create_correlations_schema(text) 
                returns integer as 
$$
  declare 
    p_schema text;
    n integer;
  begin
  --

  p_schema := $1;
  n := 0;

  -- creating the parent table 
  execute 'CREATE TABLE ' || p_schema || '.meth_filtered_correlations(
    meth_probe_a VARCHAR(16) NOT NULL,
    meth_probe_b VARCHAR(16) NOT NULL,
    correlation FLOAT(4) NOT NULL,
    pvalue FLOAT(4));'; 

    -- creating the child tables and the grand-child subtables
    FOR i IN 1..22 LOOP
        execute 'CREATE TABLE ' || p_schema || '.meth_filtered_correlations_chr' || i ||'()' ||
                 'INHERITS (' || p_schema || '.meth_filtered_correlations);';
        
        FOR j IN 1..22 LOOP 
            IF i <= j THEN
               execute 'CREATE TABLE ' 
                       || p_schema || '.meth_filtered_correlations_chr' || i ||'_chr' || j || '()'
                       || ' INHERITS (' || p_schema || '.meth_filtered_correlations_chr' || i || ');';

            END IF;   
        END LOOP;                     
    END LOOP;
    
    n := 1;
    return n;

  -- 
end;
$$ language 'plpgsql';


-- create schemas for all-correlations no-matter-rho storage
create or replace function create_correlations_no_threshold_schema (p_schema in text)
  returns integer as
$$

  declare 
    n integer;
  begin
    --
    n := 0;
  
    -- creating the parent table 
    execute 'CREATE TABLE ' || p_schema || '.meth_filtered_correlations(
            meth_probe_a VARCHAR(16) NOT NULL,
            meth_probe_b VARCHAR(16) NOT NULL,
            correlation FLOAT(4) NOT NULL,
            pvalue FLOAT(4));'; 

    execute  'CREATE TABLE ' || p_schema || '.meth_filtered_correlations_chr9_chr10() '
             || 'inherits (' || p_schema || '.meth_filtered_correlations);'; 


    execute  'CREATE TABLE ' || p_schema || '.meth_filtered_correlations_chr10_chr10() '
             || 'inherits (' || p_schema || '.meth_filtered_correlations);'; 

    n := 1;
    return n;

    --
  end;
$$ language 'plpgsql';


-- Main

-- Schemata creation

CREATE SCHEMA colonomics_normal;
GRANT USAGE ON SCHEMA colonomics_normal TO maplabr;
ALTER DEFAULT PRIVILEGES IN SCHEMA colonomics_normal
      GRANT SELECT ON TABLES TO maplabr;

CREATE SCHEMA colonomics_tumor;
GRANT USAGE ON SCHEMA colonomics_tumor TO maplabr;
ALTER DEFAULT PRIVILEGES IN SCHEMA colonomics_tumor
      GRANT SELECT ON TABLES TO maplabr;

CREATE SCHEMA colonomics_normal_no_threshold;
GRANT USAGE ON SCHEMA colonomics_normal_no_threshold TO maplabr;
ALTER DEFAULT PRIVILEGES IN SCHEMA colonomics_normal_no_threshold
      GRANT SELECT ON TABLES TO maplabr;

CREATE SCHEMA colonomics_tumor_no_threshold;
GRANT USAGE ON SCHEMA colonomics_tumor_no_threshold TO maplabr;
ALTER DEFAULT PRIVILEGES IN SCHEMA colonomics_tumor_no_threshold
      GRANT SELECT ON TABLES TO maplabr;


select * from create_correlations_schema('colonomics_normal');
select * from create_correlations_schema('colonomics_tumor');

select * from create_correlations_no_threshold_schema('colonomics_normal_no_threshold');
select * from create_correlations_no_threshold_schema('colonomics_tumor_no_threshold');

-- annotations


CREATE SCHEMA annotation;
GRANT USAGE ON SCHEMA annotation TO maplabr;
ALTER DEFAULT PRIVILEGES IN SCHEMA annotation
      GRANT SELECT ON TABLES TO maplabr;


CREATE TABLE annotation.humanmethylation450_probeinfo(
       probe VARCHAR(16), 
       Chromosome VARCHAR(2),
       Closest_TSS VARCHAR(19),
       Distance_closest_TSS INTEGER,
       Closest_TSS_gene_name VARCHAR(40),
       meth_start INTEGER,
       meth_end INTEGER,
       width INTEGER,
       strand VARCHAR(1),
       addressA INTEGER,
       addressB INTEGER,
       channel VARCHAR(4),
       platform VARCHAR(5),
       percentGC NUMERIC,
       sourceSeq VARCHAR(50),
       probeType VARCHAR(2),
       probeStart INTEGER,
       probeEnd INTEGER,
       CONSTRAINT pk_annotation_humanmethylation450_probeinfo PRIMARY KEY (probe)); 


--  \copy coad.humanmethylation450_probeinfo TO '/imppc/labs/maplab/imallona/coad_probeinfo_humanmethylation450_to_SQL.csv' WITH null AS 'NA' DELIMITER ','

-- \copy annotation.humanmethylation450_probeinfo FROM '/imppc/labs/maplab/imallona/coad_probeinfo_humanmethylation450_to_SQL.csv' WITH null AS 'NA' DELIMITER ','

-- -- Creating and populating the raw data tables for the schemata


CREATE TABLE colonomics_normal.humanmethylation450(
       probe VARCHAR(16) PRIMARY KEY,  
       A2004_N FLOAT(4), A2027_N FLOAT(4), A2050_N FLOAT(4), A2096_N FLOAT(4), B2012_N FLOAT(4), 
       B2035_N FLOAT(4), B2058_N FLOAT(4), B2081_N FLOAT(4), C2021_N FLOAT(4), C2044_N FLOAT(4), 
       C2067_N FLOAT(4), C2090_N FLOAT(4), D2010_N FLOAT(4), D2033_N FLOAT(4), D2079_N FLOAT(4),
       E2023_N FLOAT(4), E2046_N FLOAT(4), E2069_N FLOAT(4), E2092_N FLOAT(4), F2008_N FLOAT(4), 
       F2031_N FLOAT(4), F2054_N FLOAT(4), F2077_N FLOAT(4), F2100_N FLOAT(4), G2005_N FLOAT(4), 
       G2028_N FLOAT(4), G2051_N FLOAT(4), G2097_N FLOAT(4), H2019_N FLOAT(4), H2042_N FLOAT(4), 
       H2065_N FLOAT(4), H2088_N FLOAT(4), J2014_N FLOAT(4), J2037_N FLOAT(4), J2060_N FLOAT(4), 
       K2022_N FLOAT(4), K2045_N FLOAT(4), K2068_N FLOAT(4), K2091_N FLOAT(4), L2020_N FLOAT(4), 
       L2043_N FLOAT(4), L2066_N FLOAT(4), L2089_N FLOAT(4), M2006_N FLOAT(4), M2029_N FLOAT(4), 
       M2052_N FLOAT(4), M2075_N FLOAT(4), M2098_N FLOAT(4), N2013_N FLOAT(4), N2036_N FLOAT(4), 
       N2059_N FLOAT(4), P2009_N FLOAT(4), P2032_N FLOAT(4), P2055_N FLOAT(4), P2078_N FLOAT(4), 
       Q2017_N FLOAT(4), Q2040_N FLOAT(4), Q2063_N FLOAT(4), Q2086_N FLOAT(4), R2002_N FLOAT(4), 
       R2025_N FLOAT(4), R2048_N FLOAT(4), R2071_N FLOAT(4), R2094_N FLOAT(4), S2016_N FLOAT(4), 
       S2039_N FLOAT(4), S2062_N FLOAT(4), T2024_N FLOAT(4), T2047_N FLOAT(4), T2093_N FLOAT(4), 
       V2041_N FLOAT(4), V2064_N FLOAT(4), V2087_N FLOAT(4), W2026_N FLOAT(4), W2049_N FLOAT(4), 
       W2072_N FLOAT(4), W2095_N FLOAT(4), X2011_N FLOAT(4), X2034_N FLOAT(4), X2057_N FLOAT(4), 
       X2080_N FLOAT(4), Y2007_N FLOAT(4), Y2030_N FLOAT(4), Y2053_N FLOAT(4), Y2076_N FLOAT(4), 
       Y2099_N FLOAT(4), Z2015_N FLOAT(4), Z2038_N FLOAT(4), Z2061_N FLOAT(4), Z2084_N FLOAT(4));

CREATE TABLE colonomics_tumor.humanmethylation450(
       probe VARCHAR(16) PRIMARY KEY,  
       A2004_T FLOAT(4), A2027_T FLOAT(4), A2050_T FLOAT(4), A2096_T FLOAT(4), B2012_T FLOAT(4), 
       B2035_T FLOAT(4), B2058_T FLOAT(4), B2081_T FLOAT(4), C2021_T FLOAT(4), C2044_T FLOAT(4), 
       C2067_T FLOAT(4), C2090_T FLOAT(4), D2010_T FLOAT(4), D2033_T FLOAT(4), D2079_T FLOAT(4),
       E2023_T FLOAT(4), E2046_T FLOAT(4), E2069_T FLOAT(4), E2092_T FLOAT(4), F2008_T FLOAT(4), 
       F2031_T FLOAT(4), F2054_T FLOAT(4), F2077_T FLOAT(4), F2100_T FLOAT(4), G2005_T FLOAT(4), 
       G2028_T FLOAT(4), G2051_T FLOAT(4), G2097_T FLOAT(4), H2019_T FLOAT(4), H2042_T FLOAT(4), 
       H2065_T FLOAT(4), H2088_T FLOAT(4), J2014_T FLOAT(4), J2037_T FLOAT(4), J2060_T FLOAT(4), 
       K2022_T FLOAT(4), K2045_T FLOAT(4), K2068_T FLOAT(4), K2091_T FLOAT(4), L2020_T FLOAT(4), 
       L2043_T FLOAT(4), L2066_T FLOAT(4), L2089_T FLOAT(4), M2006_T FLOAT(4), M2029_T FLOAT(4), 
       M2052_T FLOAT(4), M2075_T FLOAT(4), M2098_T FLOAT(4), T2013_T FLOAT(4), T2036_T FLOAT(4), 
       T2059_T FLOAT(4), P2009_T FLOAT(4), P2032_T FLOAT(4), P2055_T FLOAT(4), P2078_T FLOAT(4), 
       Q2017_T FLOAT(4), Q2040_T FLOAT(4), Q2063_T FLOAT(4), Q2086_T FLOAT(4), R2002_T FLOAT(4), 
       R2025_T FLOAT(4), R2048_T FLOAT(4), R2071_T FLOAT(4), R2094_T FLOAT(4), S2016_T FLOAT(4), 
       S2039_T FLOAT(4), S2062_T FLOAT(4), T2024_T FLOAT(4), T2047_T FLOAT(4), T2093_T FLOAT(4), 
       V2041_T FLOAT(4), V2064_T FLOAT(4), V2087_T FLOAT(4), W2026_T FLOAT(4), W2049_T FLOAT(4), 
       W2072_T FLOAT(4), W2095_T FLOAT(4), X2011_T FLOAT(4), X2034_T FLOAT(4), X2057_T FLOAT(4), 
       X2080_T FLOAT(4), Y2007_T FLOAT(4), Y2030_T FLOAT(4), Y2053_T FLOAT(4), Y2076_T FLOAT(4), 
       Y2099_T FLOAT(4), Z2015_T FLOAT(4), Z2038_T FLOAT(4), Z2061_T FLOAT(4), Z2084_T FLOAT(4));

\copy colonomics_normal.humanmethylation450 FROM '/imppc/labs/maplab/imallona/colonomics_normal_humanmethylation450_to_SQL.csv' WITH null AS 'NA' DELIMITER ','
\copy colonomics_tumor.humanmethylation450 FROM '/imppc/labs/maplab/imallona/colonomics_tumor_humanmethylation450_to_SQL.csv' WITH null AS 'NA' DELIMITER ','


-- colonomics mucosae (normals)


CREATE SCHEMA colonomics_mucosa;
GRANT USAGE ON SCHEMA colonomics_mucosa TO maplabr;
ALTER DEFAULT PRIVILEGES IN SCHEMA colonomics_mucosa
      GRANT SELECT ON TABLES TO maplabr;

-- load('betas-filtQC-140425.RData')
-- mucosae <- betas[,c(1, grep( "*_M", colnames(betas)))]
-- mucosae <- mucosae[,2:ncol(mucosae)]/1000
-- probes <- data.frame(probe = row.names(betas))
-- mucosae <- cbind(probes, mucosae) 
-- dim(mucosae)
-- # [1] 430086     49
-- write.table(x = mucosae, file = '/imppc/labs/maplab/imallona/mucosa_humanmethylation450_to_SQL.csv', col.names = FALSE, sep = ",", row.names = FALSE, quote = FALSE)
-- n <- colnames(mucosae)[2:ncol(mucosae)]
-- for (i in n) {
--    cat(sprintf('%s FLOAT(4),\n', i))
-- }

CREATE TABLE colonomics_mucosa.humanmethylation450(
probe VARCHAR(16) PRIMARY KEY,
A6144_M FLOAT(4),
B2104_M FLOAT(4),
B2127_M FLOAT(4),
B2150_M FLOAT(4),
C2113_M FLOAT(4),
C2136_M FLOAT(4),
C6138_M FLOAT(4),
D2102_M FLOAT(4),
D2125_M FLOAT(4),
D2148_M FLOAT(4),
E2115_M FLOAT(4),
E6140_M FLOAT(4),
G2120_M FLOAT(4),
G2143_M FLOAT(4),
H2111_M FLOAT(4),
H2134_M FLOAT(4),
H6136_M FLOAT(4),
J2106_M FLOAT(4),
K2114_M FLOAT(4),
K2137_M FLOAT(4),
K6139_M FLOAT(4),
L2112_M FLOAT(4),
L2135_M FLOAT(4),
L6137_M FLOAT(4),
M2121_M FLOAT(4),
M2144_M FLOAT(4),
N2105_M FLOAT(4),
N2128_M FLOAT(4),
P2101_M FLOAT(4),
P2124_M FLOAT(4),
Q2109_M FLOAT(4),
Q2132_M FLOAT(4),
Q6134_M FLOAT(4),
R2117_M FLOAT(4),
R2140_M FLOAT(4),
R6142_M FLOAT(4),
S2108_M FLOAT(4),
S2131_M FLOAT(4),
T2116_M FLOAT(4),
T6141_M FLOAT(4),
V2110_M FLOAT(4),
V2133_M FLOAT(4),
V6135_M FLOAT(4),
W2118_M FLOAT(4),
W2141_M FLOAT(4),
W6143_M FLOAT(4),
X2126_M FLOAT(4),
Z2107_M FLOAT(4));

 \copy colonomics_mucosa.humanmethylation450 FROM '/imppc/labs/maplab/imallona/mucosa_humanmethylation450_to_SQL.csv' WITH null AS 'NA' DELIMITER ','
 
-- COPY 430086

-- this to sync old tcga/colonomics schemata
create table annotation.humanmethylation450_probeinfo
(like coad.humanmethylation450_probeinfo including all);

insert into annotation.humanmethylation450_probeinfo
select * from coad.humanmethylation450_probeinfo;

create table annotation.meth_450k_probe_density
(like coad.meth_450k_probe_density including all);

insert into annotation.meth_450k_probe_density
select * from coad.meth_450k_probe_density;

create table annotation.humanmethylation_hmm
(like coad.humanmethylation_hmm including all);

insert into annotation.humanmethylation_hmm
select * from coad.humanmethylation_hmm;
