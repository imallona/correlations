-- tables with membership data
-- November 3rd 2017
-- requires community_membership_tables.R to be run first

-- Izaskun Mallona


CREATE TABLE annotation.colonomics_tumor_membership(
       probe VARCHAR(16), 
       community SMALLINT,
       CONSTRAINT pk_tumor_membership PRIMARY KEY (probe)); 


CREATE TABLE annotation.colonomics_normal_membership(
       probe VARCHAR(16), 
       community SMALLINT,
       CONSTRAINT pk_normal_membership PRIMARY KEY (probe)); 


CREATE TABLE annotation.coad_membership(
       probe VARCHAR(16), 
       community SMALLINT,
       CONSTRAINT pk_tumor_membership PRIMARY KEY (probe)); 


CREATE TABLE annotation.coad_normal_38_membership(
       probe VARCHAR(16), 
       community SMALLINT,
       CONSTRAINT pk_normal_membership PRIMARY KEY (probe));


-- beware, colonomics and coad are distinct databases
--   (colonomics_correlations and meth_correlations)

\copy annotation.coad_membership FROM '/tmp/coad_inner_min_degree_1_min_degree_1_min_size_10_min_rho_0.8_positive_only_comm_membership.tsv' WITH null AS 'NA' DELIMITER ','

\copy annotation.coad_normal_38_membership FROM '/tmp/coad_normal_38_inner_min_degree_1_min_degree_1_min_size_10_min_rho_0.8_positive_only_comm_membership.tsv' WITH null AS 'NA' DELIMITER ','

\copy annotation.colonomics_tumor_membership FROM '/tmp/colonomics_tumor_inner_min_degree_1_min_degree_1_min_size_10_min_rho_0.8_positive_only_comm_membership.tsv' WITH null AS 'NA' DELIMITER ','

\copy annotation.colonomics_normal_membership FROM '/tmp/colonomics_normal_inner_min_degree_1_min_degree_1_min_size_10_min_rho_0.8_positive_only_comm_membership.tsv' WITH null AS 'NA' DELIMITER ','

create view normals_membership as (select * from annotation.colonomics_normal_membership);
create view tumors_membership as (select * from annotation.colonomics_tumor_membership);

-- at meth correlations
create view normals_membership as (select * from annotation.coad_normal_38_membership);
create view tumors_membership as (select * from annotation.coad_membership);
