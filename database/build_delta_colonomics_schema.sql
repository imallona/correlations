-- This builds up the schemas necessary to work with the colonomics_correlations db,
-- delta tumor minus normal schema
-- 26th june 2018

-- creates the schema and the tables that will contain the correlations inserts
create or replace function create_correlations_schema(text) 
                returns integer as 
$$
  declare 
    p_schema text;
    n integer;
  begin
  --

  p_schema := $1;
  n := 0;

  -- creating the parent table 
  execute 'CREATE TABLE ' || p_schema || '.meth_filtered_correlations(
    meth_probe_a VARCHAR(16) NOT NULL,
    meth_probe_b VARCHAR(16) NOT NULL,
    correlation FLOAT(4) NOT NULL,
    pvalue FLOAT(4));'; 

    -- creating the child tables and the grand-child subtables
    FOR i IN 1..22 LOOP
        execute 'CREATE TABLE ' || p_schema || '.meth_filtered_correlations_chr' || i ||'()' ||
                 'INHERITS (' || p_schema || '.meth_filtered_correlations);';
        
        FOR j IN 1..22 LOOP 
            IF i <= j THEN
               execute 'CREATE TABLE ' 
                       || p_schema || '.meth_filtered_correlations_chr' || i ||'_chr' || j || '()'
                       || ' INHERITS (' || p_schema || '.meth_filtered_correlations_chr' || i || ');';

            END IF;   
        END LOOP;                     
    END LOOP;
    
    n := 1;
    return n;

  -- 
end;
$$ language 'plpgsql';


-- create schemas for all-correlations no-matter-rho storage
create or replace function create_correlations_no_threshold_schema (p_schema in text)
  returns integer as
$$

  declare 
    n integer;
  begin
    --
    n := 0;
  
    -- creating the parent table 
    execute 'CREATE TABLE ' || p_schema || '.meth_filtered_correlations(
            meth_probe_a VARCHAR(16) NOT NULL,
            meth_probe_b VARCHAR(16) NOT NULL,
            correlation FLOAT(4) NOT NULL,
            pvalue FLOAT(4));'; 

    execute  'CREATE TABLE ' || p_schema || '.meth_filtered_correlations_chr9_chr10() '
             || 'inherits (' || p_schema || '.meth_filtered_correlations);'; 


    execute  'CREATE TABLE ' || p_schema || '.meth_filtered_correlations_chr10_chr10() '
             || 'inherits (' || p_schema || '.meth_filtered_correlations);'; 

    n := 1;
    return n;

    --
  end;
$$ language 'plpgsql';


-- Main

-- Schemata creation

CREATE SCHEMA colonomics_delta;
GRANT USAGE ON SCHEMA colonomics_delta TO maplabr;
ALTER DEFAULT PRIVILEGES IN SCHEMA colonomics_delta
      GRANT SELECT ON TABLES TO maplabr;

CREATE SCHEMA colonomics_delta_no_threshold;
GRANT USAGE ON SCHEMA colonomics_delta_no_threshold TO maplabr;
ALTER DEFAULT PRIVILEGES IN SCHEMA colonomics_delta_no_threshold
      GRANT SELECT ON TABLES TO maplabr;


select * from create_correlations_schema('colonomics_delta');

select * from create_correlations_no_threshold_schema('colonomics_delta_no_threshold');

-- -- Creating and populating the raw data tables for the schemata


CREATE TABLE colonomics_delta.humanmethylation450(
probe VARCHAR(16) PRIMARY KEY,
A2004 FLOAT(4),
A2027 FLOAT(4),
A2050 FLOAT(4),
A2073 FLOAT(4),
A2096 FLOAT(4),
B2012 FLOAT(4),
B2035 FLOAT(4),
B2058 FLOAT(4),
B2081 FLOAT(4),
C2021 FLOAT(4),
C2044 FLOAT(4),
C2067 FLOAT(4),
C2090 FLOAT(4),
D2010 FLOAT(4),
D2033 FLOAT(4),
D2079 FLOAT(4),
E2023 FLOAT(4),
E2046 FLOAT(4),
E2069 FLOAT(4),
E2092 FLOAT(4),
F2008 FLOAT(4),
F2031 FLOAT(4),
F2054 FLOAT(4),
F2077 FLOAT(4),
F2100 FLOAT(4),
G2005 FLOAT(4),
G2028 FLOAT(4),
G2051 FLOAT(4),
G2097 FLOAT(4),
H2019 FLOAT(4),
H2042 FLOAT(4),
H2065 FLOAT(4),
H2088 FLOAT(4),
J2014 FLOAT(4),
J2037 FLOAT(4),
J2060 FLOAT(4),
K2022 FLOAT(4),
K2045 FLOAT(4),
K2068 FLOAT(4),
K2091 FLOAT(4),
L2020 FLOAT(4),
L2043 FLOAT(4),
L2066 FLOAT(4),
L2089 FLOAT(4),
M2006 FLOAT(4),
M2029 FLOAT(4),
M2052 FLOAT(4),
M2075 FLOAT(4),
M2098 FLOAT(4),
N2013 FLOAT(4),
N2036 FLOAT(4),
N2059 FLOAT(4),
P2009 FLOAT(4),
P2032 FLOAT(4),
P2055 FLOAT(4),
P2078 FLOAT(4),
Q2017 FLOAT(4),
Q2040 FLOAT(4),
Q2063 FLOAT(4),
Q2086 FLOAT(4),
R2002 FLOAT(4),
R2025 FLOAT(4),
R2048 FLOAT(4),
R2071 FLOAT(4),
R2094 FLOAT(4),
S2016 FLOAT(4),
S2039 FLOAT(4),
S2062 FLOAT(4),
S2085 FLOAT(4),
T2024 FLOAT(4),
T2047 FLOAT(4),
T2093 FLOAT(4),
V2041 FLOAT(4),
V2064 FLOAT(4),
V2087 FLOAT(4),
W2026 FLOAT(4),
W2049 FLOAT(4),
W2072 FLOAT(4),
W2095 FLOAT(4),
X2011 FLOAT(4),
X2034 FLOAT(4),
X2057 FLOAT(4),
X2080 FLOAT(4),
Y2007 FLOAT(4),
Y2030 FLOAT(4),
Y2053 FLOAT(4),
Y2076 FLOAT(4),
Y2099 FLOAT(4),
Z2015 FLOAT(4),
Z2038 FLOAT(4),
Z2061 FLOAT(4),
Z2084 FLOAT(4));


\copy colonomics_delta.humanmethylation450 FROM '/imppc/labs/maplab/imallona/correlations_simpler/build_delta_colonomics_schema/delta.csv' WITH null AS 'NA' DELIMITER ','


