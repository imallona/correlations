-- correlations bulk delete
--
-- Izaskun Mallona, GPL
-- 28th July 2016

-- test without turning off indexing
DELETE FROM coad_mini.meth_filtered_correlations
WHERE ABS(correlation) < 0.8 ;
-- DELETE 2528010



-- no idea of what is this!
DELETE FROM coad_mini_30.meth_filtered_correlations
WHERE ABS(correlation) < 0.8 ;
--DELETE 132987


-- drop indices function start


-- generates (meth_a, meth_b) and (meth_b, meth_a) indices for every correlationt table
-- in the schema defined with @param p_schema
CREATE OR REPLACE FUNCTION drop_double_indices_in_correlation_table(p_schema IN TEXT) 
 RETURNS void AS
$$
  DECLARE
    p_schema ALIAS FOR $1;
    drop_index_forward TEXT;
    drop_index_reverse TEXT;
  BEGIN 
  --22 are the autosomes
    -- FOR i IN 1..22 LOOP
    --   FOR j in 1..22 LOOP
    FOR i IN 1..22 LOOP
      FOR j in 1..22 LOOP
        IF i <= j THEN
          drop_index_forward  := 'DROP INDEX ' || p_schema || '.meth_filtered_correlations_chr' ||
          i || '_chr' || j || '_ab_idx' ;

          EXECUTE drop_index_forward; 

          -- RETURN 'Playing with index b (% chr % chr %)', p_schema, i, j;  
          drop_index_reverse  := 'DROP INDEX ' || p_schema || '.meth_filtered_correlations_chr' ||
          i || '_chr' || j || '_ba_idx' ;

          EXECUTE drop_index_reverse;

     

        END IF;
      END LOOP;
    END LOOP;

    EXCEPTION
     WHEN others THEN -- as exception 'does not exist'
       NULL; --just continue
  END;
$$ LANGUAGE 'plpgsql';

SELECT * FROM drop_double_indices_in_correlation_table('coad_mini_40');
SELECT * FROM drop_double_indices_in_correlation_table('coad_normal_38');
SELECT * FROM drop_double_indices_in_correlation_table('coad');


SELECT concat('drop index coad_mini_40.', relname, ';') AS "relation"
  FROM pg_class C LEFT JOIN pg_namespace N ON (N.oid = C.relnamespace)
  WHERE nspname NOT IN ('pg_catalog', 'information_schema') and
  nspname = 'coad_mini_40'
  and
        relname like '%idx';


-- meth_filtered_correlations_chr1_chr2_ba_idx
-- coad_mini_40.meth_filtered_correlations_chr10_chr10_ab_idx
-- drop indices function end


DELETE FROM coad_mini_40.meth_filtered_correlations
WHERE ABS(correlation) < 0.8 ;

DELETE FROM coad.meth_filtered_correlations
WHERE ABS(correlation) < 0.8 ;

DELETE FROM coad_normal_38.meth_filtered_correlations
WHERE ABS(correlation) < 0.8 ;

-- DELETE FROM coad.meth_filtered_correlations
-- WHERE ABS(correlation) < 0.8 ;

-- DELETE FROM coad_normal_38.meth_filtered_correlations
-- WHERE ABS(correlation) < 0.8 ;

-- SELECT * FROM generate_double_indices_in_correlation_table('coad_mini_40');
-- SELECT * FROM generate_double_indices_in_correlation_table('coad_normal_38');
-- SELECT * FROM generate_double_indices_in_correlation_table('coad');



-- on colonomics_correlations

SELECT * FROM drop_double_indices_in_correlation_table('colonomics_tumor');
SELECT * FROM drop_double_indices_in_correlation_table('colonomics_normal');

SELECT relname AS "relation", nspname AS "schema",
       pg_size_pretty(pg_relation_size(C.oid)) AS "size"
FROM pg_class C LEFT JOIN pg_namespace N ON (N.oid = C.relnamespace)
WHERE nspname NOT IN ('pg_catalog', 'information_schema') and
relname like '%idx'
ORDER BY pg_relation_size(C.oid) DESC
LIMIT 100;



DELETE FROM colonomics_tumor.meth_filtered_correlations
WHERE ABS(correlation) < 0.8 ;

DELETE FROM colonomics_normal.meth_filtered_correlations
WHERE ABS(correlation) < 0.8 ;


-- colonomics_correlations=> 
-- colonomics_correlations=> DELETE FROM colonomics_tumor.meth_filtered_correlations
-- colonomics_correlations-> WHERE ABS(correlation) < 0.8 ;
-- DELETE 1226710109
-- colonomics_correlations=> 
-- colonomics_correlations=> DELETE FROM colonomics_normal.meth_filtered_correlations
-- colonomics_correlations-> WHERE ABS(correlation) < 0.8 ;
-- DELETE 320016858

-- meth_correlations=> DELETE FROM coad_mini_40.meth_filtered_correlations
-- meth_correlations-> WHERE ABS(correlation) < 0.8 ;
-- DELETE 1380483214
-- meth_correlations=> 
-- meth_correlations=> DELETE FROM coad.meth_filtered_correlations
-- meth_correlations-> WHERE ABS(correlation) < 0.8 ;
-- DELETE 608951141


-- indexing again

SELECT * FROM generate_double_indices_in_correlation_table('colonomics_normal');
SELECT * FROM generate_double_indices_in_correlation_table('colonomics_tumor');

SELECT * FROM generate_double_indices_in_correlation_table('coad');
SELECT * FROM generate_double_indices_in_correlation_table('coad_mini_40');
SELECT * FROM generate_double_indices_in_correlation_table('coad_normal_38');



-- meth_correlations=> DELETE FROM coad_normal_38.meth_filtered_correlations
-- meth_correlations-> WHERE ABS(correlation) < 0.8 ;
-- DELETE 701885695


select 
    relname, 
    n_dead_tup, 
    n_tup_ins, 
    n_tup_upd, 
    n_tup_del, 
    last_autoanalyze, 
    autoanalyze_count 
from pg_stat_user_tables 
where last_autoanalyze is not null 
order by last_autoanalyze desc;

vacuum analyze verbose colonomics_tumor.meth_filtered_correlations;
