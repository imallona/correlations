#!/bin/env/R

## getting tumor vs normal deltas from colonomics series

library('RPostgreSQL')

IHOME <- '/imppc/labs/maplab/imallona'
TASK <- 'build_delta_colonomics_schema'
WD <- file.path(IHOME, 'correlations_simpler', TASK)

DB_CONF <- file.path(IHOME, 'src', 'correlations', 'db_imallona.txt')

source(file.path(IHOME, 'src', 'correlations', 'analysis', 'database_connection.R'))

drv <- dbDriver("PostgreSQL")

db_conf <- get_db_parameters(DB_CONF)

con <- dbConnect(drv,
                 user = db_conf[['user']],
                 password = db_conf[['password']],
                 dbname = 'colonomics_correlations',
                 host = db_conf[['host']],
                 port = db_conf[['port']])

dir.create(WD)
setwd(WD)

load(file.path(IHOME, 'data', 'colonomics', 'methylation', 'betas-filtQC-140425.RData'))

meth <- list(n = betas[,grep('.*_N$', colnames(betas), perl = TRUE)]/1000,
             t = betas[,grep('.*_T$', colnames(betas), perl = TRUE)]/1000)

for (item in c('n', 't')) {
    colnames(meth[[item]]) <- strtrim(colnames(meth[[item]]), 5)
}

common <- intersect(colnames(meth$n), colnames(meth$t))

for (item in c('n', 't')) {
    meth[[item]] <- meth[[item]][,common]
}

stopifnot(colnames(meth[[1]]) == colnames(meth[[2]]))

meth$delta <- meth$t - meth$n

write.table(x = meth$delta,
            file = file.path(WD, 'delta.csv'),
            row.names = TRUE,
            col.names = FALSE,
            sep = ',',
            quote = FALSE)

sink(file.path(WD, 'colnames'))

## print(sprintf('%s FLOAT(4)\n,', colnames(meth$delta)[2:ncol(meth$delta)]))
for (item in colnames(meth$delta))
    cat(sprintf('%s FLOAT(4),\n', item))

sink()
