-- correlations db indexer
--
-- Izaskun Mallona, GPL, 2015

\set VERBOSITY verbose

-- generates (meth_a, meth_b) and (meth_b, meth_a) indices for every correlationt table
-- in the schema defined with @param p_schema
CREATE OR REPLACE FUNCTION generate_double_indices_in_correlation_table(p_schema IN TEXT) 
 RETURNS void AS
$$
  DECLARE
    p_schema ALIAS FOR $1;
    generate_index_forward TEXT;
    generate_index_reverse TEXT;
  BEGIN 
  --22 are the autosomes
    FOR i IN 1..22 LOOP
      FOR j in 1..22 LOOP
        IF i <= j THEN
          -- RETURN 'Playing with index ab (% chr % chr %)', p_schema, i, j;
          generate_index_forward  := 'CREATE UNIQUE INDEX meth_filtered_correlations_chr' ||
          i || '_chr' || j || '_ab_idx ON ' || p_schema || '.meth_filtered_correlations_chr' ||
          i || '_chr' || j || '(meth_probe_a, meth_probe_b)' ;

          EXECUTE generate_index_forward; 

          -- RETURN 'Playing with index b (% chr % chr %)', p_schema, i, j;  
          generate_index_reverse  := 'CREATE UNIQUE INDEX meth_filtered_correlations_chr' ||
          i || '_chr' || j || '_ba_idx ON '|| p_schema || '.meth_filtered_correlations_chr' ||
          i || '_chr' || j || '(meth_probe_b, meth_probe_a)' ;

          EXECUTE generate_index_reverse; 

        END IF;
      END LOOP;
    END LOOP;

    EXCEPTION
     WHEN duplicate_table THEN 
       RAISE NOTICE 'duplicate_table';
       CONTINUE;
     WHEN duplicate_object THEN 
       RAISE NOTICE 'duplicate_object';
       CONTINUE;
  END;
$$ LANGUAGE 'plpgsql';


-- main 

SELECT * FROM generate_double_indices_in_correlation_table('colonomics_normal');
SELECT * FROM generate_double_indices_in_correlation_table('colonomics_tumor');


-- checks

SELECT i.relname as indname,
       i.relowner as indowner,
       idx.indrelid::regclass,
       am.amname as indam,
       idx.indkey,
       ARRAY(
       SELECT pg_get_indexdef(idx.indexrelid, k + 1, true)
       FROM generate_subscripts(idx.indkey, 1) as k
       ORDER BY k
       ) as indkey_names,
       idx.indexprs IS NOT NULL as indexprs,
       idx.indpred IS NOT NULL as indpred
FROM   pg_index as idx
JOIN   pg_class as i
ON     i.oid = idx.indexrelid
JOIN   pg_am as am
ON     i.relam = am.oid
WHERE  i.relname LIKE 'meth_filtered_correlations_%';


SELECT relname AS "relation", nspname AS "schema", pg_size_pretty(pg_relation_size(C.oid)) AS "size"
  FROM pg_class C LEFT JOIN pg_namespace N ON (N.oid = C.relnamespace)
  WHERE nspname NOT IN ('pg_catalog', 'information_schema') and
        relname like '%idx'
  ORDER BY pg_relation_size(C.oid) DESC
  LIMIT 100;
