-- correlations db indexer
--
-- applies indexing.sql to the delta_colonomics schema (functions are defined
--  in the former)
-- Izaskun Mallona, GPL, 2015 and 2018

\set VERBOSITY verbose
-- main 

SELECT * FROM generate_double_indices_in_correlation_table('colonomics_delta');


-- checks

SELECT i.relname as indname,
       i.relowner as indowner,
       idx.indrelid::regclass,
       am.amname as indam,
       idx.indkey,
       ARRAY(
       SELECT pg_get_indexdef(idx.indexrelid, k + 1, true)
       FROM generate_subscripts(idx.indkey, 1) as k
       ORDER BY k
       ) as indkey_names,
       idx.indexprs IS NOT NULL as indexprs,
       idx.indpred IS NOT NULL as indpred
FROM   pg_index as idx
JOIN   pg_class as i
ON     i.oid = idx.indexrelid
JOIN   pg_am as am
ON     i.relam = am.oid
WHERE  i.relname LIKE 'meth_filtered_correlations_%';


SELECT relname AS "relation", nspname AS "schema", pg_size_pretty(pg_relation_size(C.oid)) AS "size"
  FROM pg_class C LEFT JOIN pg_namespace N ON (N.oid = C.relnamespace)
  WHERE nspname NOT IN ('pg_catalog', 'information_schema') and
        relname like '%idx'
  ORDER BY pg_relation_size(C.oid) DESC
  LIMIT 100;
  
SELECT relname AS "relation", nspname AS "schema", pg_size_pretty(pg_relation_size(C.oid)) AS "size"
  FROM pg_class C LEFT JOIN pg_namespace N ON (N.oid = C.relnamespace)
  WHERE nspname NOT IN ('pg_catalog', 'information_schema') and
        relname like '%idx' and
        nspname = 'colonomics_delta'
  ORDER BY pg_relation_size(C.oid) DESC
  LIMIT 10;
