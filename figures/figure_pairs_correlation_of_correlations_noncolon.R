#!/usr/bin/env R
##
## pairs smoothScatter for correlation of correlations
## largely inspired by figure_pairs_page_rank.R
##
## this script takes a lot to run: better use a tablesapling space, such as that of
## figure_pairs_correlation_of_correlations_noncolon_interactive.R
##
## Izaskun Mallona
## 17th jan 2017

library(igraph)
library(RPostgreSQL)
library(MASS)

TASK <- 'figure_pairs_correlation_of_correlations_noncolon'
IHOME = '/imppc/labs/maplab/imallona' 
SRC2 <- file.path(IHOME, 'src')
DB_CONF <- file.path(SRC2, 'correlations', 'db_imallona.txt')
WD = file.path(IHOME, 'correlations_simpler', TASK)

source(file.path(SRC2, 'correlations', 'analysis', 'database_connection.R'))

dir.create(WD)
setwd(WD)

## functions start

## accepts xlim and ylim params
smoothScatter.patched <- function (x, y = NULL, nbin = 128, bandwidth,
                                 colramp = colorRampPalette(c("white", blues9)),
                                 nrpoints = 100, ret.selection = FALSE, pch = ".", 
                                 cex = 1, col = "black", transformation = function(x) x^0.25, 
                                 postPlotHook = box, xlab = NULL, ylab = NULL, xlim, ylim, 
                                 xaxs = par("xaxs"), yaxs = par("yaxs"), ...) {
    if (!is.numeric(nrpoints) || nrpoints < 0 || length(nrpoints) != 
        1) 
        stop("'nrpoints' should be numeric scalar with value >= 0.")
    nrpoints <- round(nrpoints)
    ret.selection <- ret.selection && nrpoints > 0
    xlabel <- if (!missing(x)) 
                  deparse(substitute(x))
    ylabel <- if (!missing(y)) 
                  deparse(substitute(y))
    xy <- xy.coords(x, y, xlabel, ylabel)
    xlab <- if (is.null(xlab)) 
                xy$xlab
            else xlab
    ylab <- if (is.null(ylab)) 
                xy$ylab
            else ylab
    x <- cbind(xy$x, xy$y)[I <- is.finite(xy$x) & is.finite(xy$y), 
                         , drop = FALSE]
    if (ret.selection) 
        iS <- which(I)
    if (!missing(xlim)) {
        stopifnot(is.numeric(xlim), length(xlim) == 2, is.finite(xlim))
        x <- x[I <- min(xlim) <= x[, 1] & x[, 1] <= max(xlim), 
             , drop = FALSE]
        if (ret.selection) 
            iS <- iS[I]
    }
    else {
        xlim <- range(x[, 1])
    }
    if (!missing(ylim)) {
        stopifnot(is.numeric(ylim), length(ylim) == 2, is.finite(ylim))
        x <- x[I <- min(ylim) <= x[, 2] & x[, 2] <= max(ylim), 
             , drop = FALSE]
        if (ret.selection) 
            iS <- iS[I]
    }
    else {
        ylim <- range(x[, 2])
    }
    ## map <- grDevices:::.smoothScatterCalcDensity(x, nbin, bandwidth)
    map <- grDevices:::.smoothScatterCalcDensity(x, nbin, bandwidth,
                                                 list(xlim, ylim)) 
    xm <- map$x1
    ym <- map$x2
    dens <- map$fhat
    dens[] <- transformation(dens)
    image(xm, ym, z = dens, col = colramp(256), xlab = xlab, 
          ylab = ylab, xlim = xlim, ylim = ylim, xaxs = xaxs, yaxs = yaxs, 
          ...)
    if (!is.null(postPlotHook)) 
        postPlotHook()
    if (nrpoints > 0) {
        nrpoints <- min(nrow(x), ceiling(nrpoints))
        stopifnot((nx <- length(xm)) == nrow(dens), (ny <- length(ym)) == 
                      ncol(dens))
        ixm <- 1L + as.integer((nx - 1) * (x[, 1] - xm[1])/(xm[nx] - 
                                                                xm[1]))
        iym <- 1L + as.integer((ny - 1) * (x[, 2] - ym[1])/(ym[ny] - 
                                                                ym[1]))
        sel <- order(dens[cbind(ixm, iym)])[seq_len(nrpoints)]
        x <- x[sel, , drop = FALSE]
        points(x, pch = pch, cex = cex, col = col)
        if (ret.selection) 
            iS[sel]
    }
}


fudgeit <- function(){
  xm <- get('xm', envir = parent.frame(1))
  ym <- get('ym', envir = parent.frame(1))
  z  <- get('dens', envir = parent.frame(1))
  colramp <- get('colramp', parent.frame(1))
  fields::image.plot(xm,ym,z, col = colramp(256), legend.only = T, add =F)
}

## retrieve_whole_data <- function(con, schema, table) {
##     query <- sprintf("
## SELECT concat(d.meth_probe_a, d.meth_probe_b), correlation
## FROM
##   %s.%s d
## limit 10000
## ;
## ", schema, table)

##     return(get_query_at_once(con, query))
## }

retrieve_whole_data <- function(con, schema, table) {
    query <- sprintf("
SELECT concat(d.meth_probe_a, d.meth_probe_b), correlation
FROM
  %s.%s d
-- TABLESAMPLE system (0.2) repeatable (123)
;
", schema, table)

    return(get_query_at_once(con, query))
}


## functions end


## sink(file.path(WD, 'log'))
drv <- dbDriver("PostgreSQL")
db_conf <- get_db_parameters(DB_CONF)

con_tcga <- dbConnect(drv,
                      user = db_conf[['user']],
                      password = db_conf[['password']],
                      dbname = 'meth_correlations',
                      host = db_conf[['host']],
                      port = db_conf[['port']])

con_colonomics <- dbConnect(drv,
                            user = db_conf[['user']],
                            password = db_conf[['password']],
                            dbname = 'colonomics_correlations',
                            host = db_conf[['host']],
                            port = db_conf[['port']])

## tables <- c('meth_filtered_correlations_chr9_chr10', 'meth_filtered_correlations_chr10_chr10')

table <- 'meth_filtered_correlations_chr10_chr10'


schemata <- list('colonomics_tumor_no_threshold' = con_colonomics,
                 'colonomics_normal_no_threshold' = con_colonomics,
                 
                 'coad_no_threshold' = con_tcga,
                 'coad_normal_38_no_threshold' = con_tcga,

                 'prad_tumor_no_threshold' = con_tcga,
                 'prad_normal_no_threshold' = con_tcga,

                 'luad_tumor_no_threshold' = con_tcga,
                 'luad_normal_no_threshold' = con_tcga,

                 'thca_tumor_no_threshold' = con_tcga,
                 'thca_normal_no_threshold' = con_tcga,
                 
                 'brca_tumor_no_threshold' = con_tcga,
                 'brca_normal_no_threshold' = con_tcga)

cohort_tags <- list('colonomics_tumor_no_threshold' = 'colonomics_tumor',
                    'colonomics_normal_no_threshold' = 'colonomics_normal',                    

                    'coad_no_threshold' = 'coad_tumor',
                    'coad_normal_38_no_threshold' = 'coad_normal',

                    'prad_tumor_no_threshold' = 'prad_tumor',
                    'prad_normal_no_threshold' = 'prad_normal',

                    'luad_tumor_no_threshold' = 'luad_tumor',
                    'luad_normal_no_threshold' = 'luad_normal',

                    'thca_tumor_no_threshold' = 'thca_tumor',
                    'thca_normal_no_threshold' = 'thca_normal',
                    
                    'brca_tumor_no_threshold' = 'brca_tumor',
                    'brca_normal_no_threshold' = 'brca_normal')

d <- list()
concats <- NULL

for (schema in names(schemata)) {
    foo <- retrieve_whole_data(con = schemata[[schema]],
                                       schema = schema,
                                       table = table)
    bar <- foo$correlation
    names(bar) <- foo$concat
    d[[schema]] <- bar

    rm(foo); rm(bar)

    concats <- unique(c(concats, names(d[[schema]])))
}

########

png(file.path(WD, sprintf('correlation_of_correlations_global.png')),
    width = 4800, height = 4800, res = 180)

par(mfrow = c(12, 12))
## bottom, left, top and right
par(oma = c(4, 4, 1, 1)) # make room (i.e. the 4's) for the overall x and y axis titles
par(mar = c(1.2, 1.2, 1.2, 1.2)) # make the plots be closer together
par(pty = "s")
par(cex.axis = 1.2,
    cex.lab = 1.2,
    cex.main = 1.4,
    cex.sub = 1.2)

lim <- 1
at <- seq(from = -1, to = lim, lim/2)

for (i in 1:length(schemata)) {
    for (j in 1:length(schemata)) {
        a <- names(schemata)[i]
        b <- names(schemata)[j]
        print(sprintf('%s %s %s %s %s', i, a, j, b, Sys.time()))
        
        a_dat <- data.frame(a = d[[a]],
                            concat = names(d[[a]]))
        b_dat <- data.frame(b = d[[b]],
                            concat = names(d[[b]]))
        
        curr <- merge(a_dat, b_dat, by = 'concat')
        curr <- curr[,-1]

        if (i > j) {
            ## plot.new()
            plot(0, 0, col = 'white',
                 xlim = c(-1,1),
                 ylim = c(-1,lim),
                 axes = FALSE)

            foo <- cor.test(curr[,1], curr[,2], method = 'spearman')
            
            text(0, 0,
                 sprintf('rho=%s\nn=%s\npadj=%s',
                         round(foo$estimate, 2),
                         nrow(curr),
                         sprintf('%.2e', foo$p.value)), cex = 1.5)
            box(lty = 1, col = 'black')

        }
        else if (i == j) {
            plot(0, 0, col = 'white',
                 xlim = c(-1,1),
                 ylim = c(-1,lim),
                 axes = FALSE)


            text(0, 0, cohort_tags[[a]], cex = 1.5)
            box(lty = 1, col = 'black')
            Axis(side = 1, at = at, labels = at)
            
            Axis(side = 2, at = at, labels = at)
            ## las = 1
        } else {
            ## curr <- na.omit(curr[,c(i,j)])
            
            smoothScatter.patched(x = curr[,1],
                                  y = curr[,2],
                                  colramp = topo.colors,
                                  xlab = '',
                                  ylab = '',
                                  xlim = c(-1,1),
                                  ylim = c(-1,1),
                                  nrpoints = 0,
                                  axes = FALSE)
            
            Axis(side = 1, at = at, labels = FALSE)
            
            Axis(side = 2, at = at, labels = FALSE)
        }
    }      
}

dev.off()
