#!/usr/bin/env/R
##
## new fig2: correlations distribution at the four cohorts
##
## requires >32 GB RAM, caution
##
## Izaskun Mallona
## 2nd nov 2017

library(RPostgreSQL)

TASK ='distribution_and_distance_decay_all_cohorts'
HOME = '/home/labs/maplab/imallona'
IHOME = '/imppc/labs/maplab/imallona'
## WD = file.path(HOME, 'correlations_simpler', TASK)
NFS_WD = file.path(IHOME, 'correlations_simpler', TASK)
WD = NFS_WD
DB_CONF <- file.path(IHOME, 'src', 'correlations', 'db_imallona.txt')

## @author mages
add.alpha <- function(col, alpha = 1){
    apply(sapply(col, col2rgb)/255, 2, 
          function(x) 
              rgb(x[1], x[2], x[3], alpha = alpha))  
}

dir.create(WD)

setwd(WD)

source(file.path(IHOME, 'src', 'correlations', 'analysis', 'database_connection.R'))

drv <- dbDriver("PostgreSQL")
db_conf <- get_db_parameters(DB_CONF)

con <- list(tcga = dbConnect(drv,
                      user = db_conf[['user']],
                      password = db_conf[['password']],
                      dbname = 'meth_correlations',
                      host = db_conf[['host']],
                port = db_conf[['port']]),
            
            colonomics = dbConnect(drv,
                user = db_conf[['user']],
                password = db_conf[['password']],
                dbname = 'colonomics_correlations',
                host = db_conf[['host']],
                port = db_conf[['port']]))

print(str(con))

cohorts <- list(
    'colonomics_tumor_no_threshold' = 'colonomics',
    'colonomics_normal_no_threshold' = 'colonomics',
    'coad_no_threshold' = 'tcga',
    'coad_normal_38_no_threshold' = 'tcga')

## corr_threshold <- 0.8
corr_threshold <- 0.5

d <- list()

## png(file.path(WD, 'correlations_by_gc.png'), width = 700, height = 700)
## par(mfrow = c(2, 2))
## ## bottom, left, top and right 
## par(oma = c(4, 4, 3, 0)) # make room (i.e. the 4's) for the overall x and y axis titles
## par(mar = c(6, 2, 2, 1)) # make the plots be closer together
## par(pty = "m")
## par(cex.axis = 1.2,
##     cex.lab = 1.2,
##     cex.main = 1.4,
##     cex.sub = 1.2)


## for (cohort in names(cohorts)) {
##     print(cohort)
##     stmt <- sprintf('select abs(pa.meth_start - pb.meth_start),
##                       c.correlation,
##                       pa.channel, pb.channel,
##                       pa.percentgc, pb.percentgc
 
##              from %s.humanmethylation450_probeinfo pa,
##                %s.humanmethylation450_probeinfo pb,
##                %s.meth_filtered_correlations c
##              where (c.meth_probe_a = pa.probe
##                and c.meth_probe_b = pb.probe
##                and pa.chromosome = pb.chromosome);',
##                     'annotation',
##                     'annotation',
##                     cohort)
##     dat <- get_query_at_once(con[[cohorts[[cohort]]]],
##                              stmt)
##     ## naming convention
##     if (cohort == 'coad')
##         cohort <- 'coad_tumor'

##     colnames(dat) <- c('abs', 'correlation', 'a_channel', 'b_channel',
##                        'a_gc', 'b_gc')

##     dat$mean_gc <- (dat$a_gc + dat$b_gc)/2
##     dat$gc_discrete <- cut(dat$mean_gc, breaks = seq(from = 0, to = 1, by = 0.1))
    
##     boxplot(dat$correlation ~ gc_discrete,
##             data = dat,
##             las = 2,
##             outline = FALSE,
##             boxlwd = 2,
##             lwd = 2,
##             ylim = c(-1,1))
    
##     stripchart(correlation ~ gc_discrete,
##                ## data = head(dat, 5000),
##                data = dat[sample(x = rownames(dat), size = 100000),],
##                vertical = TRUE,
##                method = "jitter", add = TRUE, pch = 20, col = add.alpha('blue', 0.1))

##     boxplot(dat$correlation ~ gc_discrete,
##             data = dat,
##             las = 2,
##             outline = FALSE,
##             boxlwd = 2,
##             lwd = 2,
##             ylim = c(-1,1),
##             add = TRUE,
##             main = gsub('_no_threshold', '', cohort))
    
##     rm(dat)
##     gc()
## }


## mtext('mean GC content', side = 1, outer = TRUE, line = 2, cex = 1.5)
## mtext('correlation (rho)', side = 2, outer = TRUE, line = 2, cex = 1.5)

## dev.off()


## ## by channel


## png(file.path(WD, 'correlations_by_channel.png'), width = 700, height = 700)
## par(mfrow = c(2, 2))
## par(oma = c(4, 4, 3, 0)) # make room (i.e. the 4's) for the overall x and y axis titles
## par(mar = c(6, 2, 2, 1)) # make the plots be closer together
## par(pty = "m")
## par(cex.axis = 1.2,
##     cex.lab = 1.2,
##     cex.main = 1.4,
##     cex.sub = 1.2)

## for (cohort in names(cohorts)) {
##     print(cohort)
##     stmt <- sprintf('select abs(pa.meth_start - pb.meth_start),
##                       c.correlation,
##                       pa.channel, pb.channel,
##                       pa.percentgc, pb.percentgc
 
##              from %s.humanmethylation450_probeinfo pa,
##                %s.humanmethylation450_probeinfo pb,
##                %s.meth_filtered_correlations c
##              where (c.meth_probe_a = pa.probe
##                     and c.meth_probe_b = pb.probe
##                     and pa.chromosome = pb.chromosome);',
##                     'annotation',
##                     'annotation',
##                     cohort)
##     dat <- get_query_at_once(con[[cohorts[[cohort]]]],
##                              stmt)
    
##     colnames(dat) <- c('abs', 'correlation', 'a_channel', 'b_channel',
##                        'a_gc', 'b_gc')

##     ## naming convention
##     if (cohort == 'coad')
##         cohort <- 'coad_tumor'

##     dat$channel <- NA
##     for (i in 1:nrow(dat)) {
##         dat$channel[i] <- paste(sort(c(dat$a_channel[i], dat$b_channel[i])),
##                                 collapse = ' ')
##     }
    

##     boxplot(dat$correlation ~ channel,
##             data = dat,
##             las = 2,
##             outline = FALSE,
##             boxlwd = 2,
##             lwd = 2,
##             ylim = c(-1,1))
    
##     stripchart(correlation ~ channel,
##                ## data = head(dat, 5000),
##                data = dat[sample(x = rownames(dat), size = 100000),],
##                vertical = TRUE,
##                method = "jitter", add = TRUE, pch = 20, col = add.alpha('blue', 0.1))

##     boxplot(dat$correlation ~ channel,
##             data = dat,
##             las = 2,
##             outline = FALSE,
##             boxlwd = 2,
##             lwd = 2,
##             ylim = c(-1,1),
##             add = TRUE,
##             main = gsub('_no_threshold', '', cohort))
   
## }


## mtext('channel', side = 1, outer = TRUE, line = 2, cex = 1.5)
## mtext('correlation (rho)', side = 2, outer = TRUE, line = 2, cex = 1.5)

## dev.off()


## gauss


## including probes at different chromosomes
## 1% of tablesampling in order to speed up the query
pdf(file.path(WD, 'correlations_any_distance.pdf'))
par(mfrow = c(2, 2))
par(oma = c(4, 4, 3, 0)) # make room (i.e. the 4's) for the overall x and y axis titles
par(mar = c(6, 2, 2, 1)) # make the plots be closer together
par(pty = "m")
par(cex.axis = 1.2,
    cex.lab = 1.2,
    cex.main = 1.4,
    cex.sub = 1.2)

for (cohort in names(cohorts)) {
    print(cohort)

    stmt <- sprintf('
select
 c.correlation,
 abs(pa.meth_start - pb.meth_start)
from
 (select *
  from %s.meth_filtered_correlations_chr10_chr10
  -- tablesample system (11) repeatable (123)
 ) as c,
 annotation.humanmethylation450_probeinfo pa,
 annotation.humanmethylation450_probeinfo pb
where
 (c.meth_probe_a = pa.probe
   and c.meth_probe_b = pb.probe)
;', cohort)
    
    dat <- get_query_at_once(con[[cohorts[[cohort]]]],
                             stmt)

    print(dim(dat))
    
    cohort <- gsub('_no_threshold', '', cohort)
    
    ## naming convention
    if (cohort == 'coad')
        cohort <- 'coad_tumor'

    neg <- dat[dat$correlation < -corr_threshold,]
    pos <- dat[dat$correlation >= corr_threshold,]
    
    junk.x = NULL
    junk.y = NULL
    for (item in c('dat', 'pos', 'neg')){
        item <- get(item)
        if (nrow(item) > 2) {
            item <- item[,'abs']
            junk.x = c(junk.x, density(item)$x)
            junk.y = c(junk.y, density(item)$y)
        }
    }
    
    xr <- range(junk.x)
    yr <- range(junk.y)
    
    plot(density(pos$abs),
         ## xlim = xr,
         xlim = c(0, 1.5e8),
         ylim = yr,
         ## main = sprintf('%s close %s pb distances distribution',
         ##     main, distance_threshold),
         main = cohort,
         xlab = 'distance', ylab = 'density',
         col = 'darkgreen',
         lty = 1, pch = 3, lwd = 2)

    if (nrow(neg) > 2) {
        lines(density(neg$abs),
              xlim = xr, ylim = yr,
              col = 'darkred',
              lty = 1, pch = 4, lwd = 2)
    } else if (nrow(neg) <= 2) {
        abline(h = 0, lty = 1, pch = 4, lwd = 2, col = 'darkred')
    }

    lines(density(dat$abs), lty = 3, pch = NA, lwd = 2,
          xlim = xr, ylim = yr,
          col = 'black')

    legend('topright', c(sprintf("total (%s)", nrow(dat)),
                         sprintf("rho >= %s (%s)", corr_threshold, nrow(pos)),
                         sprintf("rho < -%s (%s)", corr_threshold, nrow(neg))),
           col = c('black', 'darkgreen', 'darkred'),
           lty = c(3, 1, 1),
           merge = TRUE, lwd = 2)
}


mtext('distance', side = 1, outer = TRUE, line = 2, cex = 1.5)
mtext('frequency', side = 2, outer = TRUE, line = 2, cex = 1.5)

dev.off()


## close distances

## 1% tablesampling to speed up the analysis
pdf(file.path(WD, 'correlations_close_distance.pdf'))
par(mfrow = c(2, 2))
par(oma = c(4, 4, 3, 0)) # make room (i.e. the 4's) for the overall x and y axis titles
par(mar = c(6, 2, 2, 1)) # make the plots be closer together
par(pty = "m")
par(cex.axis = 1.2,
    cex.lab = 1.2,
    cex.main = 1.4,
    cex.sub = 1.2)

for (cohort in names(cohorts)) {
    print(cohort)
   
    stmt <- sprintf('
select
 abs(pa.meth_start - pb.meth_start),
 c.correlation
from
 (select *
  from
   %s.meth_filtered_correlations_chr10_chr10
   -- tablesample system (1) repeatable (123)
  ) as c,
  annotation.humanmethylation450_probeinfo pa,
  annotation.humanmethylation450_probeinfo pb
 where
 (c.meth_probe_a = pa.probe
   and c.meth_probe_b = pb.probe
   and (abs(pa.meth_start - pb.meth_start) < 10000))

;',
                    cohort,
                    'annotation',
                    'annotation')
    
    dat <- get_query_at_once(con[[cohorts[[cohort]]]],
                             stmt)

    print(dim(dat))
    
    cohort <- gsub('_no_threshold', '', cohort)
    
    ## naming convention
    if (cohort == 'coad')
        cohort <- 'coad_tumor'

    neg <- dat[dat$correlation < -corr_threshold,]
    pos <- dat[dat$correlation >= corr_threshold,]
    
    junk.x = NULL
    junk.y = NULL
    for (item in c('dat', 'pos', 'neg')){
        item <- get(item)
        if (nrow(item) > 2) {
            item <- item[,'abs']
            junk.x = c(junk.x, density(item)$x)
            junk.y = c(junk.y, density(item)$y)
        }
    }
    
    xr <- range(junk.x)
    yr <- range(junk.y)
    
    plot(density(pos$abs),
         xlim = xr,
         ylim = yr,
         ## main = sprintf('%s close %s pb distances distribution',
         ##     main, distance_threshold),
         main = cohort,
         xlab = 'distance', ylab = 'density',
         col = 'darkgreen',
         lty = 1, pch = 3, lwd = 2)

    if (nrow(neg) > 2) {
        lines(density(neg$abs),
              xlim = xr, ylim = yr,
              col = 'darkred',
              lty = 1, pch = 4, lwd = 2)
    } else if (nrow(neg) <= 2) {
        abline(h = 0, lty = 1, pch = 4, lwd = 2, col = 'darkred')
    }

    lines(density(dat$abs), lty = 3, pch = NA, lwd = 2,
          xlim = xr, ylim = yr,
          col = 'black')
    
    legend('topright', c(sprintf("total (%s)", nrow(dat)),
                         sprintf("rho >= %s (%s)", corr_threshold, nrow(pos)),
                         sprintf("rho < -%s (%s)", corr_threshold, nrow(neg))),
           col = c('black', 'darkgreen', 'darkred'),
           lty = c(3, 1, 1),
           merge = TRUE, lwd = 2)
}


mtext('distance', side = 1, outer = TRUE, line = 2, cex = 1.5)
mtext('frequency', side = 2, outer = TRUE, line = 2, cex = 1.5)

dev.off()


## gauss

pdf(file.path(WD, 'correlations_gauss_any_distance.pdf'))
par(mfrow = c(2, 2))
par(oma = c(4, 4, 3, 0)) # make room (i.e. the 4's) for the overall x and y axis titles
par(mar = c(6, 2, 2, 1)) # make the plots be closer together
par(pty = "m")
par(cex.axis = 1.2,
    cex.lab = 1.2,
    cex.main = 1.4,
    cex.sub = 1.2)

for (cohort in names(cohorts)) {
    print(cohort)
    
    stmt <- sprintf('
select
 correlation
from
 %s.meth_filtered_correlations
-- tablesample system (1) repeatable (123)
;', cohort)
    dat <- get_query_at_once(con[[cohorts[[cohort]]]],
                             stmt)
    print(dim(dat))

    cohort <- gsub('_no_threshold', '', cohort)
    
    ## naming convention
    if (cohort == 'coad')
        cohort <- 'coad_tumor'
    
    plot(density(dat$correlation),
         xlim = c(-1,1),
         main = cohort,
         lty = 1, pch = 3, lwd = 2,
         xlab = '')

    abline(v = 0, lty = 2)
}


mtext('correlation (rho)', side = 1, outer = TRUE, line = 2, cex = 1.5)
mtext('frequency', side = 2, outer = TRUE, line = 2, cex = 1.5)

dev.off()


## gauss close


pdf(file.path(WD, 'correlations_gauss_close_distance.pdf'))
par(mfrow = c(2, 2))
par(oma = c(4, 4, 3, 0)) # make room (i.e. the 4's) for the overall x and y axis titles
par(mar = c(6, 2, 2, 1)) # make the plots be closer together
par(pty = "m")
par(cex.axis = 1.2,
    cex.lab = 1.2,
    cex.main = 1.4,
    cex.sub = 1.2)

for (cohort in names(cohorts)) {
    print(cohort)
    stmt <- sprintf('
select
 abs(pa.meth_start - pb.meth_start),
 c.correlation
from
 (select *
  from
   %s.meth_filtered_correlations_chr10_chr10
   -- tablesample system (1) repeatable (123)
  ) as c,
  annotation.humanmethylation450_probeinfo pa,
  annotation.humanmethylation450_probeinfo pb
 where
 (c.meth_probe_a = pa.probe
   and c.meth_probe_b = pb.probe
  and (abs(pa.meth_start - pb.meth_start) < 10000))

;',
                    cohort,
                    'annotation',
                    'annotation')
    
    dat <- get_query_at_once(con[[cohorts[[cohort]]]],
                             stmt)
    print(dim(dat))

    cohort <- gsub('_no_threshold', '', cohort)
    
    ## naming convention
    if (cohort == 'coad')
        cohort <- 'coad_tumor'
    
    plot(density(dat$correlation),
         xlim = c(-1,1),
         main = cohort,
         lty = 1, pch = 3, lwd = 2,
         xlab = '')

    abline(v = 0, lty = 2)
}


mtext('correlation (rho)', side = 1, outer = TRUE, line = 2, cex = 1.5)
mtext('frequency', side = 2, outer = TRUE, line = 2, cex = 1.5)

dev.off()
