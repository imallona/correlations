#!/bin/bash

# generates circos-compliant tracks from cpg and gene densities (by bins)

WD=~/tracks
BIN_SIZE=10000

mkdir -p $WD
cd $WD

echo 'Getting illumina 450k probes'

psql -h overlook -p 5432 -U imallona \
    -d meth_correlations -t -A -F"," \
    -c "select chromosome, meth_start, meth_end 
    from coad.humanmethylation450_probeinfo;" \
        | awk '{FS=","; OFS="\t"; print "chr"$1,$2,$3}' > probes.bed

sortBed -i probes.bed > probes_sorted.bed

mysql --user=genome --host=genome-mysql.cse.ucsc.edu -A -e     "select chrom, size from hg19.chromInfo" > hg19.genome

bedtools makewindows -g hg19.genome -w "$BIN_SIZE" > windows.bed

bedtools coverage -a probes.bed \
    -b windows.bed -counts > probes_coverage_by_"$BIN_SIZE"_bins.bed

echo 'Getting ensGene genes'

mysql --user=genome --host=genome-mysql.cse.ucsc.edu -A -e \
    " select chrom, txStart, txEnd from hg19.ensGene" | \
    awk '{OFS=FS="\t"; if (NR> 1) print $1,$2,$3}' | sortBed > ensgene.bed

bedtools coverage -a ensgene.bed \
    -b windows.bed -counts > ensgene_coverage_by_"$BIN_SIZE"_bins.bed

# taking out the noncanonical chromosomes, ungl and the like
sed 's/chr/hs/g'  probes_coverage_by_"$BIN_SIZE"_bins.bed | grep -v _ \
    | grep -v M >  probes_coverage_by_"$BIN_SIZE"_bins.circos.dat

sed 's/chr/hs/g'  ensgene_coverage_by_"$BIN_SIZE"_bins.bed | grep -v _ \
    | grep -v M >  ensgene_coverage_by_"$BIN_SIZE"_bins.circos.dat

rm probes.bed probes_sorted.bed windows.bed ensgene.bed probes_coverage_by_"$BIN_SIZE"_bins.bed ensgene_coverage_by_"$BIN_SIZE"_bins.bed hg19.genome
