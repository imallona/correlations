#!/bin/bash
#$ -S /bin/bash
#$ -e /imppc/labs/maplab/imallona/jobs/correlations_array/coad_mini_no_threshold-e.log
#$ -o /imppc/labs/maplab/imallona/jobs/correlations_array/coad_mini_no_threshold-o.log
#$ -N coad_mini_no_threshold
#$ -t 1-2
#$ -q imppc12
#$ -V
#$ -M imallona@imppc.org
#$ -m e
#$ -l exclusive

# set +x
SRC="$IHOME"/src/correlations

FOLDER=/home/labs/maplab/imallona/TCGA_Correlations
PARSER="$SRC"/stable_fchen_parser/Parser.R

USER="imallona"
PASSWORD=
DBNAME="meth_correlations"
HOST="overlook"
PORT=5432

# that is, we will overestimate the cpu number 1.25 times
NPROC_OVERLOAD=1.25
NTHREADS=$(echo "$(nproc)*$NPROC_OVERLOAD" | bc)

# array job naming (each line at SEEDFILE is an input filename)
SEEDFILE="$SRC"/combinations_reversed_autosomes
# mind that the file at $SEEDFILE contains something like '2 20 X', that is,
# is the second job and will involve chr20 and chrX
SEED=$(awk -v  sge="${SGE_TASK_ID}"  'NR==sge' $SEEDFILE)

FIRST_CHROM=$(echo $SEED | cut -d" " -f2)
SECOND_CHROM=$(echo $SEED | cut -d" " -f3)

# database-compliant view and table names start
SCHEMA=coad_mini_no_threshold
METH_BIG_TABLE=humanmethylation450

# data origins
# IMPORTANT mind that with this views it won't work, as the parser needs a primary key on them!
# FIRST_VIEW=chr"$FIRST_CHROM"_meth
# SECOND_VIEW=chr"$SECOND_CHROM"_meth

# data destination
FILTERED_CORR_TABLE=meth_filtered_correlations_chr${FIRST_CHROM}_chr${SECOND_CHROM}

# database-compliant view and table names end

# cut-offs for standard deviation
METH_SD_THRESHOLD=0.05

# cut-off for correlation coefficient and pvalue
METH_CORR_THRESHOLD=0
METH_PVAL_THRESHOLD=1

echo Starting at $(date) for $SEED and $FIRST_CHROM and $SECOND_CHROM as chromosomes
# echo Tables involved are "$FIRST_VIEW", "$SECOND_VIEW" and "$FILTERED_CORR_TABLE" from schema "$SCHEMA"

# Rscript $PARSER --args "readPath <- \"$FOLDER\"" "writePath <- readPath" "corFromTableToTable(drv=dbDriver(\"PostgreSQL\"), user=\"$USER\", password=\"$PASSWORD\", host=\"$HOST\", dbname=\"$DBNAME\", port=\"$PORT\", from.schema = \"coad\", from.table = \"illuminahiseq_rnaseqv2\", to.schema = \"coad\", to.table = \"expr_filtered_correlations\", stdev.threshold.from=0.5, pval.threshold=0.05, nthreads=20)"

# Rscript $PARSER --args "readPath <- \"$FOLDER\"" "writePath <- readPath" "corFromTableToTable(drv=dbDriver(\"PostgreSQL\"), user=\"$USER\", password=\"$PASSWORD\", host=\"$HOST\", dbname=\"$DBNAME\", port=\"$PORT\", from.schema = \"coad\", from.table = \"humanmethylation450\" , from.join = \"coad.humanmethylation450_probeinfo\", from.condition = \"humanmethylation450.probe = humanmethylation450_probeinfo.probe AND chromosome = \'22\'\", and.schema = \"coad\", and.table = \"illuminahiseq_rnaseqv2\", and.join = \"coad.illuminahiseq_rnaseqv2_probeinfo\", and.condition = \"illuminahiseq_rnaseqv2.exon = illuminahiseq_rnaseqv2_probeinfo.exon AND chromosome = \'22\'\", to.schema = \"coad\", to.table = \"meth_expr_filtered_correlations\", stdev.threshold.from=0.05, stdev.threshold.and=0.5, pval.threshold=0.1, nthreads=12)"

# Rscript $PARSER --args "readPath <- \"$FOLDER\"" "writePath <- readPath" "corFromTableToTable(drv=dbDriver(\"PostgreSQL\"), user=\"$USER\", password=\"$PASSWORD\", host=\"$HOST\", dbname=\"$DBNAME\", port=\"$PORT\", from.schema=\"coad\", from.table=\"$FIRST_VIEW\", and.schema=\"coad\", and.table=\"$SECOND_VIEW\", to.schema=\"coad\", to.table=\"$FILTERED_CORR_TABLE\", stdev.threshold.from=$METH_SD_THRESHOLD, stdev.threshold.and=$METH_SD_THRESHOLD, pval.threshold=$METH_PVAL_THRESHOLD, corr.threshold=$METH_CORR_THRESHOLD, nthreads=$NTHREADS)" 

# Rscript $PARSER --args "readPath <- \"$FOLDER\"" "writePath <- readPath" "corFromTableToTable(drv=dbDriver(\"PostgreSQL\"), user=\"$USER\", password=\"$PASSWORD\", host=\"$HOST\", dbname=\"$DBNAME\", port=\"$PORT\", from.schema=\"coad\", from.table=\"chr1_meth\", and.schema=\"coad\", and.table=\"chr1_meth\", to.schema=\"coad\", to.table=\"meth_filtered_correlations_chr1_chr1\", stdev.threshold.from=0.05, stdev.threshold.and=0.05, pval.threshold=0.01, corr.threshold=0.5, nthreads=12)" 



if [[ $FIRST_CHROM -eq $SECOND_CHROM ]]
then
    echo Got an intrachromosome job for $FIRST_CHROM

    Rscript $PARSER --args "readPath <- \"$FOLDER\"" "writePath <- readPath" "corFromTableToTable(drv=dbDriver(\"PostgreSQL\"), \
user=\"$USER\", \
password=\"$PASSWORD\", \
host=\"$HOST\", \
dbname=\"$DBNAME\", \
port=\"$PORT\", \
from.schema = \"$SCHEMA\", \
from.table = \"$METH_BIG_TABLE\" , \
from.join = \"coad.humanmethylation450_probeinfo\",
from.condition = \"humanmethylation450.probe = humanmethylation450_probeinfo.probe AND chromosome = \'$FIRST_CHROM\'\", 
to.schema = \"$SCHEMA\", \
to.table = \"$FILTERED_CORR_TABLE\", \
stdev.threshold.from = $METH_SD_THRESHOLD, \
stdev.threshold.and = $METH_SD_THRESHOLD, \
pval.threshold = $METH_PVAL_THRESHOLD, \
corr.threshold = $METH_CORR_THRESHOLD, \
nthreads = $NTHREADS )" 

else

    echo Got an interchromosome job for $FIRST_CHROM and $SECOND_CHROM

    Rscript $PARSER --args "readPath <- \"$FOLDER\"" "writePath <- readPath" "corFromTableToTable(drv=dbDriver(\"PostgreSQL\"), \
user=\"$USER\", \
password=\"$PASSWORD\", \
host=\"$HOST\", \
dbname=\"$DBNAME\", \
port=\"$PORT\", \
from.schema = \"$SCHEMA\", \
from.table = \"$METH_BIG_TABLE\" , \
from.join = \"coad.humanmethylation450_probeinfo\",
from.condition = \"humanmethylation450.probe = humanmethylation450_probeinfo.probe AND chromosome = \'$FIRST_CHROM\'\", 
and.schema = \"$SCHEMA\", \
and.table = \"$METH_BIG_TABLE\", \
and.join = \"coad.humanmethylation450_probeinfo\",
and.condition = \"humanmethylation450.probe = humanmethylation450_probeinfo.probe AND chromosome = \'$SECOND_CHROM\'\", 
to.schema = \"$SCHEMA\", \
to.table = \"$FILTERED_CORR_TABLE\", \
stdev.threshold.from = $METH_SD_THRESHOLD, \
stdev.threshold.and = $METH_SD_THRESHOLD, \
pval.threshold = $METH_PVAL_THRESHOLD, \
corr.threshold = $METH_CORR_THRESHOLD, \
nthreads = $NTHREADS )" 

fi

echo Ending at $(date) for $SEED and $FIRST_CHROM and $SECOND_CHROM as chromosomes
