#!/bin/bash

set +x

CORR_SRC="$ISHARE"/imallona2fchen/TCGAParser/R
PARSER="$CORR_SRC"/Parser.R

Rscript $PARSER --args "coad" "humanmethylation450" "$TMP" "$TMP" "download"