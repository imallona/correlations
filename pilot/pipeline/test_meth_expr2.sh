#!/bin/bash
#$ -S /bin/bash
#$ -e /imppc/labs/maplab/imallona/jobs/correlations_array/expr_meth-test2-e.log
#$ -o /imppc/labs/maplab/imallona/jobs/correlations_array/expr_meth-test2-o.log
#$ -N expr_meth-test2
#$ -t 1,219
#$ -q imppc
#$ -V
#$ -M imallona@imppc.org
#$ -m e
#$ -l exclusive

# set +x
SRC="$IHOME"/src/correlations

FOLDER=/home/labs/maplab/imallona/TCGA_Correlations
PARSER="$SRC"/stable_fchen_parser/Parser.R

USER="imallona"
PASSWORD=""
DBNAME="meth_correlations"
HOST="overlook"
PORT=5432

# that is, we will overestimate the cpu number to NPROC_OVERLOAD times
NPROC_OVERLOAD=1.25
NTHREADS=$(echo "$(nproc)*$NPROC_OVERLOAD" | bc)

# array job naming (each line at SEEDFILE is an input filename)
SEEDFILE="$SRC"/combinations_reversed_autosomes
# mind that the file at $SEEDFILE contains something like '2 20 X', that is,
# is the second job and will involve chr20 and chrX
SEED=$(awk -v  sge="${SGE_TASK_ID}"  'NR==sge' $SEEDFILE)

FIRST_CHROM=$(echo $SEED | cut -d" " -f2)
SECOND_CHROM=$(echo $SEED | cut -d" " -f3)

# database-compliant view and table names start
SCHEMA=coad
METH_BIG_TABLE=humanmethylation450
# mind that this is a log2(a+1) transformed version of the exons' rpkms
LOG_EXPR_BIG_TABLE=illuminahiseq_rnaseqv2_log

# data destination
# FILTERED_CORR_TABLE=filtered_correlations_expr_chr${FIRST_CHROM}_meth_chr${SECOND_CHROM}

# probe and exon information
METH_ANNOTATION="$SCHEMA".humanmethylation450_probeinfo
EXPR_ANNOTATION="$SCHEMA".illuminahiseq_rnaseqv2_probeinfo

# database-compliant view and table names end

# cut-offs for standard deviation
METH_SD_THRESHOLD=0.05
LOG_EXPR_SD_THRESHOLD=0.25

# cut-off for correlation coefficient and pvalue
CORR_THRESHOLD=0.5
PVAL_THRESHOLD=0.01

echo Starting at $(date) for $SEED and $FIRST_CHROM and $SECOND_CHROM as chromosomes

if [[ $FIRST_CHROM -eq $SECOND_CHROM ]]
then
    echo Got an intrachromosome job for $FIRST_CHROM

    Rscript $PARSER --args "readPath <- \"$FOLDER\"" "writePath <- readPath" "corFromTableToTable(drv=dbDriver(\"PostgreSQL\"), \
user=\"$USER\", \
password=\"$PASSWORD\", \
host=\"$HOST\", \
dbname=\"$DBNAME\", \
port=\"$PORT\", \
from.schema = \"$SCHEMA\", \
from.table = \"$LOG_EXPR_BIG_TABLE\" , \
from.join = \"$EXPR_ANNOTATION\",
from.condition = \"illuminahiseq_rnaseqv2_log.exon = illuminahiseq_rnaseqv2_probeinfo.exon 
  AND chromosome = \'$FIRST_CHROM\'\", 

and.schema = \"$SCHEMA\", \
and.table = \"$METH_BIG_TABLE\", \
and.join = \"coad.humanmethylation450_probeinfo\",
and.condition = \"humanmethylation450.probe = humanmethylation450_probeinfo.probe AND chromosome = \'$SECOND_CHROM\'\", 

to.schema = \"$SCHEMA\", \
to.table = \"filtered_correlations_expr_chr${FIRST_CHROM}_meth_chr${SECOND_CHROM}\", \
stdev.threshold.from = $LOG_EXPR_SD_THRESHOLD, \
stdev.threshold.and = $METH_SD_THRESHOLD, \
pval.threshold = $PVAL_THRESHOLD, \
corr.threshold = $CORR_THRESHOLD, \
nthreads = $NTHREADS )" 

else

    echo Got two interchromosome jobs for $FIRST_CHROM and $SECOND_CHROM

    # first, first_chrom expr against second_chrom meth
    Rscript $PARSER --args "readPath <- \"$FOLDER\"" "writePath <- readPath" "corFromTableToTable(drv=dbDriver(\"PostgreSQL\"), \
user=\"$USER\", \
password=\"$PASSWORD\", \
host=\"$HOST\", \
dbname=\"$DBNAME\", \
port=\"$PORT\", \
from.schema = \"$SCHEMA\", \
from.table = \"$LOG_EXPR_BIG_TABLE\" , \
from.join = \"$EXPR_ANNOTATION\",
from.condition = \"illuminahiseq_rnaseqv2_log.exon = illuminahiseq_rnaseqv2_probeinfo.exon 
  AND chromosome = \'$FIRST_CHROM\'\", 
and.schema = \"$SCHEMA\", \
and.table = \"$METH_BIG_TABLE\", \
and.join = \"coad.humanmethylation450_probeinfo\",
and.condition = \"humanmethylation450.probe = humanmethylation450_probeinfo.probe AND chromosome = \'$SECOND_CHROM\'\", 
to.schema = \"$SCHEMA\", \
to.table = \"filtered_correlations_expr_chr${FIRST_CHROM}_meth_chr${SECOND_CHROM}\", \
stdev.threshold.from = $LOG_EXPR_SD_THRESHOLD, \
stdev.threshold.and = $METH_SD_THRESHOLD, \
pval.threshold = $PVAL_THRESHOLD, \
corr.threshold = $CORR_THRESHOLD, \
nthreads = $NTHREADS )" 

    # second, first_chrom meth against second_chrom expr
    Rscript $PARSER --args "readPath <- \"$FOLDER\"" "writePath <- readPath" "corFromTableToTable(drv=dbDriver(\"PostgreSQL\"), \
user=\"$USER\", \
password=\"$PASSWORD\", \
host=\"$HOST\", \
dbname=\"$DBNAME\", \
port=\"$PORT\", \
from.schema = \"$SCHEMA\", \
from.table = \"$LOG_EXPR_BIG_TABLE\" , \
from.join = \"$EXPR_ANNOTATION\",
from.condition = \"illuminahiseq_rnaseqv2_log.exon = illuminahiseq_rnaseqv2_probeinfo.exon 
  AND chromosome = \'$SECOND_CHROM\'\", 
and.schema = \"$SCHEMA\", \
and.table = \"$METH_BIG_TABLE\", \
and.join = \"coad.humanmethylation450_probeinfo\",
and.condition = \"humanmethylation450.probe = humanmethylation450_probeinfo.probe AND chromosome = \'$FIRST_CHROM\'\", 
to.schema = \"$SCHEMA\", \
to.table = \"filtered_correlations_expr_chr${SECOND_CHROM}_meth_chr${FIRST_CHROM}\", \
stdev.threshold.from = $LOG_EXPR_SD_THRESHOLD, \
stdev.threshold.and = $METH_SD_THRESHOLD, \
pval.threshold = $PVAL_THRESHOLD, \
corr.threshold = $CORR_THRESHOLD, \
nthreads = $NTHREADS )" 

fi

echo Ending at $(date) for $SEED and $FIRST_CHROM and $SECOND_CHROM as chromosomes
