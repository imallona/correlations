#!/bin/bash 
#
# Gets the hESC HMM broad chromatin states and populates the database with them
# Izaskun Mallona 28 oct 2014


WDIR=$IHOME/tmp/hmm

mkdir -p $WDIR

cd $WDIR

mysql --user=genome --host=genome-mysql.cse.ucsc.edu -e "SELECT chrom, chromStart, chromEnd, name \
    FROM hg19.wgEncodeBroadHmmH1hescHMM" | awk '{ FS="\t"; OFS = "\t";  if (NR> 1) print $0}' > hmm_colors.bed


psql -h overlook -p 5432 -U imallona -d meth_correlations -A -F ',' \
    -c 'SELECT chromosome, meth_start, meth_end, probe
        FROM coad.humanmethylation450_probeinfo' \
            |  awk '{ FS=","; OFS = "\t";  if (NR> 1) print "chr"$1,$2,$3,$4}'  \
            | head -n-1 | sortBed > probe_info.bed

# only intersecting features will be printed
intersectBed -a probe_info.bed \
    -b hmm_colors.bed \
    -wo > probes_intersected.bed 
    
cut -f4,8 probes_intersected.bed > probes_to_color.tsv



# the idea is to fetch the data here and to intersectbed with colors


psql -h overlook -p 5432 -U imallona -d meth_correlations -A -F ',' \
    -c "CREATE TABLE coad.humanmethylation_hmm (
         probe VARCHAR(16),
         hmm VARCHAR,
         PRIMARY KEY (probe),
         FOREIGN KEY (probe) REFERENCES coad.humanmethylation450_probeinfo (probe)
        );

        \copy coad.humanmethylation_hmm FROM '/imppc/labs/maplab/imallona/tmp/hmm/probes_to_color.tsv' DELIMITER E'\t'
        "
