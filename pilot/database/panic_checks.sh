#!/bin bash

HOST=overlook
PORT=5432
USER=imallona
DB=meth_correlations

WD="/home/labs/maplab/imallona/db_checks"

INTERVAL_COUNTER="$IHOME/src/correlations/database/correlation_counter_by_intervals.sql"

mkdir -p $WD
cd $WD

pwd

date > start

echo 'Offending probe' 
psql -h $HOST -p $PORT -U $USER \
    -d $DB -t -A -F"," \
    -c "select * from coad.meth_filtered_correlations 
        where meth_probe_a = 'cg21754388' 
         and meth_probe_b = 'cg23069057';" > debug1.txt

echo 'Number of correlations by bin'
 
psql -h $HOST -p $PORT -U $USER \
    -d $DB -f $INTERVAL_COUNTER > debug2.txt

date > end

echo 'Offending_probe again'
psql -h $HOST -p $PORT -U $USER \
    -d $DB \
    -c "select * from coad.meth_filtered_correlations_chr4_chr13
        where meth_probe_a = 'cg21754388' 
         and meth_probe_b = 'cg23069057';

       select * from coad.meth_filtered_correlations_chr4
        where meth_probe_a = 'cg23069057';

       select * from coad.meth_filtered_correlations
        where meth_probe_a = 'cg23069057';" > debug3.txt


echo 'Inheritance checks'
psql -h $HOST -p $PORT -U $USER \
    -d $DB \
    -c "
SELECT
    nmsp_parent.nspname AS parent_schema,
    parent.relname      AS parent,
    nmsp_child.nspname  AS child_schema,
    child.relname       AS child
FROM pg_inherits
    JOIN pg_class parent            ON pg_inherits.inhparent = parent.oid
    JOIN pg_class child             ON pg_inherits.inhrelid   = child.oid
    JOIN pg_namespace nmsp_parent   ON nmsp_parent.oid  = parent.relnamespace
    JOIN pg_namespace nmsp_child    ON nmsp_child.oid   = child.relnamespace
WHERE parent.relname='meth_filtered_correlations';



SELECT
    nmsp_parent.nspname AS parent_schema,
    parent.relname      AS parent,
    nmsp_child.nspname  AS child_schema,
    child.relname       AS child
FROM pg_inherits
    JOIN pg_class parent            ON pg_inherits.inhparent = parent.oid
    JOIN pg_class child             ON pg_inherits.inhrelid   = child.oid
    JOIN pg_namespace nmsp_parent   ON nmsp_parent.oid  = parent.relnamespace
    JOIN pg_namespace nmsp_child    ON nmsp_child.oid   = child.relnamespace
WHERE parent.relname='meth_filtered_correlations_chr4';" > debug4.txt


echo 'Offending probe'
psql -h $HOST -p $PORT -U $USER \
    -d $DB \
    -c "
SELECT * 
FROM coad.meth_filtered_correlations 
WHERE meth_probe_a = 'cg23069057' OR meth_probe_b = 'cg23069057';
" > debug5.txt


echo 'Offending probe only'

psql -h $HOST -p $PORT -U $USER \
    -d $DB \
    -c "
SELECT * 
FROM ONLY coad.meth_filtered_correlations 
WHERE meth_probe_a = 'cg23069057' OR meth_probe_b = 'cg23069057';
" > debug6.txt

echo 'Chromosome existence checks'

