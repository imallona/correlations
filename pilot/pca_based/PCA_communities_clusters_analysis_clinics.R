Dire<-"/imppc/labs/maplab/adiez/correlations/PCA_communitites_analysis/"
source(paste0(Dire,"scripts/data_meth_download.R"))
source(paste0(Dire,"scripts/common_patients.R"))


#ens conectem a la BBDD
library(RPostgreSQL)
connect <- function(){
  dbhost = "overlook"
  drv <- dbDriver("PostgreSQL")
  dbname <-"regional_profile"
  dbuser <- "adiez"
  dbpass <- "totoroMOLA"
  dbport <- 5432
  return (dbConnect(drv, host=dbhost, port=dbport, dbname=dbname, 
                    user=dbuser, password=dbpass))
}

con <- connect()


#dades clinicas processades pel paper del quim
clinical<-read.table("/imppc/labs/maplab/adiez/correlations/PCA_communitites_analysis/data/clinic_processed.tab",sep="\t",header=TRUE)
clinical<-clinical[,c(1,3:13,17:33,36)]
#clinical$tumor_sample_procurement_country[clinical$tumor_sample_procurement_country=="Israel"]<-"Other"
#clinical$tumor_sample_procurement_country[clinical$tumor_sample_procurement_country=="Poland"]<-"Other"
#clinical$tumor_sample_procurement_country[clinical$tumor_sample_procurement_country=="Russia"]<-"Other"

##################################33
#dades expressio
ddTexpression <- dbSendQuery(con, paste0("select * from coad_tumor.illuminahiseq_rnaseqv2_by_gene;"))
ddTexpression <- fetch(ddTexpression, n = -1)
colnames(ddTexpression) <- toupper(colnames(ddTexpression))
for(i in 1:6) colnames(ddTexpression) <- sub("_","-",colnames(ddTexpression))

ddNexpression <- dbSendQuery(con, paste0("select * from coad_normal.illuminahiseq_rnaseqv2_by_gene;"))
ddNexpression <- fetch(ddNexpression, n = -1)
colnames(ddNexpression) <- toupper(colnames(ddNexpression))
for(i in 1:6) colnames(ddNexpression) <- sub("_","-",colnames(ddNexpression))

###########################################33
#probes annotations
probes <- dbSendQuery(con, paste0("select * from annotations.humanmethylation450kannot;"))
probes <- fetch(probes, n = -1)
probes<-probes[probes$probetype!="rs",]

##############
#fitxers comunitats
fitxers<-list.files(paste0(Dire,"results/"),pattern="clustering_communities")
label_fitx<-c("Normal","Tumor")

#######################################################################33
#PCA & correlation between PC1 and clinical data

taula_resum<-matrix(0,ncol=20,nrow=1)
for(i in 1:length(fitxers)){
  dd<-read.table(paste0(Dire,"results/",fitxers[i]),sep="\t",header=TRUE,stringsAsFactors=FALSE)
  dd<-data.frame(probe=dd$Row.names,community=dd$community,cluster=dd$clustcut)
  
  #filtrem les comunitats que tenen mes de 50 probes
  taula<-as.data.frame(table(dd$community))
  communities<-as.numeric(as.character(taula[which(taula$Freq>50),1]))
  dd<-dd[dd$community%in%communities,]
  community_names<-as.numeric(names(table(dd$community)))
  
  for(j in 1:length(community_names)){
    dd2<-dd[dd$community==community_names[j],]
    
    #filtrem els clusters que tenen mes de 30 probes
    taula<-as.data.frame(table(dd2$cluster))
    clusters<-as.numeric(as.character(taula[which(taula$Freq>30),1]))
    dd2<-dd2[dd2$cluster%in%clusters,]
    cluster_names<-as.numeric(names(table(dd2$cluster)))
    if(dim(dd2)[1]>0){
      for(k in 1:length(cluster_names)){
        dir.create(paste0(Dire,"results/",label_fitx[i],"/",label_fitx[i],"_comm",community_names[j],"_clus",cluster_names[k]),showWarnings=FALSE,recursive=TRUE)
        
        dd3<-dd2[dd2$cluster==cluster_names[k],]
        if(dim(dd3)[1]>0){
          
          #carreguem les dades de la BBDD
          probes_names<-as.character(unique(dd3$probe))
          probes_collapsed <- paste0("('", paste(probes_names, collapse="','"), "')")
          
          ddtot<-data_meth_download(con,probes_collapsed)
          ddN<-ddtot$ddN
          ddT<-ddtot$ddT
          
          ddtot<-common_patients(ddN,ddT,ddNexpression,ddTexpression)
          ddN<-ddtot$ddN
          ddT<-ddtot$ddT
          ddNE<-ddtot$ddNE
          ddTE<-ddtot$ddTE
          
          probes2<-probes[probes$probe%in%row.names(ddN),]
          ncpgi<-table(!is.na(probes2$cpgiid))[2]
          chrs<-table(probes2$chr)
          
          nN2<-length(probes_names)-dim(ddN)[1]
          print(paste0("sondes perdudes ",nN2))
          
          #Normal pca
          pcaN<-prcomp(t(ddN[,-1]))
          PC1N<-pcaN$x[,1]
          PC1N<-as.data.frame(PC1N)
          PC1N$patient<-toupper(row.names(PC1N))
          for(guio in 1:6) PC1N$patient <- sub("_","-",PC1N$patient)
          PC1N$codi <- substr(PC1N$patient,1,12)
          
          #Tumor pca
          pcaT<-prcomp(t(ddT[,-1]))
          PC1T<-pcaT$x[,1]
          PC1T<-as.data.frame(PC1T)
          PC1T$patient<-toupper(row.names(PC1T))
          for(guio in 1:6) PC1T$patient <- sub("_","-",PC1T$patient)
          PC1T$codi <- substr(PC1T$patient,1,12)
          
          sdN<-summary(pcaN)$importance[2,1]*100
          sdT<-summary(pcaT)$importance[2,1]*100
          sd2N<-summary(pcaN)$importance[2,2]*100
          sd2T<-summary(pcaT)$importance[2,2]*100
          
          tau_resum_aux<-c(label_fitx[i],community_names[j],cluster_names[k],dim(ddT)[1],nN2,as.numeric(ncpgi),sdN,sdT,sd2N,sd2T)
          
          
          #Normal merge
          ddN <- merge(PC1N, clinical, by="codi", all=FALSE)
          #Tumor merge
          ddT <- merge(PC1T, clinical, by="codi", all=FALSE)
          
          #Numerical variables correlation with PC1    
          numclin<-c(12,14,25,26,28)+2
          tau_numeric<-vector()
          noms_numeric<-vector()
          for(nc in numclin){
            #Normal correlations 
            correlN<-round(cor(ddN[,nc], ddN$PC1N,method="spearman", use="complete"),3)
            pvalN<-round(cor.test(ddN[,nc], ddN$PC1N,method="spearman")$p.val,3)
            
            #Tumor correlations 
            correlT<-round(cor(ddT[,nc], ddT$PC1T,method="spearman", use="complete"),3)
            pvalT<-round(cor.test(ddT[,nc], ddT$PC1T,method="spearman")$p.val,3)
            
            png(paste0(Dire,"results/",label_fitx[i],"/",label_fitx[i],"_comm",community_names[j],"_clus",cluster_names[k],"/",label_fitx[i],"_comm",community_names[j],"_clus",cluster_names[k],"_",colnames(ddN)[nc],".png"),width=1000)
            par(mfrow=c(1,2))
            #Normal
            scatter.smooth(ddN[,nc], ddN$PC1N, ylab="PC1", xlab=paste0(colnames(ddN)[nc]), las=1,
                           lpars=list(col="red",lwd=3),main=paste0(label_fitx[i]," community=",community_names[j],"\nNormal n=",dim(ddN)[1]," sd=",sdN,"\nspear. rho=",correlN," pval=",pvalN))#,cex.main=0.5)
            box(lwd=2)  
            
            #Tumor
            scatter.smooth(ddT[,nc], ddT$PC1T, ylab="PC1", xlab=paste0(colnames(ddT)[nc]), las=1,
                           lpars=list(col="red",lwd=3),main=paste0(label_fitx[i]," community=",community_names[j],"\nTumor n=",dim(ddT)[1]," sd=",sdT,"\nspear. rho=",correlT," pval=",pvalT))#,cex.main=0.5)
            box(lwd=2)  
            dev.off()
            
            tau_num_aux<-c(correlN,correlT)
            noms_num_aux<-paste0(colnames(ddN)[nc],c("_Normal","_Tumor"))
            tau_numeric<-c(tau_numeric,tau_num_aux)
            noms_numeric<-c(noms_numeric,noms_num_aux)
          }
          
          taula_resum<-rbind(taula_resum,c(tau_resum_aux,tau_numeric))
          
          #Categorical variables correlation with PC1
          
          #library(heplots) # for eta
          
          #     charclin<-c(2:11,13,15:24,27,29,30)+2
          #     for(nc in charclin){
          #       png(paste0(Dire,"results/",DataSet,"_",label_fitx[i],community_names[j],"/",label_fitx[i],community_names[j],"_",colnames(ddN)[nc],".png"),width=1000)
          #       par(mfrow=c(1,2))
          #       
          #       #Normal
          #       FreqChar<-as.data.frame(table(as.character(ddN[,nc])))$Freq
          #       if(length(table(as.factor(as.character(ddN[,nc]))))>1) {
          #         pvalN<-sprintf('%e',kruskal.test(ddN$PC1N~as.factor(as.character(ddN[,nc])))$p.val)
          #       } else{
          #         pvalN<-NULL
          #       }
          #       boxplot(ddN$PC1N~as.character(ddN[,nc]), ylab="PC1", xlab=paste0(colnames(ddN)[nc]), las=1)
          #       title(paste0(label_fitx[i]," community=",community_names[j],"\nNormal sd=",sdN, "K-W pval=",pvalN))
          #       par(xpd=TRUE)
          #       text(x=seq(1,length(FreqChar),1),y=max(ddN$PC1N)+1,FreqChar)
          #       box(lwd=2)  
          #       
          #       #Tumor
          #       FreqChar<-as.data.frame(table(ddT[,nc]))$Freq
          #       if(length(table(as.factor(as.character(ddT[,nc]))))>1) {
          #         pvalT<-sprintf('%e',kruskal.test(ddT$PC1T~as.factor(as.character(ddT[,nc])))$p.val)
          #       } else{
          #         pvalT<-NULL
          #       }
          #       boxplot(ddT$PC1T~ddT[,nc], ylab="PC1", xlab=paste0(colnames(ddT)[nc]), las=1)
          #       title(paste0(label_fitx[i]," community=",community_names[j],"\nTumor sd=",sdT," K-W pval=",pvalT))
          #       par(xpd=TRUE)
          #       text(x=seq(1,length(FreqChar),1),y=max(ddT$PC1T)+1,FreqChar)
          #       box(lwd=2)  
          #       dev.off()
          #     }
        }
      }
    }
  }
}
taula_resum<-taula_resum[-1,]
colnames(taula_resum)<-c("data","community","cluster","probes","lost_probes","CpGi_probes","sd_PC1_Normal","sd_PC1_Tumor","sd_PC2_Normal","sd_PC2_Tumor",paste(noms_numeric))
write.table(t(taula_resum),paste0(Dire,"results/community_PCA_summary_table.txt"),sep="\t",quote=FALSE,col.names=FALSE)
