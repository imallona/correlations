############################################################
#clustering with probes in the communities
############################################################

Dire<-"/imppc/labs/maplab/adiez/correlations/PCA_communitites_analysis/"
source(paste0(Dire,"scripts/data_meth_download.R"))
library(gplots)

#ens conectem a la BBDD
library(RPostgreSQL)
connect <- function(){
  dbhost = "overlook"
  drv <- dbDriver("PostgreSQL")
  dbname <-"regional_profile"
  dbuser <- "adiez"
  dbpass <- "totoroMOLA"
  dbport <- 5432
  return (dbConnect(drv, host=dbhost, port=dbport, dbname=dbname, 
                    user=dbuser, password=dbpass))
}

con <- connect()



##############
#fitxers comunitats
fitxers<-list.files(paste0(Dire,"data"),pattern="only_mini")
label_fitx<-c("Tumor","Normal")


#######################################################################33

for(i in 1:length(fitxers)){
  communities<-read.table(paste0(Dire,"data/",fitxers[i]),sep=" ",header=TRUE,stringsAsFactors=FALSE)
  communities<-data.frame(probe=rownames(communities),community=communities$x)
  
  taula<-as.data.frame(table(communities$community))
  communities2<-as.numeric(as.character(taula[which(taula$Freq>50),1]))
  communities<-communities[communities$community%in%communities2,]
  probes_names<-as.character(unique(communities$probe))
  probes_collapsed <- paste0("('", paste(probes_names, collapse="','"), "')")
  
  ###########################################33
  #data download
  ddtot<-data_meth_download(con,probes_collapsed)
  if(label_fitx[i]=="Normal"){
    ddmeth<-ddtot$ddN
    rownames(ddmeth)<-ddmeth[,1]
    ddmeth<-ddmeth[,-1]
  }
  if(label_fitx[i]=="Tumor"){
    ddmeth<-ddtot$ddT
    rownames(ddmeth)<-ddmeth[,1]
    ddmeth<-ddmeth[,-1]
  }
  
  # distmeth<-dist(ddmeth)
  # save(distmeth,file=paste0(Dire,"results/dist_",label_fitx[i],".Rda"))
  
  load(paste0(Dire,"results/dist_",label_fitx[i],".Rda"))
  communities<-communities[ communities$probe%in%rownames(ddmeth),]
  
  if(label_fitx[i]=="Tumor"){
    colores<-rep("forestgreen",length(communities$community))
    colores[communities$community==5]<-"darkred"
    colores[communities$community==2]<-"blue1"
    colores[communities$community==1]<-"deepskyblue3"
    colores[communities$community==6]<-"coral1"
    colores[communities$community==7]<-"darkmagenta"
    colores[communities$community==19]<-"greenyellow"
  }
  if(label_fitx[i]=="Normal"){
    colores<-rep("forestgreen",length(communities$community))
    colores[communities$community==2]<-"darkred"
    colores[communities$community==3]<-"blue1"
    colores[communities$community==4]<-"deepskyblue3"
    colores[communities$community==5]<-"coral1"
    colores[communities$community==6]<-"darkmagenta"
    colores[communities$community==7]<-"greenyellow"
    colores[communities$community==8]<-"firebrick2"
    colores[communities$community==9]<-"saddlebrown"
  }
    
    color.palette= colorRampPalette(c("white","gray","black"))
    clust<-hclust(distmeth,method="ward")
    clust1<-as.dendrogram(clust)
    pdf(paste0(Dire,"results/heatmap_",label_fitx[i],".pdf"))
    heatmap.2(as.matrix(ddmeth),trace="none",Rowv=clust1,labRow="",labCol="",RowSideColors=colores,col=color.palette,margins=c(5,4))
    dev.off()
    
    clustcut<-cutree(clust,length(unique(communities$community)))
    resclus<-as.data.frame(clustcut)
    resclus<-merge(resclus,communities,by.x="row.names",by.y="probe",all=FALSE)  
    write.table(resclus,paste0(Dire,"results/clustering_communities_",label_fitx[i],".tab"),sep="\t",quote=FALSE,row.names=FALSE)
    
    taula<-table(resclus$clustcut,resclus$community)
    write.table(taula,paste0(Dire,"results/clusteringVScommunities_",label_fitx[i],".tab"),sep="\t",quote=FALSE)
    
  }
