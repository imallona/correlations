#!/bin/bash
#
# TCGA ftp wget spider
#
# 18th October 2013
# Izaskun Mallona and someone from the superuser forum, as written below
#

function usage() {
    echo 
    echo Scrapes the TCGA ftp for a given set cancer datasets and techniques
    echo Writes the URLs to the stdout
    echo
    echo USAGE:
    echo
    echo 'tcga_spider.sh <base_path>'
    echo 'tcga_spider.sh <base_path> > output'
    echo
    echo PARAMETERS:
    echo
    echo '  <base_path>        the path the script will be run at'

    exit 1
}
:<<EOF
The function spider was taken from
http://superuser.com/questions/545316/getting-all-the-filenames-not-content-recursively-from-an-http-directory

Some sidenotes:

    This will produce a list which still contains duplicates (of directories), so you need to redirect the output to a file and use uniq for a pruned list.
    --spider causes wget not to download anything, but it still will do a HTTP HEAD request on each of the files it deems to enqueue. This will cause a lot more traffic than is actually needed/intended and cause the whole thing to be quite slow.
    -e robots=off is needed to ignore a robots.txt file which may cause wget to not start searching (which is the case for the server you gave in your question).
    If you have wget 1.14 or newer, you can use --reject-regex="\?C=" to reduce the number of needless requests (for those "sort-by" links already mentioned by @slm). This also eliminates the need for the grep -Ev "\/\?C=" step afterwards.

EOF

function spider {
    wget -d -r -np -N --spider -e robots=off --no-check-certificate  "$1" \
	2>&1 | grep " -> " | grep -Ev "\/\?C=" | sed "s/.* -> //"
}

if [ $# = 0 ]
then
    usage
fi

BASE_PATH=$1

tumors="acc blca brca cesc cntl coad dlbc esca gbm hnsc kich kirc kirp laml lcml lgg lihc lnnh luad lusc meso misc ov paad pcpg prad read sarc skcm stad tgct thca ucec ucs uvm"
base="https://tcga-data.nci.nih.gov/tcgafiles/ftp_auth/distro_ftpusers/anonymous/tumor"
expression="cgcc/unc.edu"
methylation="cgcc/jhu-usc.edu/humanmethylation450"

cd $BASE_PATH

for tumor in ${tumors[@]}; do
    url_expression="$base"/"$tumor"/"$expression"
    spider "$url_expression" #"$tumor"_expression
    
    url_methylation="$base"/"$tumor"/"$methylation"
    spider "$url_methylation" #"$tumor"_methylation
done