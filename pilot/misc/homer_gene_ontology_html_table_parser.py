#!/bin/python

"""
A parser of HOMER's geneOntology.html file that produces tab-separated outputs

Izaskun Mallona, GPL v2
23rd July 2015
"""

from BeautifulSoup import BeautifulSoup
import os.path as op

IHOME = '/imppc/labs/maplab/imallona'
WD = op.join(IHOME, 'correlations', 'communities_subclusters')

fn = op.join(WD, 'normal', 'comm_1_clust_1', 'go', 'geneOntology.html')


"""
@param in_fn the homer's geneOntology.html output
@param out_fn the filename (full_path) to save the csv version of the input html

"""
def extract_homer_annotation_table(in_fn, out_fn):
    with open(in_fn, 'rb') as fh:
        page = fh.read()
        
    soup = BeautifulSoup(page)
    
    table = soup.find("table", border=1)
    
    tsv = '\t'.join(('p-value', 'term', 'go_tree', 'go_id', 'num_of_genes_in_term',
                    'num_of_target_genes_in_term', 'num_of_total_genes', 'num_of_target_genes', 
                     'num_of_common_genes', '\n'))
    
    for row in table.findAll('tr')[1:]:
        col = row.findAll('td')    
        pval = col[0].string
        term = col[2].string
        go_tree = col[3].string
        goid = col[4].string
        num_genes_term = col[5].string
        num_targets_term = col[6].string
        num_total_genes = col[7].string
        num_total_targets = col[8].string
        
        record = (pval, term, go_tree, goid, num_genes_term, num_targets_term, num_total_genes, 
                  num_total_targets)
        
        tsv += '\t'.join(record) + '\n'
        
    with open(out_fn, 'wb') as fo:
        fo.write(tsv)


if __name__ == '__main__':
    datasets = ('normal', 'tumor')
    for dataset in datasets:
        for i in range(9):
            for j in range(9):
                curr_dir = op.join(WD, dataset, 'comm_%s_clust_%s' %(i,j), 'go')
                if op.isfile(op.join(curr_dir, 'geneOntology.html')) :
                    print('Processing %s comm %s clus %s' %(dataset, i, j))
                    extract_homer_annotation_table(in_fn = op.join(curr_dir, 'geneOntology.html'),
                                                   out_fn = op.join(curr_dir, 'geneOntology.tsv'))
                         
    print('Done')
