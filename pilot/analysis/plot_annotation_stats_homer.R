#!/usr/bin/env R
#
## 30 june 2015
## 

IHOME <- '/imppc/labs/maplab/imallona'
WD <- file.path(IHOME, 'correlations', 'communities_subclusters')

## max number of clusters 
NUM <- 9
DATASETS <- c('normal', 'tumor')

## data for normal and tumors, separately

plot_homer_annotations <- function(fn, label) {
    
    ## normal <- read.table(file.path(WD, 'anna', 'clustering_communities_Normal.tab'), header = TRUE)
    ## tumor <- read.table(file.path(WD, 'anna', 'clustering_communities_Tumor.tab'), header = TRUE)

    
    dat <- read.table(fn, header = TRUE, sep = "\t")
    broad <- dat[1:12,]
    broad[,2] <- as.numeric(as.character(broad[,2]))
    detailed <- dat[14:nrow(dat),]
    detailed[,2] <- as.numeric(as.character(detailed[,2]))

    par(mfrow = c(2,1))
    num_features <- sum(detailed$Number.of.peaks)
    barplot(broad$Number.of.peaks, names.arg = broad$Annotation,
            main = sprintf('%s n=%s', label, num_features),
            las = 2,
            ## xlab = 'genomic category',
            ylab = 'num of features')

    barplot(detailed$Number.of.peaks, names.arg = detailed$Annotation,
            main = sprintf('%s n=%s', label, num_features),
            las = 2,
            ## xlab = 'genomic category',
            ylab = 'num of features')
   
    ## dev.off()
    
}



## plot_homer_annotations('/imppc/labs/maplab/imallona/correlations/communities_subclusters/tumor/comm_1_clust_4/probes_annotation_stats.bed')

pdf(file.path(WD, 'communities_clusters_homer_genomic_annotations.pdf'), useDingbats = FALSE)

for (dataset in DATASETS) {
    for (comm in 1:NUM) {
        for (clust in 1:NUM) {
            tryCatch({
                label <- sprintf('comm_%s_clust_%s', comm, clust)
                fn <- file.path(WD, dataset, label, 'probes_annotation_stats.bed')
                print(fn)
                plot_homer_annotations(fn = fn, label = paste(dataset, label))
            }, error = function(x) print('Skipped'))
        }
    }
}

dev.off()

