#!/bin/bash
#
# Annotates the probes present at the illumina infinium 450k array into exons, introsn, five and three prima
# utr categories as well CpG islands and repetitive elements. Finally, a matrix of distance to TSS is included
#
# Izaskun Mallona, GPLv2
# Fri Dec 12 09:45:01 CET 2014

HOST=overlook
PORT=5432
USER=imallona
DB=meth_correlations
PASS=""

BASE=~/cpgs_annotation
# mind that there is a RefSeq equivalent at $IHOME/data/genomic_categories_refGene_hg19
ENSGENE_DIR=$IHOME/data/genomic_categories_ensGene_hg19
# exons, introns and the like (for intersect-bedding)
CATEGORIES=(coding_exons.bed cpgIslandExt.bed five_utr_exons.bed introns.bed rmsk.bed three_utr_exons.bed)

mkdir -p $BASE
cd $BASE

echo Retrieving Infinium probe positions as bed3

psql -h $HOST -p $PORT -U $USER -d $DB -t -A -F $'\t' -c "select concat('chr', chromosome), meth_start, meth_end, probe from coad.humanmethylation450_probeinfo;" | sortBed > infinium.bed

for category in ${CATEGORIES[@]}
do    
    # -c will report 0 if not overlapping and >=1 if doing it (as many as found)
    bedtools intersect -a infinium.bed -b $ENSGENE_DIR/$category -c > infinium_intersected_with_$category
done

paste infinium_intersected_with_{coding_exons.bed,cpgIslandExt.bed,five_utr_exons.bed,introns.bed,rmsk.bed,three_utr_exons.bed} | cut -f4,5,10,15,20,25,30 > merged.bed

echo probe$'\t'coding_exons$'\t'cpgIslandExt$'\t'five_utr_exons$'\t'introns$'\t'rmsk$'\t'three_utr_exons > header.bed

cat header.bed merged.bed > infinium_annotation_by_ensGene_hg19_genomic_categories.bed

