#!/bin/bash


set -e
set -u

PSQL_ON_CORR="psql -X -U imallona -h overlook -p 5432 --set ON_ERROR_STOP=on --set AUTOCOMMIT=off meth_correlations"

WFOLDER="$HOME"/graphs


# view_creation=$(cat <<EOF

# CREATE OR REPLACE VIEW coad.graph_meth_chr20_chr20 AS (

# (SELECT m.meth_probe_a, m.meth_probe_b, CASE WHEN m.correlation < 0
#         THEN 'neg' WHEN m.correlation >=0 THEN 'pos' END FROM
#         coad.meth_filtered_correlations_chr20_chr20 m )
# );

# \copy (select * from coad.graph_meth_chr20_chr20) to '/tmp/graph_20_20.tsv' with delimiter E'\t'; 

# EOF
 
# )

# mkdir -p $WFOLDER
# echo -e "$view_creation"

# psql -h overlook -p 5432 -U imallona -d meth_correlations -c $(echo "$view_creation")

# mv /tmp/graph_20_20.tsv $WFOLDER


# # mini dataset with correlations

# cat<<EOF

# CREATE OR REPLACE VIEW test.graph_meth_chr20_chr20_numeric AS (

# (SELECT m.meth_probe_a, 
#         m.meth_probe_b,
#         m.correlation 

# FROM
#         coad.meth_filtered_correlations_chr20_chr20 m 
# LIMIT 5000
# )
# );

# \copy (select * from test.graph_meth_chr20_chr20_numeric) to '/tmp/graph_20_20_numeric.tsv' with delimiter E'\t'; 

# EOF 

# psql -h overlook -p 5432 -U imallona -d meth_correlations

# mv /tmp/graph_20_20_numeric.tsv $WFOLDER

$PSQL_ON_CORR <<SQL

\copy (select * from coad.meth_filtered_correlations_chr9_chr9) to '/tmp/graph_meth_9_meth_9.tsv' with delimiter E'\t'

COMMIT;
SQL

mkdir -p $WFOLDER

mv /tmp/graph_meth_9_meth_9.tsv $WFOLDER



