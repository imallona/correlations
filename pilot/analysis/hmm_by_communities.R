#!/usr/bin/env R
## Mon Dec  1 17:07:33 CET 2014

## data got from 
## meth_correlations=> select count(*), c.membership, h.hmm
## meth_correlations-> from coad.meth_filtered_correlations r, 
## meth_correlations->      coad.communities_sampled_second_iteration c ,
## meth_correlations->      coad.humanmethylation_hmm h
## )eth_correlations-> where (r.meth_probe_a = c.probe or r.meth_probe_b = c.probe  
## meth_correlations->       and h.probe = c.probe
## meth_correlations-> group by c.membership, h.hmm;

library(lattice)

FN <- 'hmm_by_community.csv'
FP<- '/imppc/labs/maplab/imallona/correlations/hmm_by_communities'


dat <-  read.table(file.path(FP, FN), sep = ',', header = TRUE)

minidat <- dat[dat$membership<=5,]

png('five_bigger_groups_hmm_abundance.png')

barchart(minidat$hmm~log(minidat$count)|as.factor(minidat$membership), horizontal = T)

dev.off()
