#!/bin/bash
#$ -S /bin/bash
#$ -e /imppc/labs/maplab/imallona/jobs/comm_iterative-e.log
#$ -o /imppc/labs/maplab/imallona/jobs/comm_iterative-o.log
#$ -N comm_iterative
#$ -V
#$ -M imallona@imppc.org
#$ -m e
#$ -q imppc12
#$ -l exclusive

set +x

echo Starting at $(date)

cd $IHOME/tmp

R --slave --vanilla < $IHOME/src/correlations/analysis/fast_greedy_chr_exploring.R &> igraph_communities_sampled_$(date +"%m-%d-%Y").log

echo Ending at $(date)
