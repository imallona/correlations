#!/bin/bash
#
# Summarizes a correlation's schema
# 
# Izaskun Mallona, GPLv2
# 11th Feb 2015

HOST=overlook
PORT=5432
USER=imallona
DB=meth_correlations
PASS=""


RCHUNK=$IHOME/src/correlations/analysis/overall_summary_table.R

export PGPASSWORD=$PASS

# to iterate later on
SCHEMAS=(coad coad_normal_38 coad_mini_40)
rhos=("<-0.8" "<-0.5" ">0.5" ">0.8")
genomic_categories=(coding_exons cpgislandext five_utr_exons introns rmsk three_utr_exons)


METH_SD_THRESHOLD=0.05

SCHEMA=coad


WDIR=$IHOME/correlations/overall_summary_table_new

mkdir -p $WDIR
cd $WDIR

echo 1 Counting total number of probes


for schema in ${SCHEMAS[@]}
do
    stmt1="SELECT COUNT(*) FROM "$schema".humanmethylation450"
    stmt1_result=$(psql -t -d $DB -U $USER -h $HOST -p $PORT -c "$stmt1")
    echo $stmt1 > stmt1_${schema}
    echo ${stmt1_result} > stmt1_${schema}_result
done

echo 2 Counting the NA- containing elements

for schema in ${SCHEMAS[@]}
do
    stmt2="SELECT * FROM get_with_nulls_rowcount('"$schema"', 'humanmethylation450');"
    stmt2_result=$(psql -t -d $DB -U $USER -h $HOST -p $PORT -c "$stmt2")
    echo $stmt2 > stmt2_${schema}
    echo ${stmt2_result} > stmt2_${schema}_result
done

echo 3 Counting the sd-noncompliant elements (independently of the nas)

for schema in ${SCHEMAS[@]}
do

    stmt3="SELECT COUNT(*) FROM "$schema".meth_probe_standard_deviation where sd<"$METH_SD_THRESHOLD";"
    stmt3_result=$(psql -t -d $DB -U $USER -h $HOST -p $PORT -c "$stmt3")
    echo $stmt3 > stmt3_${schema}
    echo ${stmt3_result} > stmt3_${schema}_result
done


echo 4 counting using R

Rscript $RCHUNK


echo 5 counting total correlations

for schema in ${SCHEMAS[@]}
do

    stmt4="SELECT COUNT(*) FROM "$schema".meth_filtered_correlations"
    stmt4_result=$(psql -t -d $DB -U $USER -h $HOST -p $PORT -c "$stmt4")
    echo $stmt4 > stmt4_${schema}
    echo ${stmt4_result} > stmt4_${schema}_result
done

echo 6 counting total correlations above and below given thresholds

for schema in ${SCHEMAS[@]}
do

    stmt5="SELECT COUNT(*) 
           FROM "$schema".meth_filtered_correlations
           WHERE correlation >= 0.8"
    stmt5_result=$(psql -t -d $DB -U $USER -h $HOST -p $PORT -c "$stmt5")
    echo $stmt5 > stmt5_${schema}
    echo ${stmt5_result} > stmt5_${schema}_result

    stmt5b="SELECT COUNT(*) 
           FROM "$schema".meth_filtered_correlations
           WHERE correlation >= 0.5"
    stmt5b_result=$(psql -t -d $DB -U $USER -h $HOST -p $PORT -c "$stmt5b")
    echo $stmt5b > stmt5b_${schema}
    echo ${stmt5b_result} > stmt5b_${schema}_result


    stmt6="SELECT COUNT(*) 
           FROM "$schema".meth_filtered_correlations
           WHERE correlation < -0.8"
    stmt6_result=$(psql -t -d $DB -U $USER -h $HOST -p $PORT -c "$stmt6")
    echo $stmt6 > stmt6_${schema}
    echo ${stmt6_result} > stmt6_${schema}_result

    stmt6b="SELECT COUNT(*) 
           FROM "$schema".meth_filtered_correlations
           WHERE correlation < -0.5"
    stmt6b_result=$(psql -t -d $DB -U $USER -h $HOST -p $PORT -c "$stmt6b")
    echo $stmt6b > stmt6b_${schema}
    echo ${stmt6b_result} > stmt6b_${schema}_result
done

echo 7 counting elements by genomic category


for schema in ${SCHEMAS[@]}
do
    for category in ${genomic_categories[@]}
    do
        # stmt7="SELECT count(distinct(meth_probe_a))
        #      FROM "$schema".meth_filtered_correlations c
        #      WHERE meth_probe_a IN
        #       (SELECT probe
        #       FROM annotations.infinium_probes_by_ensgene_hg19_genomic_categories
        #       WHERE "$category" = 1
        #       );"

        stmt7="
           SELECT COUNT(DISTINCT(probe)) 
           FROM
           (
            SELECT distinct(meth_probe_a) as probe
             FROM "$schema".meth_filtered_correlations c
             WHERE meth_probe_a IN
              (SELECT probe
              FROM annotations.infinium_probes_by_ensgene_hg19_genomic_categories
              WHERE "$category" = 1)

              UNION ALL

             SELECT distinct(meth_probe_b) as probe
             FROM "$schema".meth_filtered_correlations c
             WHERE meth_probe_b IN
              (SELECT probe
              FROM annotations.infinium_probes_by_ensgene_hg19_genomic_categories
              WHERE "$category" = 1)
             ) AS categorizer;"

        stmt7_result=$(psql -t -d $DB -U $USER -h $HOST -p $PORT -c "$stmt7")
        echo $stmt7 > stmt7_${schema}_${category}
        echo ${stmt7_result} > stmt7_${schema}_${category}_result
    done
done



exit


echo 8 counting elements by chromatin color overall


for schema in ${SCHEMAS[@]}
do
    for rho in ${rhos[@]}
    do
 
    stmt8="

 SELECT count(distinct(probe)), hmm
 FROM (
    SELECT distinct(h_a.probe) as probe, 
    h_a.hmm AS hmm
    FROM "$schema".meth_filtered_correlations r 
     LEFT JOIN coad.humanmethylation_hmm h_a
    ON r.meth_probe_a = h_a.probe
    WHERE r.correlation "$rho"
    GROUP BY h_a.probe, h_a.hmm

    UNION ALL

    SELECT distinct(h_b.probe) as probe, 
    h_b.hmm AS hmm
    FROM "$schema".meth_filtered_correlations r 
     LEFT JOIN coad.humanmethylation_hmm h_b
    ON r.meth_probe_b = h_b.probe
    WHERE r.correlation "$rho"
    GROUP BY h_b.probe, h_b.hmm
 ) AS colored
 GROUP BY hmm;
 "

        psql -t -d $DB -U $USER -h $HOST -p $PORT -c "$stmt8" >  stmt8_${schema}_${rho}_result
        echo $stmt8 > stmt8_${schema}_${rho}

    done
done


echo 9 Categories by correlation MAYBE WRONG VERSION


for schema in ${SCHEMAS[@]}
do
    for rho in ${rhos[@]}
    do

        for category in ${genomic_categories[@]}
        do
 
            stmt9="

 SELECT count(distinct(probe))
 FROM (
    SELECT distinct(h_a.probe) as probe, 
    h_a."$category" AS "$category"
    FROM "$schema".meth_filtered_correlations r 
     LEFT JOIN annotations.infinium_probes_by_ensgene_hg19_genomic_categories h_a
    ON r.meth_probe_a = h_a.probe
    WHERE r.correlation "$rho"
     AND h_a."$category" = 1
    GROUP BY h_a.probe, h_a."$category"

    UNION ALL

    SELECT distinct(h_b.probe) as probe, 
    h_b."$category" AS "$category"
    FROM "$schema".meth_filtered_correlations r 
     LEFT JOIN annotations.infinium_probes_by_ensgene_hg19_genomic_categories h_b
    ON r.meth_probe_b = h_b.probe
    WHERE r.correlation "$rho"
     AND h_b."$category" = 1
    GROUP BY h_b.probe, h_b."$category"
 ) AS categorized
 GROUP BY "$category";
 "

        psql -t -d $DB -U $USER -h $HOST -p $PORT -c "$stmt9" >  stmt9_${schema}_${rho}_${category}_result
        echo $stmt9 > stmt9_${schema}_${rho}_${category}
        done
    done
done

echo 10 std deviation and is-null passing elements

# based in the views created by hmm_enrichment_overview.sql and so forth

for schema in ${SCHEMAS[@]}
do
    # sd and na- compliant
    stmt10="
  SELECT COUNT(*), 
      h.hmm
   FROM "$schema".meth_probe_standard_deviation sd
    JOIN coad.humanmethylation_hmm h                      
    ON sd.meth_probe = h.probe
   WHERE sd.sd >= 0.05
   GROUP BY h.hmm
   ORDER BY h.hmm;
   "

    # probes without nulls
   stmt11="
  SELECT COUNT(*), 
      h.hmm
   FROM "$schema".meth_probe_standard_deviation sd
    JOIN coad.humanmethylation_hmm h                      
    ON sd.meth_probe = h.probe
   WHERE  sd.meth_probe = h.probe
   GROUP BY h.hmm 
   ORDER BY h.hmm;
   "
   # all the probes within the array
   stmt12="
  SELECT COUNT(*), 
         h.hmm
   FROM coad.humanmethylation_hmm h
   GROUP BY h.hmm
   ORDER BY h.hmm;
"

    psql -t -d $DB -U $USER -h $HOST -p $PORT -c "$stmt10"  > stmt10_${schema}_result
    echo $stmt10 > stmt10_${schema}


    psql -t -d $DB -U $USER -h $HOST -p $PORT -c "$stmt11"  > stmt11_${schema}_result
    echo $stmt11 > stmt11_${schema}

   
    psql -t -d $DB -U $USER -h $HOST -p $PORT -c "$stmt12"  > stmt12_${schema}_result
    echo $stmt12 > stmt12_${schema}

done




# CREATE VIEW annotations.infinium_probes_by_ensgene_hg19_genomic_categories_case
# AS 
# (SELECT probe, 
#   CASE 
#     WHEN coding_exons = 1 THEN 'coding_exons' 
#     WHEN cpgislandext = 1 THEN 'cpgislandext'
#     WHEN five_utr_exons = 1 THEN 'five_utr_exons'
#     WHEN introns = 1 THEN 'introns'
#     WHEN rmsk = 1 THEN 'rmsk' 
#     WHEN three_utr_exons = 1 THEN 'three_utr_exons'
#     ELSE 'others'
#   END 
# FROM annotations.infinium_probes_by_ensgene_hg19_genomic_categories)
# ;


echo 11 std deviation and is-null passing elemens genomic categories


for schema in ${SCHEMAS[@]}
do
    
    # sd and na- compliant
    stmt13="
  SELECT COUNT(*),
   h.genomic_category
   FROM "$schema".meth_probe_standard_deviation sd
    JOIN annotations.infinium_probes_by_ensgene_hg19_genomic_categories_case h
    ON sd.meth_probe = h.probe
   WHERE sd.sd >= 0.05 
   GROUP BY h.genomic_category
   ORDER BY h.genomic_category
   ;
   "

    # probes without nulls
    stmt14="
  SELECT COUNT(*), 
      h.genomic_category
   FROM "$schema".meth_probe_standard_deviation sd
    JOIN annotations.infinium_probes_by_ensgene_hg19_genomic_categories_case h
    ON sd.meth_probe = h.probe
   WHERE  sd.meth_probe = h.probe
   GROUP BY h.genomic_category 
   ORDER BY h.genomic_category;
   "
    # all the probes within the array
    stmt15="
   SELECT COUNT(*), 
     h.genomic_category
   FROM annotations.infinium_probes_by_ensgene_hg19_genomic_categories_case h
   GROUP BY h.genomic_category
   ORDER BY h.genomic_category;
"

    psql -t -d $DB -U $USER -h $HOST -p $PORT -c "$stmt13"  > stmt13_${schema}_result
    echo $stmt13 > stmt13_${schema}


    psql -t -d $DB -U $USER -h $HOST -p $PORT -c "$stmt14"  > stmt14_${schema}_result
    echo $stmt14 > stmt14_${schema}

    
    psql -t -d $DB -U $USER -h $HOST -p $PORT -c "$stmt15"  > stmt15_${schema}_result
    echo $stmt15 > stmt15_${schema}


done

echo "Beware, stmt7 is a wrong approximation to stmt15"


echo "Refactoring stmt9 as it counts only once elements correlations "
echo " with the two probes sitting in the same element"

# this does not work


# for schema in ${SCHEMAS[@]}
# do
#     for rho in ${rhos[@]}
#     do
 
#     stmt16="

#  SELECT count(distinct(probe)), genomic_category
#  FROM (
#     SELECT distinct(h_a.probe) as probe, 
#     h_a.genomic_category as genomic_category
#     FROM "$schema".meth_filtered_correlations r 
#      LEFT JOIN annotations.infinium_probes_by_ensgene_hg19_genomic_categories_case h_a
#     ON r.meth_probe_a = h_a.probe
#     WHERE r.correlation "$rho"
#     GROUP BY h_a.probe, h_a.genomic_category

#     UNION ALL

#     SELECT distinct(h_b.probe) as probe, 
#     h_b.genomic_category AS genomic_category
#     FROM "$schema".meth_filtered_correlations r 
#      LEFT JOIN annotations.infinium_probes_by_ensgene_hg19_genomic_categories_case h_b
#     ON r.meth_probe_b = h_b.probe
#     WHERE r.correlation "$rho"
#     GROUP BY h_b.probe, h_b.genomic_category
#  ) AS colored
#  GROUP BY genomic_category;
#  "

#         psql -t -d $DB -U $USER -h $HOST -p $PORT -c "$stmt8" >  stmt16_${schema}_${rho}_result
#         echo $stmt16 > stmt16_${schema}_${rho}

#     done
# done

echo hmm and beta meth
