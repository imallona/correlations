library(igraph)

g <- graph.full(50) %du% graph.full(10) %du% graph.full(10) %du% graph.full(14) %du% graph.full(10) %du% graph.full(10)

g <- add.edges(g, c(55,1, 54,2, 59,94, 59,95, 59,96, 69,104, 72,55, 72,57, 69,93, 76, 96, 79,45))

fc <- fastgreedy.community(g)

plot(fc, g)
