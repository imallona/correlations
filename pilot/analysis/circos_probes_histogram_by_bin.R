#!/usr/bin/env Rscript

## bundled to the filtered_by_sd_data_load.sql and filtering_call.sh and filtering.R scripts
## takes into account the sd in order to analyze chromosomal bins of N probes to check the number of positive,
## negative correlations; the number of NAs; and the number of probes that do not pass the sd threshold

## 22 sept 2014

## Izaskun Mallona

library(RPostgreSQL)

SRC <- '/imppc/labs/maplab/imallona/src/correlations/database'
DB_CONF <- file.path(SRC, 'db.txt')

METH_SD_THRESHOLD = 0.05
OUTDIR = '/imppc/labs/maplab/imallona/src/correlations/circos/histogram_circos/data/8'

# the number of CpGs by bin
BIN_SIZE = 200

## database-related functions start

get_db_parameters <- function(conf) {
  params <- read.table(conf, sep = ",", stringsAsFactors = FALSE)
  return(list(user = params$V2[1],
              password = params$V2[2],
              dbname = params$V2[3],
              host = params$V2[4],
              port = params$V2[5]))
}

sql_quote <- function(x, quote = "'") {
  y <- gsub(quote, paste0(quote, quote), x, fixed = TRUE)
  y <- paste0(quote, y, quote)
  y[is.na(x)] <- "NULL"
  names(y) <- names(x)  
  return(y)
}

db_query <- function(db_conf, stmt) {
   drv <- dbDriver("PostgreSQL")

   db_conf <- get_db_parameters(DB_CONF)

   con <- dbConnect(drv,
                     user = db_conf[['user']],
                     password = db_conf[['password']],
                     dbname = db_conf[['dbname']],
                     host = db_conf[['host']],
                     port = db_conf[['port']])
    
   on.exit(dbDisconnect(con), add = TRUE)

   out <- fetch(dbSendQuery(con, statement = stmt), n = -1)
   
   return(out)
}


## database-related functions start
drv <- dbDriver("PostgreSQL")

db_conf <- get_db_parameters(DB_CONF)

con <- dbConnect(drv,
                 user = db_conf[['user']],
                 password = db_conf[['password']],
                 dbname = db_conf[['dbname']],
                 host = db_conf[['host']],
                 port = db_conf[['port']])



## stmt <- "
## SELECT probe, meth_start 
## FROM coad.humanmethylation450_probeinfo
## WHERE chromosome = '9'
## ORDER BY meth_start
## ;
## "

for (chrom in 1:22) {
    cat(sprintf('Running on chr %s', chrom))

    stmt <- sprintf("
SELECT probe, meth_start 
FROM coad.humanmethylation450_probeinfo
WHERE chromosome = '%s'
ORDER BY meth_start
;
", chrom)


    query <- dbSendQuery(con, statement = stmt)

    while (! dbHasCompleted(query)) {

        ## pos number of positive corr
        ## neg number of neg corr
        ## sd filtered those below the threshold
        ## start the initial probe
        ## end the end probe
        tot <- list(pos = 0, neg = 0, sd_filtered = 0, start = 0, end = 0)
        
        probes <- fetch(query, n = BIN_SIZE)

        probes_parsed <- sprintf("('%s')",paste(probes$probe, collapse = "','"))

        con2 <-  dbConnect(drv,
                           user = db_conf[['user']],
                           password = db_conf[['password']],
                           dbname = db_conf[['dbname']],
                           host = db_conf[['host']],
                           port = db_conf[['port']])

        on.exit(dbDisconnect(con2), add = TRUE)

        # just to take advantage of the indexes
        stmt_corr_a <- sprintf(
            "SELECT *
FROM coad.meth_filtered_correlations_chr%s
WHERE meth_probe_a IN %s
;
", chrom, probes_parsed)

        stmt_corr_b <- sprintf(
            "SELECT *
FROM coad.meth_filtered_correlations_chr%s
WHERE meth_probe_b IN %s
;
",  chrom, probes_parsed)
        
##         stmt_corr_a_pos <- sprintf(
##             "SELECT COUNT(*)
## FROM coad_normal.meth_filtered_correlations
## WHERE meth_probe_a IN %s
## AND correlation > 0
## ;
## ",  probes_parsed)

##         stmt_corr_b_pos <- sprintf(
##             "SELECT COUNT(*)
## FROM coad_normal.meth_filtered_correlations
## WHERE meth_probe_b IN %s
## AND correlation > 0
## ;
## ", probes_parsed)


##         stmt_corr_a_neg <- sprintf(
##             "SELECT COUNT(*)
## FROM coad_normal.meth_filtered_correlations
## WHERE meth_probe_a IN %s
## AND correlation < 0
## ;
## ",  probes_parsed)

##         stmt_corr_b_neg <- sprintf(
##             "SELECT COUNT(*)
## FROM coad_normal.meth_filtered_correlations
## WHERE meth_probe_b IN %s
## AND correlation < 0
## ;
## ",  probes_parsed)

        
        stmt_sd <- sprintf(
            "
SELECT COUNT(*) from coad_normal.meth_probe_standard_deviation
WHERE meth_probe in %s
AND sd >= %s
", probes_parsed, METH_SD_THRESHOLD)

        dat_a <- fetch(dbSendQuery(con2, stmt_corr_a), n = -1)        
        dat_b <- fetch(dbSendQuery(con2, stmt_corr_b), n = -1)

        ## tot[['pos']] <- as.numeric(fetch(dbSendQuery(con2, stmt_corr_a_pos), n = -1))
        ## tot[['pos']] <- as.numeric(tot[['pos']]) + as.numeric(fetch(dbSendQuery(con2, stmt_corr_b_pos), n = -1))
        ## tot[['neg']] <- as.numeric(fetch(dbSendQuery(con2, stmt_corr_a_neg), n = -1))
        ## tot[['neg']] <- tot[['neg']] + as.numeric(fetch(dbSendQuery(con2, stmt_corr_b_neg), n = -1))

        tot[['pos']] <- nrow(dat_a[dat_a$correlation > 0,])
        tot[['pos']] <- tot[['pos']] + nrow(dat_b[dat_b$correlation > 0,])

        tot[['neg']] <- nrow(dat_a[dat_a$correlation < 0,])
        tot[['neg']] <- tot[['neg']] + nrow(dat_b[dat_b$correlation < 0,])
        
        tot[['sd_filtered']]  <- as.numeric(fetch(dbSendQuery(con2, stmt_sd), n = -1))

        tot[['start']] <- as.numeric(probes[1,]$meth_start)
        tot[['end']] <- as.numeric(probes[nrow(probes),]$meth_start + 1)

        tot[['sum']] <- tot[['pos']] + tot[['neg']] + tot[['sd_filtered']]

        print(tot)
        dbDisconnect(con2)
        ## str(tot)

        curr <- sprintf('hs%s %s %s %s,%s,%s,%s',
                        chrom,
                        tot[['start']], tot[['end']],
                        ## tot[['pos']]/tot[['sum']], tot[['neg']]/tot[['sum']], tot[['sd_filtered']]/tot[['sum']]
                        tot[['sum']],tot[['pos']], tot[['neg']], tot[['sd_filtered']]
                        )

        write.table(curr, file = file.path(OUTDIR, sprintf('coad_all_chr_by_%s_absolute.csv', BIN_SIZE)),
                    append = TRUE, row.names = FALSE,
                    col.names = FALSE, quote = FALSE)
        
    }
    ## dbDisconnect(con)
} # for chromosome end
