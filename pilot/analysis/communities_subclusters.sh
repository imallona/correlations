#!/bin/bash


WD="$IHOME"/correlations/communities_subclusters
DATASETS="normal tumor"
NUM=9 # of clusters and communities

## bedfile generation
## commented because was already run
echo 'Check if the Rscript is needed or not (and toggle comment)'
# Rscript $SRC/correlations/analysis/communities_subclusters.R

## homer-based annotation

for dataset in ${DATASETS[@]}
do
    for comm in $(seq 1 $NUM)
    do 
        for clust in $(seq 1 $NUM)
        do
            perl /soft/bio/homer-4.7/bin/annotatePeaks.pl \
                "$WD"/"$dataset"/comm_"$comm"_clust_"$clust"/probes.bed \
                hg19  \
                -annStats "$WD"/"$dataset"/comm_"$comm"_clust_"$clust"/probes_annotation_stats.bed \
                -go "$WD"/"$dataset"/comm_"$comm"_clust_"$clust"/go \
                > "$WD"/"$dataset"/comm_"$comm"_clust_"$clust"/probes_annotated.tab
        done
    done  
done

echo 'Check whether the Rscript is needed or not (and toggle comment)'

# Rscript plot_annotation_stats_homer.R
