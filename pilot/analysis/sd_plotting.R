#!/usr/bin/env R

## brief sd data exploring (live MAP meeting)
## 23 sept 2014


library(RPostgreSQL)

SRC <- '/imppc/labs/maplab/imallona/src/correlations/database'
DB_CONF <- file.path(SRC, 'db.txt')

METH_SD_THRESHOLD = 0.05
OUTDIR = '/imppc/labs/maplab/imallona/src/correlations/circos/histogram_circos/data/8'

# the number of CpGs by bin
BIN_SIZE = 100

## database-related functions start

get_db_parameters <- function(conf) {
  params <- read.table(conf, sep = ",", stringsAsFactors = FALSE)
  return(list(user = params$V2[1],
              password = params$V2[2],
              dbname = params$V2[3],
              host = params$V2[4],
              port = params$V2[5]))
}

sql_quote <- function(x, quote = "'") {
  y <- gsub(quote, paste0(quote, quote), x, fixed = TRUE)
  y <- paste0(quote, y, quote)
  y[is.na(x)] <- "NULL"
  names(y) <- names(x)  
  return(y)
}

db_query <- function(db_conf, stmt) {
   drv <- dbDriver("PostgreSQL")

   db_conf <- get_db_parameters(DB_CONF)

   con <- dbConnect(drv,
                     user = db_conf[['user']],
                     password = db_conf[['password']],
                     dbname = db_conf[['dbname']],
                     host = db_conf[['host']],
                     port = db_conf[['port']])
    
   on.exit(dbDisconnect(con), add = TRUE)

   out <- fetch(dbSendQuery(con, statement = stmt), n = -1)
   
   return(out)
}

## sends a stmt and retrieves all the result
get_query_at_once <- function(con, stmt) {
    query <- dbSendQuery(con, stmt)
    return(fetch(query, n = -1))
}

stmt1 <- 'select meth_probe, sd from coad.meth_probe_standard_deviation'

stmt2 <- 'select meth_probe, sd from coad_normal.meth_probe_standard_deviation'


n_sd <- get_query_at_once(con, stmt2)

t_sd <- get_query_at_once(con, stmt1)

dat <- merge(n_sd, t_sd, by = 'meth_probe')

dat2 <- dat[dat$sd.x>0.05, ]
dat3 <- dat2[dat2$sd.y>0.05,]

pdf('sd_exploring_coad_coad_normal.pdf', useDingbats = FALSE)

smoothScatter(dat$sd.x~dat$sd.y, ylab = 'normals sd', xlab = 'coad sd')

smoothScatter(dat3$sd.x~dat3$sd.y, ylab = 'normals sd', xlab = 'coad sd')

plot(density(dat$sd.x), xlab = 'tumors sd')
plot(density(dat$sd.y), xlab = 'normals sd')

dev.off()
