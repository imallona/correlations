-- nonfunctional tests on dblink to be run
------------

CREATE OR REPLACE FUNCTION comethylation_remote_table(meth_probe_a varchar(16), meth_probe_b varchar(16))
  RETURNS TABLE (
      remote_meth_probe_a varchar(16),
      remote_meth_probe_b varchar(16),
      remote_correlation real
  ) AS
$$
 BEGIN

 SELECT dblink_connect('foo', 'hostaddr=172.19.5.20 dbname=meth_correlations user=imallona port=5432 password=hehe');

 RETURN QUERY SELECT * FROM dblink('foo',
    'SELECT meth_probe_a, meth_probe_b, correlation
     FROM coad_normal_no_threshold.meth_filtered_correlations 
     WHERE meth_probe_a = ' || $1  || ' AND
           meth_probe_b = ' || $2) 
  AS t1(
      remote_meth_probe_a varchar(16),
      remote_meth_probe_b varchar(16),
      remote_correlation real)
;


 END
$$
  LANGUAGE plpgsql IMMUTABLE;  


SELECT dblink_disconnect('foo');


select * from comethylation_remote_table('cg01987962', 'cg01993208');

SELECT  comethylation_remote_table('cg01987962', 'cg01993208');
