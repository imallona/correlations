#dowload methylation data


data_meth_download<-function(con,probes_collapsed){
  

  #methylation array tumor data download
  ddT <- dbSendQuery(con, paste0("select * from coad_tumor.humanmethylation450
          where probe in ", probes_collapsed, ";"))
  ddT <- fetch(ddT, n = -1)

  #methylation array normal data download
  ddN <- dbSendQuery(con, paste0("select * from coad_normal.humanmethylation450
          where probe in ", probes_collapsed, ";"))
  ddN <- fetch(ddN, n = -1)
  
  #removing NA's
  naT <- which(apply(is.na(ddT), 1, sum) >0)#== (dim(ddT)[2] - 1))
  if(length(naT)>0){
    #probes <- probes[-naT,]
    ddN <- ddN[-naT,]
    ddT <- ddT[-naT,]
  }
  
  naN <- which(apply(is.na(ddN), 1, sum) >0)#== (dim(ddN)[2] - 1))
  if(length(naN)>0){
    #probes <- probes[-naN,]
    ddT <- ddT[-naN,]
    ddN <- ddN[-naN,]
  }
  
  colnames(ddN) <- toupper(colnames(ddN))
  for(i in 1:6) colnames(ddN) <- sub("_","-",colnames(ddN))
  colnames(ddT) <- toupper(colnames(ddT))
  for(i in 1:6) colnames(ddT) <- sub("_","-",colnames(ddT))
  
  return(list(ddN=ddN,ddT=ddT))
  
}

