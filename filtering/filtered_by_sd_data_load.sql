--#!/bin/bash 
--
-- Storage of those probes that were removed due to their sd below a given
-- standard deviation threshold
-- using the filtering_call.sh and filtering.R scripts
--
-- 22 sept 2014

CREATE TABLE colonomics_tumor.meth_probe_standard_deviation(
    meth_probe VARCHAR(16),
    sd FLOAT(4) NOT NULL,
    CONSTRAINT meth_probe_sd_pk PRIMARY KEY(meth_probe)
);


CREATE TABLE colonomics_normal.meth_probe_standard_deviation(
    meth_probe VARCHAR(16),
    sd FLOAT(4) NOT NULL,
    CONSTRAINT meth_probe_sd_pk PRIMARY KEY(meth_probe)
);

\copy colonomics_tumor.meth_probe_standard_deviation FROM '/home/labs/maplab/imallona/filtered_probes_colonomics_tumor.csv' WITH null AS 'NA' DELIMITER ','

\copy colonomics_normal.meth_probe_standard_deviation FROM '/home/labs/maplab/imallona/filtered_probes_colonomics_normal.csv' WITH null AS 'NA' DELIMITER ','
