#!/bin/bash

# set +x
SRC="$IHOME"/src/correlations

FOLDER=/home/labs/maplab/imallona/TCGA_Correlations
FILTER="$SRC"/filtering/filtering.R
RSCRIPT=/soft/general/R-3.1.0/bin/Rscript

USER="imallona"
PASSWORD=
DBNAME="colonomics_correlations"
HOST="overlook"
PORT=5432

# that is, we will overestimate the cpu number 0.25 times
NPROC_OVERLOAD=0.25
NTHREADS=$(echo "$(nproc)*$NPROC_OVERLOAD" | bc)

# database-compliant view and table names start
SCHEMAS=(colonomics_normal colonomics_tumor)
METH_BIG_TABLE=humanmethylation450

# data origins
# IMPORTANT mind that with this views it won't work, as the parser needs a primary key on them!
# FIRST_VIEW=chr"$FIRST_CHROM"_meth
# SECOND_VIEW=chr"$SECOND_CHROM"_meth

# data destination
FILTERED_CORR_TABLE=null

# database-compliant view and table names end

# cut-offs for standard deviation
# METH_SD_THRESHOLD=0.05
# just to have them all
METH_SD_THRESHOLD=-1


# cut-off for correlation coefficient and pvalue
METH_CORR_THRESHOLD=0.5
METH_PVAL_THRESHOLD=0.01

for SCHEMA in ${SCHEMAS[@]}
do

    echo Starting at $(date) 

    Rscript $FILTER --args "readPath <- \"$FOLDER\"" "writePath <- readPath" "corFromTableToTable(drv=dbDriver(\"PostgreSQL\"), \
user=\"$USER\", \
password=\"$PASSWORD\", \
host=\"$HOST\", \
dbname=\"$DBNAME\", \
port=\"$PORT\", \
from.schema = \"$SCHEMA\" , \
from.table = \"$METH_BIG_TABLE\" , \
to.schema = \"test\", \
to.table = \"test\", \
stdev.threshold.from = $METH_SD_THRESHOLD, \
pval.threshold = $METH_PVAL_THRESHOLD, \
corr.threshold = $METH_CORR_THRESHOLD, \
nthreads = $NTHREADS )" 

    echo Ending at $(date)

    echo Removing non cg probes an the first column and the header 

    awk '{OFS=FS=","; if (NR> 1) print $2,$3}' ~/filtered_probes > ~/foo

    grep cg ~/foo | sed 's/"//g' > ~/"$SCHEMA"_filtered_probes

done
